/**
 *
 */
package org.gcube.portlets.user.geoportaldataviewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataviewer.server.GeoportalExporterActionServlet;
import org.gcube.portlets.user.uriresolvermanager.geoportal.GeoportalExporterAPI;

/**
 * The Class GeoportalExporterActionServletTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 24, 2024
 */
public class GeoportalExporterActionServletTest {

	public static final String SCOPE = "/gcube/devsec/devVRE";
	public static final String UCD_ID = "profiledConcessioni";
	public static final String PROJECT_ID = "670e2f1534da72046a66f247";

	public static final String GCUBE_TOKEN = ""; // devVRE

	public static final String IDENTITY = "francesco.mangiacrapa";

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		ScopeProvider.instance.set(SCOPE);

		// This parameter is used as header parameter in the GET requests for the sticky
		// session on the uri-resolver load balancer, see #28580
		String geoportalexporter_prj = GeoportalExporterActionServlet.getGeoportalExporterJobKey(UCD_ID, PROJECT_ID);
		try {
			GeoportalExporterAPI geAPI = new GeoportalExporterAPI();
			URL pdfURL = geAPI.exportProject(null, UCD_ID, PROJECT_ID, true);
			System.out.println("Start");
			InputStream response;

			response = GeoportalExporterActionServlet.performHttpRequestToSGService(pdfURL.toString(), IDENTITY,
					GCUBE_TOKEN, geoportalexporter_prj);
			System.out.println(IOUtils.toString(response));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("End");
	}

}
