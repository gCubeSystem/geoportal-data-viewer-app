package org.gcube.portlets.user.geoportaldataviewer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.urlshortener.UrlShortener;

public class Shortener {

	//@Test
	public void availableInTheScope() {
		System.out.println("called availableInTheScope test");
		try {
			String toShort = "https://next.d4science.org/group/nextnext/geona-data-viewer";
			String[] splitted = toShort.split("\\?");
			System.out.println("Splitted is: "+Arrays.asList(splitted));
			String link = toShort;
			if (splitted.length > 1) {
				String encodedQuery = splitted[1];
				
				try {
					encodedQuery = URLEncoder.encode(splitted[1], "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();

				}
				System.out.println("encodedQuery is: "+encodedQuery);
				link = String.format("%s?%s", splitted[0], encodedQuery);
			}
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UrlShortener shortener = new UrlShortener();
			System.out.println(shortener.shorten(link));
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

}
