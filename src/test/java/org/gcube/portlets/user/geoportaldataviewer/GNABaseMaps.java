package org.gcube.portlets.user.geoportaldataviewer;

import java.util.List;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataviewer.server.GNABaseMapsResourceReader;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;

public class GNABaseMaps {

	private static final String SCOPE = "/gcube/devsec/devVRE";

	//@Test
	public void readBaseMaps() {
		System.out.println("called readBaseMaps test");
		ScopeProvider.instance.set(SCOPE);
		GNABaseMapsResourceReader gnaBaseMapsR = new GNABaseMapsResourceReader();
		List<BaseMapLayer> lstBML = null;
		try {
			lstBML = gnaBaseMapsR.getListBaseMaps();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Read base map layers: " + lstBML);
	}

}
