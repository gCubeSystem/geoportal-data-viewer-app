package org.gcube.portlets.user.geoportaldataviewer;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.identification.IdentificationReference;
import org.gcube.application.geoportal.common.model.rest.QueryRequest;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.ProjectDVBuilder;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.geoportal.access.GeportalCheckAccessPolicy;
import org.gcube.application.geoportalcommon.geoportal.access.GeportalCheckAccessPolicy.ACCESS_POLICY;
import org.gcube.application.geoportalcommon.geoportal.util.GisUtil;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;
import org.gcube.application.geoportalcommon.shared.WhereClause;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.Crs;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.Geoportal_JSON_Mapper;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.json.JSONObject;

/**
 * The Class GeoportalViewer_Tests.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 9, 2022
 */
public class GeoportalViewer_Tests {

	private UseCaseDescriptorCaller clientUCD;

	private ProjectsCaller clientProjects;

	private static String CONTEXT = "/gcube/devsec/devVRE";
	// private static String CONTEXT = "/pred4s/preprod/preVRE";
//	private static String CONTEXT = "/d4science.research-infrastructures.eu/D4OS/GNA";
	private static String TOKEN = ""; // GNA

	// private static String TOKEN = ""; // devVRE

	private static String PROFILE_ID = "profiledConcessioni";
	private static String PROJECT_ID = "63d011c4dcac4551b9a6b930";

	private static String USERNAME = "francesco.mangiacrapa";

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	//@Before
	public void getClient() {
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
		clientProjects = GeoportalClientCaller.projects();
	}

	//@Test
	public void getProjectViewForID() {

		try {
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project project = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
			ProjectDV projectDV = ConvertToDataValueObjectModel.toProjectDV(project, projectBuilder);

			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(projectDV, CONTEXT, USERNAME);
			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// @Test
	public void getLayersForId() {
		System.out.println("getLayersForId [profileID: " + PROFILE_ID + ", projectID: " + PROJECT_ID + "] called");

		List<GCubeSDIViewerLayerDV> listLayers = null;

		try {

			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project project = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			String jsonDocument = project.getTheDocument().toJson();
			System.out.println("JSON Project is: " + jsonDocument);

			ACCESS_POLICY accessPolicy = GeportalCheckAccessPolicy.accessPolicyFromSessionLogin(null);

			String materializationParentJSONPath = null;

			if (accessPolicy.equals(ACCESS_POLICY.OPEN)) {
				// Filtering for ACCESS_POLICY.OPEN when the username is not in session. IAM
				// identity is running, no logged in user.
				materializationParentJSONPath = String.format("%s..%s[?(@._access._policy=='%s')]",
						Geoportal_JSON_Mapper.JSON_$_POINTER, Geoportal_JSON_Mapper.FILESET, accessPolicy);
			} else {
				// Accessing to all fileset (with any ACCESS_POLICY) when the user is logged in.
				materializationParentJSONPath = String.format("%s..%s", Geoportal_JSON_Mapper.JSON_$_POINTER,
						Geoportal_JSON_Mapper.FILESET);
			}

			System.out.println("jsonPathQuery is: " + materializationParentJSONPath);

//			String materializationParentJSONPath = String.format("%s..%s", Geoportal_JSON_Mapper.JSON_$_POINTER,
//					Geoportal_JSON_Mapper.FILESET);
//
			listLayers = Geoportal_JSON_Mapper.readGcubeSDILayersForFileset(materializationParentJSONPath,
					jsonDocument);

			listLayers.stream().forEach(s -> System.out.println(s));

		} catch (Exception e) {
			String erroMsg = "Layers are not available for profileID " + PROFILE_ID + " with projectID " + PROJECT_ID;
			System.out.println(erroMsg);
			e.printStackTrace();
		}

	}

	// @Test
	public void getSpatialReference() {
		System.out.println("getSpatialReference [profileID: " + PROFILE_ID + ", projectID: " + PROJECT_ID + "] called");
		try {

			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project project = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);

			List<IdentificationReference> itentificationReference = project.getIdentificationReferences();

			for (IdentificationReference identificationReference : itentificationReference) {
				String geoJSON = identificationReference.get("geoJSON").toString();
				JSONObject jsonObject = new JSONObject(geoJSON);

				GeoJSON geoJson = new GeoJSON();
				geoJson.setType(jsonObject.getString("type"));
				geoJson.setBbox(GisUtil.fromJSONArray(jsonObject.getJSONArray("bbox")));
				Crs crs = org.gcube.application.geoportal.client.utils.Serialization
						.convert(jsonObject.getJSONObject("crs").toString(), Crs.class);
				geoJson.setCrs(crs);
				geoJson.setGeoJSON(geoJSON);

				System.out.println(geoJson);
			}

		} catch (Exception e) {
			String erroMsg = "getSpatialReference not available for profileID " + PROFILE_ID + " with projectID "
					+ PROJECT_ID;
			System.out.println(erroMsg);
			e.printStackTrace();
		}

	}

	// @Test
	public void getImagesForId() {
		System.out.println("getImagesForId [profileID: " + PROFILE_ID + ", projectID: " + PROJECT_ID + "] called");

		List<PayloadDV> listImages = null;

		try {

			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project project = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			String jsonDocument = project.getTheDocument().toJson();
			System.out.println("JSON Project is: " + jsonDocument);

			String filesetJSONPath = String.format("%s..%s", Geoportal_JSON_Mapper.JSON_$_POINTER,
					Geoportal_JSON_Mapper.FILESET);
			listImages = Geoportal_JSON_Mapper.readImagesForFileset(filesetJSONPath, jsonDocument, true);

			listImages.stream().forEach(s -> System.out.println("Image: " + s));

		} catch (Exception e) {
			String erroMsg = "Images are not available for profileID " + PROFILE_ID + " with projectID " + PROJECT_ID;
			System.out.println(erroMsg);
			e.printStackTrace();
		}

	}

	// @Test
	public void testUCDQuery() {
		QueryRequest request = new QueryRequest();
		request.setFilter(
				Document.parse("{\"" + UseCaseDescriptor.ID + "\" : " + "{\"$eq\" : \"" + PROFILE_ID + "\"}}"));
		request.setProjection(Document.parse("{\"" + UseCaseDescriptor.NAME + "\" : " + "1}"));

		try {
			useCaseDescriptors().build().query(request).forEachRemaining(u -> {
				try {
					System.out.println("UCD for id" + u.getId() + " returend name: " + u.getName());
					ProjectDV projectDV = new ProjectDV();
					projectDV.setId(PROJECT_ID);
					projectDV.setProfileName(u.getName());
					projectDV.setProfileID(u.getId());
					System.out.println(projectDV);
				} catch (Exception e) {
					System.out.println("Invalid UCD, UCID : " + u.getId());
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			// silent
		}
	}

	// @Test
	public void testReadProjectEdit() {

		try {
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project theProject = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
			projectBuilder.relationships(true);
			ProjectDV theProjectDV = ConvertToDataValueObjectModel.toProjectDV(theProject, projectBuilder);
			Geoportal_JSON_Mapper geojsonM = new Geoportal_JSON_Mapper();
			ProjectEdit projectEdit = Geoportal_JSON_Mapper.loadProjectEdit(theProjectDV, CONTEXT, USERNAME);
			Geoportal_JSON_Mapper.prettyPrintProjectEdit(projectEdit);

//			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
//			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
			System.out.println("testReadProjectEdit terminated");
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}
	}

	// @Test
	public void testTemporalReference() {
		try {
			ScopeProvider.instance.set(CONTEXT);
			SecurityTokenProvider.instance.set(TOKEN);
			Project theProject = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
			Document temporalJsonTemplate = GeoportalClientCaller.useCaseDescriptors()
					.readTemporalDimensionTemplate(theProject.getProfileID());

			// String template = "{\"content\": \"$.nome\"," + "\"title\": \"$.nome\","
			// + "\"start\" : \"$.dataInizioProgetto\"," + "\"end\" :
			// \"$.dataFineProgetto\"}";

			if (temporalJsonTemplate != null) {

				JSONObject sourceJsonTemplate = new JSONObject(temporalJsonTemplate);

				JSONObject targetJsonObject = ConvertToDataValueObjectModel.toTimelineJSONModel(theProject,
						sourceJsonTemplate, theProject.getProfileID(), theProject.getId(), null);

				TemporalReferenceDV temporalDV = ConvertToDataValueObjectModel.toTemporalReferenceDV(theProject,
						targetJsonObject);
				System.out.println(temporalDV);
//			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
//			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void getResultsWithSize() throws Exception {
		// List<Project> listOfProjects = client.getListForProfileID(PROFILE_ID);

		long start = System.currentTimeMillis();
		System.out.println("start at: " + start);
		SearchingFilter filter = new SearchingFilter();

		// Where Clause
		List<WhereClause> conditions = new ArrayList<WhereClause>();
		Map<String, Object> searchInto = new HashMap<String, Object>();
		searchInto.put("_theDocument.dataInizioProgetto", "2021");
		WhereClause whereClause = new WhereClause(LOGICAL_OP.OR, searchInto);

		conditions.add(whereClause);
		filter.setConditions(conditions);

		// NEED TO GET projections

		// SETTING PROJECTION ONLY FOR PROEJCT ID
		LinkedHashMap<String, Object> projectionForIDs = new LinkedHashMap<String, Object>();
		projectionForIDs.put(Project.ID, 1);
		filter.setProjection(projectionForIDs);

		// LIMIT IS NULL MEANS THAT IT IS EQUAL TO NUMBER TOTAL OF DOCUMENTS
		Integer totalDocs = clientProjects.getTotalDocument(PROFILE_ID);
		Iterator<Project> projects = clientProjects.queryOnMongo(PROFILE_ID, totalDocs, 0, null, filter);

		// CALCULATING SIZE OF THE ITERATOR OF PROJECT IDs
		Iterable<Project> itP = () -> projects;
		Stream<Project> targetStream = StreamSupport.stream(itP.spliterator(), false);
		List<String> listProjectIDs = targetStream.map(Project::getId).collect(Collectors.toList());

		System.out.println("Total Ids are: " + listProjectIDs.size());
		// listProjectIDs.stream().forEach(p -> System.out.println(p));

		long end = System.currentTimeMillis();
		System.out.println("IDS returned in ms: " + (end - start));

		// RESETTIG FILTER AND PROJECT TO CLIENT QUERY
		LinkedHashMap<String, Object> projection = new LinkedHashMap<String, Object>();
		// default
		// PROJECTION
		projection.put(Project.ID, 1);
		projection.put("_theDocument.nome", 1);
		projection.put("_profileID", 1);
		projection.put("_profileVersion", 1);
		projection.put("_version", 1);
		projection.put("_theDocument", 1);

		projection.put("_theDocument.paroleChiaveLibere", 1);
		projection.put("_theDocument.editore", 1);
		projection.put("_theDocument.paroleChiaveICCD", 1);
		projection.put("_theDocument.responsabile", 1);

		projection.put("_theDocument.introduzione", 1);
		projection.put("_theDocument.authors", 1);
		projection.put("_theDocument.dataInizioProgetto", 1);
		filter.setProjection(projection);

		// Integer totalDocs = client.getTotalDocument(PROFILE_ID);
		Iterator<Project> itProjects = clientProjects.queryOnMongo(PROFILE_ID, totalDocs, 0, 50, filter);

		List<ResultDocumentDV> results = ConvertToDataValueObjectModel.toListResultDocument(itProjects);
		int i = 0;
		for (ResultDocumentDV projectDV : results) {
			System.out.println(++i + ") " + projectDV.getId() + " dataInizioProgetto: "
					+ projectDV.getDocumentAsMap().get("dataInizioProgetto"));
		}

		System.out.println("Result listed has size: " + results.size());

		end = System.currentTimeMillis();
		System.out.println("Result listed in ms: " + (end - start));

	}

}
