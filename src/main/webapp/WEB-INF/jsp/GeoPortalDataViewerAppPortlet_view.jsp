<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%-- Uncomment below lines to add portlet taglibs to jsp
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<portlet:defineObjects />
--%>

<script
	src='<%=request.getContextPath()%>/GeoportalDataViewer/js/jquery-1.10.1.min.js'></script>

<script
	src='<%=request.getContextPath()%>/GeoportalDataViewer/js/bootstrap.min.js'></script>

<link
	href="//cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@main/dist/en/v6.6.1/css/ol.css"
	rel="stylesheet" type="text/css">

<script
	src="//cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@main/dist/en/v6.6.1/build/ol.js"
	type="text/javascript"></script>

<link
	href="//cdnjs.cloudflare.com/ajax/libs/nanogallery2/3.0.5/css/nanogallery2.min.css"
	rel="stylesheet" type="text/css">

<script
	src="//unpkg.com/vis-timeline@7.7.0/standalone/umd/vis-timeline-graph2d.min.js"
	type="text/javascript"></script>

<script
	src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"
	type="text/javascript"></script>

<link
	href="//unpkg.com/vis-timeline@7.7.0/styles/vis-timeline-graph2d.min.css"
	rel="stylesheet" type="text/css">

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/GeoportalDataViewer.css"
	type="text/css">

<script type="text/javascript"
	src="<%=request.getContextPath()%>/GeoportalDataViewer/GeoportalDataViewer.nocache.js"></script>

<script id="item-template" type="text/x-handlebars-template">
      <table>
      {{#if selected}}
      <tr><td>
				<span id="relation-style">{{selected}}</span>
        </tr></td>
			{{/if}}
      {{#if image_url}}
      <tr><td>
				<img src="{{image_url}}"></img>
        </tr></td>
			{{/if}}
      <tr>
        <th colspan="3" class="description">{{truncate_msg content}}</th>
      </tr>
      <tr>
        <td><code>{{dateformat start}} / {{dateformat end}}</code></td>
      </tr>
    </table>
</script>

<script type="text/javascript">
	Handlebars
			.registerHelper('dateformat',
					function(date) {
						try {
							if (date) {
								var d = new Date(date), month = ''
										+ (d.getMonth() + 1), day = ''
										+ d.getDate(), year = d.getFullYear();

								if (month.length < 2)
									month = '0' + month;
								if (day.length < 2)
									day = '0' + day;

								return [ year, month, day ].join('-');
							}
							return "";
						} catch (err) {
							return date + "";
						}
					});

	Handlebars.registerHelper("truncate_msg", function(input) {

		max_lenght = 20;

		if (input.length > max_lenght) {
			return input.substring(0, max_lenght) + '...';
		}
		return input;

	});

	var templateHandlebars = Handlebars.compile(document
			.getElementById('item-template').innerHTML);

	var optionsTimeline = {
		// specify a template for the items
		template : templateHandlebars,
		type : 'box'
	};
</script>

<div id="app-notifier" class="app-notifier"></div>
<div id="layer-viewer" class="layer-panel"></div>
<div id="geoportal-data-viewer"></div>
<div class="timeline-container">
	<div id="timeline-data"></div>
</div>