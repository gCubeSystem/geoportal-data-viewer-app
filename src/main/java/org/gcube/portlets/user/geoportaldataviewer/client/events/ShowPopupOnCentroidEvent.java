package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ShowPopupOnCentroidEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 12, 2023
 */
public class ShowPopupOnCentroidEvent extends GwtEvent<ShowPopupOnCentroidEventHandler> {
	public static Type<ShowPopupOnCentroidEventHandler> TYPE = new Type<ShowPopupOnCentroidEventHandler>();
	private String profileID;
	private String projectID;
	private GeoJSON spatialReference;
	private ProjectView projectView;

	/**
	 * Instantiates a new show popup on centroid event.
	 *
	 * @param profileID        the profile ID
	 * @param projectID        the project ID
	 * @param spatialReference the spatial reference
	 */
	public ShowPopupOnCentroidEvent(String profileID, String projectID, GeoJSON spatialReference) {
		this.profileID = profileID;
		this.projectID = projectID;
		this.spatialReference = spatialReference;

	}

	/**
	 * Instantiates a new show popup on centroid event.
	 *
	 * @param projectView the project view
	 */
	public ShowPopupOnCentroidEvent(ProjectView projectView) {
		this.projectView = projectView;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ShowPopupOnCentroidEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ShowPopupOnCentroidEventHandler handler) {
		handler.onShowPopup(this);

	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the project ID.
	 *
	 * @return the project ID
	 */
	public String getProjectID() {
		return projectID;
	}

	/**
	 * Gets the spatial reference.
	 *
	 * @return the spatial reference
	 */
	public GeoJSON getSpatialReference() {
		return spatialReference;
	}

	/**
	 * Gets the project view.
	 *
	 * @return the project view
	 */
	public ProjectView getProjectView() {
		return projectView;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ShowPopupOnCentroidEvent [profileID=");
		builder.append(profileID);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append(", spatialReference=");
		builder.append(spatialReference);
		builder.append(", projectView=");
		builder.append(projectView);
		builder.append("]");
		return builder.toString();
	}

}