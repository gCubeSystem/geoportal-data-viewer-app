package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;

import com.google.gwt.core.client.GWT;

import ol.Collection;
import ol.Coordinate;
import ol.Extent;
import ol.control.Attribution;
import ol.control.Control;
import ol.control.FullScreen;
import ol.control.MousePosition;
import ol.control.ZoomSlider;
import ol.geom.LineString;
import ol.proj.Projection;

/**
 * The Class MapUtils.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 12, 2020
 */
public final class MapUtils {

	/**
	 * Creates some default controls and adds it to the collection.
	 *
	 * @param controls collection with controls
	 */
	public static void addDefaultControls(final Collection<Control> controls) {

		controls.push(new FullScreen());
		controls.push(new ZoomSlider());
		MousePosition mousePosition = new MousePosition();
		mousePosition.setCoordinateFormat(Coordinate.createStringXY(5));
		controls.push(mousePosition);

		Attribution attribution = new Attribution();
		attribution.setCollapsed(true);
		controls.push(attribution);
		// controls.push(new ZoomToExtent());

	}

	/**
	 * Transform coordiante.
	 *
	 * @param centerCoordinate the center coordinate
	 * @param source           the source
	 * @param target           the target
	 * @return the coordinate
	 */
	public static Coordinate transformCoordiante(Coordinate centerCoordinate, String source, String target) {
		return Projection.transform(centerCoordinate, source, target);
	}

	/**
	 * Transform extent.
	 *
	 * @param extent the extent
	 * @param source the source
	 * @param target the target
	 * @return the extent
	 */
	public static Extent transformExtent(Extent extent, String source, String target) {
		return Projection.transformExtent(extent, source, target);
	}

	/**
	 * Distance between centroid.
	 *
	 * @param ex1 the ex 1
	 * @param ex2 the ex 2
	 * @return the long
	 */
	public static long distanceBetweenCentroid(ExtentWrapped ex1, ExtentWrapped ex2) {
		return distanceBetween(ex1.getCenter(), ex2.getCenter());
	}

	/**
	 * Distance between.
	 *
	 * @param c1 the c 1
	 * @param c2 the c 2
	 * @return the long
	 */
	public static long distanceBetween(Coordinate c1, Coordinate c2) {

		Coordinate[] arrayCoordinate = new Coordinate[2];
		arrayCoordinate[0] = c1;
		arrayCoordinate[1] = c2;

		LineString ls = new LineString(arrayCoordinate);
//		GWT.log("Line length is: "+ls);
		return Math.round(ls.getLength() * 100) / 100;
	}

	/**
	 * Reverse coordinate.
	 *
	 * @param coord the coord
	 * @return the coordinate
	 */
	public static Coordinate reverseCoordinate(Coordinate coord) {
		return new Coordinate(coord.getY(), coord.getX());
	}

	/**
	 * Geo JSON to B box center.
	 *
	 * @param spatialReference the spatial reference
	 * @param transforFrom     the transfor from
	 * @param transformTo      the transform to
	 * @return the coordinate
	 */
	public static Coordinate geoJSONToBBOXCenter(GeoJSON spatialReference, String transforFrom, String transformTo) {

		try {
			if (spatialReference != null) {
				ExtentWrapped ew = new ExtentWrapped(spatialReference.getBbox().getMinX(),
						spatialReference.getBbox().getMinY(), spatialReference.getBbox().getMaxX(),
						spatialReference.getBbox().getMaxY());

				Coordinate center = ew.getCenter();
				if (transforFrom != null && transformTo != null)
					center = transformCoordiante(center, transforFrom, transformTo);

				return center;
			}
		} catch (Exception e) {
			GWT.log("geoJSONTToBBoxCenter error: " + e.getMessage());
		}

		return null;
	}
}
