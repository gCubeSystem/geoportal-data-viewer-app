package org.gcube.portlets.user.geoportaldataviewer.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GroupedLayersDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.LayerIDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDILayer;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.IndexLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.RelationshipDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MapEventType;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddLayerToMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEvent.LAYER_TYPE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ApplyCQLToLayerOnMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ApplyCQLToLayerOnMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ChangeMapLayerEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ChangeMapLayerEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ClosedViewDetailsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ClosedViewDetailsEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.DO_LAYER_ACTION;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.SwapLayer;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.FitMapToExtentEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.FitMapToExtentEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.MapExtentToEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.MapExtentToEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.OverlayCustomLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.OverlayCustomLayerToMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.QueryDataEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.SearchPerformedEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.SearchPerformedEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent.EVENT_SOURCE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowPopupOnCentroidEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowPopupOnCentroidEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.TimelineProjectRelationsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.TimelineProjectRelationsEvent.EVENT_TYPE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.TimelineProjectRelationsEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ZoomOutOverMinimumEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ZoomOutOverMinimumEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.CloseCollectionEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.CloseCollectionEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.OpenCollectionEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.OpenCollectionEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.ExtentWrapped;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayerMap;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayerMap.CQL_FACILITY_ORIGIN;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalImages;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.GeonaDataViewMainPanel;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation.TimelineManagerStatus;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.util.OLGeoJSONUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.util.ControlledCallBack;
import org.gcube.portlets.user.geoportaldataviewer.client.util.LoaderIcon;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.GCubeCollection;
import org.gcube.portlets.user.geoportaldataviewer.shared.ItemFieldsResponse;
import org.gcube.portlets.user.geoportaldataviewer.shared.ViewerConfiguration;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.Popover;
import com.github.gwtbootstrap.client.ui.constants.Placement;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import ol.Coordinate;
import ol.Feature;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 27, 2020
 */
public class GeoportalDataViewer implements EntryPoint {

	public static final String DIV_TIMELINE_DATA = "timeline-data";
	
	public static final String DIV_LAYER_VIEW = "layer-viewer";

	public static final String APP_NOTIFIER = "app-notifier";

	/** The Constant APP_DIV. */
	public final static String APP_DIV = "geoportal-data-viewer";

	/** The ol map. */
	// private OpenLayerMap olMap = null;

	/** The main panel. */
	private GeonaDataViewMainPanel mainPanel;

	/** The param wms request. */
	private String paramWmsRequest;

	/** The param UUID. */
	// private String paramUUID;

	private String paramGeonaItemType;

	/** The param layer title. */
	private String paramLayerTitle;

	private HandlerManager applicationBus = new HandlerManager("");

	/** The layer manager. */
	private LayerManager layerManager = new LayerManager(applicationBus);

	private String paramGeonaItemID;

	private GNADataViewerConfigProfile geonaDataViewerProfile;

	private OLMapManager olMapMng = null;

	int attempt = 0;

	private LoaderIcon loaderApp = new LoaderIcon("Loading application... please wait",
			new com.google.gwt.user.client.ui.Image(GeoportalImages.ICONS.spinnerEarth()));

	private HTML attributionDiv = new HTML();

	private static List<BaseMapLayer> listBaseMapLayers = null;

	private static ViewerStatus viewerStatus = new ViewerStatus();

	private TimelineManagerStatus timelineMS = new TimelineManagerStatus(applicationBus);

	private boolean initApplication;

	// A cache (Layer_ID, GeoJSON) of layer features loaded via GetFeature Request
	private HashMap<String, String> layerFeaturesCache = new HashMap<String, String>();

	private ViewerConfiguration viewerConfiguration;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		loaderApp.getElement().addClassName("loader-gna-app");
		RootPanel.get(APP_DIV).add(loaderApp);

		paramWmsRequest = Window.Location.getParameter(GeoportalDataViewerConstants.GET_WMS_PARAMETER);
		paramGeonaItemType = Window.Location.getParameter(GeoportalDataViewerConstants.GET_GEONA_ITEM_TYPE);
		paramGeonaItemID = Window.Location.getParameter(GeoportalDataViewerConstants.GET_GEONA_ITEM_ID);
		paramLayerTitle = Window.Location.getParameter(GeoportalDataViewerConstants.GET_LAYER_TITLE);
		GWT.log(GeoportalDataViewerConstants.GET_WMS_PARAMETER + " = " + paramWmsRequest);
		GWT.log(GeoportalDataViewerConstants.GET_GEONA_ITEM_TYPE + " = " + paramGeonaItemType);
		GWT.log(GeoportalDataViewerConstants.GET_GEONA_ITEM_ID + " = " + paramGeonaItemID);
		GWT.log(GeoportalDataViewerConstants.GET_LAYER_TITLE + " = " + paramLayerTitle);

		// RootPanel.get(APP_DIV).add(loaderApp);

		timelineMS.hideTimelineProjectRelations();

		GeoportalDataViewerServiceAsync.Util.getInstance()
				.getConfigListOfFieldsForSearching(new AsyncCallback<List<ItemFieldsResponse>>() {

					@Override
					public void onFailure(Throwable caught) {
						RootPanel.get(APP_DIV).remove(loaderApp);
						Window.alert(caught.getMessage());
						RootPanel.get(APP_DIV).setHeight("800px");
						RootPanel.get(APP_DIV).add(getGeneralErrorPanel());
					}

					@Override
					public void onSuccess(List<ItemFieldsResponse> itemFieldsResult) {
						GWT.log("listOfFieldsForSearching: " + itemFieldsResult);

						mainPanel = new GeonaDataViewMainPanel(applicationBus, getClientHeight(), itemFieldsResult);
						RootPanel.get(APP_DIV).add(mainPanel);

						initApplication();
					}
				});

	}

	/**
	 * Inits the application.
	 */
	private void initApplication() {

		initApplication = true;

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {

				GeoportalDataViewerServiceAsync.Util.getInstance()
						.getInitialConfiguration(new ControlledCallBack<ViewerConfiguration>() {

							@Override
							public void onFailure(Throwable caught) {
								RootPanel.get(APP_DIV).remove(loaderApp);
								Window.alert(caught.getMessage());
								RootPanel.get(APP_DIV).add(getGeneralErrorPanel());
							}

							@Override
							public void after() {
								RootPanel.get(APP_DIV).remove(loaderApp);
							}

							@Override
							public void onSuccess(ViewerConfiguration result) {
								super.onSuccess(result);
								viewerConfiguration = result;

								GWT.log("Received init config : " + result);
								GWT.log("Loading base layers, count : " + result.getBaseLayers().size());
								//GWT.log("Available collections : " + result.getAvailableCollections());

								GeoportalDataViewer.getStatus().setConfig(result);

								listBaseMapLayers = result.getBaseLayers();
								mainPanel.setBaseLayers(result.getBaseLayers());

								BaseMapLayer firstBaseLayer = result.getBaseLayers().get(0);
								// Passing the first base map layer that will be applied as first base layer
								olMapMng = new OLMapManager(mainPanel.getMapPanel().getElement().getId(),
										layerManager.getLayerManagerBus(), firstBaseLayer);
								OpenLayerMap olMap = olMapMng.getOLMap();
								layerManager.setOLMap(olMap);
								mainPanel.setMap(olMap);

								updateSize();

								ScriptInjector.fromUrl(
										"//cdnjs.cloudflare.com/ajax/libs/nanogallery2/3.0.5/jquery.nanogallery2.min.js")
										.setWindow(ScriptInjector.TOP_WINDOW).inject();

								if (result.getAvailableCollections() == null
										|| result.getAvailableCollections().size() == 0) {
									RootPanel.get(APP_DIV).remove(loaderApp);
									Window.alert("No available collection in this context");
									RootPanel.get(APP_DIV).add(getGeneralErrorPanel());
									return;
								}

								GWT.log("Loading collections, count " + result.getAvailableCollections().size());

								ArrayList<GCubeCollection> listColl = new ArrayList<GCubeCollection>();
								listColl.addAll(result.getAvailableCollections().values());
								Collections.sort(listColl);
								mainPanel.setAvailableCollections(listColl, paramGeonaItemType);

								if (paramGeonaItemID != null) {
									if (paramGeonaItemType == null) {
										Window.alert(
												"Missing parameter " + GeoportalDataViewerConstants.GET_GEONA_ITEM_TYPE
														+ " (GeoNa Data Type) where to search the item id: "
														+ paramGeonaItemID);
										return;
									}
									GeoportalItemReferences gir = new GeoportalItemReferences(paramGeonaItemID,
											paramGeonaItemType);
									applicationBus.fireEvent(
											new ShowDetailsEvent(gir, null, true, true, EVENT_SOURCE.LOCATE_ON_INIT));
								}

								mainPanel.openCollectionMenu();

								loadConfigureduGroupedLayersForUCD();

								GWT.log("DONE INIT LOAD");

							}

						});
			}
		});

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				updateSize();
			}
		});

		bindEvents();

		RootPanel.get(APP_DIV).add(attributionDiv);
	}

	/**
	 * Load Overlay Layers and Cross-Filtering layers for UCD
	 * 
	 * Load configuredu grouped layers for UCD.
	 */
	public void loadConfigureduGroupedLayersForUCD() {

		GeoportalDataViewerServiceAsync.Util.getInstance().getAvaiableCustomGroupedLayersForUCD(
				GEOPORTAL_DATA_HANDLER.geoportal_grouped_overlay_layers,
				new AsyncCallback<Map<String, List<GroupedLayersDV<? extends LayerIDV>>>>() {

					@Override
					public void onFailure(Throwable caught) {
						GWT.log("loadConfigureduGroupedLayersForUCD error for "
								+ GEOPORTAL_DATA_HANDLER.geoportal_grouped_overlay_layers + ": " + caught.getMessage());

					}

					@Override
					public void onSuccess(Map<String, List<GroupedLayersDV<? extends LayerIDV>>> result) {
						GWT.log(GEOPORTAL_DATA_HANDLER.geoportal_grouped_overlay_layers + " returned: " + result);
						if (result != null && result.size() > 0) {

							Map<String, GCubeCollection> collections = viewerConfiguration.getAvailableCollections();

							for (String profileId : result.keySet()) {
								List<GroupedLayersDV<? extends LayerIDV>> list = result.get(profileId);
								if (list != null && list.size() > 0) {
									GCubeCollection collection = collections.get(profileId);
									mainPanel.addOverlayLayers(collection, list);

								}
							}

						}
					}
				});

		GeoportalDataViewerServiceAsync.Util.getInstance().getAvaiableCustomGroupedLayersForUCD(
				GEOPORTAL_DATA_HANDLER.geoportal_grouped_cross_filtering,
				new AsyncCallback<Map<String, List<GroupedLayersDV<? extends LayerIDV>>>>() {

					@Override
					public void onFailure(Throwable caught) {
						GWT.log("loadConfigureduGroupedLayersForUCD error for "
								+ GEOPORTAL_DATA_HANDLER.geoportal_grouped_cross_filtering + ": "
								+ caught.getMessage());

					}

					@Override
					public void onSuccess(Map<String, List<GroupedLayersDV<? extends LayerIDV>>> result) {
						GWT.log(GEOPORTAL_DATA_HANDLER.geoportal_grouped_cross_filtering + " returned: " + result);
						if (result != null && result.size() > 0) {

							Map<String, GCubeCollection> collections = viewerConfiguration.getAvailableCollections();

							for (String profileId : result.keySet()) {
								List<GroupedLayersDV<? extends LayerIDV>> list = result.get(profileId);
								if (list != null && list.size() > 0) {
									GCubeCollection collection = collections.get(profileId);
									mainPanel.addCrossFilteringLayers(collection, list);

								}
							}

						}
					}
				});
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public static ViewerStatus getStatus() {
		return viewerStatus;
	}

	/**
	 * Gets the list base map layers.
	 *
	 * @return the list base map layers
	 */
	public static List<BaseMapLayer> getListBaseMapLayers() {
		return listBaseMapLayers;
	}

	/**
	 * Update window size.
	 */
	public void updateSize() {

		int rootHeight = getClientHeight();
		int height = rootHeight;
		mainPanel.setPanelsHeight(height);
		GWT.log("Set height: " + height);
		
		mainPanel.setPanelsWidth(Window.getClientWidth());
	}

	/**
	 * Gets the client height.
	 *
	 * @return the client height
	 */
	public int getClientHeight() {
		RootPanel principalDiv = RootPanel.get(APP_DIV);
		int topBorder = principalDiv.getAbsoluteTop();
		int rootHeight = Window.getClientHeight() - topBorder;
		return rootHeight;
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		applicationBus.addHandler(OpenCollectionEvent.TYPE, new OpenCollectionEventHandler() {

			@Override
			public void onOpenCollection(OpenCollectionEvent openCollectionEvent) {
				GWT.log("OpenCollectionEvent Fired event: " + openCollectionEvent);
				// Check if ID is available
				String collId = openCollectionEvent.getCollectionId();
				ViewerConfiguration theConfig = GeoportalDataViewer.getStatus().getViewerConfig();

				Set<String> ucdIds = theConfig.getAvailableCollections().keySet();

				if (!ucdIds.contains(collId)) {
					GWT.log("!! Collection " + collId + " is not available");
					Window.alert("Collection " + collId + " not found.");
				} else {
					GCubeCollection toOpen = theConfig.getAvailableCollections().get(collId);

					// Check if indexes is empty
					if (toOpen.getIndexes() == null || toOpen.getIndexes().isEmpty()) {
						GWT.log("Unexpected empty indexes in collection " + toOpen);
						Window.alert("Cannot open collection index layer for " + toOpen.getUcd().getName() + ".");
						return;
					}
					// TODO Get Default Index Layer
					// For now we just take the first - only 1 is expected
					IndexLayerDV layer = toOpen.getIndexes().get(0);
					// Open Index Layer

					layerManager.addIndexLayer(layer, toOpen.getUcd().getProfileID());

				}
			}
		});

		applicationBus.addHandler(CloseCollectionEvent.TYPE, new CloseCollectionEventHandler() {

			@Override
			public void onCloseCollection(CloseCollectionEvent closeCollectionEvent) {

				String collId = closeCollectionEvent.getCollectionId();

				if (collId != null) {

					ViewerConfiguration theConfig = GeoportalDataViewer.getStatus().getViewerConfig();
					GCubeCollection toCLose = theConfig.getAvailableCollections().get(collId);
					IndexLayerDV layer = toCLose.getIndexes().get(0);
					layerManager.removeIndexLayer(layer);
				}

			}

		});

		applicationBus.addHandler(AddLayerToMapEvent.TYPE, new AddLayerToMapEventHandler() {

			@Override
			public void onAddingLayerForProject(AddLayerToMapEvent addLayerToMapEvent) {

				if (addLayerToMapEvent.getTheProjectDV() != null) {
					layerManager.addLayerToMapForProject(addLayerToMapEvent.getTheProjectDV().getProfileID(),
							addLayerToMapEvent.getTheProjectDV().getId(), addLayerToMapEvent.getTheProjectDV(),
							addLayerToMapEvent.isNotifyClashing());
				}

			}
		});

		applicationBus.addHandler(ShowDetailsEvent.TYPE, new ShowDetailsEventHandler() {

			@Override
			public void onShowDetails(final ShowDetailsEvent showDetailsEvent) {
				GWT.log("Fired event: " + showDetailsEvent);

				String projectID = showDetailsEvent.getProjectID();
				String profileID = showDetailsEvent.getProfileID();

				if (projectID == null) {
					Window.alert("Item Id not found");
					return;
				}

				// TODO
				boolean found = false;
				GWT.log("Product with id: " + projectID + " found? " + found);

				final EVENT_SOURCE sourceEvent = showDetailsEvent.getSourceEvent();

				if (!sourceEvent.equals(EVENT_SOURCE.LOCATE_FROM_TIMELINE)) {
					timelineMS.hideTimelineProjectRelations();
				}

				if (sourceEvent.equals(EVENT_SOURCE.LOCATE_SEARCHED)) {
					layerManager.removeAllDetailLayers();
				}

				GeoportalDataViewerServiceAsync.Util.getInstance().getProjectViewForId(profileID, projectID,
						new AsyncCallback<ProjectView>() {

							@Override
							public void onFailure(Throwable caught) {
								// Window.alert(caught.getMessage());
								mainPanel.hidePanelDetails();

								Modal errorModal = new Modal(true, true);
								errorModal.setCloseVisible(true);
								errorModal.setTitle("Oops...");
								Image geoportalError = new Image(GeoportalImages.ICONS.warningError());
								FlowPanel errorPanelMsg = new FlowPanel();
								errorPanelMsg.getElement().addClassName("general_warning");
								errorPanelMsg.add(geoportalError);
								errorPanelMsg.add(new HTML("D4S GeoPortal"));
								HTML erroMessage = new HTML(
										"Oops something went wrong, either the project does not exist or it has not been published yet");
								errorPanelMsg.add(erroMessage);
								errorModal.add(errorPanelMsg);
								errorModal.show();
							}

							@Override
							public void onSuccess(ProjectView result) {
								mainPanel.showAsDetails(result, showDetailsEvent.getGeonaItemRef());

								List<RelationshipDV> relationships = result.getTheProjectDV().getRelationships();

								if (showDetailsEvent.isLoadTimelineRelationships() && relationships != null
										&& relationships.size() > 0) {
									GWT.log("LoadTimelineRelationships is true and the project has Relationships");
									timelineMS.showTimelineProjectRelations(result.getTheProjectDV(), false);
								}

								if (sourceEvent != null) {

									switch (sourceEvent) {
									case LOCATE_FROM_TIMELINE:
									case LOCATE_SEARCHED: {
										applicationBus.fireEvent(new ShowPopupOnCentroidEvent(result));
										break;
									}
									case LOCATE_ON_INIT: {
										applicationBus.fireEvent(new ShowPopupOnCentroidEvent(result));
										layerManager.fireGoToLayers();
										break;
									}

									default:
										break;
									}
								}
							}
						});

			}

		});

		applicationBus.addHandler(AddedLayerToMapEvent.TYPE, new AddedLayerToMapEventHandler() {

			@Override
			public void onLayerRendered(AddedLayerToMapEvent addedLayerToMapEvent) {
				GWT.log("Fired AddedLayerToMapEvent " + addedLayerToMapEvent.getLayerItem() + "layer type: "
						+ addedLayerToMapEvent.getLayerType());

				LAYER_TYPE layerType = addedLayerToMapEvent.getLayerType();

				if (layerType != null) {

					switch (layerType) {
					case BASE:

						break;
					case OVERLAY:
						mainPanel.showOverlayLayers(layerManager.getOverlayLayerManager().getPanel());
						break;

					default:
						break;
					}
				}

			}
		});

		applicationBus.addHandler(ZoomOutOverMinimumEvent.TYPE, new ZoomOutOverMinimumEventHandler() {

			@Override
			public void onZoomOut(ZoomOutOverMinimumEvent zoomOutEvent) {

				if (mainPanel.getDisplayedProject() == null && !olMapMng.isQueryPointActive()) {
					olMapMng.hidePopInfo();
					layerManager.removeAllDetailLayers();

				}

				mainPanel.hideOverlayLayers();
			}
		});

		applicationBus.addHandler(ClosedViewDetailsEvent.TYPE, new ClosedViewDetailsEventHandler() {

			@Override
			public void onClosed(ClosedViewDetailsEvent closedViewDetailsEvent) {
				olMapMng.hidePopInfo();
				applicationBus.fireEvent(new TimelineProjectRelationsEvent(null, EVENT_TYPE.HIDE));
			}
		});

		applicationBus.addHandler(MapExtentToEvent.TYPE, new MapExtentToEventHandler() {

			@Override
			public void onExtentEvent(MapExtentToEvent mapExtentToEvent) {
				// TODO Auto-generated method stub

			}
		});

		applicationBus.addHandler(TimelineProjectRelationsEvent.TYPE, new TimelineProjectRelationsEventHandler() {

			@Override
			public void onTimelineRelationEvent(TimelineProjectRelationsEvent timelineProjectRelationsEvent) {

				switch (timelineProjectRelationsEvent.getEventType()) {
				case SHOW: {
					timelineMS.showTimelineProjectRelations(timelineProjectRelationsEvent.getTheProjectDV(), true);
					break;
				}
				case HIDE:
				default: {
					timelineMS.hideTimelineProjectRelations();
					break;
				}
				}

			}
		});

		applicationBus.addHandler(DoActionOnDetailLayersEvent.TYPE, new DoActionOnDetailLayersEventHandler() {

			@Override
			public void onDoActionOnDetailLayers(final DoActionOnDetailLayersEvent doLayerActionEvent) {

				DO_LAYER_ACTION doAction = doLayerActionEvent.getDoAction();
				GWT.log("event: " + doAction);
				GWT.log("layer source: " + doLayerActionEvent.getLayerItem().getName());
				OpenLayerMap olMap = olMapMng.getOLMap();

				switch (doAction) {
				case OPACITY:
					GWT.log("opacity: " + doLayerActionEvent.getOpacity());
					olMap.setWMSDetailLayerOpacity(doLayerActionEvent.getLayerItem(), doLayerActionEvent.getOpacity());
					break;

				case SWAP:
					SwapLayer swLS = doLayerActionEvent.getSourceLayerSwap();
					SwapLayer swLT = doLayerActionEvent.getTargetLayerSwap();
					GWT.log("SWAPPING source " + swLS.getLayerItem().getName() + ", target: "
							+ swLT.getLayerItem().getName());
					olMap.swapDetailsLayers(swLS, swLT);
					break;

				case VISIBILITY:
					GWT.log("visibility: " + doLayerActionEvent.getVisibility());
					olMap.setWMSDetailLayerVisible(doLayerActionEvent.getLayerItem(),
							doLayerActionEvent.getVisibility());

					break;
				case LOCATE_LAYER:

					final LayerItem layerItem = doLayerActionEvent.getLayerItem();

					boolean layerHighlighted = olMap.areLayerFeaturesAsHighlight(layerItem);

					if (layerHighlighted) {
						doLayerActionEvent.getSourceEventUI().setVisibleLoaderFeatures(false, null);
						olMap.removeLayerFeaturesAsHighlight(layerItem.getName());
						return;
					}

					final String geoJSONFeatures = layerFeaturesCache.get(layerItem.getName());

					if (geoJSONFeatures == null) {

						GeoportalDataViewerServiceAsync.Util.getInstance().getWFSResponse(layerItem,
								olMap.getProjectionCode(), null, 0, "json", new AsyncCallback<String>() {

									@Override
									public void onFailure(Throwable caught) {
										doLayerActionEvent.getSourceEventUI().setVisibleLoaderFeatures(false, null);

									}

									@Override
									public void onSuccess(String geoJSONFeaturesResp) {
										doLayerActionEvent.getSourceEventUI().setVisibleLoaderFeatures(false, null);

										if (geoJSONFeaturesResp != null && !geoJSONFeaturesResp.isEmpty()) {
											layerFeaturesCache.put(layerItem.getName(), geoJSONFeaturesResp);

											GWT.log("GeoJSON: " + geoJSONFeaturesResp);

											showLayerFeatures(layerItem, geoJSONFeaturesResp,
													doLayerActionEvent.getLayerObject().getProjectDV());

										}
									}

								});
					} else {

						doLayerActionEvent.getSourceEventUI().setVisibleLoaderFeatures(false, null);
						showLayerFeatures(layerItem, geoJSONFeatures,
								doLayerActionEvent.getLayerObject().getProjectDV());

					}

					break;

				default:
					break;
				}

			}
		});

		applicationBus.addHandler(OverlayCustomLayerToMapEvent.TYPE, new OverlayCustomLayerToMapEventHandler() {

			@Override
			public void onCustomOverlayLayerAction(OverlayCustomLayerToMapEvent customOverLayerToMapEvent) {

				if (customOverLayerToMapEvent.getLayerItem() == null)
					return;

				if (customOverLayerToMapEvent.getActionType()
						.equals(OverlayCustomLayerToMapEvent.ACTION_TYPE.VISIBILITY)) {
					OpenLayerMap olMap = olMapMng.getOLMap();
					olMap.setWMSGroupedCustomLayerVisible(customOverLayerToMapEvent.getLayerItem(),
							customOverLayerToMapEvent.isEnabled());
				}

			}
		});

		applicationBus.addHandler(ChangeMapLayerEvent.TYPE, new ChangeMapLayerEventHandler() {

			@Override
			public void onChangeBaseMapLayer(ChangeMapLayerEvent changeMapLayerEvent) {

				BaseMapLayer baseLayer = changeMapLayerEvent.getBaseMapLayer();

				if (baseLayer == null)
					return;

				String attributionHTML = "<div class='map-credits'><div class='map-credits-container'>"
						+ baseLayer.getAttribution() + "</div></div>";
				olMapMng.getOLMap().changeBaseMap(baseLayer);

				// THE OSM Contributors are automatically added by gwt-ol, others ones not.
				if (!baseLayer.getType().equals(BaseMapLayer.OL_BASE_MAP.OSM)) {
					attributionDiv.setHTML(attributionHTML);
				} else
					attributionDiv.setHTML("");

			}
		});

		applicationBus.addHandler(ShowPopupOnCentroidEvent.TYPE, new ShowPopupOnCentroidEventHandler() {

			@Override
			public void onShowPopup(ShowPopupOnCentroidEvent showPopupOnCentroiEvent) {
				GWT.log("ShowPopupOnCentroidEvent: " + showPopupOnCentroiEvent);

				// Showing popup after clicking on map
				if (showPopupOnCentroiEvent.getProfileID() != null && showPopupOnCentroiEvent.getProjectID() != null) {
					Coordinate transfCoord = MapUtils.geoJSONToBBOXCenter(showPopupOnCentroiEvent.getSpatialReference(),
							null, null);

					performWFSQueryOnCentroid(showPopupOnCentroiEvent.getProjectID(), transfCoord.getX(),
							transfCoord.getY());

					// Showing popup after locate on Map
				} else if (showPopupOnCentroiEvent.getProjectView() != null) {
					layerManager.showPopupInfoForCentroidLayer(showPopupOnCentroiEvent.getProjectView());
				}

			}
		});

		applicationBus.addHandler(SearchPerformedEvent.TYPE, new SearchPerformedEventHandler() {

			@Override
			public void onSearchDone(SearchPerformedEvent searchPerformedEvent) {

				if (searchPerformedEvent != null) {

					LinkedHashMap<String, ol.layer.Image> wmsMap = olMapMng.getOLMap().getWmsLayerMap();
					// String firstWMSKey = wmsMap.keySet().iterator().next();
					List<String> result = searchPerformedEvent.getListProjectIDs();
					ViewerConfiguration viewerConfig = GeoportalDataViewer.getStatus().getViewerConfig();

					String layerName = null;
					try {

						GCubeCollection toOpen = viewerConfig.getAvailableCollections()
								.get(searchPerformedEvent.getProfileID());
						// Check if indexes is empty
						if (toOpen.getIndexes() == null || toOpen.getIndexes().isEmpty()) {
							GWT.log("SearchPerformedEvent Unexpected empty indexes in collection " + toOpen);
							return;
						}
						// TODO Get Default Index Layer
						// For now we just take the first - only 1 is expected
						IndexLayerDV layer = toOpen.getIndexes().get(0);
						// Open Index Layer

						String wmsLink = layer.getLayer().getOgcLinks().get("wms");
						layerName = URLUtil.extractValueOfParameterFromURL("layers", wmsLink);
					} catch (Exception e) {
						// TODO: handle exception
					}

					String newCqlFilter = null; // default

					if (layerName == null) {
						GWT.log("CQL FILTER skipped, layerName is NULL!!");
						return;
					}

					// Applying CQL filter only if there are results and its number are less or
					// equal to SEARCH_LIMIT_RESULTS_TO_MAXIMUM
					if (result != null && result.size() > 0
							&& result.size() <= GeoportalDataViewerConstants.SEARCH_LIMIT_RESULTS_TO_MAXIMUM) {
						String cqlFilter = GeoportalDataViewerConstants.PROJECT_ID_KEY_FEATURE + " IN(";
						for (String projectId : result) {
							cqlFilter += "'" + projectId + "',";
						}
						cqlFilter = cqlFilter.substring(0, cqlFilter.length() - 1) + ")";

						newCqlFilter = olMapMng.getOLMap().setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN.SEARCH, layerName,
								cqlFilter);

					} else {
						newCqlFilter = olMapMng.getOLMap().setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN.SEARCH, layerName,
								null);
					}

					if (searchPerformedEvent.isSearchReset()) {
						newCqlFilter = olMapMng.getOLMap().setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN.SEARCH, layerName,
								null);
					}

					GWT.log("New CQL Filter is: " + newCqlFilter);

					// Resetting CQL Filter
					if (layerName != null)
						layerManager.setCQLForLayerToIndexLayer(layerName, newCqlFilter);

				}

			}
		});

		applicationBus.addHandler(ApplyCQLToLayerOnMapEvent.TYPE, new ApplyCQLToLayerOnMapEventHandler() {

			@Override
			public void onApplyCQL(ApplyCQLToLayerOnMapEvent applyCQLToLayerMapEvent) {
				GWT.log("Fired: " + applyCQLToLayerMapEvent);

				if (applyCQLToLayerMapEvent != null) {

					String layerName = null;

					GCubeSDILayer layer = applyCQLToLayerMapEvent.getTheIndeLayer().getLayer();

					try {
						String wmsLink = layer.getOgcLinks().get("wms");
						layerName = URLUtil.extractValueOfParameterFromURL("layers", wmsLink);
					} catch (Exception e) {
						// TODO: handle exception
					}

					String setCqlFilter = null;

					if (layerName != null) {

						GWT.log("ApplyCQLToLayerOnMapEvent: " + layerName + ", filter : "
								+ applyCQLToLayerMapEvent.getCqlFilterValue());

						if (applyCQLToLayerMapEvent.getCqlFilterValue() == null) {
							olMapMng.getOLMap().setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN.CROSS_FILTERING, layerName,
									null);
							setCqlFilter = null; // is already null
						} else {
							setCqlFilter = applyCQLToLayerMapEvent.getCqlFilterValue();
							olMapMng.getOLMap().setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN.CROSS_FILTERING, layerName,
									setCqlFilter);
						}

						layerManager.setCQLForLayerToIndexLayer(layerName, setCqlFilter);
					}
				}

			}
		});

		applicationBus.addHandler(FitMapToExtentEvent.TYPE, new FitMapToExtentEventHandler() {

			@Override
			public void onFitToMap(FitMapToExtentEvent fitMapToExtentEvent) {

				if (fitMapToExtentEvent.getExtent() != null) {
					olMapMng.getOLMap().fitToExtent(fitMapToExtentEvent.getExtent());
				}

			}
		});

		applicationBus.addHandler(UpdateLayerToMapEvent.TYPE, new UpdateLayerToMapEventHandler() {

			@Override
			public void onUpdateLayer(UpdateLayerToMapEvent updateLayerToMapEvent) {

				if (updateLayerToMapEvent != null && updateLayerToMapEvent.getOperation() != null) {

					if (updateLayerToMapEvent.getLayerType().equals(
							org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEvent.LAYER_TYPE.INDEX)) {

						switch (updateLayerToMapEvent.getOperation()) {
						case STYLE:
							olMapMng.getOLMap().setLayerStyleForIndex(updateLayerToMapEvent.getLayerName(),
									updateLayerToMapEvent.getValue());
							break;

						default:
							break;
						}

					}
				}
			}
		});
	}

	/**
	 * Sets the.
	 *
	 * @param json the json
	 * @return the object
	 */
	public static native Object toJSON(String json) /*-{
		var jsonObj = JSON.parse(json);
		//console.log("jsonObj: "+jsonObj);
		return jsonObj;
	}-*/;

//	/**
//	 * Handler popup closer.
//	 *
//	 * @param divId     the div id
//	 * @param overlayId the overlay id
//	 */
//	public static native void handlerPopuCloser(String divId, String overlayId) /*-{
//		var closer = $doc.getElementById(divId);
//		var overlay = $doc.getElementById(overlayId);
//		closer.onclick = function() {
//			overlay.setPosition(undefined);
//			closer.blur();
//			return false;
//		};
//
//	}-*/;

	/**
	 * Perform WFS query on centroid.
	 *
	 * @param projectID    the project ID
	 * @param centroidLong the centroid long
	 * @param centroidLat  the centroid lat
	 */
	private void performWFSQueryOnCentroid(String projectID, Double centroidLong, Double centroidLat) {
		GWT.log("Perform performWFSQueryOnCentroid: " + projectID + " long: " + centroidLong + ", lat: " + centroidLat);
		if (projectID != null) {
			Double x = centroidLong;
			Double y = centroidLat;
			GWT.log("X: " + x + ", Y:" + y);
			if (x != null && y != null) {
				Coordinate transfCoord = MapUtils.transformCoordiante(new Coordinate(x, y),
						MAP_PROJECTION.EPSG_4326.getName(), MAP_PROJECTION.EPSG_3857.getName());
				GeoQuery select = olMapMng.toDataPointQuery(transfCoord, false);
				GWT.log("GeoQuery: " + select);
				// GeoportalDataViewerConstants.print("fireEvent QueryDataEvent");
				ExtentWrapped toExt = new ExtentWrapped(transfCoord.getX(), transfCoord.getY(), transfCoord.getX(),
						transfCoord.getY());
				layerManager.getLayerManagerBus().fireEvent(
						new QueryDataEvent(select, toExt, projectID, true, MapEventType.ADDED_CENTROID_LAYER_TO_MAP));
			} else {
				GeoportalDataViewerConstants
						.printJs("I cannot select the point one or both coordiantes are null. X: " + x + ", Y:" + y);
			}

		}
	}

	/**
	 * Show popover.
	 *
	 * @param w       the w
	 * @param message the message
	 * @param heading the heading
	 */
	public static void showPopover(final Widget w, String message, String heading) {

		final Popover popover = new Popover();
		popover.setWidget(w);
		popover.setText(message);
		if (heading != null)
			popover.setHeading(heading);
		popover.setPlacement(Placement.BOTTOM);
		popover.reconfigure();
		popover.show();
		Timer timer = new Timer() {

			@Override
			public void run() {
				popover.hide();
			}
		};

		timer.schedule(3000);
	}

	/**
	 * Gets the general error panel.
	 *
	 * @return the general error panel
	 */
	private FlowPanel getGeneralErrorPanel() {
		Image geoportalError = new Image(GeoportalImages.ICONS.geoportaServiceError());
		FlowPanel errorPanelMsg = new FlowPanel();
		errorPanelMsg.getElement().addClassName("general_error");
		errorPanelMsg.add(geoportalError);
		errorPanelMsg.add(new HTML("D4S GeoPortal"));
		HTML erroMessage = new HTML(
				"Oops something went wrong, please refresh and try again. Feel free to contact us if the problem persists");
		errorPanelMsg.add(erroMessage);
		return errorPanelMsg;
	}

	/**
	 * Show layer features.
	 *
	 * @param layerItem       the layer item
	 * @param geoJSONFeatures the geo JSON features
	 * @param projectDV       the project DV
	 */
	private void showLayerFeatures(LayerItem layerItem, String geoJSONFeatures, ProjectDV projectDV) {

		Feature[] features = OLGeoJSONUtil.readGeoJsonFeatures(MAP_PROJECTION.EPSG_4326, geoJSONFeatures);

		GWT.log("features: " + features);
		olMapMng.getOLMap().addLayerFeaturesAsHighlight(layerItem, features, true);
	}

}
