package org.gcube.portlets.user.geoportaldataviewer.client.ui.map;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewer;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.ExtentWrapped;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.LightOpenLayerMap;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalIcons;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.PLACE;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;

import com.github.gwtbootstrap.client.ui.DropdownButton;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import ol.Coordinate;
import ol.OLFactory;

/**
 * The Class MapView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 11, 2020
 */
public class MapView extends Composite {

	private static MapViewUiBinder uiBinder = GWT.create(MapViewUiBinder.class);

	/**
	 * The Interface MapViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Nov 11, 2020
	 */
	interface MapViewUiBinder extends UiBinder<Widget, MapView> {
	}

	@UiField
	HTMLPanel panelBaseLayers;
	
	@UiField
	HTMLPanel theMapPanel;
	
	@UiField
	DropdownButton linkMap;

	@UiField
	HorizontalPanel coordinatePanel;

	private LightOpenLayerMap lightOLM;
	
	private HTML attributionDiv = null;

	private String theMapId = null;

	/**
	 * Instantiates a new map view.
	 *
	 * @param centerTo the center to
	 * @param zoom     the zoom
	 */
	public MapView(Coordinate centerTo, int zoom, String internalMapWidth, String internalMapHeight) {
		initWidget(uiBinder.createAndBindUi(this));
		theMapId = "map" + Random.nextInt();
		theMapPanel.getElement().setId(theMapId);
		theMapPanel.setWidth(internalMapWidth);
		theMapPanel.setHeight(internalMapHeight);
		
		linkMap.setCustomIconStyle(GeoportalIcons.CustomIconType.MAP.get());

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				lightOLM = new LightOpenLayerMap(theMapId);
				setBaseLayers();

				// EPSG_3857 LOCATION TO ITALY
				Location italyLocation = ExtentMapUtil.getLocation(PLACE.ITALY);
				Coordinate transformedCenterCoordinate = italyLocation.getCoordinate(MAP_PROJECTION.EPSG_3857);
				lightOLM.setCenter(transformedCenterCoordinate);
				lightOLM.setZoom(GeoportalDataViewerConstants.LIGHT_MAP_ITALY_FIT_ZOOM_ON);
			}
		});
	}
	
	protected void setBaseLayers() {
		List<BaseMapLayer> listBaseLayers = GeoportalDataViewer.getListBaseMapLayers();
		
		if(listBaseLayers==null)
			return;
		
		int i = 0;

		String radioId = "base_layer_" + Random.nextInt()+Random.nextInt();
		
		for (BaseMapLayer baseMapLayer : listBaseLayers) {
			
			RadioButton radio = new RadioButton(radioId);
			radio.setText(baseMapLayer.getName());
			if(i==0) {
				radio.setValue(true, false);
				setMapAttribution(baseMapLayer);
			}
			
			radio.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					lightOLM.changeBaseMap(baseMapLayer);
					setMapAttribution(baseMapLayer);
				}
			});
			
			SimplePanel sp = new SimplePanel(radio);
			sp.getElement().addClassName("form-inline");
			sp.getElement().addClassName("map-layers-radio");
			panelBaseLayers.add(sp);
			i++;
		}
		
	}
	
	private void setMapAttribution(BaseMapLayer baseMapLayer) {
		
		if(attributionDiv!=null)
			theMapPanel.remove(attributionDiv);
		
		String attributionHTML = "<div class='map-internal-credits'><div class='map-internal-credits-container'>"
				+ baseMapLayer.getAttribution() + "</div></div>";
		attributionDiv = new HTML();
		
		// THE OSM Contributors are automatically added by gwt-ol, others ones not.
		if (!baseMapLayer.getType().equals(BaseMapLayer.OL_BASE_MAP.OSM)) {
			attributionDiv.setHTML(attributionHTML);
			theMapPanel.add(attributionDiv);
		}
	}


	private void setMapSize() {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				int width = theMapPanel.getParent().getOffsetWidth();
				int height = theMapPanel.getParent().getOffsetHeight();
				if (width == 0)
					width = 300;

				if (height == 0)
					height = 300;

				GWT.log("Internal Map w: " + width + ", h: " + height);
				theMapPanel.setSize(width + "px", height + "px");
			}
		});
	}

	public String getPanelMapElementId() {
		return theMapId;
	}

	/**
	 * Adds the marker.
	 *
	 * @param coordinate         the coordinate
	 * @param showCoordinateText the show coordinate text
	 */
	public void addMarker(Coordinate coordinate, boolean showCoordinateText) {

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				if (lightOLM != null) {
					lightOLM.addPoint(coordinate, showCoordinateText, true);
					lightOLM.getMap().getView().setCenter(coordinate);
				}
			}
		});
	}

	/**
	 * Adds the WMS layer.
	 *
	 * @param mapServerHost the map server host
	 * @param layerName     the layer name
	 * @param bbox          the bbox
	 */
	public void addWMSLayer(String mapServerHost, String layerName, BoundsMap bbox) {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {

				ExtentWrapped ew = null;
				BoundsMap theBBOX = bbox;

				if (bbox != null) {

					Coordinate lower = OLFactory.createCoordinate(bbox.getLowerLeftX(), bbox.getLowerLeftY());
					Coordinate lowerCoord = MapUtils.transformCoordiante(lower, MAP_PROJECTION.EPSG_4326.getName(),
							MAP_PROJECTION.EPSG_3857.getName());

					Coordinate upper = OLFactory.createCoordinate(bbox.getUpperRightX(), bbox.getUpperRightY());
					Coordinate upperCoord = MapUtils.transformCoordiante(upper, MAP_PROJECTION.EPSG_4326.getName(),
							MAP_PROJECTION.EPSG_3857.getName());

					ew = new ExtentWrapped(lowerCoord.getX(), lowerCoord.getY(), upperCoord.getX(), upperCoord.getY());
					theBBOX = new BoundsMap(lowerCoord.getX(), lowerCoord.getY(), upperCoord.getX(), upperCoord.getY(),
							null);
				}

				lightOLM.addWMSLayer(mapServerHost, layerName, theBBOX);

				if (ew != null) {

					lightOLM.getMap().getView().fit(ew);

				}

			}
		});

	}

	public LightOpenLayerMap getLightOLM() {
		return lightOLM;
	}

}
