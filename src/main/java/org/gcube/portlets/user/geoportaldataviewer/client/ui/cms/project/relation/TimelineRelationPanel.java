package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent.EVENT_SOURCE;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalImages;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.ProjectUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.util.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class TimelineRelationPanel extends Composite {

	@UiField
	ScrollPanel timelineContainer;

	@UiField
	HTMLPanel timelineHeader;

	@UiField
	Anchor timelinePopupCloser;

	private LoaderIcon loaderData = new LoaderIcon("Loading data... please wait",
			new Image(GeoportalImages.ICONS.spinnerClock()));

	private TimelineRelationPanel instance = this;

	private String selectedTimelineProjectID;

	private ProjectDV theProjectDV;

	private HandlerManager applicationBus;

	private static TimelineRelationPanelUiBinder uiBinder = GWT.create(TimelineRelationPanelUiBinder.class);

	interface TimelineRelationPanelUiBinder extends UiBinder<Widget, TimelineRelationPanel> {
	}

	public TimelineRelationPanel(HandlerManager applicationBus, ProjectDV theProjectDV) {
		initWidget(uiBinder.createAndBindUi(this));
		this.theProjectDV = theProjectDV;
		this.applicationBus = applicationBus;

		String html = ProjectUtil.toHMLCode(theProjectDV.getTheDocument());
		HTML title = new HTML("Relationships of the Project" + " <br>" + html);
		timelineHeader.add(title);
		timelineContainer.getElement().setId("timeline-content");
		timelineHeader.getElement().setId("timeline-header");

		loaderData.getElement().getStyle().setTextAlign(TextAlign.CENTER);

		timelineContainer.getElement().setId("visualization");

		timelineContainer.add(loaderData);

		timelinePopupCloser.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (timelineContainer.isVisible())
					setTimelineContainerVisible(false);
				else
					setTimelineContainerVisible(true);

			}
		});

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			public void execute() {

				GeoportalDataViewerServiceAsync.Util.getInstance().getRelationshipsForTimeline(
						theProjectDV.getProfileID(), theProjectDV.getId(), new AsyncCallback<List<String>>() {

							@Override
							public void onSuccess(List<String> result) {
								timelineContainer.remove(loaderData);

								JSONArray toJsonArr = new JSONArray();
								for (int i = 0; i < result.size(); i++) {
									toJsonArr.set(i, JSONParser.parseStrict(result.get(i)));
								}

								instanceTimeline(timelineContainer.getElement().getId(), toJsonArr, instance);

							}

							@Override
							public void onFailure(Throwable caught) {
								timelineContainer.remove(loaderData);
								Alert alert = new Alert(caught.getMessage());
								alert.setType(AlertType.ERROR);
								alert.setClose(false);
								timelineHeader.add(alert);
							}
						});

			};
		});

	}

	public void setTimelineContainerVisible(boolean bool) {
		timelineContainer.setVisible(bool);
	}
	/*
	 * Called when a project (box) is clicked into Timeline
	 */
	private void setSelectedProject(String theComposedProjectID) {
		this.selectedTimelineProjectID = theComposedProjectID;
		//selectedTimelineProjectID is the couple ProfileID,ProjectID
		GWT.log("selectedTimelineProjectID: " + selectedTimelineProjectID);

		if (selectedTimelineProjectID == null || selectedTimelineProjectID.isEmpty())
			return;

		String[] references = this.selectedTimelineProjectID.split(",");

		final String thePofileID = references[0];
		final String theProjectID = references[1];
		GeoportalItemReferences gir = new GeoportalItemReferences(theProjectID, thePofileID);
		ShowDetailsEvent showDetailsEvent = new ShowDetailsEvent(gir, null, false, false,
				EVENT_SOURCE.LOCATE_FROM_TIMELINE);
		showDetailsEvent.setEventFromTimeline(true);
		GWT.log("fireEvent: " + showDetailsEvent);

		applicationBus.fireEvent(showDetailsEvent);
	}

	public String getSelectedProjectID() {
		return selectedTimelineProjectID;
	}

	public static native String instanceTimeline(String idDivContainer, JSONArray jsonItems,
			TimelineRelationPanel instance) /*-{
		console.log('showing timeline');
		//console.log('showTimeline_instanceTimeline for json items: '+jsonItems);

		//console.log("showTimeline_template: " + $wnd.templateHandlebars);

		// DOM element where the Timeline will be attached
		var container = $doc.getElementById(idDivContainer);

		//console.log("showTimeline_container: " + container);

		var myArray = $wnd.JSON.parse(jsonItems);

		//console.log("showTimeline_jsonItems: " + myArray);

		var container = $doc.getElementById(idDivContainer);

		// Create a DataSet (allows two way data-binding)
		var items = new $wnd.vis.DataSet(myArray);

		// Create a Timeline
		var timeline = new $wnd.vis.Timeline(container, items,
				$wnd.optionsTimeline);

		timeline
				.on(
						'select',
						function(properties, items) {
							//console.log('selected: ' + $wnd.JSON.stringify(properties));
							console.log('selected items: ' + properties.items);
							instance.@org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation.TimelineRelationPanel::setSelectedProject(Ljava/lang/String;)(properties.items+'')
						});

		//timeline.redraw();

	}-*/;
}
