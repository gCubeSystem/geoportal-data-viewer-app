package org.gcube.portlets.user.geoportaldataviewer.client.ui.map;

import java.util.HashMap;
import java.util.Map;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;

import ol.Coordinate;
import ol.OLFactory;

/**
 * The Class ExtentMapUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Sep 1, 2021
 */
public class ExtentMapUtil {

	/**
	 * The Enum PLACE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Sep 1, 2021
	 */
	public static enum PLACE {
		ITALY, WORLD
	}

	final static Map<PLACE, Location> mapExtent = new HashMap<PLACE, Location>();

	static {

		// EPSG_4326 ITALY LOCATION
		Location ita = new Location(PLACE.ITALY.name(), GeoportalDataViewerConstants.ITALY_CENTER_LONG,
				GeoportalDataViewerConstants.ITALY_CENTER_LAT, MAP_PROJECTION.EPSG_4326,
				GeoportalDataViewerConstants.MAP_ITALY_FIT_ZOOM_ON);
		
		// EPSG_4326 WORLD LOCATION
		Location earth = new Location(PLACE.WORLD.name(), 0,
				0, MAP_PROJECTION.EPSG_4326,
				2);

		mapExtent.put(PLACE.ITALY, ita);
		mapExtent.put(PLACE.WORLD, earth);

	}

	public static Location getLocation(PLACE place) {
		return mapExtent.get(place);
	}

	/**
	 * The Class Location.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Sep 1, 2021
	 */
	public static class Location {
		public String name;
		public double coordinateX;
		public double coordinateY;
		public MAP_PROJECTION projection;
		public int zoomLevel;

		/**
		 * Instantiates a new location.
		 *
		 * @param name        the name
		 * @param coordinateX the coordinate X
		 * @param coordinateY the coordinate Y
		 * @param projection  the projection
		 * @param zoomLevel   the zoom level
		 */
		public Location(String name, double coordinateX, double coordinateY, MAP_PROJECTION projection, int zoomLevel) {
			super();
			this.name = name;
			this.coordinateX = coordinateX;
			this.coordinateY = coordinateY;
			this.projection = projection;
			this.zoomLevel = zoomLevel;
		}

		/**
		 * Gets the name.
		 *
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Gets the coordinate X.
		 *
		 * @return the coordinate X
		 */
		public double getCoordinateX() {
			return coordinateX;
		}

		/**
		 * Gets the coordinate Y.
		 *
		 * @return the coordinate Y
		 */
		public double getCoordinateY() {
			return coordinateY;
		}

		/**
		 * Gets the projection.
		 *
		 * @return the projection
		 */
		public MAP_PROJECTION getProjection() {
			return projection;
		}

		/**
		 * Gets the zoom level.
		 *
		 * @return the zoom level
		 */
		public int getZoomLevel() {
			return zoomLevel;
		}

		/**
		 * Gets the coordinate.
		 *
		 * @param targetProjection the target projection
		 * @return the coordinate
		 */
		public Coordinate getCoordinate(MAP_PROJECTION targetProjection) {

			Coordinate coordinate = OLFactory.createCoordinate(coordinateX, coordinateY);
			if (projection.equals(targetProjection)) {
				return coordinate;
			} else {
				Coordinate transformedCenterCoordinate = MapUtils.transformCoordiante(coordinate, projection.getName(),
						targetProjection.getName());
				return transformedCenterCoordinate;
			}

		}

		/**
		 * To string.
		 *
		 * @return the string
		 */
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Location [name=");
			builder.append(name);
			builder.append(", coordinateX=");
			builder.append(coordinateX);
			builder.append(", coordinateY=");
			builder.append(coordinateY);
			builder.append(", projection=");
			builder.append(projection);
			builder.append(", zoomLevel=");
			builder.append(zoomLevel);
			builder.append("]");
			return builder.toString();
		}

	}

}
