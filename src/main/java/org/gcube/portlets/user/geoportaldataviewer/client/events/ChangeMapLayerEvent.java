package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ChangeMapLayerEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 12, 2021
 */
public class ChangeMapLayerEvent extends GwtEvent<ChangeMapLayerEventHandler> {
	public static Type<ChangeMapLayerEventHandler> TYPE = new Type<ChangeMapLayerEventHandler>();
	private BaseMapLayer baseMapLayer;

	/**
	 * Instantiates a new change map layer event.
	 *
	 * @param baseMapLayer the base map layer
	 */
	public ChangeMapLayerEvent(BaseMapLayer baseMapLayer) {
		this.baseMapLayer = baseMapLayer;

	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ChangeMapLayerEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ChangeMapLayerEventHandler handler) {
		handler.onChangeBaseMapLayer(this);

	}

	/**
	 * Gets the base map layer.
	 *
	 * @return the base map layer
	 */
	public BaseMapLayer getBaseMapLayer() {
		return baseMapLayer;
	}
}