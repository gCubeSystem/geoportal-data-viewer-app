package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEvent.LAYER_TYPE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.SwapLayer;
import org.gcube.portlets.user.geoportaldataviewer.client.util.WFSMakerUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;

import ol.Collection;
import ol.Coordinate;
import ol.Feature;
import ol.FeatureOptions;
import ol.Map;
import ol.MapBrowserEvent;
import ol.MapEvent;
import ol.MapOptions;
import ol.OLFactory;
import ol.Overlay;
import ol.OverlayOptions;
import ol.Size;
import ol.View;
import ol.ViewFitOptions;
import ol.ViewOptions;
import ol.animation.AnimationOptions;
import ol.color.Color;
import ol.event.EventListener;
import ol.geom.Geometry;
import ol.interaction.Draw;
import ol.interaction.DrawOptions;
import ol.interaction.Extent;
import ol.interaction.ExtentOptions;
import ol.interaction.Interaction;
import ol.interaction.KeyboardPan;
import ol.interaction.KeyboardZoom;
import ol.layer.Base;
import ol.layer.Image;
import ol.layer.Layer;
import ol.layer.LayerOptions;
import ol.layer.Tile;
import ol.layer.VectorLayerOptions;
import ol.proj.Projection;
import ol.proj.ProjectionOptions;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;
import ol.source.Osm;
import ol.source.Source;
import ol.source.Vector;
import ol.source.Xyz;
import ol.source.XyzOptions;
import ol.style.Fill;
import ol.style.FillOptions;
import ol.style.Stroke;
import ol.style.StrokeOptions;
import ol.style.Style;

/**
 * The Class OpenLayerMap.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 27, 2020
 */
public abstract class OpenLayerMap {

	public static final String CQL_FILTER_PARAMETER = WFSMakerUtil.CQL_FILTER_PARAMETER;

	private static final int MAX_LENGHT_CQL_FOR_GET_REQUEST = 1600; // 1600 characters

	public static final int SET_CENTER_ANIMATED_DURATION = 500;

	public static final int ZOOM_ANIMATED_DURATION = 3000;

	public static final int MAX_ZOOM = 21;

	/**
	 * The Enum CQL_FACILITY_ORIGIN.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 6, 2023
	 */
	public static enum CQL_FACILITY_ORIGIN {
		SEARCH, CROSS_FILTERING
	}

	public HashMap<CQL_FACILITY_ORIGIN, String> cqlFilterMap = new HashMap<CQL_FACILITY_ORIGIN, String>();

	/**
	 * Click listener.
	 *
	 * @param event the event
	 */
	public abstract void clickListener(MapBrowserEvent event);

	/**
	 * Move end listener.
	 *
	 * @param event the event
	 */
	public abstract void moveEndListener(MapEvent event);

	/**
	 * Move start listener.
	 *
	 * @param event the event
	 */
	public abstract void moveStartListener(MapEvent event);

	/**
	 * Map zoom listener.
	 *
	 * @param event the event
	 */
	public abstract void mapZoomListener(MapEvent event);

	/**
	 * Map zoom end listener.
	 *
	 * @param event the event
	 */
	public abstract void mapZoomEndListener(MapEvent event);

	/** The map. */
	private Map map;

	/** The view. */
	private View view;

	/** The view options. */
	private ViewOptions viewOptions = OLFactory.createOptions();

	/** The projection options. */
	private ProjectionOptions projectionOptions = OLFactory.createOptions();

	/** The point draw. */
	private Draw queryPoint;

	private Extent queryBox;

	/** The popup overlay. */
	private Overlay popupOverlay;

	private HandlerManager eventBus;

	private boolean isQueryBoxActive;

	private boolean isQueryPointActive;

	private LinkedHashMap<String, Image> wmsDetailsLayerMap;

	private LinkedHashMap<String, Image> wmsGroupedCustomLayerMap;

	private LinkedHashMap<String, Image> wmsLayerMap;

	private LinkedHashMap<String, ol.layer.Vector> vectorLayersHighlighted = new LinkedHashMap<String, ol.layer.Vector>();

	private Layer baseLayerTile;

	private LayerOrder layerOrder = new LayerOrder();

	/**
	 * Instantiates a new open layer OSM.
	 *
	 * @param divTargetId the div target id
	 * @param eventBus    the event bus
	 * @param baseLayer   the base layer
	 */
	public OpenLayerMap(String divTargetId, HandlerManager eventBus, BaseMapLayer baseLayer) {
		this.eventBus = eventBus;

		// create a projection
		projectionOptions.setCode(MAP_PROJECTION.EPSG_3857.getName());
		projectionOptions.setUnits("m");

		Projection projection = new Projection(projectionOptions);
		viewOptions.setProjection(projection);
		viewOptions.setMaxZoom(MAX_ZOOM);

		// create a view
		view = new View(viewOptions);

		// create the map
		MapOptions mapOptions = OLFactory.createOptions();
		mapOptions.setTarget(divTargetId);
		mapOptions.setView(view);

		map = new Map(mapOptions);

		// map.addLayer(baseLayerTile);
		// map.addLayer(tileDebugLayer);

		// add some controls
		map.addControl(OLFactory.createScaleLine());
		MapUtils.addDefaultControls(map.getControls());

		// add some interactions
		map.addInteraction(new KeyboardPan());
		map.addInteraction(new KeyboardZoom());

		// applying base map
		changeBaseMap(baseLayer);

		bindEvents();

	}

	/**
	 * Change base map.
	 *
	 * @param baseLayer the base layer
	 */
	public void changeBaseMap(BaseMapLayer baseLayer) {

		BaseMapLayer.OL_BASE_MAP baseMap = baseLayer.getType() == null ? BaseMapLayer.OL_BASE_MAP.OSM
				: baseLayer.getType();

		try {
			if (baseLayerTile != null)
				map.removeLayer(baseLayerTile);
		} catch (Exception e) {
			// TODO: handle exception
		}

		switch (baseMap) {
		case OSM:
			XyzOptions xyzOptions = OLFactory.createOptions();
			Osm osmSource = new Osm(xyzOptions);
			osmSource.setUrl(baseLayer.getUrl());
			// setAttributions is buggy
			// osmSource.setAttributions(baseLayer.getAttribution());
			LayerOptions layerOptions = OLFactory.createOptions();
			layerOptions.setSource(osmSource);
			int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.BASE_MAP) + 1;
			layerOptions.setZIndex(zIndex);
			baseLayerTile = new Tile(layerOptions);

			break;
		case MAPBOX:
		case OTHER:

			XyzOptions xyzOptions2 = OLFactory.createOptions();
			Xyz xyz = new Xyz(xyzOptions2);
			// setAttributions is buggy
			// xyz.setAttributions(baseLayer.getAttribution());
			xyz.setUrl(baseLayer.getUrl());

			LayerOptions layerOptions2 = OLFactory.createOptions();
			layerOptions2.setSource(xyz);
			zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.BASE_MAP) + 1;
			layerOptions2.setZIndex(zIndex);
			baseLayerTile = new Tile(layerOptions2);

			break;

		}

		// map == null at init time
		if (map != null) {
			map.addLayer(baseLayerTile);
		}

	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		map.addClickListener(new EventListener<MapBrowserEvent>() {

			@Override
			public void onEvent(MapBrowserEvent event) {
				clickListener(event);
			}
		});

		map.addMoveEndListener(new EventListener<MapEvent>() {

			@Override
			public void onEvent(MapEvent event) {
				moveEndListener(event);
			}
		});

		map.addMoveStartListener(new EventListener<MapEvent>() {

			@Override
			public void onEvent(MapEvent event) {
				moveStartListener(event);
			}
		});

		map.addMapZoomListener(new EventListener<MapEvent>() {

			@Override
			public void onEvent(MapEvent event) {
				mapZoomListener(event);
			}
		});

		map.addMapZoomEndListener(new EventListener<MapEvent>() {

			@Override
			public void onEvent(MapEvent event) {
				mapZoomEndListener(event);
			}
		});
	}

	/**
	 * Sets the center.
	 *
	 * @param centerCoordinate the new center
	 */
	public void setCenter(Coordinate centerCoordinate) {

		AnimationOptions animations = new AnimationOptions();
		animations.setCenter(centerCoordinate);
		animations.setDuration(SET_CENTER_ANIMATED_DURATION);
		view.animate(animations);

		// view.setCenter(centerCoordinate);
	}

	/**
	 * Sets the center.
	 *
	 * @param zoom the new zoom
	 */
	public void setZoom(int zoom) {
		AnimationOptions animations = new AnimationOptions();
		animations.setDuration(ZOOM_ANIMATED_DURATION);
		animations.setZoom(zoom);
		view.animate(animations);
		// view.setZoom(zoom);
	}

	/**
	 * Show popup.
	 *
	 * @param html       the html
	 * @param coordinate the coordinate
	 */
	public void showPopup(String html, Coordinate coordinate) {
		GWT.log("Showing popup on: " + coordinate);
		// GeoportalDataViewerConstants.print("Showing popup on: "+coordinate);
		Element elPopup = DOM.getElementById("popup");
		elPopup.getStyle().setVisibility(Visibility.VISIBLE);
		if (popupOverlay == null) {
			popupOverlay = addOverlay(elPopup);
			addPopupCloserHandler(popupOverlay);
		}
		Element popContent = DOM.getElementById("popup-content");
		popContent.setInnerHTML(html);
		popupOverlay.setPosition(coordinate);
	}

	/**
	 * Hide popup.
	 */
	public void hidePopup() {
		if (popupOverlay != null) {
			Element elPopup = DOM.getElementById("popup");
			elPopup.getStyle().setVisibility(Visibility.HIDDEN);
		}
	}

	/**
	 * Adds the popup closer handler.
	 *
	 * @param popupOverlay the popup overlay
	 */
	private void addPopupCloserHandler(Overlay popupOverlay) {
		Element elPopupCloser = DOM.getElementById("popup-closer");
		Event.sinkEvents(elPopupCloser, Event.ONCLICK);
		Event.setEventListener(elPopupCloser, new com.google.gwt.user.client.EventListener() {

			@Override
			public void onBrowserEvent(Event event) {
				if (Event.ONCLICK == event.getTypeInt()) {
					popupOverlay.setPosition(null);
				}

			}
		});
	}

	/**
	 * Handler popu closer.
	 *
	 * @param divId     the div id
	 * @param overlayId the overlay id
	 */
	public static native void handlerPopuCloser(String divId, String overlayId) /*-{
		var closer = $doc.getElementById(divId);
		var overlay = $doc.getElementById(overlayId);
		closer.onclick = function() {
			overlay.setPosition(undefined);
			closer.blur();
			return false;
		};

	}-*/;

	/**
	 * Adds the WMS layer.
	 *
	 * @param layerItem the layer item
	 */
	public void addWMSLayer(LayerItem layerItem) {

		if (wmsLayerMap == null)
			wmsLayerMap = new LinkedHashMap<String, Image>();

		String key = layerItem.getName();

		Image layer = wmsLayerMap.get(layerItem.getName());
		if (layer == null) {
			ImageWmsParams imageWMSParams = OLFactory.createOptions();
			imageWMSParams.setLayers(layerItem.getName());

			ImageWmsOptions imageWMSOptions = OLFactory.createOptions();
			imageWMSOptions.setUrl(layerItem.getMapServerEndpoint());

			imageWMSOptions.setParams(imageWMSParams);
			// imageWMSOptions.setRatio(1.5f);

			ImageWms imageWMSSource = new ImageWms(imageWMSOptions);
			LayerOptions layerOptions = OLFactory.createOptions();
			layerOptions.setSource(imageWMSSource);

			// Settings MIN and MAX Resolution
			if (layerItem.getMinResolution() != null) {
				layerOptions.setMinResolution(layerItem.getMinResolution());
			}
			if (layerItem.getMaxResolution() != null) {
				layerOptions.setMaxResolution(layerItem.getMaxResolution());
			}

			Image wmsLayer = new Image(layerOptions);

			int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.BASE_WMS) + wmsLayerMap.size() + 1;
			wmsLayer.setZIndex(zIndex);
			// visibleLayerItems

			map.addLayer(wmsLayer);
			wmsLayerMap.put(key, wmsLayer);

			GWT.log("Added WMSLayer for layer: " + layerItem.getName());
			eventBus.fireEvent(new AddedLayerToMapEvent(layerItem, LAYER_TYPE.BASE));
		} else {
			GWT.log("The WMS layer with key: " + key + " already exists, skipping");
		}

	}

	/**
	 * Sets the CQL filter to WMS layer.
	 *
	 * @param origin                 the origin
	 * @param layerName              the key
	 * @param newCQLFilterExpression the cql filter expression
	 * @return the new CQL Filter
	 */
	public String setCQLFilterToWMSLayer(CQL_FACILITY_ORIGIN origin, String layerName, String newCQLFilterExpression) {
		GWT.log("Getting key (layerName): " + layerName);
		GWT.log("Adding CQL FILTER: " + newCQLFilterExpression);

		Image wmsLayer = wmsLayerMap.get(layerName);
		GWT.log("WMS layer is: " + wmsLayer);

		ImageWms imageWMSSource = wmsLayer.getSource();
		ImageWmsParams imageWMSParams = imageWMSSource.getParams();

		// Setting new CQL filter for Origin
		cqlFilterMap.put(origin, newCQLFilterExpression);

		String setCQLFilter = "";

		// Building new CQL filter
		for (CQL_FACILITY_ORIGIN originKey : cqlFilterMap.keySet()) {

			String originCQLFilter = cqlFilterMap.get(originKey);
			if (originCQLFilter != null) {
				if (setCQLFilter.isEmpty()) {
					setCQLFilter = originCQLFilter;
				} else {
					setCQLFilter += " AND " + originCQLFilter;
				}
			}
		}

		if (setCQLFilter.isEmpty())
			imageWMSParams.delete(CQL_FILTER_PARAMETER);
		else
			imageWMSParams.set(CQL_FILTER_PARAMETER, setCQLFilter);

		imageWMSSource.updateParams(imageWMSParams);
		wmsLayer.setSource(imageWMSSource);

		wmsLayer.changed();

		GWT.log("returning " + CQL_FILTER_PARAMETER + ": " + setCQLFilter);
		return setCQLFilter;
	}

	/**
	 * Adds the WMS detail layer.
	 *
	 * @param layerItem      the layer item
	 * @param initialOpacity the initial opacity
	 */
	public void addWMSDetailLayer(LayerItem layerItem, double initialOpacity) {

		if (wmsDetailsLayerMap == null)
			wmsDetailsLayerMap = new LinkedHashMap<String, Image>();

		String key = layerItem.getName();

		Image layer = wmsDetailsLayerMap.get(key);

		if (layer == null) {
			GWT.log("The detail layer with key: " + key + " does not exist, creating and adding it to map");
			ImageWmsParams imageWMSParams = OLFactory.createOptions();
			imageWMSParams.setLayers(layerItem.getName());

			ImageWmsOptions imageWMSOptions = OLFactory.createOptions();
			imageWMSOptions.setUrl(layerItem.getMapServerEndpoint());
			imageWMSOptions.setParams(imageWMSParams);
			// imageWMSOptions.setRatio(1.5f);

			ImageWms imageWMSSource = new ImageWms(imageWMSOptions);
			LayerOptions layerOptions = OLFactory.createOptions();
			layerOptions.setSource(imageWMSSource);

			// Settings MIN and MAX Resolution
			if (layerItem.getMinResolution() != null) {
				layerOptions.setMinResolution(layerItem.getMinResolution());
			}
			if (layerItem.getMaxResolution() != null) {
				layerOptions.setMaxResolution(layerItem.getMaxResolution());
			}

			Image wmsLayer = new Image(layerOptions);
			int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.WMS_DETAIL) + wmsDetailsLayerMap.size() + 1;
			wmsLayer.setZIndex(zIndex);
			wmsLayer.setOpacity(initialOpacity);
			map.addLayer(wmsLayer);
			wmsDetailsLayerMap.put(key, wmsLayer);

			GWT.log("Added WMSDetailLayer for layer name: " + layerItem.getName());
			eventBus.fireEvent(new AddedLayerToMapEvent(layerItem, LAYER_TYPE.OVERLAY));
		} else {
			GWT.log("The WMS detail layer with key: " + key + " already exists, skipping");
		}

	}

	/**
	 * Adds the custom WMS detail layer.
	 *
	 * @param layerItem the layer item
	 */
	public void addGroupedCustomWMSLayer(LayerItem layerItem) {

		if (wmsGroupedCustomLayerMap == null)
			wmsGroupedCustomLayerMap = new LinkedHashMap<String, Image>();

		String key = layerItem.getName();

		Image layer = wmsGroupedCustomLayerMap.get(key);

		if (layer == null) {
			GWT.log("The grouped custom layer with key: " + key + " does not exist, creating and adding it to map");
			ImageWmsParams imageWMSParams = OLFactory.createOptions();
			imageWMSParams.setLayers(layerItem.getName());

			ImageWmsOptions imageWMSOptions = OLFactory.createOptions();
			imageWMSOptions.setUrl(layerItem.getMapServerEndpoint());
			imageWMSOptions.setParams(imageWMSParams);
			// imageWMSOptions.setRatio(1.5f);

			ImageWms imageWMSSource = new ImageWms(imageWMSOptions);
			LayerOptions layerOptions = OLFactory.createOptions();
			layerOptions.setSource(imageWMSSource);

			// Settings MIN and MAX Resolution
			if (layerItem.getMinResolution() != null) {
				layerOptions.setMinResolution(layerItem.getMinResolution());
			}
			if (layerItem.getMaxResolution() != null) {
				layerOptions.setMaxResolution(layerItem.getMaxResolution());
			}

			Image wmsLayer = new Image(layerOptions);
			int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.CUSTOM_WMS_DETAIL) + wmsGroupedCustomLayerMap.size()
					+ 1;
			wmsLayer.setZIndex(zIndex);
			map.addLayer(wmsLayer);
			wmsGroupedCustomLayerMap.put(key, wmsLayer);

			GWT.log("Added GroupedCustomWMSLayer for layer name: " + layerItem.getName());
			eventBus.fireEvent(new AddedLayerToMapEvent(layerItem, LAYER_TYPE.OVERLAY));
		} else {
			GWT.log("The WMS detail layer with key: " + key + " already exists, skipping");
		}

	}

	/**
	 * Removes the all detail layers.
	 *
	 * @param layerName the layer name
	 */
	public void removeWMSLayer(String layerName) {
		GWT.log("Removing layerName: " + layerName + " from map: " + wmsLayerMap);

		// NOT NEEDED ANYMORE.. I'M USING MIN/MAX LAYER RESOLUTION

		if (wmsLayerMap == null)
			return;

		Image layer = wmsLayerMap.get(layerName);

		if (layer != null) {
			map.removeLayer(layer);
			wmsLayerMap.remove(layerName);
			GWT.log("Removed layerName: " + layerName + " from map: " + wmsLayerMap.keySet());
		}

	}

	/**
	 * Removes the all detail layers.
	 */
	public void removeAllDetailLayers() {

		if (wmsDetailsLayerMap == null)
			return;

		GWT.log("Removing wmsDetailsLayerMap: " + wmsDetailsLayerMap.keySet() + " from map");

		for (String key : wmsDetailsLayerMap.keySet()) {
			Image layer = wmsDetailsLayerMap.get(key);
			map.removeLayer(layer);
		}

		wmsDetailsLayerMap.clear();

	}

	/**
	 * Removes the all layer features as highlight.
	 */
	public void removeAllLayerFeaturesAsHighlight() {

		if (vectorLayersHighlighted == null)
			return;

		GWT.log("Removing vectorLayersHighlighted: " + vectorLayersHighlighted.keySet() + " from map");

		for (String layerName : vectorLayersHighlighted.keySet()) {
			removeLayerFeaturesAsHighlight(layerName);
		}

		vectorLayersHighlighted.clear();

	}

	/**
	 * Adds the vector.
	 *
	 * @param geometry the geometry
	 */
	public void addVector(Geometry geometry) {

		VectorLayerOptions vectorLayerOptions = new VectorLayerOptions();
		vectorLayerOptions.setMap(map);

		Style style = new Style();
		FillOptions fillOptions = new FillOptions();
		Color color = new Color(0, 0, 255, 1.0);
		fillOptions.setColor(color);
		Fill fill = new Fill(fillOptions);
		style.setFill(fill);

		FeatureOptions featureOptions = new FeatureOptions();
		featureOptions.setGeometry(geometry);
		Feature feature = OLFactory.createFeature(featureOptions);

		Vector vectorSource = OLFactory.createVectorSource();
		vectorSource.addFeature(feature);
		vectorLayerOptions.setSource(vectorSource);

		ol.layer.Vector vector = OLFactory.createVector(vectorLayerOptions);

		int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.VECTOR) + 1000;
		vector.setZIndex(zIndex);

		map.addLayer(vector);
	}

	/**
	 * Adds the vector.
	 *
	 * @param layerItem              the layer item
	 * @param features               the features
	 * @param fitMapToFeaturesExtent the fit map to features extent
	 */
	public void addLayerFeaturesAsHighlight(LayerItem layerItem, Feature[] features, boolean fitMapToFeaturesExtent) {

		removeLayerFeaturesAsHighlight(layerItem.getName());

		Style style = new Style();
		StrokeOptions strokeOptions = new StrokeOptions();
		strokeOptions.setColor(new Color(255, 69, 0, 0.8));
		strokeOptions.setWidth(5);
		Stroke stroke = new Stroke(strokeOptions);
		stroke.setWidth(2);
		style.setStroke(stroke);

		Vector vectorSource = OLFactory.createVectorSource();

		EventListener<ol.events.Event> listenerE = new EventListener<ol.events.Event>() {

			@Override
			public void onEvent(ol.events.Event event) {
				ol.Extent theExtent = vectorSource.getExtent();
				// GWT.log(theExtent.toString());
				map.getView().fit(theExtent);

			}
		};

		if (fitMapToFeaturesExtent)
			vectorSource.addChangeListener(listenerE);

		vectorSource.addFeatures(features);

		// GWT.log("features: " + features);

		VectorLayerOptions vectorLayerOptions = new VectorLayerOptions();
		vectorLayerOptions.setSource(vectorSource);
		vectorLayerOptions.setStyle(style);

		// GWT.log("vectorLayerOptions: " + vectorLayerOptions);

		ol.layer.Vector vector = OLFactory.createVector(vectorLayerOptions);
		// vector.setStyle(style);
		vector.setVisible(true);
		vector.setZIndex(5000);

		vectorLayersHighlighted.put(layerItem.getName(), vector);

		map.addLayer(vector);

	}

	/**
	 * Are layer features A as highlight.
	 *
	 * @param layerItem the layer item
	 * @return true, if successful
	 */
	public boolean areLayerFeaturesAsHighlight(LayerItem layerItem) {
		return vectorLayersHighlighted.get(layerItem.getName()) != null;
	}

	/**
	 * Removes the layer features A as highlight.
	 *
	 * @param layerName the layer name
	 * @return true, if successful
	 */
	public boolean removeLayerFeaturesAsHighlight(String layerName) {

		try {

			ol.layer.Vector vl = vectorLayersHighlighted.get(layerName);

			if (vl != null) {
				map.removeLayer(vl);
				vectorLayersHighlighted.remove(layerName);
				return true;
			}
		} catch (Exception e) {

		}

		return false;
	}

	/**
	 * Adds the point vector source.
	 *
	 * @return the draw
	 */
	public Draw addPointVectorSource() {
		if (queryPoint == null)
			initPointInteraction();

		map.addInteraction(queryPoint);
		isQueryPointActive = true;
		return queryPoint;
	}

	/**
	 * Inits the point interaction.
	 */
	private void initPointInteraction() {
		Vector vectorSource = new Vector();
		DrawOptions drawOptions = new DrawOptions();
		drawOptions.setSource(vectorSource);
		drawOptions.setType("Point");
		drawOptions.setMaxPoints(1);
		drawOptions.setMinPoints(1);
		drawOptions.setWrapX(false);
		queryPoint = new Draw(drawOptions);

		queryPoint.addChangeListener(new EventListener<ol.events.Event>() {

			@Override
			public void onEvent(ol.events.Event event) {
				GWT.log(event.getType());

			}
		});
	}

	/**
	 * Removes the interaction.
	 *
	 * @param interaction the interaction
	 */
	public void removeInteraction(Interaction interaction) {
		map.removeInteraction(interaction);
	}

	/**
	 * Removes the interactions.
	 */
	public void removeQueryInteractions() {
		Collection<Interaction> interactions = map.getInteractions();
		if (interactions != null) {
			map.removeInteraction(queryBox);
			map.removeInteraction(queryPoint);
			isQueryBoxActive = false;
			isQueryPointActive = false;
		}
	}

	/**
	 * Adds the extent interaction.
	 *
	 * @return the extent
	 */
	public Extent addExtentInteraction() {
		ExtentOptions extentOptions = new ExtentOptions();
		extentOptions.setWrapX(false);
		// StyleOptions styleOptions = new StyleOptions();
		// styleOptions.setStroke(stroke);
		// styleOptions.set
		// extentOptions.setBoxStyle(new ol.style.Style(styleOptions));
		queryBox = new Extent(extentOptions);
		map.addInteraction(queryBox);
		isQueryBoxActive = true;
		return queryBox;
	}

	/**
	 * Adds the overlay.
	 *
	 * @param element the element
	 * @return the overlay
	 */
	public Overlay addOverlay(Element element) {
		/**
		 * Create an overlay to anchor the popup to the map.
		 */
		OverlayOptions overlayOptions = new OverlayOptions();
		overlayOptions.setAutoPan(true);
		Overlay overlay = new Overlay(overlayOptions);
		overlay.setElement(element);
		map.addOverlay(overlay);
		return overlay;
	}

	/**
	 * Gets the projection code.
	 *
	 * @return the projection code
	 */
	public String getProjectionCode() {
		return map.getView().getProjection().getCode();
	}

	/**
	 * Gets the current zoom level.
	 *
	 * @return the current zoom level
	 */
	public double getCurrentZoomLevel() {
		return map.getView().getZoom();
	}

	/**
	 * Gets the current zoom level.
	 *
	 * @return the current zoom level
	 */
	public double getCurrentResolution() {
		return map.getView().getResolution();
	}

	/**
	 * Gets the bbox.
	 *
	 * @return the bbox
	 */
	public ol.Extent getBBOX() {
		return getExtent();
	}

	/**
	 * Gets the extent.
	 *
	 * @return the extent
	 */
	public ol.Extent getExtent() {
		return this.map.getView().calculateExtent(map.getSize());
	}

	/**
	 * Transform.
	 *
	 * @param centerCoordinate the center coordinate
	 * @param source           the source
	 * @param target           the target
	 * @return the coordinate
	 */
	public Coordinate transform(Coordinate centerCoordinate, String source, String target) {
		return Projection.transform(centerCoordinate, source, target);
	}

	/**
	 * Checks if is query box active.
	 *
	 * @return true, if is query box active
	 */
	public boolean isQueryBoxActive() {
		return isQueryBoxActive;
	}

	/**
	 * Checks if is query point active.
	 *
	 * @return true, if is query point active
	 */
	public boolean isQueryPointActive() {
		return isQueryPointActive;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public Size getSize() {
		return map.getSize();
	}

	/**
	 * Map instancied.
	 *
	 * @return true, if successful
	 */
	public boolean mapInstancied() {
		return this.map != null;
	}

	/**
	 * Gets the layers from map.
	 *
	 * @return the layers from map
	 */
	public ArrayList<String> getLayersFromMap() {
		Collection<Base> layers = map.getLayers();
		ArrayList<String> layerNames = null;
		if (layers != null) {
			Base[] layersArr = layers.getArray();
			layerNames = new ArrayList<String>(layersArr.length);
			for (int i = 0; i < layersArr.length; i++) {
				Base layer = layersArr[i];
				if (layer instanceof Image) {
					Image layerImage = (Image) layer;

					Source source = layerImage.getSource();
					// GeoportalDataViewerConstants.printJsObj(source);
					String sorceRootObj = GeoportalDataViewerConstants.toJsonObj(source);
					JSONValue jsonObj = JSONParser.parseStrict(sorceRootObj);
					// GWT.log("jsonObj: " + jsonObj.toString());
					JSONObject jsonSourceObj = (JSONObject) jsonObj;
					GeoportalDataViewerConstants.printJsObj(jsonSourceObj);

					JSONObject jsonParamsObj = (JSONObject) jsonSourceObj.get("params_");
					// GWT.log("jsonParamsObj is: "+jsonParamsObj);
					JSONValue jsonLayers = jsonParamsObj.get("LAYERS");
					GWT.log("theLayerName name is: " + jsonLayers);
					layerNames.add(jsonLayers.toString());

					JSONObject imagesParamsObj = (JSONObject) jsonSourceObj.get("image_");
					JSONArray extent = (JSONArray) imagesParamsObj.get("extent");
					GWT.log("extentLayer: " + extent.toString());
				}
			}
		}
		return layerNames;
	}

	/**
	 * Gets the source extent for layer.
	 *
	 * @param layerName the layer name
	 * @return the source extent for layer
	 */
	public ExtentWrapped getSourceExtentForLayer(String layerName) {
		Collection<Base> layers = map.getLayers();
		if (layers != null) {
			Base[] layersArr = layers.getArray();
			for (int i = 0; i < layersArr.length; i++) {
				Base layer = layersArr[i];
				if (layer instanceof Image) {
					Image layerImage = (Image) layer;

					Source source = layerImage.getSource();
					// GeoportalDataViewerConstants.printJsObj(source);
					String sorceRootObj = GeoportalDataViewerConstants.toJsonObj(source);
					JSONValue jsonObj = JSONParser.parseStrict(sorceRootObj);
					// GWT.log("jsonObj: " + jsonObj.toString());
					JSONObject jsonSourceObj = (JSONObject) jsonObj;
					// GeoportalDataViewerConstants.printJsObj(jsonSourceObj);

					JSONObject jsonParamsObj = (JSONObject) jsonSourceObj.get("params_");
					// GWT.log("jsonParamsObj is: "+jsonParamsObj);
					JSONValue jsonLayers = jsonParamsObj.get("LAYERS");

					String layerNameIntoMap = jsonLayers.toString().replaceAll("\"", "");
					if (layerName.compareTo(layerNameIntoMap) == 0) {
						JSONObject imagesParamsObj = (JSONObject) jsonSourceObj.get("image_");
						JSONArray extent = (JSONArray) imagesParamsObj.get("extent");
						// GWT.log("extentLayer: "+extent.toString());
						double minX = Double.parseDouble(extent.get(0).toString());
						double minY = Double.parseDouble(extent.get(1).toString());
						double maxX = Double.parseDouble(extent.get(2).toString());
						double maxY = Double.parseDouble(extent.get(3).toString());
						return new ExtentWrapped(minX, minY, maxX, maxY);
					}
				}
			}
		}
		return null;
	}

	/**
	 * Gets the wms details layer map.
	 *
	 * @return the wms details layer map
	 */
	public LinkedHashMap<String, Image> getWmsDetailsLayerMap() {
		return wmsDetailsLayerMap;
	}

	/**
	 * Gets the wms layer map.
	 *
	 * @return the wms layer map
	 */
	public LinkedHashMap<String, Image> getWmsLayerMap() {
		return wmsLayerMap;
	}

	/**
	 * Checks if is layer visible.
	 *
	 * @param layerName the layer name
	 * @return true, if is layer visible
	 */
	public boolean isLayerVisible(String layerName) {

		String key = layerName;
		Image layer = wmsLayerMap.get(key);
		if (layer != null)
			return layer.getVisible();

		layer = wmsDetailsLayerMap.get(key);
		if (layer != null)
			return layer.getVisible();

		return false;
	}

	/**
	 * Sets the WMS detail layer visible.
	 *
	 * @param layerItem the layer item
	 * @param visible   the visible
	 */
	public void setWMSDetailLayerVisible(LayerItem layerItem, boolean visible) {
		String key = layerItem.getName();
		Image layer = wmsDetailsLayerMap.get(key);
		layer.setVisible(visible);
	}

	public void setLayerStyleForIndex(String layerName, String value) {
		GWT.log("Setting style " + value + " for " + layerName);

		Image indexWmsLayer = wmsLayerMap.get(layerName);
		GWT.log("WMS layer is: " + indexWmsLayer);

		if (indexWmsLayer != null) {
			ImageWms imageWMSSource = indexWmsLayer.getSource();
			ImageWmsParams imageWMSParams = imageWMSSource.getParams();
			imageWMSParams.set("STYLES", value);
			
			imageWMSSource.updateParams(imageWMSParams);
			indexWmsLayer.setSource(imageWMSSource);
			indexWmsLayer.changed();
		}
	}

	/**
	 * Sets the WMS detail layer opacity.
	 *
	 * @param layerItem the layer item
	 * @param opacity   the opacity
	 */
	public void setWMSDetailLayerOpacity(LayerItem layerItem, double opacity) {
		String key = layerItem.getName();
		Image layer = wmsDetailsLayerMap.get(key);
		if (layer != null)
			layer.setOpacity(opacity);
	}

	/**
	 * Sets the WMS detail layer visible.
	 *
	 * @param layerItem the layer item
	 * @param visible   the visible
	 */
	public void setWMSGroupedCustomLayerVisible(LayerItem layerItem, boolean visible) {
		String key = layerItem.getName();
		Image layer = wmsGroupedCustomLayerMap.get(key);
		if (layer != null)
			layer.setVisible(visible);
	}

	/**
	 * Swap details layers.
	 *
	 * @param swapLSource the swap L source
	 * @param swapLTarget the swap L target
	 */
	public void swapDetailsLayers(SwapLayer swapLSource, SwapLayer swapLTarget) {

		String layerSource = swapLSource.getLayerItem().getName();
		String layerTarget = swapLTarget.getLayerItem().getName();
		Image layer1 = wmsDetailsLayerMap.get(layerSource);
		Image layer2 = wmsDetailsLayerMap.get(layerTarget);

		int zIndexOffset = layerOrder.getOffset(LayerOrder.LAYER_TYPE.WMS_DETAIL);
		int zIndexS = swapLSource.getPosition() + zIndexOffset + 1;
		int zIndexT = swapLTarget.getPosition() + zIndexOffset + 1;
		GWT.log("new zindex source: " + zIndexS + ", new zTarget: " + zIndexT);

		layer1.setZIndex(zIndexT);
		layer2.setZIndex(zIndexS);

		GWT.log("layer1 source: " + layerSource + ", new zIndex: " + layer1.getZIndex());
		GWT.log("layer1 target: " + layerTarget + ", new zIndex: " + layer2.getZIndex());

	}

	/**
	 * Gets the layer by index.
	 *
	 * @param map   the map
	 * @param index the index
	 * @return the layer by index
	 */
	private Image getLayerByIndex(LinkedHashMap<String, Image> map, int index) {
		return map.get((map.keySet().toArray())[index]);
	}

	/**
	 * Fit to extent.
	 *
	 * @param extent the extent
	 */
	public void fitToExtent(ol.Extent extent) {
		ViewFitOptions opt = new ViewFitOptions();
		opt.setMaxZoom(16);
		opt.setDuration(SET_CENTER_ANIMATED_DURATION * 5);
		map.getView().fit(extent, opt);

	}

}
