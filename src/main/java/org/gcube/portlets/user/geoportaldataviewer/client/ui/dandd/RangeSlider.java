package org.gcube.portlets.user.geoportaldataviewer.client.ui.dandd;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.DO_LAYER_ACTION;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class RangeSlider extends Composite {

	private static RangeSliderUiBinder uiBinder = GWT.create(RangeSliderUiBinder.class);

	interface RangeSliderUiBinder extends UiBinder<Widget, RangeSlider> {
	}

	@UiField
	Element theSlider;

	private String sliderId;

	private HandlerManager applicationBus;

	private LayerItem layer;

	public RangeSlider(HandlerManager applicationBus, LayerItem layer) {
		this.applicationBus = applicationBus;
		this.layer = layer;
		initWidget(uiBinder.createAndBindUi(this));
		sliderId = "slider-" + Random.nextInt();
		theSlider.addClassName("slider");
		theSlider.setId(sliderId);
		theSlider.setTitle("Set opacity of the layer");
		theSlider.setPropertyObject("value", GeoportalDataViewerConstants.INITIAL_LAYER_OPACITY*100);

		bindEvents();
	}

	private void bindEvents() {

		Event.sinkEvents(theSlider, Event.ONMOUSEUP);
		Event.setEventListener(theSlider, new EventListener() {

			@Override
			public void onBrowserEvent(Event event) {
				if (Event.ONMOUSEUP == event.getTypeInt()) {
					GWT.log("Event.ONMOUSEUP");
					DoActionOnDetailLayersEvent actionChangeOpacity = new DoActionOnDetailLayersEvent(
							DO_LAYER_ACTION.OPACITY, layer, null);
					int value = Integer.parseInt((String) theSlider.getPropertyObject("value"));
					// GWT.log("opacity int value: "+value);
					double opacity = (double) ((double) value / 100);
					GWT.log("opacity double value: " + opacity);
					actionChangeOpacity.setOpacity(opacity);
					applicationBus.fireEvent(actionChangeOpacity);
				}

			}
		});

	}

}
