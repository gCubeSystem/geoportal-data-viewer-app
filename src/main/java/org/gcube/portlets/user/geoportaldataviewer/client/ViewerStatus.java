package org.gcube.portlets.user.geoportaldataviewer.client;

import org.gcube.portlets.user.geoportaldataviewer.shared.ViewerConfiguration;

public class ViewerStatus {

	private ViewerConfiguration viewerConfig;

	public void setConfig(ViewerConfiguration result) {
		this.viewerConfig = result;
		
	}
	
	public ViewerConfiguration getViewerConfig() {
		return viewerConfig;
	}

}
