package org.gcube.portlets.user.geoportaldataviewer.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;

public class ItemFieldsResponse implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6723902864074541278L;
	private UseCaseDescriptorDV useCaseDescriptorDV;
	private List<ItemFieldDV> listItemFields;

	public ItemFieldsResponse() {

	}

	public UseCaseDescriptorDV getUseCaseDescriptorDV() {
		return useCaseDescriptorDV;
	}

	public List<ItemFieldDV> getListItemFields() {
		return listItemFields;
	}

	public void setUseCaseDescriptorDV(UseCaseDescriptorDV useCaseDescriptorDV) {
		this.useCaseDescriptorDV = useCaseDescriptorDV;
	}

	public void setListItemFields(List<ItemFieldDV> listItemFields) {
		this.listItemFields = listItemFields;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ItemFieldsResponse [useCaseDescriptorDV=");
		builder.append(useCaseDescriptorDV);
		builder.append(", listItemFields=");
		builder.append(listItemFields);
		builder.append("]");
		return builder.toString();
	}

}
