package org.gcube.portlets.user.geoportaldataviewer.server.mongoservice;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.Collection;
import java.util.List;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity.IAMClientCredentials;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GNABaseMapsResourceReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 23, 2021
 */
public class IAMClientCredentialsReader {

	private static Logger LOG = LoggerFactory.getLogger(IAMClientCredentialsReader.class);

	private static final String SE_PROFILE_NAME = "geoportal-data-viewer-app";
	private static final String SE_CATEGORY_NAME = "SystemClient";

	/**
	 * Gets the credentials.
	 *
	 * @return the credentials
	 * @throws Exception the exception
	 */
	public static IAMClientCredentials getCredentials() throws Exception {

		LOG.info("Searching SE in the scope: " + ScopeProvider.instance.get() + " with profile name: " + SE_PROFILE_NAME
				+ " and category name: " + SE_CATEGORY_NAME);

		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Name/text() eq '" + SE_PROFILE_NAME + "'");
		query.addCondition("$resource/Profile/Category/text() eq '" + SE_CATEGORY_NAME + "'");

		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> resources = client.submit(query);

		if (resources.size() > 0)
			LOG.info("The query returned " + resources.size() + " ServiceEndpoint/s");
		else
			throw new RuntimeException("ServiceEndpoint not found. Searching for profile name: " + SE_PROFILE_NAME
					+ " and category name: " + SE_CATEGORY_NAME + "in the scope: " + ScopeProvider.instance.get());

		ServiceEndpoint se = resources.get(0);
		Collection<AccessPoint> theAccessPoints = se.profile().accessPoints().asCollection();
		String clientId = null;
		String secredPwd = null;
		for (AccessPoint accessPoint : theAccessPoints) {
			clientId = accessPoint.username();
			secredPwd = accessPoint.password();
			LOG.debug("Found clientId: " + clientId + " and encrypted secret: " + secredPwd);
			// decrypting the pwd
			try {
				if (secredPwd != null) {
					secredPwd = StringEncrypter.getEncrypter().decrypt(secredPwd);
					LOG.debug("Secret decrypted is: " + secredPwd.substring(0, secredPwd.length() / 2)
							+ "_MASKED_TOKEN_");
				}
			} catch (Exception e) {
				throw new RuntimeException("Error on decrypting the pwd: ", e);
			}
		}

		LOG.info("Returning keycloack credentials read from SE");
		return new IAMClientCredentials(clientId, secredPwd);

	}

}
