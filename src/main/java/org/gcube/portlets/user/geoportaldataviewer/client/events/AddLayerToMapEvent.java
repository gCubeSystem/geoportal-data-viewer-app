package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class AddedLayerToMapEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 18, 2020
 */
public class AddLayerToMapEvent extends GwtEvent<AddLayerToMapEventHandler> {
	public static Type<AddLayerToMapEventHandler> TYPE = new Type<AddLayerToMapEventHandler>();
	private ProjectDV theProjectDV;
	private boolean notifyClashing;

	/**
	 * Instantiates a new adds the layer to map event.
	 *
	 * @param theProjectDV the the project DV
	 * @param notifyClashing the notify clashing
	 */
	public AddLayerToMapEvent(ProjectDV theProjectDV, boolean notifyClashing) {
		this.theProjectDV = theProjectDV;
		this.notifyClashing = notifyClashing;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<AddLayerToMapEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(AddLayerToMapEventHandler handler) {
		handler.onAddingLayerForProject(this);

	}

	/**
	 * Gets the the project DV.
	 *
	 * @return the the project DV
	 */
	public ProjectDV getTheProjectDV() {
		return theProjectDV;
	}

	/**
	 * Checks if is notify clashing.
	 *
	 * @return true, if is notify clashing
	 */
	public boolean isNotifyClashing() {
		return notifyClashing;
	}

}