package org.gcube.portlets.user.geoportaldataviewer.client.util;

import org.gcube.portlets.user.geoportaldataviewer.shared.faults.ControlledError;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public abstract class ControlledCallBack<T> implements AsyncCallback<T> {

	@Override
	public void onFailure(Throwable caught) {
		before();
		if(caught instanceof ControlledError) {
			GWT.log("Controlled error "+caught);
			Window.alert(caught.getMessage());
		}else {
			GWT.log("Uncontrolled failure, check server log.", caught);
			Window.alert("Sorry, an unexpected error occurred. Please, contact the support");
		}
		after();
	}

	public void before() {}

	public void after() {}
	
	@Override
	public void onSuccess(T result) {
		before();
		after();
	}
}
