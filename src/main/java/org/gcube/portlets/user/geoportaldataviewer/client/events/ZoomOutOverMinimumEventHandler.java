package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface ZoomOutEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 19, 2020
 */
public interface ZoomOutOverMinimumEventHandler extends EventHandler {


	/**
	 * On zoom out.
	 *
	 * @param zoomOutEvent the zoom out event
	 */
	void onZoomOut(ZoomOutOverMinimumEvent zoomOutEvent);
}
