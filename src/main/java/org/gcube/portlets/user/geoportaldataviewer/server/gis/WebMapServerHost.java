/**
 *
 */
package org.gcube.portlets.user.geoportaldataviewer.server.gis;

/**
 * The Class WebMapServerHost.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 12, 2020
 */
public class WebMapServerHost {

	private String host = "";
	private String scope = "";

	
	/**
	 * Instantiates a new web map server host.
	 */
	public WebMapServerHost() {
	}

	/**
	 * Instantiates a new geoserver base uri.
	 *
	 * @param host the base url
	 * @param scope the scope
	 */
	public WebMapServerHost(String host, String scope) {
		this.host = host;
		this.scope = scope;
	}


	/**
	 * Gets the host.
	 *
	 * @return the host
	 */
	public String getHost() {

		return host;
	}


	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {

		return scope;
	}


	/**
	 * Sets the host.
	 *
	 * @param host the host to set
	 */
	public void setHost(String host) {

		this.host = host;
	}


	/**
	 * Sets the scope.
	 *
	 * @param scope the scope to set
	 */
	public void setScope(String scope) {

		this.scope = scope;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("WebMapServerHost [host=");
		builder.append(host);
		builder.append(", scope=");
		builder.append(scope);
		builder.append("]");
		return builder.toString();
	}
}
