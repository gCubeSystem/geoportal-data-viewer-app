package org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity;

import java.io.Serializable;


/**
 * The Class IAMClientCredentials.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 23, 2021
 */
public class IAMClientCredentials implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7242909633989611318L;
	private String clientId;
	private String clientSecret;

	/**
	 * Instantiates a new IAM client credentials.
	 */
	public IAMClientCredentials() {
	}

	/**
	 * Instantiates a new IAM client credentials.
	 *
	 * @param clientId the client id
	 * @param clientSecret the client secret
	 */
	public IAMClientCredentials(String clientId, String clientSecret) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public String getClientId() {
		return clientId;
	}

	/**
	 * Gets the client secret.
	 *
	 * @return the client secret
	 */
	public String getClientSecret() {
		return clientSecret;
	}

	/**
	 * Sets the client id.
	 *
	 * @param clientId the new client id
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	/**
	 * Sets the client secret.
	 *
	 * @param clientSecret the new client secret
	 */
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IAMClientCredentials [clientId=");
		builder.append(clientId);
		builder.append(", clientSecret=");
		builder.append(clientSecret);
		builder.append("]");
		return builder.toString();
	}

}
