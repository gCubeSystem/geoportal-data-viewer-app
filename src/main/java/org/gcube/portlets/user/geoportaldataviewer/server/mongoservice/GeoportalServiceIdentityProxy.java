package org.gcube.portlets.user.geoportaldataviewer.server.mongoservice;

import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity.GcubeIdentity;
import org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity.IAMClientIdentity;
import org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity.UserIdentity;
import org.gcube.portlets.user.geoportaldataviewer.server.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GeoportalServiceIdentityProxy.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 23, 2021
 */
public class GeoportalServiceIdentityProxy {

	private static final Logger LOG = LoggerFactory.getLogger(GeoportalServiceIdentityProxy.class);

	private GcubeIdentity gCubeIdentity;

	private boolean isUser = false;

	private boolean isIAMClient = false;

	/**
	 * Instantiates a new concessioni mongo service proxy.
	 *
	 * @param request the request
	 * @throws Exception the exception
	 */
	public GeoportalServiceIdentityProxy(HttpServletRequest request) throws Exception {
		SessionUtil.getCurrentContext(request, true);
		PortalContext pContext = PortalContext.getConfiguration();
		String username = null;
		try {
			username = pContext.getCurrentUser(request).getUsername();
			if (username == null || username.isEmpty())
				throw new Exception("Invalid username");
		} catch (Exception e) {
			LOG.info("Username not detected in session");
			username = null; // to be sure that is null and not empty string
		}
		if (username != null) {
			isUser = true;
			LOG.info("User detected, using its identity");
			gCubeIdentity = new UserIdentity();
		} else {
			isIAMClient = true;
			LOG.info("User not detected, using IAM Client identity");
			gCubeIdentity = new IAMClientIdentity();
		}

		gCubeIdentity.setIdentity(request);
	}

	public boolean isUser() {
		return isUser;
	}

	public boolean isIAMClient() {
		return isIAMClient;
	}

	public String getDescription() {
		return gCubeIdentity.getIdentityDescription();
	}

	public String getToken() {
		return gCubeIdentity.getToken();
	}

	public String getIdentity() {
		return gCubeIdentity.getIdentity();
	}
}
