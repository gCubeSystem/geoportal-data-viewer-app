/**
 *
 */
package org.gcube.portlets.user.geoportaldataviewer.client.util;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * The Class StringUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 16, 2020
 */
public class StringUtil {
	
	private static final String NO_TIME = "_00-00-00";

	/**
	 * Ellipsize.
	 *
	 * @param input the input string that may be subjected to shortening
	 * @param maxCharacters the maximum characters that must be returned for the
	 *                      input string
	 * @return the string
	 * @throws Exception the exception
	 */
	public static String ellipsize(String input, int maxCharacters) {
		
	  if(input==null)
		  return "";

	  if (input.length() < maxCharacters)
	    return input;
	  
	  return input.substring(0, maxCharacters)+"...";
	}
	
	public static String formatDate(String date) {
		if(date==null)
			return date;
		
		Date theDate = null;
		try {
			if(date.endsWith(NO_TIME)) {
				date = date.replaceAll(NO_TIME, "");
				theDate = DateTimeFormat.getFormat("yyyyMMdd").parse(date);
			}else {
				theDate = DateTimeFormat.getFormat("yyyyMMdd_HH-mm-ss").parse(date);
			}
		}catch (Exception e) {
			//Window.alert(e.toString());
		}
		
		if(theDate!=null) {
			return DateTimeFormat.getFormat("dd-MM-yyyy").format(theDate);
		}
		
		return date;
	}

	
	/**
	 * Full name to layer name.
	 *
	 * @param layerFullName the layer full name
	 * @param splitChar the split char
	 * @return the string
	 */
	public static String fullNameToLayerName(String layerFullName, String splitChar) {

		if (layerFullName == null || layerFullName.isEmpty())
			return layerFullName;

		int layerSep = layerFullName.lastIndexOf(splitChar);
		return layerSep > 0 && (layerSep + 1) < layerFullName.length()
				? layerFullName.substring(layerSep + 1, layerFullName.length())
				: layerFullName;
	}
}
