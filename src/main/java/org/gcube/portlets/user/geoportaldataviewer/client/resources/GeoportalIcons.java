package org.gcube.portlets.user.geoportaldataviewer.client.resources;

import com.github.gwtbootstrap.client.ui.constants.BaseIconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

public interface GeoportalIcons extends ClientBundle {

	/** My custom base icon collection */
	public enum CustomIconType implements BaseIconType {
		
		MAP("map"),
		PRESET_LOCATION("preset_location"),
		LAYERS("layers");
		
	    /** Inject the icon's css once at first usage */
	    static {
	    	GeoportalImages icons = GWT.create(GeoportalImages.class);
	        icons.css().ensureInjected();
	    }

		private static final String PREFIX = "myBaseIcon_";
		private String className;

	    private CustomIconType(String className) {
	        this.className = className;
	    }

	    /**
	     * {@inheritDoc}
	     */
	    @Override
	    public String get() {
	        return PREFIX + className;
	    }
	}
}
