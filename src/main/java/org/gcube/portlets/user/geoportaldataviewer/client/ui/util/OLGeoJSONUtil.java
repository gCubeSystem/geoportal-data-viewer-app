package org.gcube.portlets.user.geoportaldataviewer.client.ui.util;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;

import ol.Feature;
import ol.OLFactory;
import ol.format.GeoJson;
import ol.format.GeoJsonFeatureOptions;
import ol.format.GeoJsonOptions;
import ol.proj.Projection;
import ol.proj.ProjectionOptions;

/**
 * The Class OLGeoJSONUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 25, 2023
 */
public class OLGeoJSONUtil {

	/**
	 * Builds the geo JSON.
	 *
	 * @param projection the projection
	 * @return the geo json
	 */
	public static GeoJson buildGeoJSON(MAP_PROJECTION projection) {
		try {
			GeoJsonFeatureOptions fo = new GeoJsonFeatureOptions();
			ProjectionOptions projectionOptions = new ProjectionOptions();
			projectionOptions.setCode(projection.getName());
			Projection fp = new Projection(projectionOptions);
			fo.setFeatureProjection(fp);
			fo.setDataProjection(fp);
			GeoJsonOptions geoJsonOpt = new GeoJsonOptions();
			geoJsonOpt.setDefaultDataProjection(fp);
			geoJsonOpt.setFeatureProjection(fp);
			return OLFactory.createGeoJSON(geoJsonOpt);
		} catch (Exception e) {
			// silent
			return OLFactory.createGeoJSON();
		}

	}

	/**
	 * Read geo json features.
	 *
	 * @param projection    the projection
	 * @param geoJSONString the geo JSON string
	 * @return the feature[]
	 */
	public static Feature[] readGeoJsonFeatures(MAP_PROJECTION projection, String geoJSONString) {
		GeoJson geoJson = buildGeoJSON(projection);
		return geoJson.readFeatures(geoJSONString);
	}

}
