package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ShowPopupOnCentroidEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 12, 2023
 */
public interface ShowPopupOnCentroidEventHandler extends EventHandler {

	/**
	 * On show popup.
	 *
	 * @param showPopupOnCentroiEvent the show popup on centroi event
	 */
	void onShowPopup(ShowPopupOnCentroidEvent showPopupOnCentroiEvent);
}
