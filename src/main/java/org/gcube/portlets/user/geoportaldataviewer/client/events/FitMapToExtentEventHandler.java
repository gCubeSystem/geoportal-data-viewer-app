package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface FitMapToExtentEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 6, 2023
 */
public interface FitMapToExtentEventHandler extends EventHandler {

	/**
	 * On fit to map.
	 *
	 * @param fitMapToExtentEvent the fit map to extent event
	 */
	void onFitToMap(FitMapToExtentEvent fitMapToExtentEvent);
}
