package org.gcube.portlets.user.geoportaldataviewer.client.util;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs.WFSGetFeature;

import com.google.gwt.http.client.URL;

public class WFSMakerUtil {

	public static final String CQL_FILTER_PARAMETER = "CQL_FILTER";


	public static String buildWFSRequest(String webserviceURL, String layerName, int maxFeatures, String propertyName,
			String cqlFilter) throws Exception {

		if (layerName == null)
			throw new Exception("Bad parameter layerName is null");

		if (webserviceURL == null)
			throw new Exception("Bad parameter webserviceURL is null");

		StringBuilder builder = new StringBuilder();
		builder.append(webserviceURL);
		builder.append("?");
		builder.append(WFSGetFeature.SERVICE.getParameter() + "=" + WFSGetFeature.SERVICE.getValue());
		builder.append("&");
		builder.append(WFSGetFeature.VERSION.getParameter() + "=" + WFSGetFeature.VERSION.getValue());
		builder.append("&");
		builder.append(WFSGetFeature.REQUEST.getParameter() + "=" + WFSGetFeature.REQUEST.getValue());
		builder.append("&");
		builder.append(WFSGetFeature.OUTPUTFORMAT.getParameter() + "=" + URL.encode("application/json"));
		builder.append("&");
		builder.append(WFSGetFeature.TYPENAME.getParameter() + "=" + layerName);
		builder.append("&");

		if (maxFeatures > 0) {
			builder.append(WFSGetFeature.MAXFEATURES.getParameter() + "=" + maxFeatures);
			builder.append("&");
		}
		if (propertyName != null) {
			builder.append(WFSGetFeature.PROPERTYNAME.getParameter() + "=" + propertyName);
			builder.append("&");
		}
		if (cqlFilter != null) {
			builder.append(CQL_FILTER_PARAMETER + "=" + cqlFilter);
		}

		return builder.toString();
	}

}
