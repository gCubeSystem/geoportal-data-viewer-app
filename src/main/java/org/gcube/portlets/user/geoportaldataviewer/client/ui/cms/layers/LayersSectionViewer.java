package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.layers;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.BBOXDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.PLACE;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.MapView;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.DropdownButton;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

import ol.Coordinate;

public class LayersSectionViewer extends Composite {

	private static LayersSectionViewerUiBinder uiBinder = GWT.create(LayersSectionViewerUiBinder.class);

	interface LayersSectionViewerUiBinder extends UiBinder<Widget, LayersSectionViewer> {
	}

	@UiField
	HTMLPanel layerPanel;

	@UiField
	HTMLPanel mapViewPanel;

	private static enum IMAGE_EXPORT_AS {
		PNG, JPEG
	};

	int downloadAttempt = 0;

	private GCubeSDIViewerLayerDV layer;

	private SubDocumentView subDocumentView;

	public LayersSectionViewer(GCubeSDIViewerLayerDV layer, SubDocumentView subDocumentView) {
		initWidget(uiBinder.createAndBindUi(this));
		GWT.log("Showing: " + layer);
		this.layer = layer;
		this.subDocumentView = subDocumentView;

		String wmsLink = null;
		try {
			wmsLink = layer.getWMSLink();
		} catch (Exception e) {
			return;
		}

		if (wmsLink == null) {
			GeoportalDataViewerConstants.printJs("Wms link is null");
			return;
		}

		String layerName = URLUtil.extractValueOfParameterFromURL("layers", wmsLink);

		GWT.log("WMS LINK: " + wmsLink);
		GWT.log("layerName: " + layerName);

		DropdownButton exportMapButton = new DropdownButton("Export Map as...");
		exportMapButton.setType(ButtonType.LINK);
		exportMapButton.setIcon(IconType.DOWNLOAD_ALT);
		exportMapButton.setTitle("Export the map view (OSM + layer) as an image...");
		Style exportStyle = exportMapButton.getElement().getStyle();
		// exportStyle.setWidth(100, Unit.PCT);
		exportStyle.setMarginLeft(70, Unit.PCT);

		Location italyLocation = ExtentMapUtil.getLocation(PLACE.ITALY);
		Coordinate transformedCenterCoordinate = italyLocation.getCoordinate(MAP_PROJECTION.EPSG_3857);
		MapView mapView = new MapView(transformedCenterCoordinate,
				GeoportalDataViewerConstants.LIGHT_MAP_ITALY_FIT_ZOOM_ON, "70%", "300px");

		mapViewPanel.add(mapView);
		mapViewPanel.add(exportMapButton);

		String mapServerHost = wmsLink.contains("?") ? wmsLink.substring(0, wmsLink.indexOf("?")) : wmsLink;

		GWT.log("mapServerHost: " + mapServerHost);

		BBOXDV bbox = layer.getBbox();
		
		try {

			if (bbox != null) {

				if (!GWT.isProdMode()) {
					GWT.log("bbox: " + bbox);
//				GWT.log("bbox keys: " + bbox.keySet());
//				GWT.log("bbox values: " + bbox.values());
					GWT.log("minX: " + bbox.getMinX());
					GWT.log("minY: " + bbox.getMinY());
					GWT.log("maxX: " + bbox.getMaxX());
					GWT.log("maxY: " + bbox.getMaxY());
				}

				BoundsMap bounds = new BoundsMap(bbox.getMinX(), bbox.getMinY(), bbox.getMaxX(), bbox.getMaxY(), "");
				GWT.log("bounds: " + bounds);
				mapView.addWMSLayer(mapServerHost, layerName, bounds);
			} else {
				Alert alert = new Alert();
				alert.setType(AlertType.ERROR);
				alert.setText("No BBOX found for this layer");
				alert.setClose(false);
				layerPanel.add(alert);
			}

		} catch (Exception e) {
			Alert alert = new Alert();
			alert.setType(AlertType.ERROR);
			alert.setHTML("Error on adding the layer to Map. Please check its WMS link: <br/>"+layer.getWMSLink());
			alert.getElement().getStyle().setProperty("wordBreak", "break-all");
			alert.setClose(false);
			layerPanel.add(alert);
		}
		

		String htmlLinkId = mapView.getPanelMapElementId() + "-image-download";

		for (IMAGE_EXPORT_AS exportType : IMAGE_EXPORT_AS.values()) {
			NavLink navLink = new NavLink(exportType.name());
			String exportExt = exportType.name().toLowerCase();
			String filename = layerName + "." + exportExt;
			String mimeType = "image/" + exportExt;
			final HTML htmlLink = new HTML("<a id=\"" + htmlLinkId + "\" download=\"" + filename + "\"></a>");
			navLink.getElement().appendChild(htmlLink.getElement());
			navLink.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					downloadAttempt++;

					if (downloadAttempt > 3) {
						Window.alert("LOOP");
						return;
					}

					// DOWNLOAD the OSM + layer by canvas
					downloadMap(mapView.getLightOLM().getMap(), mapView.getPanelMapElementId(), htmlLinkId, filename,
							mimeType);
				}
			});

			exportMapButton.add(navLink);

		}
		
		if (subDocumentView.getMetadataAsJSON() != null) {
			String table = GeoportalDataViewerConstants.jsonToTableHTML(subDocumentView.getMetadataAsJSON());
			layerPanel.add(new HTML(table));
		}
	}

	public static native void exportPDF(String layerURL) /*-{
		var image = new Image(layerURL);

		var doc = new $wnd.jspdf.jsPDF();
		doc.addImage(layerURL, 'PNG', 0, 0, image.width, image.height);
		doc.save("map.pdf")
	}-*/;

	/**
	 * Prints the.
	 *
	 * @param msg the msg
	 */
	public static native void downloadMap(ol.Map map, String mapPanelId, String linkId, String filename,
			String mimeType)/*-{
		//console.log("map: " + map);

		map.once('rendercomplete', function() {
			var mapCanvas = $doc.createElement('canvas');
			var size = map.getSize();
			mapCanvas.width = size[0];
			mapCanvas.height = size[1];
			var mapContext = mapCanvas.getContext('2d');
			var mapContainer = $doc.querySelector('#' + mapPanelId);
			//console.log("mapContainer:" +JSON.stringify(mapContainer, null, 4));
			Array.prototype.forEach.call(mapContainer
					.querySelectorAll('.ol-layer canvas'), function(canvas) {
				if (canvas.width > 0) {
					var opacity = canvas.parentNode.style.opacity;
					mapContext.globalAlpha = opacity === '' ? 1
							: Number(opacity);
					var transform = canvas.style.transform;
					// Get the transform parameters from the style's transform matrix
					var matrix = transform.match(/^matrix\(([^\(]*)\)$/)[1]
							.split(',').map(Number);
					// Apply the transform to the export map context
					CanvasRenderingContext2D.prototype.setTransform.apply(
							mapContext, matrix);
					mapContext.drawImage(canvas, 0, 0);
				}
			});
			if (navigator.msSaveBlob) {
				// link download attribute does not work on MS browsers
				navigator.msSaveBlob(mapCanvas.msToBlob(), filename);
			} else {
				var link = $doc.getElementById(linkId);
				link.href = mapCanvas.toDataURL(mimeType);
				link.click();
			}
		});
		map.renderSync();
	}-*/;
}
