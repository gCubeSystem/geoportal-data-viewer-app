package org.gcube.portlets.user.geoportaldataviewer.client;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GroupedLayersDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.LayerIDV;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.shared.GeoportalSpatialQueryResult;
import org.gcube.portlets.user.geoportaldataviewer.shared.ItemFieldsResponse;
import org.gcube.portlets.user.geoportaldataviewer.shared.ResultSetPaginatedDataIDs;
import org.gcube.portlets.user.geoportaldataviewer.shared.ViewerConfiguration;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.GeoInformationForWMSRequest;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GeoportalDataViewerServiceAsync {
	/**
	 * Utility class to get the RPC Async interface from client-side code
	 */
	public static final class Util {
		private static GeoportalDataViewerServiceAsync instance;

		public static final GeoportalDataViewerServiceAsync getInstance() {
			if (instance == null) {
				instance = (GeoportalDataViewerServiceAsync) GWT.create(GeoportalDataViewerService.class);
			}
			return instance;
		}

		private Util() {
			// Utility class should not be instantiated
		}
	}

	void parseWmsRequest(String wmsRequest, String layerName, AsyncCallback<GeoInformationForWMSRequest> callback);

	void getDataResult(List<LayerObject> layerObjects, String mapSrsName, BoundsMap mapBBOX, int maxWFSFeature,
			double zoomLevel, AsyncCallback<List<GeoportalSpatialQueryResult>> callback);

	void getMyLogin(AsyncCallback<String> callback);

	void getGeoNaDataViewProfile(AsyncCallback<GNADataViewerConfigProfile> callback);

	void getPublicLinksFor(GeoportalItemReferences item, AsyncCallback<GeoportalItemReferences> asyncCallback);

	void getLayersForId(String theProfileID, String theProductID,
			AsyncCallback<List<GCubeSDIViewerLayerDV>> asyncCallback);

	void getWFSFeatures(List<LayerObject> layerObjects, String mapSrsName, BoundsMap selectBBOX, int maxWFSFeature,
			double zoomLevel, AsyncCallback<List<GeoportalSpatialQueryResult>> callback);

	void getListBaseLayers(AsyncCallback<List<BaseMapLayer>> callback);

	void getConfigListOfFieldsForSearching(AsyncCallback<List<ItemFieldsResponse>> callback);

	void getProjectViewForId(String profileID, String projectID, AsyncCallback<ProjectView> callback);

	void getListProjects(String theProfileID, Integer start, Integer limit, SearchingFilter filter,
			boolean reloadFromService, AsyncCallback<ResultSetPaginatedDataIDs> callback);

	void getInitialConfiguration(AsyncCallback<ViewerConfiguration> callback);

	void getEntrySetsDocumentForProjectID(String profileID, String projectID, int limit,
			AsyncCallback<LinkedHashMap<String, Object>> callback);

	void getRelationshipsForTimeline(String profileID, String projectID, AsyncCallback<List<String>> callback);

	void getSpatialReference(String profileID, String projectID, AsyncCallback<GeoJSON> callback);

	void getWFSRequest(LayerItem layerItem, String mapSrsName, BoundsMap mapBBOX, int maxFeatures, String outputFormat,
			AsyncCallback<String> callback);

	void getWFSResponse(LayerItem layerItem, String mapSrsName, BoundsMap mapBBOX, int maxFeatures, String outputFormat,
			AsyncCallback<String> callback);

	void getAvaiableCustomGroupedLayersForUCD(GEOPORTAL_DATA_HANDLER theHandler,
			AsyncCallback<Map<String, List<GroupedLayersDV<? extends LayerIDV>>>> callback);

	void getHTTPResponseForURL(String url, AsyncCallback<String> callback);

	void getCountFor(String profileID, String phase, String status, AsyncCallback<Integer> callback);

	void checkExportAsPDFConfig(AsyncCallback<Boolean> callback);

}
