package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.ProjectUtil;

import com.github.gwtbootstrap.client.ui.Column;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class RelationshipPanel extends Composite {

	private static ViewRelationshipPanelUiBinder uiBinder = GWT.create(ViewRelationshipPanelUiBinder.class);

	interface ViewRelationshipPanelUiBinder extends UiBinder<Widget, RelationshipPanel> {
	}
	
	@UiField
	HTMLPanel rootRow;
	
	@UiField
	Column leftChildColumn;
	
	@UiField
	Column rightChildColumn;

	private HandlerManager appManagerBus;


	public RelationshipPanel(HandlerManager appManagerBus, ProjectDV fromProject, List<ProjectDV> relationships) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;
		
		String html = ProjectUtil.toHMLCode(false, fromProject.getTheDocument(), fromProject.getId());
		rootRow.add(new HTML(html));
		
		showRelationsOf(fromProject);
	}

	public void showRelationsOf(ProjectDV project) {
	}

}
