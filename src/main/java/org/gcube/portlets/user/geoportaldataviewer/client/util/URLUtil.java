package org.gcube.portlets.user.geoportaldataviewer.client.util;

/**
 * The Class HTTPRequestUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 28, 2022
 */
public class URLUtil {

	/**
	 * Gets the value of parameter.
	 *
	 * @param paramName the param name
	 * @param url       the url
	 * @return the value of parameter
	 */
	public static String getValueOfParameter(String paramName, String url) {
//		LOG.trace("finding: "+wmsParam +" into "+url);
		int index = url.toLowerCase().indexOf(paramName.toLowerCase() + "="); // ADDING CHAR "=" IN TAIL TO BE SECURE IT
																				// IS A PARAMETER
//		LOG.trace("start index of "+wmsParam+ " is: "+index);
		String value = "";
		if (index > -1) {
			int start = index + paramName.length() + 1; // add +1 for char '='
			String sub = url.substring(start, url.length());
			int indexOfSeparator = sub.indexOf("&");
			int end = indexOfSeparator != -1 ? indexOfSeparator : sub.length();
			value = sub.substring(0, end);
		} else
			return null;

//		LOG.trace("return value: "+value);
		return value;
	}

	/**
	 * Gets the path URL.
	 *
	 * @param url the url
	 * @return the path URL
	 */
	public static String getPathURL(String url) {
		int index = url.toLowerCase().indexOf("?");
		if (index > -1) {
			return url.substring(0, index);
		} else
			return null;
	}

	/**
	 * Adds the parameter to query string.
	 *
	 * @param key             the key
	 * @param value           the value
	 * @param prefixAmpersand the prefix ampersand
	 * @param suffixAmpersand the suffix ampersand
	 * @return the string
	 */
	public static String addParameterToQueryString(String key, String value, boolean prefixAmpersand,
			boolean suffixAmpersand) {

		String queryParameter = "";

		if (prefixAmpersand)
			queryParameter += "&";

		queryParameter += key + "=" + value;

		if (suffixAmpersand)
			queryParameter += "&";

		return queryParameter;

	}

	/**
	 * Sets the value of parameter.
	 *
	 * @param wmsParam       the wms param
	 * @param wmsLink        the wms link
	 * @param newValue       the new value
	 * @param addIfNotExists the add if not exists
	 * @return the string
	 */
	public static String setValueOfParameter(String wmsParam, String wmsLink, String newValue, boolean addIfNotExists) {
		String toLowerWmsLink = wmsLink.toLowerCase();
		String toLowerWmsParam = wmsParam.toLowerCase();

		int index = toLowerWmsLink.indexOf(toLowerWmsParam + "="); // END WITH CHAR "=" TO BE SURE THAT IT IS A
																	// PARAMETER
		if (index > -1) {
			int indexStartValue = index + toLowerWmsParam.length() + 1; // add +1 for char '='
			int indexOfSeparator = toLowerWmsLink.indexOf("&", indexStartValue); // GET THE FIRST "&" STARTING FROM
																					// INDEX VALUE
//			LOG.trace("indexOfSeparator index of "+wmsParam+ " is: "+indexOfSeparator);
			int indexEndValue = indexOfSeparator != -1 ? indexOfSeparator : toLowerWmsLink.length();
//			LOG.trace("end: "+indexEndValue);
			return wmsLink.substring(0, indexStartValue) + newValue
					+ wmsLink.substring(indexEndValue, wmsLink.length());
		} else if (addIfNotExists) {
			wmsLink += "&" + wmsParam + "=" + newValue;
		}
//		LOG.trace("return value: "+value);
		return wmsLink;
	}

	/**
	 * Extract value of parameter from URL.
	 *
	 * @param paramName the param name
	 * @param url       the url
	 * @return the string
	 */
	public static String extractValueOfParameterFromURL(String paramName, String url) {
		int index = url.toLowerCase().indexOf(paramName.toLowerCase() + "="); // ADDING CHAR "=" IN TAIL TO BE SURE THAT
																				// IT
																				// IS A PARAMETER
		String value = "";
		if (index > -1) {

			int start = index + paramName.length() + 1; // add +1 for char '='
			String sub = url.substring(start, url.length());
			int indexOfSeparator = sub.indexOf("&");
			int end = indexOfSeparator != -1 ? indexOfSeparator : sub.length();
			value = sub.substring(0, end);
		} else
			return null;

		return value;
	}

}
