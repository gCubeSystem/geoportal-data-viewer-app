/*
 * 
 */
package org.gcube.portlets.user.geoportaldataviewer.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.IndexLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.LayerType;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MapEventType;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddedLayerToMapEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.QueryDataEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.QueryDataEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ShowDetailsEvent.EVENT_SOURCE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ZoomOutOverMinimumEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ZoomOutOverMinimumEventHandler;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.ExtentWrapped;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayerMap;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.ModalWindow;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.ProjectUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.util.StringUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.GeoportalSpatialQueryResult;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObjectType;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs.FeatureRow;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.GeoInformationForWMSRequest;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.ZAxis;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import ol.Coordinate;
import ol.Extent;
import ol.OLFactory;
import ol.format.GeoJson;
import ol.geom.Geometry;

/**
 * The Class LayerManager.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 27, 2020
 */
public class LayerManager {

	private static final String ZOOM_TO_LAYERS_LABEL = "Zoom...";
	private static final String ZOOM_TO_LAYERS_TITLE = "Zoom to Layers";

	/** The ol map. */
	private OpenLayerMap olMap;

	private Map<String, LayerObject> mapOtherLayerObjects = new HashMap<String, LayerObject>();
	private Map<String, LayerObject> mapIndexLayerObjects = new HashMap<String, LayerObject>();
	private HandlerManager layerManagerBus = new HandlerManager("");

	private HandlerManager applicationBus;

	private OverlayLayerManager overlayLayerManager;

	/**
	 * Instantiates a new layer manager.
	 *
	 * @param applicationBus the application bus
	 */
	public LayerManager(HandlerManager applicationBus) {
		this.applicationBus = applicationBus;
		overlayLayerManager = new OverlayLayerManager(applicationBus);
		bindEvents();
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		layerManagerBus.addHandler(QueryDataEvent.TYPE, new QueryDataEventHandler() {

			@Override
			public void onQueryInteraction(final QueryDataEvent queryEvent) {
				// GeoportalDataViewerConstants.print("fired QueryDataEvent:
				// "+queryEvent.toString());

				if (queryEvent.getGeoQuery() != null) {
					GeoQuery selectDataInfo = queryEvent.getGeoQuery();
					final String productID = queryEvent.getProductID();

					GWT.log("(" + selectDataInfo.getX1() + "," + selectDataInfo.getY1() + ")(" + selectDataInfo.getX2()
							+ "," + selectDataInfo.getY2() + ")");

					BoundsMap mapBBOX = new BoundsMap();
					double minX = queryEvent.getGeoQuery().getX1();
					double minY = queryEvent.getGeoQuery().getY1();
					Coordinate centerCoordinate = new Coordinate(minX, minY);
					centerCoordinate = olMap.transform(centerCoordinate, MAP_PROJECTION.EPSG_3857.getName(),
							MAP_PROJECTION.EPSG_4326.getName());
					mapBBOX.setLowerLeftX(centerCoordinate.getX());
					mapBBOX.setLowerLeftY(centerCoordinate.getY());

//					double maxX = olMap.getExtent().getUpperRightX();
//					double maxY = olMap.getExtent().getUpperRightY();
					double maxX = queryEvent.getGeoQuery().getX2();
					double maxY = queryEvent.getGeoQuery().getY2();
					centerCoordinate = new Coordinate(maxX, maxY);
					centerCoordinate = olMap.transform(centerCoordinate, MAP_PROJECTION.EPSG_3857.getName(),
							MAP_PROJECTION.EPSG_4326.getName());
					mapBBOX.setUpperRightX(centerCoordinate.getX());
					mapBBOX.setUpperRightY(centerCoordinate.getY());

					mapBBOX.setCrs(MAP_PROJECTION.EPSG_4326.getName());

					GWT.log("Bounds is: " + mapBBOX);
					GWT.log("Bounds to" + MAP_PROJECTION.EPSG_4326.getName() + " Long/Lat: " + mapBBOX.getLowerLeftY()
							+ "," + mapBBOX.getLowerLeftX() + "," + mapBBOX.getUpperRightY() + ","
							+ mapBBOX.getUpperRightX());
					GWT.log("MAX_WFS_FEATURES is: " + GeoportalDataViewerConstants.MAX_WFS_FEATURES);
					// GeoportalDataViewerConstants.print("calling getDataResult");

					List<LayerObject> listLO = new ArrayList<LayerObject>();
					// for querying base layers
					listLO.addAll(mapIndexLayerObjects.values());

					// for querying detail layers only in this case
					if (olMap.getCurrentResolution() < OLMapManager.LAYER_DETAIL_MAX_RESOLUTION) {
						Collection<LayerObject> collLO = mapOtherLayerObjects.values();
						listLO.addAll(collLO);

						ArrayList<LayerObject> layerVisibility = new ArrayList<LayerObject>(collLO.size());
						layerVisibility.addAll(collLO);
						// Managing layer visibility. If a layer is not visible has to be not queryable
						for (LayerObject layerObject : layerVisibility) {
							String layerName = layerObject.getLayerItem().getName();
							boolean isLayerVisible = olMap.isLayerVisible(layerName);
							if (!isLayerVisible) {
								GWT.log("From querying removing layername: " + layerName + ", it is not visible");
								listLO.remove(layerObject);
							}
						}
					}

					GeoportalDataViewerServiceAsync.Util.getInstance().getDataResult(listLO, olMap.getProjectionCode(),
							mapBBOX, GeoportalDataViewerConstants.MAX_WFS_FEATURES, olMap.getCurrentZoomLevel(),
							new AsyncCallback<List<GeoportalSpatialQueryResult>>() {

								@Override
								public void onFailure(Throwable caught) {
									// GeoportalDataViewerConstants.print("on Failure DataResult:
									// "+caught.getMessage());
								}

								@Override
								public void onSuccess(List<GeoportalSpatialQueryResult> listGeonaDataObjects) {

									if (listGeonaDataObjects == null)
										return;

									GWT.log("GeoNaDataObject's is/are " + listGeonaDataObjects.size());

									MapEventType sourceEvent = queryEvent.getSourceMapEventType();
									GWT.log("sourceEvent is: " + sourceEvent);
									if (sourceEvent.equals(MapEventType.ADDED_CENTROID_LAYER_TO_MAP)
											&& (listGeonaDataObjects.size() == 0)) {
										// here the centroid is not available
//										GeoportalDataViewer.showPopover(RootPanel.get(GeoportalDataViewer.APP_NOTIFIER),
//												"No centroid available for the selected project",
//												"Project without a centroid");
										olMap.setCenter(queryEvent.getQueryClickExtent().getCenter());
										return;
									}

									List<FeatureRow> theFeatures = null;

									// TODO SWTCH FOR EARCH ITEM TYPE
									for (final GeoportalSpatialQueryResult geoNaDataObject : listGeonaDataObjects) {
										GWT.log("GeoNaDataObject: " + geoNaDataObject);
										List<FeatureRow> features = geoNaDataObject.getFeatures();
										LayerItem layerItem = geoNaDataObject.getSourceLayerObject().getLayerItem();
										GWT.log("   features: " + features);
										GWT.log("   layerItem: " + layerItem);

										GWT.log("GeoNaDataObject Source layer item name: " + layerItem.getName());

										HashMap<String, List<FeatureRow>> listUniqueProducts = new HashMap<String, List<FeatureRow>>();
										// Clustering features for productId
										if (features != null && features.size() > 0) {
											String theProductId = null;
											for (FeatureRow fRow : features) {
												List<String> productIdLst = fRow.getMapProperties().get("projectid");
												if (productIdLst != null) {
													theProductId = productIdLst.get(0);
													if (theProductId != null) {
														List<FeatureRow> listFeatures = listUniqueProducts
																.get(theProductId);
														if (listFeatures == null) {
															listFeatures = new ArrayList<FeatureRow>();
														}

														listFeatures.add(fRow);
														listUniqueProducts.put(theProductId, listFeatures);
													}
												}
											}

										}

										GWT.log("listUniqueProducts keyset: " + listUniqueProducts.keySet());

										// USING ONLY THE FIRST FEATURE IN THE LIST
										if (features != null && features.size() > 0) {

											// I need to show exactly the feature with produc_id == recordId
											if (productID != null) {
												GWT.log("productID is not null: " + productID);
												// Searching mongoItemId in the list of product
												theFeatures = listUniqueProducts.get(productID);
												GWT.log("Loaded productid == product_id with id: " + productID
														+ ", the features are: " + theFeatures);

												// the recordId/mongoItemId to show has been passed but not found into
												// list of
												// FeatureRow
												if (theFeatures == null || theFeatures.isEmpty()) {
													// means that another version of the project has been passed without
													// features
													// stored into centroid layers.
													// so centering the map on the centroid passed into query and stored
													// into project metadata
													olMap.setCenter(queryEvent.getQueryClickExtent().getCenter());
													return;
												}
											}

											// If mongoItemId not passed
											if (productID == null) {
												GWT.log("productID is null");
												// Checking if the features returned belonging to detail layers (not
												// centroids).
												if (listUniqueProducts.isEmpty()) {
													// in this case the feature/s returned is/are a (detail) layer/s
													// belonging
													// to a record/concessione (not centroid layer),
													// so calling show popuup info on detail layers if the
													// following events are true.
													if (sourceEvent.equals(MapEventType.MOUSE_CLICK) && olMap
															.getCurrentZoomLevel() > OLMapManager.QUERY_MIN_ZOOM_LEVEL) {

														// Here I need to pass only the visible layers
														showPopupInfoForLayer(listGeonaDataObjects,
																queryEvent.getQueryClickExtent());
													}
													return;
												}
											}

											// The features of the first centroid layer selected
											if (theFeatures == null || theFeatures.size() == 0) {
												for (String theProductId : listUniqueProducts.keySet()) {

													theFeatures = listUniqueProducts.get(theProductId);

													if (theFeatures.size() > 0)
														break;

												}
											}

											// Displaying popup info for centroid layer passing the first feature
											if (sourceEvent.equals(MapEventType.MOUSE_CLICK)
													|| sourceEvent.equals(MapEventType.ADDED_CENTROID_LAYER_TO_MAP)) {
												GWT.log("Source event is: " + MapEventType.MOUSE_CLICK + ", or "
														+ MapEventType.ADDED_CENTROID_LAYER_TO_MAP);

												// Displaying popup info for centroid layer passing the first feature
												showPopupInfoForCentroidLayer(geoNaDataObject, theFeatures.get(0),
														queryEvent.getQueryClickExtent().getCenter());
											}

											if (olMap.getCurrentZoomLevel() > OLMapManager.QUERY_MIN_ZOOM_LEVEL) {
												GWT.log("Current zoom level is less than QUERY_MIN_ZOOM_LEVEL, Loading all layers of any product");
												for (final String theProductID : listUniqueProducts.keySet()) {
													GWT.log("Loading layers of theProductId: " + theProductID);
													// retrieving and showing WMS layers of a concessione if the ZOOM
													// level
													// is >
													// QUERY_MIN_ZOOM_LEVEL
													final String theProfileID = geoNaDataObject.getSourceLayerObject()
															.getProfileID();
													addLayerToMapForProject(theProfileID, theProductID,
															geoNaDataObject.getSourceLayerObject().getProjectDV(),
															false);
												}
											} else {
												// removing all WMS detail layers if the ZOOM level is <
												// QUERY_MIN_ZOOM_LEVEL
												removeAllDetailLayers();
											}
										}
									}

								}
							});
				}
			}
		});

		layerManagerBus.addHandler(AddedLayerToMapEvent.TYPE, new AddedLayerToMapEventHandler() {

			@Override
			public void onLayerRendered(AddedLayerToMapEvent addedLayerToMapEvent) {
				GWT.log("Fired into layerManagerBus " + addedLayerToMapEvent);
				applicationBus.fireEvent(addedLayerToMapEvent);
			}
		});

		layerManagerBus.addHandler(ZoomOutOverMinimumEvent.TYPE, new ZoomOutOverMinimumEventHandler() {

			@Override
			public void onZoomOut(ZoomOutOverMinimumEvent zoomOutEvent) {
				GWT.log("Fired into layerManagerBus " + zoomOutEvent);
				removeAllDetailLayers();
				applicationBus.fireEvent(zoomOutEvent);
			}
		});
	}

	/**
	 * Adds the layer to map for project.
	 *
	 * @param theProfileID            the the profile ID
	 * @param theProductID            the the product ID
	 * @param theProjectDV            the the project DV
	 * @param notifyLayerAlreadyAdded the notify layer already added
	 */
	public void addLayerToMapForProject(String theProfileID, String theProductID, ProjectDV theProjectDV,
			final boolean notifyLayerAlreadyAdded) {

		GeoportalDataViewerServiceAsync.Util.getInstance().getLayersForId(theProfileID, theProductID,
				new AsyncCallback<List<GCubeSDIViewerLayerDV>>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(List<GCubeSDIViewerLayerDV> result) {

						for (GCubeSDIViewerLayerDV layer : result) {
							GWT.log("Adding layer: " + layer.getLayerName());
							addLayer(LayerObjectType.PROJECT_LAYER, layer.getLayerName(), layer.getLayerName(),
									layer.getWMSLink(), false, false, null, true,
									OLMapManager.LAYER_DETAIL_MIN_RESOLUTION, OLMapManager.LAYER_DETAIL_MAX_RESOLUTION,
									theProfileID, theProductID, theProjectDV, notifyLayerAlreadyAdded);
						}

					}
				});

	}

	/**
	 * Removes the all detail layers.
	 */
	public void removeAllDetailLayers() {
		mapOtherLayerObjects.clear();
		olMap.removeAllDetailLayers();
		olMap.removeAllLayerFeaturesAsHighlight();
		overlayLayerManager.resetLayers();
	}

	/**
	 * Parses the geona references.
	 *
	 * @param layerObject the layer object
	 * @param fRow        the f row
	 * @return the show details event
	 */
	private ShowDetailsEvent parseGeonaReferences(LayerObject layerObject, FeatureRow fRow) {
		if (fRow == null)
			return null;

		List<String> productIds = fRow.getMapProperties().get("projectid");
		if (productIds != null && productIds.size() > 0) {
			String productId = productIds.get(0);
			LayerObjectType layerObjectType = layerObject.getType();
			return new ShowDetailsEvent(layerObjectType != null ? layerObjectType.name() : null,
					layerObject.getProfileID(), productId, fRow, true, EVENT_SOURCE.OPEN_DETAILS);
		}

		return null;
	}

	// ******************************** LAYERS Management

	/**
	 * Adds the layer.
	 *
	 * @param object the object
	 */
	public void addLayer(LayerObject object) {
		GWT.log("Layers management : Add layer " + object);

		switch (object.getType()) {
		case GENERIC_LAYER: {
			GWT.log("SUPPORT TO EXTERNAL LAYERS TO BE IMPLEMENTED");
			break;
		}

		case PROJECT_LAYER: {

			break;
		}

		case INDEX_LAYER: {
			break;
		}
		}
	}

	/**
	 * Adds the layer.
	 *
	 * @param lot                     the lot
	 * @param layerTitle              the layer title
	 * @param layerName               the layer name
	 * @param wmsLink                 the wms link
	 * @param isBase                  the is base
	 * @param displayInLayerSwitcher  the display in layer switcher
	 * @param UUID                    the uuid
	 * @param asDetailLayer           the as detail layer
	 * @param minResolution           the min resolution
	 * @param maxResolution           the max resolution
	 * @param profileID               the profile ID
	 * @param projectID               the project ID
	 * @param projectDV               the project DV
	 * @param notifyLayerAlreadyAdded the notify layer already added
	 */
	private void addLayer(final LayerObjectType lot, final String layerTitle, final String layerName,
			final String wmsLink, final boolean isBase, final boolean displayInLayerSwitcher, final String UUID,
			final boolean asDetailLayer, Double minResolution, Double maxResolution, final String profileID,
			final String projectID, final ProjectDV projectDV, final boolean notifyLayerAlreadyAdded) {
		GWT.log("addLayer called for profileID: " + profileID + ", projectID: " + projectID + ", projectDV: "
				+ projectDV);

		final LayerType featureType = isBase ? LayerType.RASTER_BASELAYER : LayerType.FEATURE_TYPE;

		if (wmsLink == null || wmsLink.isEmpty()) {
			GeoportalDataViewerConstants.printJs("Skipping add layer for wmsLink as null or empty");
			return;
		}

		GeoportalDataViewerServiceAsync.Util.getInstance().parseWmsRequest(wmsLink, layerName,
				new AsyncCallback<GeoInformationForWMSRequest>() {

					@Override
					public void onFailure(Throwable caught) {

						Window.alert(caught.getMessage());
					}

					@Override
					public void onSuccess(GeoInformationForWMSRequest geoInfoWMS) {

						GWT.log("Parsed WMS Request returned: " + geoInfoWMS);

						LayerItem layerItem = toLayerItem(featureType, layerTitle, geoInfoWMS.getLayerName(),
								geoInfoWMS.getWmsRequest(), geoInfoWMS.getServiceEndPoint(), true, isBase,
								displayInLayerSwitcher, (ArrayList<String>) geoInfoWMS.getStyles().getGeoStyles(),
								geoInfoWMS.getWmsRequest(), false, geoInfoWMS.getMapWMSNoStandard(),
								geoInfoWMS.isNcWMS(), UUID, geoInfoWMS.getZAxis(), minResolution, maxResolution);

						LayerObject lo = new LayerObject();
						lo.setLayerItem(layerItem);
						lo.setType(lot);
						lo.setProjectID(projectID);
						lo.setProfileID(profileID);
						lo.setProjectDV(projectDV);

						GWT.log("LayerObject ID is: " + lo.getProjectID());
						if (lo.getProjectDV() != null)
							GWT.log("LayerObject into ProjectDV is: " + lo.getProjectDV().getId());

						String layerNameKey = layerItem.getName();

						LayerObject theLo;
						switch (lo.getType()) {
						case INDEX_LAYER:
							theLo = mapIndexLayerObjects.get(layerNameKey);
							if (theLo == null) {
								theLo = lo;
								mapIndexLayerObjects.put(layerNameKey, theLo);
								GWT.log("INDEX_LAYER  mapIndexLayerObjects is: " + mapIndexLayerObjects);
								olMap.addWMSLayer(layerItem);
							} else {
								GWT.log("Skipping " + lo.getType() + " layer " + theLo.getLayerItem().getName()
										+ " already added to Map");
							}
							break;
						case PROJECT_LAYER:
							theLo = mapOtherLayerObjects.get(layerNameKey);
							if (theLo == null) {
								theLo = lo;
								mapOtherLayerObjects.put(layerNameKey, theLo);
								GWT.log("PROJECT_LAYER mapOtherLayerObjects is: " + mapOtherLayerObjects);
								olMap.addWMSDetailLayer(layerItem, GeoportalDataViewerConstants.INITIAL_LAYER_OPACITY);
								overlayLayerManager.addLayerItem(theLo);
							} else {
								GWT.log("Skipping " + lo.getType() + " layer " + theLo.getLayerItem().getName()
										+ " already added to Map");
								if (notifyLayerAlreadyAdded) {
									GeoportalDataViewer.showPopover(RootPanel.get(GeoportalDataViewer.APP_NOTIFIER),
											"Layers from the selected project are already displayed",
											"Layers displayed");
								}

							}
							break;
						default:
							break;
						}
					}
				});
	}

	/**
	 * Sets the CQL for layer to index layer.
	 *
	 * @param layerName the layer name
	 * @param cqlFilter the cql filter
	 */
	public void setCQLForLayerToIndexLayer(String layerName, String cqlFilter) {
		LayerObject theLo = mapIndexLayerObjects.get(layerName);
		if (theLo != null) {
			theLo.getLayerItem().setCqlFilter(cqlFilter);

		}
	}

	/**
	 * Adds the index layer.
	 *
	 * @param layer     the layer
	 * @param profileID the profile ID
	 */
	public void addIndexLayer(IndexLayerDV layer, String profileID) {
		GWT.log("Adding index layer: " + layer);
		String wmsLink = layer.getLayer().getOgcLinks().get("wms");
		GWT.log("index layer wmsLink: " + wmsLink);
		addLayer(LayerObjectType.INDEX_LAYER, null, null, wmsLink, false, false, null, false, null, null, profileID,
				null, null, false);

	}

	/**
	 * Removes the index layer.
	 *
	 * @param layer the layer
	 */
	public void removeIndexLayer(IndexLayerDV layer) {

		String wmsLink = layer.getLayer().getOgcLinks().get("wms");
		GWT.log("index layer wmsLink: " + wmsLink);

		String layerNameKey = URLUtil.getValueOfParameter("layers", wmsLink);
		olMap.removeWMSLayer(layerNameKey);
		mapIndexLayerObjects.remove(layerNameKey);
		GWT.log("New INDEX_LAYER  mapIndexLayerObjects is: " + mapIndexLayerObjects);

	}

	/**
	 * To layer item.
	 *
	 * @param layerType              the layer type
	 * @param layerTitle             the layer title
	 * @param layerName              the layer name
	 * @param layerURL               the layer URL
	 * @param mapServerHost          the map server host
	 * @param isExternal             the is external
	 * @param isBase                 the is base
	 * @param displayInLayerSwitcher the display in layer switcher
	 * @param styles                 the styles
	 * @param wmsLink                the wms link
	 * @param onTop                  the on top
	 * @param wmsNotStandardParams   the wms not standard params
	 * @param isNcWms                the is nc wms
	 * @param UUID                   the uuid
	 * @param zAxis                  the z axis
	 * @param minResolution          the min resolution
	 * @param maxResolution          the max resolution
	 * @return the layer item
	 */
	private LayerItem toLayerItem(LayerType layerType, String layerTitle, String layerName, String layerURL,
			String mapServerHost, boolean isExternal, boolean isBase, boolean displayInLayerSwitcher,
			ArrayList<String> styles, String wmsLink, boolean onTop, HashMap<String, String> wmsNotStandardParams,
			boolean isNcWms, String UUID, ZAxis zAxis, Double minResolution, Double maxResolution) {

//		GWT.log("Add addLayerByWms 1");
		LayerItem layerItem = new LayerItem();
		layerItem.setBaseLayer(isBase);
		layerItem.setTitle(layerTitle);
		layerItem.setName(layerName);
		layerItem.setUrl(layerURL);
		layerItem.setMapServerHost(mapServerHost);
		// l.setExternal(isExternal);
		layerItem.setOpacity(1d);
		layerItem.setBuffer(2);
		layerItem.setWmsLink(wmsLink);
		layerItem.setWmsNotStandardParams(wmsNotStandardParams);
		layerItem.setNcWms(isNcWms);
		layerItem.setUUID(UUID);
		layerItem.setZAxis(zAxis);
		layerItem.setMinResolution(minResolution);
		layerItem.setMaxResolution(maxResolution);

		switch (layerType) {

		// TODO IMPLEMENT THIS CASE
		case RASTER_BASELAYER:

//				l.setHasLegend(false);
			layerItem.setBaseLayer(true);
			layerItem.setTrasparent(false);
			layerItem.setClickData(false);
			break;

		case FEATURE_TYPE:

			// CASE FEATURE TYPE
			layerItem.setBaseLayer(false);
			layerItem.setClickData(true);
			layerItem.setTrasparent(true);
			break;

		}

		GWT.log("styles " + styles);

		if (styles != null && styles.size() > 0) {
			layerItem.setHasLegend(true);
			layerItem.setDefaultStyle(styles.get(0));
			layerItem.setStyle(styles.get(0));
			layerItem.setStyles(styles);
		} else {
			String style = URLUtil.getValueOfParameter("styles", wmsLink);
			if (style != null) { // CASE OF STYLE ="";
				// TENTATIVE TO GET LEGEND
				layerItem.setHasLegend(true);
			}
		}
		GWT.log("Built layer: " + layerItem);
		return layerItem;
	}

	/**
	 * Show popup info for layer.
	 *
	 * @param listGeoNaDataObject the list geo na data object
	 * @param queryClick          the query click
	 */
	public void showPopupInfoForLayer(List<GeoportalSpatialQueryResult> listGeoNaDataObject, ExtentWrapped queryClick) {
		GWT.log("showPopupInfoForLayer called for " + listGeoNaDataObject);

		ScrollPanel scrollPanel = new ScrollPanel();
		final FlowPanel flowPanel = new FlowPanel();
		flowPanel.getElement().getStyle().setProperty("maxHeight", "600px");
		scrollPanel.add(flowPanel);

		List<Button> listOpenProject = new ArrayList<Button>();

		final Button expandButton = new Button("Expand");
		expandButton.setType(ButtonType.LINK);
		expandButton.setIcon(IconType.EXPAND);
		expandButton.getElement().setId("expand-query");
		expandButton.setTitle("Show this view in new Window");

		flowPanel.add(expandButton);

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				Element buttonElement = DOM.getElementById("expand-query");
				Event.sinkEvents(buttonElement, Event.ONCLICK);
				Event.setEventListener(buttonElement, new EventListener() {

					@Override
					public void onBrowserEvent(Event event) {
						if (Event.ONCLICK == event.getTypeInt()) {
							expandButton.setVisible(false);
							// hiding the Open Project in expand mode (Modal Window)
							for (Button butt : listOpenProject) {
								butt.setVisible(false);
							}
							flowPanel.getElement().getStyle().setProperty("maxHeight", "100%");
							int width = Window.getClientWidth() * 75 / 100;
							int height = Window.getClientHeight() * 70 / 100;
							Coordinate center = queryClick.getCenter();
							double x = center.getX();
							double y = center.getY();
							// "with center x: "+x +", y: "+y;
							ModalWindow mw = new ModalWindow("Query results...", width, height);
							mw.add(flowPanel);
							mw.show();
						}
					}
				});
			}
		});

//		//Ordering for temporal dimension
//		try {
//			Collections.sort(listGeoNaDataObject, new TemporalComparatorUtil());
//		}catch (Exception e) {
//			// TODO: handle exception
//		}

		String prevProjectId = "";
		for (GeoportalSpatialQueryResult geoNaSpatialQueryResult : listGeoNaDataObject) {
			try {

				Set<String> indexLayers = mapIndexLayerObjects.keySet();
				LayerObject lo = geoNaSpatialQueryResult.getSourceLayerObject();
				LayerItem sourceLI = lo.getLayerItem();

				String layerSourceName = sourceLI.getName();

				// skipping centroid layer
				if (layerSourceName == null || indexLayers.contains(layerSourceName)
						|| lo.getType().equals(LayerObjectType.INDEX_LAYER)) {
					continue;
				}

				List<FeatureRow> features = geoNaSpatialQueryResult.getFeatures();

				if (features == null || features.isEmpty()) {
					continue;
				}

				String newProjectID = lo.getProjectID();
				if (prevProjectId.compareTo(newProjectID) != 0) {

					ProjectDV projectDV = lo.getProjectDV();
					String htmlMsg = ProjectUtil.toHMLCode(projectDV.getTheDocument());

					String projectIntro = htmlMsg.length() > 100 ? StringUtil.ellipsize(htmlMsg, 100) : htmlMsg;

					Label headingProfileName = new Label(lo.getProjectDV().getProfileName());
					headingProfileName.setType(LabelType.WARNING);
					headingProfileName.setTitle("Project ID: " + newProjectID);
					headingProfileName.getElement().getStyle().setMarginBottom(10, Unit.PX);
					FlowPanel headingPanel = new FlowPanel();
					headingPanel.add(headingProfileName);
					flowPanel.add(headingPanel);
					flowPanel.add(new HTML(projectIntro));

					TemporalReferenceDV tempRef = projectDV.getTemporalReference();
					if (tempRef != null) {
						if(tempRef.getStart()!=null || tempRef.getEnd()!=null)
							flowPanel.add(new HTML(ProjectUtil.toHTMLCode(tempRef)));
					}

					Button buttOpenProject = new Button("Open Project");
					final String buttId = "open-details-" + Random.nextInt();
					Element bEl = buttOpenProject.getElement();
					bEl.setId(buttId);
					bEl.getStyle().setPaddingLeft(0, Unit.PX);
					buttOpenProject.setType(ButtonType.LINK);

					if (buttOpenProject != null) {
						flowPanel.add(buttOpenProject);
						buttOpenProject.setType(ButtonType.LINK);
						Scheduler.get().scheduleDeferred(new ScheduledCommand() {

							@Override
							public void execute() {
								Element buttonElement = DOM.getElementById(buttId);
								Event.sinkEvents(buttonElement, Event.ONCLICK);
								Event.setEventListener(buttonElement, new EventListener() {

									@Override
									public void onBrowserEvent(Event event) {
										if (Event.ONCLICK == event.getTypeInt()) {
											applicationBus.fireEvent(
													new ShowDetailsEvent(lo.getType().name(), lo.getProfileID(),
															newProjectID, null, true, EVENT_SOURCE.OPEN_PROJECTS));

										}
									}
								});
							}
						});
					}
					listOpenProject.add(buttOpenProject);

					HTML subText = new HTML(
							"<p style=\"color:#999; font-size:14px; margin:5px 0 5px 0;\">Layers and Properties</p>");
					flowPanel.add(subText);
				}

				prevProjectId = newProjectID;

				Label layerLabel = new Label();
				layerLabel.setType(LabelType.INFO);

				String layerName = StringUtil.fullNameToLayerName(layerSourceName, ":");
				layerLabel.setText(layerName);
				layerLabel.setTitle(layerSourceName);
				layerLabel.getElement().getStyle().setMarginTop(10, Unit.PX);
				layerLabel.getElement().getStyle().setMarginBottom(5, Unit.PX);
				flowPanel.add(layerLabel);

				GWT.log("Displaying " + features.size() + " features");
				FlexTable intFlex = new FlexTable();
				intFlex.setCellPadding(1);
				intFlex.setCellSpacing(1);
				intFlex.getElement().addClassName("table-feature");
				intFlex.setHTML(0, 0, new HTML("Feature Id").toString());

				int i = 0;
				for (FeatureRow feature : features) {

					intFlex.setHTML(i + 1, 0, new HTML(feature.getId()).toString());

					// Showing properties belonging to concessioni layer
					Map<String, List<String>> entries = feature.getMapProperties();

					if (entries.size() == 0) {
						// Adding this row to show "no property" for feature
						// intFlex.setHTML(i + 1, 1, new HTML("<i>No property</i>").toString());
					}
					int j = 0;
					for (String key : entries.keySet()) {
						List<String> theValues = entries.get(key);
						String valueToDisplay = "";
						for (String value : theValues) {
							valueToDisplay += value + ", ";

						}
						valueToDisplay = valueToDisplay.substring(0, valueToDisplay.length() - 2);

						// adding the keys only of first feature row. They are equal for all features
						// (beloning to same layer).
						if (i == 0)
							intFlex.setHTML(0, j + 1, new HTML(key).toString());

						intFlex.setHTML(i + 1, j + 1, new HTML(valueToDisplay).toString());
						j++;

					}
					i++;
				}
				flowPanel.add(intFlex);
			} catch (Exception e) {
				GeoportalDataViewerConstants.printJs("Error: " + e.getMessage());
			}
		}

		olMap.showPopup(scrollPanel.toString(), queryClick.getCenter());

	}

	/**
	 * Show popup info for centroid layer.
	 *
	 * @param projectView the project view
	 */
	public void showPopupInfoForCentroidLayer(ProjectView projectView) {
		GWT.log("showPopupInfoForCentroidLayer for projectView: " + projectView);

		FlexTable flex = new FlexTable();
		flex.setCellPadding(1);
		flex.setCellSpacing(1);
		flex.getElement().addClassName("popup-table");
		VerticalPanel vpPanel = new VerticalPanel();
		vpPanel.add(flex);

		final ProjectDV projectDV = projectView.getTheProjectDV();

		FlexTable intFlex = new FlexTable();
		intFlex.setCellPadding(1);
		intFlex.setCellSpacing(1);
		if (projectDV.getTheDocument() != null) {
			LinkedHashMap<String, Object> map = projectDV.getTheDocument().getDocumentAsMap();
			// my-html-table
			// intFlex.getElement().addClassName("my-html-table");
			int i = 0;
			for (String key : map.keySet()) {
				try {
					Object theValue = map.get(key);
					String toStringValue = theValue + "";
					// intFlex.setHTML(i, 0, key);
					intFlex.setHTML(intFlex.getRowCount() + 1, 0, "<span title='" + toStringValue + "'>"
							+ StringUtil.ellipsize(toStringValue, 100) + "</span>");
					i++;
					// showing the first two fields returned into document
					if (i == 2)
						break;
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			flex.setHTML(flex.getRowCount() + 1, 0, intFlex.getElement().getString());
		}

		TemporalReferenceDV tempRef = projectDV.getTemporalReference();
		GWT.log("Temporal reference is: "+tempRef);
		if (tempRef != null) {
			
			if(tempRef.getStart()!=null || tempRef.getEnd()!=null)
				flex.setHTML(flex.getRowCount() + 1, 0, ProjectUtil.toHTMLCode(tempRef));
		}

		List<SectionView> selections = projectView.getListSections();

		boolean firstImageFound = false;
		for (SectionView sectionView : selections) {
			List<SubDocumentView> subDocuments = sectionView.getListSubDocuments();
			for (SubDocumentView subDoc : subDocuments) {
				List<FilesetDV> images = subDoc.getListImages();
				if (images != null && images.size() > 0) {
					for (FilesetDV fileset : images) {
						List<PayloadDV> payloads = fileset.getListPayload();
						if (payloads != null && payloads.size() > 0) {
							PayloadDV img = payloads.get(0);
							if (img != null && img.getLink() != null) {
								String theImgHTML = "<div id='pop-img'><img src=\"" + img.getLink() + "\"></img></div>";
								GWT.log("theImgHTML: " + theImgHTML);
								flex.setHTML(flex.getRowCount() + 1, 0, theImgHTML);
								firstImageFound = true;
								break;
							}
						}
					}
				}

				if (firstImageFound)
					break;

			}

			if (firstImageFound)
				break;
		}

		Button buttonOpenDtl = new Button("Open Details");
		buttonOpenDtl.getElement().setId("open-details");
		buttonOpenDtl.setType(ButtonType.LINK);

		buttonOpenDtl.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Open Details clicked");
				ShowDetailsEvent toEvent = new ShowDetailsEvent(LayerObjectType.INDEX_LAYER.name(),
						projectDV.getProfileID(), projectDV.getId(), null, true, EVENT_SOURCE.OPEN_DETAILS);
				applicationBus.fireEvent(toEvent);

			}
		});

		final Button buttonViewLayers = new Button(ZOOM_TO_LAYERS_LABEL);
		buttonViewLayers.getElement().setId("go-to-layers");
		buttonViewLayers.getElement().setTitle(ZOOM_TO_LAYERS_TITLE);
		buttonViewLayers.setType(ButtonType.LINK);

		if (olMap.getCurrentZoomLevel() >= OLMapManager.LAYER_DETAIL_MAX_RESOLUTION) {
			buttonViewLayers.setVisible(false);
		}

		buttonViewLayers.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("View Layers clicked");
				fireGoToLayers();
				buttonViewLayers.setVisible(false);
			}
		});

		FlowPanel flow = new FlowPanel();
		flow.add(buttonOpenDtl);
		flow.add(buttonViewLayers);
		vpPanel.add(flow);

		Coordinate positionTo = null;

		GeoJSON spatialReference = projectDV.getSpatialReference();
		if (spatialReference == null) {
			return;
		}
		try {
			ExtentWrapped ew = new ExtentWrapped(spatialReference.getBbox().getMinX(),
					spatialReference.getBbox().getMinY(), spatialReference.getBbox().getMaxX(),
					spatialReference.getBbox().getMaxY());

			positionTo = MapUtils.transformCoordiante(ew.getCenter(), MAP_PROJECTION.EPSG_4326.getName(),
					MAP_PROJECTION.EPSG_3857.getName());
		} catch (Exception e) {
			GWT.log("Error on calculating center of the extent " + e.getMessage());
		}

		// fallback
		if (positionTo == null)
			return;

		final Coordinate centerTo = positionTo;
		new Timer() {

			@Override
			public void run() {
				olMap.showPopup(vpPanel.toString(), centerTo);

				Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
						Element buttonElement = DOM.getElementById("open-details");
						Event.sinkEvents(buttonElement, Event.ONCLICK);
						Event.setEventListener(buttonElement, new EventListener() {

							@Override
							public void onBrowserEvent(Event event) {
								if (Event.ONCLICK == event.getTypeInt()) {

									ShowDetailsEvent toEvent = new ShowDetailsEvent(LayerObjectType.INDEX_LAYER.name(),
											projectDV.getProfileID(), projectDV.getId(), null, true,
											EVENT_SOURCE.OPEN_DETAILS);
									applicationBus.fireEvent(toEvent);
								}
							}
						});
					}

				});

				Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
						Element buttonElement = DOM.getElementById("go-to-layers");
						Event.sinkEvents(buttonElement, Event.ONCLICK);
						Event.setEventListener(buttonElement, new EventListener() {

							@Override
							public void onBrowserEvent(Event event) {
								if (Event.ONCLICK == event.getTypeInt()) {
									// buttonElement.setAttribute("visible", "none");
									// buttonElement.getStyle().setVisibility(Visibility.HIDDEN);
									fireGoToLayers();

								}
							}
						});
					}

				});

			}

		}.schedule(OpenLayerMap.SET_CENTER_ANIMATED_DURATION + 100);

		olMap.setCenter(centerTo);

	}

	/**
	 * Show popup info for centroid layer after clicking on map
	 *
	 * @param geoNaDataObject   the geo na data object
	 * @param feature           the feature
	 * @param onFailureCenterTo the on failure center to
	 */
	public void showPopupInfoForCentroidLayer(GeoportalSpatialQueryResult geoNaDataObject, FeatureRow feature,
			Coordinate onFailureCenterTo) {
		GWT.log("showPopupInfoForCentroidLayer showing feature: " + feature);
		GWT.log("showPopupInfoForCentroidLayer showing layerObject: " + geoNaDataObject.getSourceLayerObject());

		FlexTable flex = new FlexTable();
		flex.setCellPadding(1);
		flex.setCellSpacing(1);
		flex.getElement().addClassName("popup-table");
		VerticalPanel vpPanel = new VerticalPanel();
		vpPanel.add(flex);

		ProjectDV projectDV = geoNaDataObject.getSourceLayerObject().getProjectDV();

		if (projectDV != null) {

			FlexTable intFlex = new FlexTable();
			intFlex.setCellPadding(1);
			intFlex.setCellSpacing(1);
			intFlex.getElement().addClassName("thePopDataTable");
			if (projectDV.getTheDocument() != null) {
				LinkedHashMap<String, Object> map = projectDV.getTheDocument().getDocumentAsMap();
				// my-html-table
				// intFlex.getElement().addClassName("my-html-table");
				// int i = 0;
				for (String key : map.keySet()) {
					try {
						Object theValue = map.get(key);
						String toStringValue = theValue + "";
						// intFlex.setHTML(i, 0, key);
						intFlex.setHTML(intFlex.getRowCount() + 1, 0, "<span title='" + toStringValue + "'>"
								+ StringUtil.ellipsize(toStringValue, 100) + "</span>");
						// i++;
					} catch (Exception e) {
						// TODO: handle exception
					}
				}

				flex.setHTML(flex.getRowCount() + 1, 0, intFlex.getElement().getString());
			}

			TemporalReferenceDV tempRef = projectDV.getTemporalReference();
			if (tempRef != null) {
				if(tempRef.getStart()!=null || tempRef.getEnd()!=null)
					flex.setHTML(flex.getRowCount() + 1, 0, ProjectUtil.toHTMLCode(tempRef));
			}
		}

		if (geoNaDataObject.getMapImages() != null) {
			for (String key : geoNaDataObject.getMapImages().keySet()) {
				List<PayloadDV> listUI = geoNaDataObject.getMapImages().get(key);
				GWT.log("Adding images: " + listUI);
				if (listUI != null && listUI.size() > 0) {
					PayloadDV img = listUI.get(0);
					if (img != null && img.getLink() != null) {
						String theImgHTML = "<div id='pop-img'><img src=\"" + img.getLink() + "\"></img></div>";
						GWT.log("theImgHTML: " + theImgHTML);
						// GeoportalDataViewerConstants.print("The row are:
						// "+flex.getRowCount());
						flex.setHTML(flex.getRowCount() + 1, 0, theImgHTML);
					}
				}
			}
		}

		GWT.log("The selected Feature is: " + feature);
		FeatureRow theFeature = feature;
		Button buttonOpenDtl = new Button("Open Details");
		buttonOpenDtl.getElement().setId("open-details");
		buttonOpenDtl.setType(ButtonType.LINK);

		buttonOpenDtl.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Open Details clicked");
				fireOpenDetails(geoNaDataObject.getSourceLayerObject(), theFeature);

			}
		});

		final Button buttonViewLayers = new Button(ZOOM_TO_LAYERS_LABEL);
		buttonViewLayers.getElement().setId("go-to-layers");
		buttonViewLayers.getElement().setTitle(ZOOM_TO_LAYERS_TITLE);
		buttonViewLayers.setType(ButtonType.LINK);

		if (olMap.getCurrentZoomLevel() >= OLMapManager.LAYER_DETAIL_MAX_RESOLUTION) {
			buttonViewLayers.setVisible(false);
		}

		buttonViewLayers.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				GWT.log("View Layers clicked");
				fireGoToLayers();
				buttonViewLayers.setVisible(false);
			}
		});

		FlowPanel flow = new FlowPanel();
		flow.add(buttonOpenDtl);
		flow.add(buttonViewLayers);
		vpPanel.add(flow);

		Coordinate positionTo = null;

		if (feature == null) {
			positionTo = onFailureCenterTo;
		} else {

			GWT.log("geometry is: " + feature.getGeometry());
			if (feature.getGeometry() != null) {
				GWT.log("trasforming geometry: " + feature.getGeometry().getToJSONObject());

				Geometry geom = new GeoJson().readGeometry(feature.getGeometry().getToJSONObject(), null);

				// POINT
				if (geom.getType().equalsIgnoreCase("Point")) {
					GWT.log("geometry: is a point");
					String coordinateJSON = feature.getGeometry().getCoordinatesJSON();
					JSONArray centerJSON = (JSONArray) JSONParser.parseStrict(coordinateJSON);

					Coordinate center = new Coordinate(new Double(centerJSON.get(0).toString()),
							new Double(centerJSON.get(1).toString()));

					if (feature.getCrsName() != null
							&& feature.getCrsName().endsWith(MAP_PROJECTION.EPSG_4326.getId())) {

						center = MapUtils.transformCoordiante(center, MAP_PROJECTION.EPSG_4326.getName(),
								MAP_PROJECTION.EPSG_3857.getName());
					}
					positionTo = center;
				} else {

					Extent geomExtent = geom.getExtent();

					Coordinate lower = OLFactory.createCoordinate(geomExtent.getLowerLeftX(),
							geomExtent.getLowerLeftY());
					Coordinate upper = OLFactory.createCoordinate(geomExtent.getUpperRightX(),
							geomExtent.getUpperRightY());
					Coordinate lowerCoord = lower;
					Coordinate upperCoord = upper;
					if (feature.getCrsName() != null
							&& feature.getCrsName().endsWith(MAP_PROJECTION.EPSG_4326.getId())) {

						lowerCoord = MapUtils.transformCoordiante(lower, MAP_PROJECTION.EPSG_4326.getName(),
								MAP_PROJECTION.EPSG_3857.getName());
						upperCoord = MapUtils.transformCoordiante(upper, MAP_PROJECTION.EPSG_4326.getName(),
								MAP_PROJECTION.EPSG_3857.getName());
					}

					ExtentWrapped ew = new ExtentWrapped(lowerCoord.getX(), lowerCoord.getY(), upperCoord.getX(),
							upperCoord.getY());

					positionTo = new Coordinate(ew.getCenter().getX(), ew.getCenter().getY());

				}
				GWT.log("positionTo is: " + positionTo);
			}
		}

		// fallback
		if (positionTo == null)
			positionTo = onFailureCenterTo;

		final Coordinate centerTo = positionTo;
		new Timer() {

			@Override
			public void run() {
				olMap.showPopup(vpPanel.toString(), centerTo);

				Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
						Element buttonElement = DOM.getElementById("open-details");
						Event.sinkEvents(buttonElement, Event.ONCLICK);
						Event.setEventListener(buttonElement, new EventListener() {

							@Override
							public void onBrowserEvent(Event event) {
								if (Event.ONCLICK == event.getTypeInt()) {
									fireOpenDetails(geoNaDataObject.getSourceLayerObject(), theFeature);
								}
							}
						});
					}

				});

				Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
						Element buttonElement = DOM.getElementById("go-to-layers");
						Event.sinkEvents(buttonElement, Event.ONCLICK);
						Event.setEventListener(buttonElement, new EventListener() {

							@Override
							public void onBrowserEvent(Event event) {
								if (Event.ONCLICK == event.getTypeInt()) {
									// buttonElement.setAttribute("visible", "none");
									// buttonElement.getStyle().setVisibility(Visibility.HIDDEN);
									fireGoToLayers();

								}
							}
						});
					}

				});

			}

		}.schedule(OpenLayerMap.SET_CENTER_ANIMATED_DURATION + 100);

		olMap.setCenter(centerTo);

	}

	/**
	 * Fire go to layers.
	 */
	protected void fireGoToLayers() {
		int zoomValue = (int) (OLMapManager.LAYER_DETAIL_MAX_RESOLUTION + 0.1);
		olMap.setZoom(zoomValue);
		try {
			Element buttonElement = DOM.getElementById("go-to-layers");
			buttonElement.getStyle().setVisibility(Visibility.HIDDEN);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Fire open details.
	 *
	 * @param layerObject the layer object
	 * @param theFeature  the the feature
	 */
	protected void fireOpenDetails(LayerObject layerObject, FeatureRow theFeature) {
		ShowDetailsEvent toEvent = parseGeonaReferences(layerObject, theFeature);
		applicationBus.fireEvent(toEvent);
	}

	/**
	 * Sets the ol map.
	 *
	 * @param olMap the new ol map
	 */
	public void setOLMap(OpenLayerMap olMap) {
		this.olMap = olMap;
	}

	/**
	 * Gets the layer manager bus.
	 *
	 * @return the layer manager bus
	 */
	public HandlerManager getLayerManagerBus() {
		return layerManagerBus;
	}

	/**
	 * Gets the overlay layer manager.
	 *
	 * @return the overlay layer manager
	 */
	public OverlayLayerManager getOverlayLayerManager() {
		return overlayLayerManager;
	}

}
