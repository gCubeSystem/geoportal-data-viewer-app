//package org.gcube.portlets.user.geoportaldataviewer.client.events;
//
//import org.gcube.application.geoportalcommon.shared.SearchingFilter;
//import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.RECORD_TYPE;
//
//import com.google.gwt.event.shared.GwtEvent;
//
///**
// * The Class GetListOfRecordsEvent.
// *
// * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
// * 
// * Dec 7, 2021
// */
//public class GetListOfRecordsEvent extends GwtEvent<GetListOfRecordsEventHandler> {
//
//	/** The type. */
//	public static Type<GetListOfRecordsEventHandler> TYPE = new Type<GetListOfRecordsEventHandler>();
//	private RECORD_TYPE recordType;
//	private SearchingFilter sortFilter;
//
//	/**
//	 * Instantiates a new cancel upload event.
//	 *
//	 * @param recordType the record type
//	 * @param sortFilter the sort filter
//	 */
//	public GetListOfRecordsEvent(RECORD_TYPE recordType, SearchingFilter sortFilter) {
//		this.recordType = recordType;
//		this.sortFilter = sortFilter;
//	}
//
//	/**
//	 * Gets the associated type.
//	 *
//	 * @return the associated type
//	 */
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
//	 */
//	@Override
//	public Type<GetListOfRecordsEventHandler> getAssociatedType() {
//		return TYPE;
//	}
//
//	/**
//	 * Dispatch.
//	 *
//	 * @param handler the handler
//	 */
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see
//	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
//	 * EventHandler)
//	 */
//	@Override
//	protected void dispatch(GetListOfRecordsEventHandler handler) {
//		handler.onGetList(this);
//	}
//
//	/**
//	 * Gets the record type.
//	 *
//	 * @return the record type
//	 */
//	public RECORD_TYPE getRecordType() {
//		return recordType;
//	}
//
//	/**
//	 * Gets the sort filter.
//	 *
//	 * @return the sort filter
//	 */
//	public SearchingFilter getSortFilter() {
//		return sortFilter;
//	}
//
//}
