package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface AddedLayerToMapEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 18, 2020
 */
public interface AddedLayerToMapEventHandler extends EventHandler {
	/**
	 * On layer rendered.
	 *
	 * @param addedLayerToMapEvent the added layer to map event
	 */
	void onLayerRendered(AddedLayerToMapEvent addedLayerToMapEvent);
}
