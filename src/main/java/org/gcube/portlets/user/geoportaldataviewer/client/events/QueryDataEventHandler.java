package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface QueryDataEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 29, 2020
 */
public interface QueryDataEventHandler extends EventHandler {
	
	/**
	 * On query interaction.
	 *
	 * @param query the query
	 */
	void onQueryInteraction(QueryDataEvent query);
}
