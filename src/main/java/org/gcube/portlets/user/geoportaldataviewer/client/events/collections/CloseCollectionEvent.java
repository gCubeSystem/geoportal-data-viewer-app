package org.gcube.portlets.user.geoportaldataviewer.client.events.collections;


import com.google.gwt.event.shared.GwtEvent;

public class CloseCollectionEvent extends GwtEvent<CloseCollectionEventHandler> {
	
	
	private String collectionId = null;
	
    public static Type<CloseCollectionEventHandler> TYPE = new Type<CloseCollectionEventHandler>();

    @Override
    public Type<CloseCollectionEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CloseCollectionEventHandler handler) {
    	handler.onCloseCollection(this);
    }
    
    public CloseCollectionEvent(String collectionId) {
		this.collectionId=collectionId;
	}
    public String getCollectionId() {
		return collectionId;
	}
    
}
