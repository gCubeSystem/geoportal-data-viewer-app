package org.gcube.portlets.user.geoportaldataviewer.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;

public interface GeoportalImages extends ClientBundle {

	/** Get access to the css resource during gwt compilation */
	@Source("baseIcons.css")
	CssResource css();

	public static final GeoportalImages ICONS = GWT.create(GeoportalImages.class);

	/**
	 * Our sample image icon. Makes the image resource for the gwt-compiler's css
	 * composer accessible
	 */
	@Source("map1.png")
	ImageResource map();

	@Source("preset_location.png")
	ImageResource preset_location();

	@Source("layers.png")
	ImageResource layers();

	@Source("loading.gif")
	ImageResource loading();

	@Source("map-marker-icon.png")
	ImageResource mapMarkerIcon();

	@Source("icon_share.png")
	ImageResource shareIcon();

	@Source("italy.png")
	ImageResource italyIcon();

	@Source("world.png")
	ImageResource worldIcon();

	@Source("layer_visible.png")
	ImageResource layerVisible();

	@Source("layer_invisible.png")
	ImageResource layerInvisible();
	
	@Source("locate.png")
	ImageResource locate();

	@Source("locate_none.png")
	ImageResource locateNone();
	
	@Source("spinner_earth.gif")
	ImageResource spinnerEarth();
	
	@Source("spinner_clock.gif")
	ImageResource spinnerClock();
	
	@Source("geoportal_error.png")
	ImageResource geoportaServiceError();
	
	@Source("warning_error.png")
	ImageResource warningError();

}
