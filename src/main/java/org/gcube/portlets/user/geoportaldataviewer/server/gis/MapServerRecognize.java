/**
 * 
 */
package org.gcube.portlets.user.geoportaldataviewer.server.gis;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.google.gwt.core.shared.GWT;

/**
 * The Class MapServerRecognize.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Aug 29, 2014
 * 
 * THIS CLASS DISCRIMINATES BETWEEN GEOSERVER AND MAPSERVER
 * ACCORDING TO THE VALUE OF LAYER URL
 */
public class MapServerRecognize {

	/**
	 * The Enum SERVERTYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Oct 29, 2020
	 */
	public static enum SERVERTYPE{GEOSEVER, MAPSERVER};
	
	/**
	 * Recongnize.
	 *
	 * @param l the l
	 * @return the servertype
	 */
	public static SERVERTYPE recongnize(LayerItem l){
		
		if(l==null)
			return null;
		
		return recongnize(l.getUrl());
	}
	
	/**
	 * Recongnize.
	 *
	 * @param baseServerUrl the base server url
	 * @return the servertype
	 */
	public static SERVERTYPE recongnize(String baseServerUrl){
		
		if(baseServerUrl==null || baseServerUrl.isEmpty())
			return null;
		
		//CASE MAP SERVER
		if(baseServerUrl.contains(GisMakers.WXS)){
			GWT.log("wms url contains 'wxs' returning "+SERVERTYPE.MAPSERVER);
			return SERVERTYPE.MAPSERVER;
		}else{
			GWT.log("wms url doesn't contains 'wxs' returning "+SERVERTYPE.GEOSEVER);
		//CASE GEOSEVER
			return SERVERTYPE.GEOSEVER;
		}
	}
	
	/**
	 * Output format recognize.
	 *
	 * @param serverType the server type
	 * @param output the output
	 * @return the string
	 */
	public static String outputFormatRecognize(SERVERTYPE serverType, String output){
		
		if(output==null || output.isEmpty())
			return output;
		
		if(serverType==null)
			return output;
		
		switch (serverType) {
		
			case GEOSEVER:
				if(output.contains(GisMakers.JSON))
					return "json";
				else if(output.contains(GisMakers.CSV))
					return "csv";
			break;

			case MAPSERVER:
				
				if(output.contains(GisMakers.JSON))
					return "application/json;%20subtype=geojson";
				else if(output.contains(GisMakers.CSV))
					return "csv";
			break;
		}
		
		return output;
		
	}
}
