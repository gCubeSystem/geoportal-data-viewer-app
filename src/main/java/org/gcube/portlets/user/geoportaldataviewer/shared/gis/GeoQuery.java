package org.gcube.portlets.user.geoportaldataviewer.shared.gis;

import java.io.Serializable;


/**
 * The Class GeoQuery.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 29, 2020
 */
public class GeoQuery implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7500455699960112209L;

	/**
	 * The Enum TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Oct 29, 2020
	 */
	public static enum TYPE {
		POINT,
		BOX
	}
	
	/** The bbox. */
	private String bbox;

	/** The type. */
	private TYPE type=null;
	
	/** The x 1. */
	private double x1;
	
	/** The y 1. */
	private double y1;
	
	/** The x 2. */
	private double x2;
	
	/** The y 2. */
	private double y2; 
	
	/** The limit. */
	//ADDED BY FRANCESCO M.
	private int limit = Integer.MAX_VALUE;
	
	/**
	 * Instantiates a new click data info.
	 *
	 * @param x1 the x 1
	 * @param y1 the y 1
	 * @param x2 the x 2
	 * @param y2 the y 2
	 * @param theType the the type
	 */
	public GeoQuery(double x1, double y1, double x2, double y2, TYPE theType) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		
        // note: the bounding box is left,lower,right,upper
        this.bbox = y1 +","+ x1 +","+ y2 +","+ x2;
		this.type = theType;
		double area = (x2-x1) * (y2-y1);
//      System.out.println("BOX SELECTED: BBOX="+bbox + ", AREA="+area);
	}

	/**
	 * Gets the bbox.
	 *
	 * @return the bbox
	 */
	public String getBbox() {
		return bbox;
	}
	
	/**
	 * Sets the bbox.
	 *
	 * @param bbox the new bbox
	 */
	public void setBbox(String bbox) {
		this.bbox = bbox;
	}
	
	/**
	 * Checks if is point.
	 *
	 * @return true, if is point
	 */
	public boolean isPoint() {
		return this.type==TYPE.POINT;
	}

	/**
	 * Checks if is box.
	 *
	 * @return true, if is box
	 */
	public boolean isBox() {
		return this.type==TYPE.BOX;
	}
	
	
	/**
	 * Gets the x1.
	 *
	 * @return the x1
	 */
	public double getX1() {
		return x1;
	}
	
	/**
	 * Gets the x2.
	 *
	 * @return the x2
	 */
	public double getX2() {
		return x2;
	}
	
	/**
	 * Gets the y1.
	 *
	 * @return the y1
	 */
	public double getY1() {
		return y1;
	}
	
	/**
	 * Gets the y2.
	 *
	 * @return the y2
	 */
	public double getY2() {
		return y2;
	}

	/**
	 * Gets the limit.
	 *
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * Sets the limit.
	 *
	 * @param limit the new limit
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoQuery [bbox=");
		builder.append(bbox);
		builder.append(", type=");
		builder.append(type);
		builder.append(", x1=");
		builder.append(x1);
		builder.append(", y1=");
		builder.append(y1);
		builder.append(", x2=");
		builder.append(x2);
		builder.append(", y2=");
		builder.append(y2);
		builder.append(", limit=");
		builder.append(limit);
		builder.append("]");
		return builder.toString();
	}
	
	
}
