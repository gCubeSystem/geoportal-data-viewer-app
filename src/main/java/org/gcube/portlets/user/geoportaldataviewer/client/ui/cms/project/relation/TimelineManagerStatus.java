package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.RootPanel;

public class TimelineManagerStatus {

	private HandlerManager applicationBus;

	private TimelineStatus status;

	public TimelineManagerStatus(HandlerManager applicationBus) {
		this.applicationBus = applicationBus;
	}

	protected TimelineStatus getTimelineOf(ProjectDV theProject) {

		if (status == null) {
			status = new TimelineStatus(true, new TimelineRelationPanel(applicationBus, theProject), theProject);
			GWT.log("Displaying new Timeline of " + status);
			return status;

		} else {

			boolean c1 = status.getTheProjectDV().getId().compareTo(theProject.getId()) == 0;
			boolean c2 = status.getTheProjectDV().getProfileID().compareTo(theProject.getProfileID()) == 0;

			if (!c1 || !c2) {
				GWT.log("Displaying Timeline of " + status);
				status = new TimelineStatus(true, new TimelineRelationPanel(applicationBus, theProject), theProject);
				return status;
			}

			status.statusChanged(false);
			GWT.log("Skipping already displayed Timeline of " + status);
			return status;

		}

	}

	public void showTimelineProjectRelations(ProjectDV theProject, boolean showNotify) {
		GWT.log("Showing project relations for: " + theProject);

		TimelineStatus timelineStatus = getTimelineOf(theProject);
		if (timelineStatus.isStatusChanged()) {
			RootPanel.get(GeoportalDataViewer.DIV_TIMELINE_DATA).clear();
			RootPanel.get(GeoportalDataViewer.DIV_TIMELINE_DATA).add(timelineStatus.getTimelineRP());

		} else if (showNotify) {
			GeoportalDataViewer.showPopover(RootPanel.get(GeoportalDataViewer.APP_NOTIFIER),
					"Timeline from the selected project is already displayed", "Timeline displayed");
		}

		RootPanel.get(GeoportalDataViewer.DIV_TIMELINE_DATA).setVisible(true);
		timelineStatus.getTimelineRP().setTimelineContainerVisible(true);
	}
	
	
	public void hideTimelineProjectRelations() {
		GWT.log("hideTimelineProjectRelations");
		RootPanel.get(GeoportalDataViewer.DIV_TIMELINE_DATA).setVisible(false);
		// RootPanel.get(GeoportalDataViewer.DIV_TIMELINE_DATA).clear();
	}

	public class TimelineStatus {

		public boolean statusChanged = false;
		private TimelineRelationPanel timelineRP;
		private ProjectDV theProjectDV;

		public TimelineStatus(boolean statusChanged, TimelineRelationPanel timelineRP, ProjectDV theProjectDV) {
			this.statusChanged = statusChanged;
			this.timelineRP = timelineRP;
			this.theProjectDV = theProjectDV;
		}

		public void statusChanged(boolean bool) {
			this.statusChanged = bool;
		}

		public boolean isStatusChanged() {
			return statusChanged;
		}

		public TimelineRelationPanel getTimelineRP() {
			return timelineRP;
		}

		public ProjectDV getTheProjectDV() {
			return theProjectDV;
		}

	}

}
