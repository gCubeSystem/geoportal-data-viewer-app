package org.gcube.portlets.user.geoportaldataviewer.shared.gis;

import java.io.Serializable;

/**
 * The Class BaseMapLayer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 11, 2021
 */
public class BaseMapLayer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6204769725053516674L;

	/**
	 * The Enum OL_BASE_MAP.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 11, 2021
	 */
	public static enum OL_BASE_MAP {
		OSM, MAPBOX, OTHER
	};

	private String name;
	private String url;
	private String attribution;
	private OL_BASE_MAP type;

	/**
	 * Instantiates a new base map layer.
	 */
	public BaseMapLayer() {

	}

	public BaseMapLayer(String name, String url, String attribution, OL_BASE_MAP type) {
		super();
		this.name = name;
		this.url = url;
		this.attribution = attribution;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public String getAttribution() {
		return attribution;
	}

	public OL_BASE_MAP getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAttribution(String attribution) {
		this.attribution = attribution;
	}

	public void setType(OL_BASE_MAP type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BaseMapLayer [name=");
		builder.append(name);
		builder.append(", url=");
		builder.append(url);
		builder.append(", attribution=");
		builder.append(attribution);
		builder.append(", type=");
		builder.append(type);
		builder.append("]");
		return builder.toString();
	}

}
