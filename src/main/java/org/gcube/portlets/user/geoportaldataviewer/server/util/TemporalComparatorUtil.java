package org.gcube.portlets.user.geoportaldataviewer.server.util;

import java.util.Comparator;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataviewer.shared.GeoportalSpatialQueryResult;

/**
 * The Class TemporalComparatorUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 18, 2022
 */
public class TemporalComparatorUtil implements Comparator<GeoportalSpatialQueryResult> {

	/**
	 * Compare.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the int
	 */
	@Override
	public int compare(GeoportalSpatialQueryResult a, GeoportalSpatialQueryResult b) {
		if (a == null || a.getSourceLayerObject() == null || a.getSourceLayerObject().getProjectDV() == null) {
			return -1;
		}
		if (b == null || b.getSourceLayerObject() == null || b.getSourceLayerObject().getProjectDV() == null) {
			return 1;
		}

		ProjectDV project1 = a.getSourceLayerObject().getProjectDV();
		ProjectDV project2 = b.getSourceLayerObject().getProjectDV();

		if (project1.getTemporalReference() == null || project1.getTemporalReference().getStart() == null)
			return -1;

		if (project2.getTemporalReference() == null || project2.getTemporalReference().getStart() == null)
			return 1;

		return project1.getTemporalReference().getStart().compareTo(project2.getTemporalReference().getStart());

	}
}