package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsOverlay;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import ol.Coordinate;
import ol.Extent;

/**
 * The Class ExtentWrapped.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 11, 2020
 */
@JsType(isNative = true, name = "Array", namespace = JsPackage.GLOBAL)
public class ExtentWrapped extends ol.Extent {

	@JsOverlay
	private static final String PACKAGE_EXTENT = "ol.extent";

	/**
	 * Instantiates a new extent wrapped.
	 *
	 * @param minX the min X
	 * @param minY the min Y
	 * @param maxX the max X
	 * @param maxY the max Y
	 */
	public ExtentWrapped(double minX, double minY, double maxX, double maxY) {
		super(minX, minY, maxX, maxY);
	}

//	/**
//	 * @param coordinate coordinate to check.
//	 * @return true if the passed coordinate is contained or on the edge of the
//	 *         extent.
//	 */
//	@JsOverlay
//	public final Coordinate getCenter() {
//		return getCenter((ol.Extent) this);
//	}

	@JsMethod(name = "getCenter", namespace = PACKAGE_EXTENT)
	private static native Coordinate getCenter(Extent extent);

}
