package org.gcube.portlets.user.geoportaldataviewer.client;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MapEventType;
import org.gcube.portlets.user.geoportaldataviewer.client.events.QueryDataEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ZoomOutOverMinimumEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.ExtentWrapped;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayerMap;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.PLACE;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery.TYPE;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;

import ol.Coordinate;
import ol.MapBrowserEvent;
import ol.MapEvent;

/**
 * The Class OLMapManager.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 19, 2020
 */
public class OLMapManager {

	private String targetId;
	private HandlerManager layerManagerBus;
	private OpenLayerMap olMap;
	private ol.Extent dragStartExtent = null;
	private Double zoomStart = null;
	private Double zoomEnd = null;
	private ol.Extent dragEndExtent = null;
	public static final int QUERY_MIN_ZOOM_LEVEL = 13;
	public static final Double LAYER_DETAIL_MIN_RESOLUTION = 0.01;
	public static final Double LAYER_DETAIL_MAX_RESOLUTION = 18.5;

	/**
	 * Instantiates a new OL map manager.
	 *
	 * @param targetId        the target id
	 * @param layerManagerBus the layer manager bus
	 * @param baseMapLayer    the base map layer
	 */
	public OLMapManager(String targetId, HandlerManager layerManagerBus, BaseMapLayer baseMapLayer) {
		this.targetId = targetId;
		this.layerManagerBus = layerManagerBus;
		instanceOLMap(baseMapLayer);
	}

	/**
	 * Instance OL map.
	 *
	 * @param baseMapLayer the base map layer
	 */
	public void instanceOLMap(BaseMapLayer baseMapLayer) {

		olMap = new OpenLayerMap(targetId, layerManagerBus, baseMapLayer) {

			@Override
			public void clickListener(MapBrowserEvent event) {
				Coordinate coordinate = event.getCoordinate();
				if (!olMap.mapInstancied())
					return;

//				if (olMap.isQueryPointActive()) {
//					GeoQuery select = toDataPointQuery(coordinate);
//					layerManagerBus.fireEvent(new QueryDataEvent(select, coordinate));
//
//				}

				ExtentWrapped toExt = new ExtentWrapped(coordinate.getX(), coordinate.getY(), coordinate.getX(),
						coordinate.getY());
				GeoQuery select = toDataPointQuery(coordinate, true);
				layerManagerBus.fireEvent(new QueryDataEvent(select, toExt, null, true, MapEventType.MOUSE_CLICK));

			}

			@Override
			public void moveEndListener(MapEvent event) {
				// onInit
				if (!olMap.mapInstancied())
					return;

				checkSelectQuery(MapEventType.MOVE_END);

			}

			@Override
			public void moveStartListener(MapEvent event) {

				// onInit
				if (!olMap.mapInstancied())
					return;

				setDragStart();

			}

			@Override
			public void mapZoomListener(MapEvent event) {

				// onInit
				if (!olMap.mapInstancied())
					return;

				setDragStart();
				zoomStart = olMap.getCurrentZoomLevel();
				GWT.log("zoomStart: " + zoomStart);

			}

			@Override
			public void mapZoomEndListener(MapEvent event) {
				// onInit
				if (!olMap.mapInstancied())
					return;

				setDragStart();
				zoomEnd = olMap.getCurrentZoomLevel();
				GWT.log("zoomEnd: " + zoomEnd);
				checkSelectQuery(MapEventType.MAP_ZOOM_END);

			}

		};

		Location italyLocation = ExtentMapUtil.getLocation(PLACE.ITALY);
		Coordinate transformedCenterCoordinate = italyLocation.getCoordinate(MAP_PROJECTION.EPSG_3857);
		olMap.setCenter(transformedCenterCoordinate);
		olMap.setZoom(italyLocation.getZoomLevel());

	}

	/**
	 * To data point query.
	 *
	 * @param coordinate  the coordinate
	 * @param manualClick the manual click
	 * @return the geo query
	 */
	public GeoQuery toDataPointQuery(Coordinate coordinate, boolean manualClick) {

		double lon = coordinate.getX();
		double lat = coordinate.getY();
		GWT.log("To quey DP: " + coordinate);

		int w = (int) olMap.getSize().getWidth();
		int h = (int) olMap.getSize().getHeight();
		// handler.clickOnMap(x, y, w, h);

		// ratio - mapPixelWeight : bboxWeight = 20px : geoRectangleWidth
		// where 10px is the pixel diameter dimension of the clicked point
		double bboxWidth = Math.abs(olMap.getExtent().getLowerLeftX() - olMap.getExtent().getUpperRightX());

		double geoWidth = 0;
		// adding a tolerance in case of manual click
		if (manualClick) {
			// adding a tolerance for clicking
			// geoWidth = (bboxWidth / w) * (14 / 2);
			geoWidth = (bboxWidth / w) * (16 / 2);
		} else {
			// data point selection for coordinate loaded from the centroid
			geoWidth = (bboxWidth / w) / 60;
			GWT.log("geo width: "+geoWidth);
		}

		double x1 = Math.min(lon + geoWidth, lon - geoWidth);
		double x2 = Math.max(lon + geoWidth, lon - geoWidth);
		double y1 = Math.min(lat + geoWidth, lat - geoWidth);
		double y2 = Math.max(lat + geoWidth, lat - geoWidth);
		//GWT.log("("+x1+","+y1+")("+x2+","+y2+")");

		GeoQuery select = new GeoQuery(x1, y1, x2, y2, TYPE.POINT);
		return select;
	}

	/**
	 * Gets the OL map.
	 *
	 * @return the OL map
	 */
	public OpenLayerMap getOLMap() {
		return olMap;
	}

	/**
	 * Check select query.
	 *
	 * @param mapEventType the map event type
	 */
	private void checkSelectQuery(MapEventType mapEventType) {

		GWT.log("Zoom is:" + olMap.getCurrentZoomLevel());
		GWT.log("Resolution is:" + olMap.getCurrentResolution());

		if (dragStartExtent != null && olMap.getCurrentZoomLevel() > QUERY_MIN_ZOOM_LEVEL) {
			dragEndExtent = olMap.getExtent();
//			GWT.log("Drag end is: "+dragEndExtent);
			ExtentWrapped startExt = new ExtentWrapped(dragStartExtent.getLowerLeftX(), dragStartExtent.getLowerLeftY(),
					dragStartExtent.getUpperRightX(), dragStartExtent.getUpperRightY());
			ExtentWrapped endExt = new ExtentWrapped(dragEndExtent.getLowerLeftX(), dragEndExtent.getLowerLeftY(),
					dragEndExtent.getUpperRightX(), dragEndExtent.getUpperRightY());
//			GWT.log("start Ext: "+startExt);
//			GWT.log("endExt Ext: "+endExt);
			long dist = MapUtils.distanceBetweenCentroid(startExt, endExt);
			GWT.log("the distance is: " + dist);
			if (dist > 5000 || startExt.containsExtent(endExt)) {
				GeoQuery select = toDataBoxQuery(dragEndExtent);
				// TODO THE AUTOMATICALLY SHOWING POP-UP ACCORDING TO ZOOM IS BUGGY
				layerManagerBus.fireEvent(new QueryDataEvent(select, endExt, null, false, mapEventType));
			}
		} else if (zoomStart != null && zoomEnd != null) {

			if (zoomEnd < QUERY_MIN_ZOOM_LEVEL) {
				layerManagerBus.fireEvent(new ZoomOutOverMinimumEvent(zoomStart, zoomEnd, QUERY_MIN_ZOOM_LEVEL));
			}
		}

	}

	/**
	 * Sets the drag start.
	 */
	private void setDragStart() {
		if (olMap.getCurrentZoomLevel() > QUERY_MIN_ZOOM_LEVEL) {
			dragStartExtent = olMap.getExtent();
			GWT.log("Drag Start is: " + dragStartExtent);
		}
	}

	/**
	 * To data point query.
	 *
	 * @param extent the extent
	 * @return the geo query
	 */
	private GeoQuery toDataBoxQuery(ol.Extent extent) {

		return new GeoQuery(extent.getLowerLeftX(), extent.getLowerLeftY(), extent.getUpperRightX(),
				extent.getUpperRightY(), TYPE.BOX);
	}

	/**
	 * Checks if is query point active.
	 *
	 * @return true, if is query point active
	 */
	public boolean isQueryPointActive() {
		return olMap.isQueryPointActive();
	}

	/**
	 * Hide pop info.
	 */
	public void hidePopInfo() {
		olMap.hidePopup();
	}

}
