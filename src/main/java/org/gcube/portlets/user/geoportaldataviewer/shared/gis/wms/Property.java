package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms;

import java.io.Serializable;


// TODO: Auto-generated Javadoc
/**
 * The Class Property.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 27, 2020
 */
public class Property implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8289583721513187916L;

	/** The name. */
	private String name;
	
	/** The type. */
	private String type;	
	
	/** The nillable. */
	private boolean nillable=true;
	
	/** The max occours. */
	private int maxOccours=1;
	
	/** The min occours. */
	private int minOccours=0;
	
	/**
	 * Instantiates a new property.
	 */
	public Property() {
		super();
	}
	
	/**
	 * Instantiates a new property.
	 *
	 * @param name the name
	 * @param type the type
	 * @param nillable the nillable
	 * @param maxOccours the max occours
	 * @param minOccours the min occours
	 */
	public Property(String name, String type, boolean nillable,
			int maxOccours, int minOccours) {
		super();
		
		this.name = name;
		this.type = type;
		this.nillable = nillable;
		this.maxOccours = maxOccours;
		this.minOccours = minOccours;
	}

	/**
	 * Instantiates a new property.
	 *
	 * @param name the name
	 * @param type the type
	 */
	public Property(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Checks if is nillable.
	 *
	 * @return true, if is nillable
	 */
	public boolean isNillable() {
		return nillable;
	}

	/**
	 * Sets the nillable.
	 *
	 * @param nillable the new nillable
	 */
	public void setNillable(boolean nillable) {
		this.nillable = nillable;
	}

	/**
	 * Gets the max occours.
	 *
	 * @return the max occours
	 */
	public int getMaxOccours() {
		return maxOccours;
	}

	/**
	 * Sets the max occours.
	 *
	 * @param maxOccours the new max occours
	 */
	public void setMaxOccours(int maxOccours) {
		this.maxOccours = maxOccours;
	}

	/**
	 * Gets the min occours.
	 *
	 * @return the min occours
	 */
	public int getMinOccours() {
		return minOccours;
	}

	/**
	 * Sets the min occours.
	 *
	 * @param minOccours the new min occours
	 */
	public void setMinOccours(int minOccours) {
		this.minOccours = minOccours;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Property [name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", nillable=");
		builder.append(nillable);
		builder.append(", maxOccours=");
		builder.append(maxOccours);
		builder.append(", minOccours=");
		builder.append(minOccours);
		builder.append("]");
		return builder.toString();
	}
	
	
}
