package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class OverlayCustomLayerToMapEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 15, 2023
 */
public class OverlayCustomLayerToMapEvent extends GwtEvent<OverlayCustomLayerToMapEventHandler> {
	public static Type<OverlayCustomLayerToMapEventHandler> TYPE = new Type<OverlayCustomLayerToMapEventHandler>();
	private LayerItem layerItem;
	private ACTION_TYPE actionType;
	private boolean enabled;

	/**
	 * The Enum ACTION_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         May 15, 2023
	 */
	public enum ACTION_TYPE {
		VISIBILITY
	}

	/**
	 * Instantiates a new overlay custom layer to map event.
	 *
	 * @param layerItem  the layer item
	 * @param actionType the action type
	 * @param enabled    the enabled
	 */
	public OverlayCustomLayerToMapEvent(LayerItem layerItem, ACTION_TYPE actionType, boolean enabled) {
		this.layerItem = layerItem;
		this.actionType = actionType;
		this.enabled = enabled;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<OverlayCustomLayerToMapEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(OverlayCustomLayerToMapEventHandler handler) {
		handler.onCustomOverlayLayerAction(this);

	}

	/**
	 * Gets the layer item.
	 *
	 * @return the layer item
	 */
	public LayerItem getLayerItem() {
		return layerItem;
	}

	/**
	 * Gets the action type.
	 *
	 * @return the action type
	 */
	public ACTION_TYPE getActionType() {
		return actionType;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

}