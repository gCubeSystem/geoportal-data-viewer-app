package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs.FeatureRow;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ShowDetailsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 14, 2022
 */
public class ShowDetailsEvent extends GwtEvent<ShowDetailsEventHandler> {
	public static Type<ShowDetailsEventHandler> TYPE = new Type<ShowDetailsEventHandler>();
	private String layerObjectType;
	private FeatureRow featureRow;
	private String itemName;
	private String profileID;
	private String projectID;
	private boolean loadTimelineRelationships;
	private boolean isEventFromTimeline;
	private EVENT_SOURCE sourceEvent;

	/**
	 * The Enum EVENT_SOURCE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Dec 22, 2022
	 */
	public static enum EVENT_SOURCE {
		LOCATE_ON_INIT, OPEN_DETAILS, OPEN_PROJECTS, LOCATE_SEARCHED, LOCATE_FROM_TIMELINE, GO_TO_LAYERS
	}

	/**
	 * Instantiates a new show details event.
	 *
	 * @param layerObjectType           the layer object type
	 * @param profileID                 the profile ID
	 * @param projectID                 the project ID
	 * @param featureRow                the feature row
	 * @param loadTimelineRelationships the load timeline relationships
	 * @param sourceEvent               the source event
	 */
	public ShowDetailsEvent(String layerObjectType, String profileID, String projectID, FeatureRow featureRow,
			boolean loadTimelineRelationships, EVENT_SOURCE sourceEvent) {
		this.layerObjectType = layerObjectType;
		this.profileID = profileID;
		this.projectID = projectID;
		this.featureRow = featureRow;
		this.loadTimelineRelationships = loadTimelineRelationships;
		this.sourceEvent = sourceEvent;
	}

	/**
	 * Instantiates a new show details event.
	 *
	 * @param gir                       the gir
	 * @param featureRow                the feature row
	 * @param loadTimelineRelationships the load timeline relationships
	 * @param sourceEvent               the source event
	 */
	public ShowDetailsEvent(GeoportalItemReferences gir, FeatureRow featureRow, boolean loadTimelineRelationships,
			boolean goToLayers, EVENT_SOURCE sourceEvent) {
		this.layerObjectType = gir.getLayerObjectType();
		this.profileID = gir.getProfileID();
		this.projectID = gir.getProjectID();
		this.featureRow = featureRow;
		this.loadTimelineRelationships = loadTimelineRelationships;
		this.sourceEvent = sourceEvent;
	}

	/**
	 * Event from timeline.
	 *
	 * @param bool the bool
	 */
	public void setEventFromTimeline(boolean bool) {
		this.isEventFromTimeline = bool;
	}

	/**
	 * Checks if is event from timeline.
	 *
	 * @return true, if is event from timeline
	 */
	public boolean isEventFromTimeline() {
		return isEventFromTimeline;
	}

	/**
	 * Gets the source event.
	 *
	 * @return the source event
	 */
	public EVENT_SOURCE getSourceEvent() {
		return sourceEvent;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public static Type<ShowDetailsEventHandler> getTYPE() {
		return TYPE;
	}

	/**
	 * Gets the layer object type.
	 *
	 * @return the layer object type
	 */
	public String getLayerObjectType() {
		return layerObjectType;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the project ID.
	 *
	 * @return the project ID
	 */
	public String getProjectID() {
		return projectID;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ShowDetailsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ShowDetailsEventHandler handler) {
		handler.onShowDetails(this);

	}

	/**
	 * Gets the feature row.
	 *
	 * @return the feature row
	 */
	public FeatureRow getFeatureRow() {
		return featureRow;
	}

	/**
	 * Checks if is load timeline relationships.
	 *
	 * @return true, if is load timeline relationships
	 */
	public boolean isLoadTimelineRelationships() {
		return loadTimelineRelationships;
	}

	/**
	 * Gets the geona item ref.
	 *
	 * @return the geona item ref
	 */
	public GeoportalItemReferences getGeonaItemRef() {
		return new GeoportalItemReferences(projectID, profileID, layerObjectType);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ShowDetailsEvent [layerObjectType=");
		builder.append(layerObjectType);
		builder.append(", featureRow=");
		builder.append(featureRow);
		builder.append(", itemName=");
		builder.append(itemName);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append(", loadTimelineRelationships=");
		builder.append(loadTimelineRelationships);
		builder.append(", isEventFromTimeline=");
		builder.append(isEventFromTimeline);
		builder.append(", sourceEvent=");
		builder.append(sourceEvent);
		builder.append("]");
		return builder.toString();
	}

}