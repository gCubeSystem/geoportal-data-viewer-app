package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ChangeMapLayerEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 12, 2021
 */
public interface ChangeMapLayerEventHandler extends EventHandler {

	/**
	 * On change base map layer.
	 *
	 * @param changeMapLayerEvent the change map layer event
	 */
	void onChangeBaseMapLayer(ChangeMapLayerEvent changeMapLayerEvent);
}
