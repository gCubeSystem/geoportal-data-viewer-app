package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.client.ui.dandd.DragDropLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class DoActionOnDetailLayersEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 12, 2021
 */
public class DoActionOnDetailLayersEvent extends GwtEvent<DoActionOnDetailLayersEventHandler> {
	public static Type<DoActionOnDetailLayersEventHandler> TYPE = new Type<DoActionOnDetailLayersEventHandler>();

	/**
	 * The Enum DO_LAYER_ACTION.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Oct 12, 2021
	 */
	public static enum DO_LAYER_ACTION {
		VISIBILITY, SWAP, OPACITY, LOCATE_LAYER
	}

	private DO_LAYER_ACTION doAction;
	private LayerItem layerItem;
	private double opacity;
	private boolean visibility;
	private SwapLayer sourceLayerSwap;
	private SwapLayer targetLayerSwap;
	private LayerObject layerObject;
	private DragDropLayer sourceEventUI;

	/**
	 * The Class SwapLayer.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Oct 15, 2021
	 */
	public static class SwapLayer {
		private LayerItem layerItem;
		private int position;

		/**
		 * Instantiates a new swap layer.
		 *
		 * @param layerItem the layer item
		 * @param position  the position
		 */
		public SwapLayer(LayerItem layerItem, int position) {
			this.layerItem = layerItem;
			this.position = position;
		}

		/**
		 * Gets the layer item.
		 *
		 * @return the layer item
		 */
		public LayerItem getLayerItem() {
			return layerItem;
		}

		/**
		 * Gets the position.
		 *
		 * @return the position
		 */
		public int getPosition() {
			return position;
		}
	}

	/**
	 * Instantiates a new do action on detail layers event.
	 *
	 * @param action      the action
	 * @param layerItem   the layer item
	 * @param layerObject the layer object
	 * @param layerObject the layer object
	 */
	public DoActionOnDetailLayersEvent(DO_LAYER_ACTION action, LayerItem layerItem, LayerObject layerObject) {
		this.layerItem = layerItem;
		this.layerObject = layerObject;
		this.doAction = action;
	}

	/**
	 * Sets the swap layers.
	 *
	 * @param sourceLayerItem the source layer item
	 * @param targetLayerItem the target layer item
	 */
	public void setSwapLayers(SwapLayer sourceLayerItem, SwapLayer targetLayerItem) {
		this.sourceLayerSwap = sourceLayerItem;
		this.targetLayerSwap = targetLayerItem;

	}

	/**
	 * Sets the opacity.
	 *
	 * @param opacity the new opacity
	 */
	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

	/**
	 * Sets the visibility.
	 *
	 * @param visibility the new visibility
	 */
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<DoActionOnDetailLayersEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(DoActionOnDetailLayersEventHandler handler) {
		handler.onDoActionOnDetailLayers(this);

	}

	/**
	 * Gets the layer object.
	 *
	 * @return the layer object
	 */
	public LayerObject getLayerObject() {
		return layerObject;
	}

	/**
	 * Sets the layer object.
	 *
	 * @param layerObject the new layer object
	 */
	public void setLayerObject(LayerObject layerObject) {
		this.layerObject = layerObject;
	}

	/**
	 * Gets the do action.
	 *
	 * @return the do action
	 */
	public DO_LAYER_ACTION getDoAction() {
		return doAction;
	}

	/**
	 * Gets the layer item.
	 *
	 * @return the layer item
	 */
	public LayerItem getLayerItem() {
		return layerItem;
	}

	/**
	 * Gets the source layer swap.
	 *
	 * @return the source layer swap
	 */
	public SwapLayer getSourceLayerSwap() {
		return sourceLayerSwap;
	}

	/**
	 * Gets the target layer swap.
	 *
	 * @return the target layer swap
	 */
	public SwapLayer getTargetLayerSwap() {
		return targetLayerSwap;
	}

	/**
	 * Gets the opacity.
	 *
	 * @return the opacity
	 */
	public double getOpacity() {
		return opacity;
	}

	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public boolean getVisibility() {
		return visibility;
	}

	/**
	 * Sets the UI event source.
	 *
	 * @param sourceEventUI the new UI event source
	 */
	public void setUIEventSource(DragDropLayer sourceEventUI) {
		this.sourceEventUI = sourceEventUI;

	}

	/**
	 * Gets the source event UI.
	 *
	 * @return the source event UI
	 */
	public DragDropLayer getSourceEventUI() {
		return sourceEventUI;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DoActionOnDetailLayersEvent [doAction=");
		builder.append(doAction);
		builder.append(", layerItem=");
		builder.append(layerItem);
		builder.append(", opacity=");
		builder.append(opacity);
		builder.append(", visibility=");
		builder.append(visibility);
		builder.append(", sourceLayerSwap=");
		builder.append(sourceLayerSwap);
		builder.append(", targetLayerSwap=");
		builder.append(targetLayerSwap);
		builder.append("]");
		return builder.toString();
	}

}