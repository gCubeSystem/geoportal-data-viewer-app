package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import java.util.HashMap;
import java.util.Map;

/**
 * The Class OpenLayersMapParameters.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * May 7, 2020
 */
public class OpenLayersMapParameters {
	
	
	private Map<String,String> mapToInitOLMap = new HashMap<String, String>();
	
	public enum OL_MAP_PARAM {maxzoomlevel,zoom,centermap}
	
	/**
	 * Instantiates a new gis viewer map parameters.
	 */
	public OpenLayersMapParameters() {
	}
	
	/**
	 * Adds the parameter.
	 *
	 * @param key the key
	 * @param value the value
	 */
	public void addParameter(String key, String value) {
		mapToInitOLMap.put(key, value);
	}
	
	
	/**
	 * Adds the parameter.
	 *
	 * @param key the key
	 * @param value the value
	 */
	public void addParameter(OL_MAP_PARAM key, String value) {
		if(key!=null)
			mapToInitOLMap.put(key.name(), value);
	}
	
	/**
	 * Gets the pameter.
	 *
	 * @param key the key
	 * @return the pameter
	 */
	public String getPameter(String key) {
		return mapToInitOLMap.get(key);
	}
}
