package org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity;

import javax.servlet.http.HttpServletRequest;

/**
 * The Interface GcubeIdentity.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 23, 2021
 */
public interface GcubeIdentity {

	/**
	 * Sets the identity.
	 *
	 * @param httpRequest the new identity
	 * @throws Exception the exception
	 */
	public void setIdentity(HttpServletRequest httpRequest) throws Exception;

	/**
	 * Reset identity.
	 */
	public void resetIdentity();

	/**
	 * Gets the identity description.
	 *
	 * @return the identity description
	 */
	public String getIdentityDescription();
	
	
	/**
	 * Gets the identity.
	 *
	 * @return the identity
	 */
	public String getIdentity();

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public String getToken();

}
