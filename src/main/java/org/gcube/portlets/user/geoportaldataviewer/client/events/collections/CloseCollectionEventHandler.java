package org.gcube.portlets.user.geoportaldataviewer.client.events.collections;

import com.google.gwt.event.shared.EventHandler;

public interface CloseCollectionEventHandler extends EventHandler{

	void onCloseCollection(CloseCollectionEvent closeCollectionEvent);

}
