package org.gcube.portlets.user.geoportaldataviewer.shared.gis;

public enum LayerObjectType {
	PROJECT_LAYER, INDEX_LAYER, GENERIC_LAYER
}