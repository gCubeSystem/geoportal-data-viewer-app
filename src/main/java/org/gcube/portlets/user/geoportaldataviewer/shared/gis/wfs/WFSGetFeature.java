package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs;

/**
 * The Enum WfsParameters.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 29, 2020
 */
public enum WFSGetFeature {

	SERVICE("SERVICE", "WFS"),
	VERSION("VERSION", "1.1.0"),
	REQUEST("REQUEST", "GetFeature"),
	TYPENAME("TYPENAME", ""),
	STYLES("STYLES",""),
	BBOX("BBOX","-180,-90,180,90"),
	WIDTH("WIDTH","676"),
	HEIGHT("HEIGHT","230"),
	SRSNAME("srsName","EPSG:4326"),
	PROPERTYNAME("propertyName", ""),
//	CRS("CRS","EPSG:4326"), //WMS 1.3.0 COMPLIANT
	OUTPUTFORMAT("OUTPUTFORMAT","json"),
	MAXFEATURES("MAXFEATURES","");
	
	private String parameter;
	private String value;
	
	/**
	 * Instantiates a new wfs parameters.
	 *
	 * @param parameter the parameter
	 * @param value the value
	 */
	WFSGetFeature(String parameter, String value){
		this.parameter = parameter;
		this.value = value;
	}

	/**
	 * Gets the parameter.
	 *
	 * @return the parameter
	 */
	public String getParameter() {
		return parameter;
	}

	/**
	 * Sets the parameter.
	 *
	 * @param parameter the new parameter
	 */
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 *
	 * @param value the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
