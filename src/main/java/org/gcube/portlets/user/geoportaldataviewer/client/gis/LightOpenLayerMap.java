package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewer;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalImages;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery.TYPE;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import ol.Collection;
import ol.Coordinate;
import ol.Feature;
import ol.Map;
import ol.MapBrowserEvent;
import ol.MapOptions;
import ol.OLFactory;
import ol.View;
import ol.ViewOptions;
import ol.color.Color;
import ol.control.Attribution;
import ol.event.EventListener;
import ol.geom.Point;
import ol.interaction.KeyboardPan;
import ol.interaction.KeyboardZoom;
import ol.layer.Base;
import ol.layer.Image;
import ol.layer.Layer;
import ol.layer.LayerOptions;
import ol.layer.Tile;
import ol.layer.VectorLayerOptions;
import ol.proj.Projection;
import ol.proj.ProjectionOptions;
import ol.source.ImageWms;
import ol.source.ImageWmsOptions;
import ol.source.ImageWmsParams;
import ol.source.Osm;
import ol.source.Source;
import ol.source.Xyz;
import ol.source.XyzOptions;
import ol.style.Fill;
import ol.style.FillOptions;
import ol.style.Icon;
import ol.style.IconOptions;
import ol.style.Stroke;
import ol.style.StrokeOptions;
import ol.style.Style;
import ol.style.Text;
import ol.style.TextOptions;

/**
 * The Class LightOpenLayerMap.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 12, 2021
 */
public class LightOpenLayerMap {

	public static final int MAX_ZOOM = 20;

	/** The map. */
	private Map map;

	/** The view. */
	private View view;

	/** The view options. */
	private ViewOptions viewOptions = OLFactory.createOptions();

	/** The projection options. */
	private ProjectionOptions projectionOptions = OLFactory.createOptions();

	private boolean isQueryPointActive;

	private ol.layer.Vector geometryLayer;

	private String markerURL = GeoportalImages.ICONS.mapMarkerIcon().getURL();

	private LayerOrder layerOrder = new LayerOrder();
	
	private Layer baseLayerTile;
	
	/**
	 * Instantiates a new light open layer map.
	 *
	 * @param divTargetId the div target id
	 */
	public LightOpenLayerMap(String divTargetId) {

		// create a projection
		projectionOptions.setCode(MAP_PROJECTION.EPSG_3857.getName());
		projectionOptions.setUnits("m");

		Projection projection = new Projection(projectionOptions);
		viewOptions.setProjection(projection);
		viewOptions.setMaxZoom(20);

		// create a view
		view = new View(viewOptions);

		// create the map
		MapOptions mapOptions = OLFactory.createOptions();
		mapOptions.setTarget(divTargetId);
		mapOptions.setView(view);

		map = new Map(mapOptions);

		//map.addLayer(osmLayer);
		// map.addLayer(tileDebugLayer);

		Attribution attribution = new Attribution();
		attribution.setCollapsed(true);

		map.addClickListener(new EventListener<MapBrowserEvent>() {

			@Override
			public void onEvent(MapBrowserEvent event) {
				// TODO Auto-generated method stub
				Coordinate coordinate = event.getCoordinate();
				if (isQueryPointActive) {

					double lon = coordinate.getX();
					double lat = coordinate.getY();

					int w = (int) map.getSize().getWidth();
					int h = (int) map.getSize().getHeight();
					// handler.clickOnMap(x, y, w, h);

					// ratio - mapPixelWeight : bboxWeight = 10px : geoRectangleWidth
					// where 10px is the pixel diameter dimension of the clicked point
					double bboxWidth = Math.abs(getExtent().getLowerLeftX() - getExtent().getUpperRightX());
					double geoWidth = (bboxWidth / w) * (20 / 2);
					double x1 = Math.min(lon + geoWidth, lon - geoWidth);
					double x2 = Math.max(lon + geoWidth, lon - geoWidth);
					double y1 = Math.min(lat + geoWidth, lat - geoWidth);
					double y2 = Math.max(lat + geoWidth, lat - geoWidth);
					// GWT.log("("+x1+","+y1+")("+x2+","+y2+")");

//					Point pt = new Point(coordinate);
//					ol.Extent extent = pt.getExtent();
//					//new ClickDataInfo(x1, y1, x2, y2)
//					SelectDataInfo selectDataInfo
					// selectBox(new GeoQuery(x1, y1, x2, y2, GeoQuery.TYPE.POINT));
					GeoQuery select = new GeoQuery(x1, y1, x2, y2, TYPE.POINT);

				}
			}
		});

		map.addControl(attribution);

		// add some interactions
		map.addInteraction(new KeyboardPan());
		map.addInteraction(new KeyboardZoom());
		
		List<BaseMapLayer> listBaseMapLayers = GeoportalDataViewer.getListBaseMapLayers();
		BaseMapLayer bml = null;
		if(listBaseMapLayers!=null)
			bml = listBaseMapLayers.get(0);
		
		changeBaseMap(bml);

	}
	
	/**
	 * Change base map.
	 *
	 * @param baseLayer the base layer
	 */
	public void changeBaseMap(BaseMapLayer baseLayer) {

		BaseMapLayer.OL_BASE_MAP baseMap = baseLayer.getType() == null ? BaseMapLayer.OL_BASE_MAP.OSM
				: baseLayer.getType();

		try {
			if (baseLayerTile != null)
				map.removeLayer(baseLayerTile);
		} catch (Exception e) {
		}

		switch (baseMap) {
		case MAPBOX:
		case OTHER:

			XyzOptions xyzOptions2 = OLFactory.createOptions();
			xyzOptions2.setCrossOrigin("Anonymous");
			Xyz xyz = new Xyz(xyzOptions2);
			//setAttributions is buggy
			//xyz.setAttributions(baseLayer.getAttribution());
			xyz.setUrl(baseLayer.getUrl());

			LayerOptions layerOptions2 = OLFactory.createOptions();
			layerOptions2.setSource(xyz);
			int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.BASE_MAP)+1;
			layerOptions2.setZIndex(zIndex);
			baseLayerTile = new Tile(layerOptions2);
	
			break;
		case OSM:
		default:
			XyzOptions osmSourceOptions = OLFactory.createOptions();
			osmSourceOptions.setCrossOrigin("Anonymous");
			Osm osmSource = new Osm(osmSourceOptions);
			//osmSource.setUrl(baseLayer.getUrl());
			//setAttributions is buggy
			//osmSource.setAttributions(baseLayer.getAttribution());
			LayerOptions layerOptions = OLFactory.createOptions();
			layerOptions.setSource(osmSource);
			zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.BASE_MAP)+1;
			layerOptions.setZIndex(zIndex);
			baseLayerTile = new Tile(layerOptions);

			break;

		}

		// map == null at init time
		if (map != null) {
			map.addLayer(baseLayerTile);
		}

	}

	/**
	 * Sets the center.
	 *
	 * @param centerCoordinate the new center
	 */
	public void setCenter(Coordinate centerCoordinate) {
		view.setCenter(centerCoordinate);
	}

	/**
	 * Sets the center.
	 *
	 * @param zoom the new zoom
	 */
	public void setZoom(int zoom) {
		view.setZoom(zoom);
	}

	/**
	 * Adds the WMS layer.
	 *
	 * @param mapServerHost the map server host
	 * @param layerName     the layer name
	 * @param bbox          the bbox
	 * @return the image
	 */
	public Image addWMSLayer(String mapServerHost, String layerName, BoundsMap bbox) {
		GWT.log("Adding wmsLayer with mapServerHost: " + mapServerHost + ", layerName: " + layerName);

		ImageWmsParams imageWMSParams = OLFactory.createOptions();
		imageWMSParams.setLayers(layerName);
		// imageWMSParams.setSize(new Size(400,400));
		// imageWMSParams.setVersion("1.1.0");
//		if(bbox!=null)
//			imageWMSParams.set("BBOX", bbox.getLowerLeftX()+","+bbox.getLowerLeftY()+","+bbox.getUpperRightX()+","+bbox.getUpperRightY());

		ImageWmsOptions imageWMSOptions = OLFactory.createOptions();
		imageWMSOptions.setCrossOrigin("Anonymous");
		imageWMSOptions.setUrl(mapServerHost);
		imageWMSOptions.setParams(imageWMSParams);
		// imageWMSOptions.setRatio(1.5f);

		ImageWms imageWMSSource = new ImageWms(imageWMSOptions);

		LayerOptions layerOptions = OLFactory.createOptions();
		layerOptions.setSource(imageWMSSource);

		ol.layer.Image wmsLayer = new ol.layer.Image(layerOptions);
		int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.WMS_DETAIL)+1;
		wmsLayer.setZIndex(zIndex);
		// visibleLayerItems
		map.addLayer(wmsLayer);

		return wmsLayer;
	}

	/**
	 * Gets the first layer.
	 *
	 * @return the first layer
	 */
	public Image getFirstLayer() {
		if (map.getLayers() != null) {
			return (Image) map.getLayers().getArray()[0];
		}
		return null;
	}

	/**
	 * Gets the layers.
	 *
	 * @return the layers
	 */
	public List<String> getLayers() {
		Collection<Base> layers = map.getLayers();
		List<String> layerNames = null;
		if (layers != null) {
			Base[] layersArr = layers.getArray();
			layerNames = new ArrayList<String>(layersArr.length);
			for (int i = 0; i < layersArr.length; i++) {
				Base layer = layersArr[i];
				if (layer instanceof Image) {
					Image layerImage = (Image) layer;

					Source source = layerImage.getSource();
//					GWT.log("source: "+source.toString());
					GeoportalDataViewerConstants.printJsObj(source);
					String sorceRootObj = GeoportalDataViewerConstants.toJsonObj(source);
					JSONValue jsonObj = JSONParser.parseStrict(sorceRootObj);
					// GWT.log("jsonObj: " + jsonObj.toString());
					JSONObject jsonSourceObj = (JSONObject) jsonObj;

					JSONObject jsonParamsObj = (JSONObject) jsonSourceObj.get("params_");
					// GWT.log("jsonParamsObj is: "+jsonParamsObj);
					JSONValue jsonLayers = jsonParamsObj.get("LAYERS");
					GWT.log("theLayerName name is: " + jsonLayers);
					layerNames.add(jsonLayers.toString());
				}
			}
		}
		return layerNames;
	}

	/**
	 * Gets the layer UR ls property.
	 *
	 * @return the layer UR ls property
	 */
	public java.util.Map<String, String> getLayerURLsProperty() {

		Collection<Base> layers = map.getLayerGroup().getLayers();
		java.util.Map<String, String> mapLayerNameURL = new HashMap<String, String>();
		if (layers != null) {
			Base[] layersArr = layers.getArray();
			for (int i = 0; i < layersArr.length; i++) {
				Base layer = layersArr[i];
				// GeoportalDataViewerConstants.printJs(layer.toString());
				if (layer instanceof Image) {
					Image layerImage = (Image) layer;

					Source source = layerImage.getSource();
//					GWT.log("source: "+source.toString());
//					GeoportalDataViewerConstants.printJsObj(source);
					String sorceRootObj = GeoportalDataViewerConstants.toJsonObj(source);
					JSONValue jsonObj = JSONParser.parseStrict(sorceRootObj);
//					GWT.log("jsonObj: " + jsonObj.toString());
					JSONObject jsonSourceObj = (JSONObject) jsonObj;

					JSONObject jsonParamsObj = (JSONObject) jsonSourceObj.get("params_");
					// GWT.log("jsonParamsObj is: "+jsonParamsObj);
					JSONString jsonLayers = (JSONString) jsonParamsObj.get("LAYERS");
					String layerName = jsonLayers.stringValue();
					GWT.log("jsonLayers is: " + layerName);
//					GWT.log("theLayerName name is: " + jsonLayers);

					JSONValue jsonImage = jsonSourceObj.get("image_");
//					GWT.log("jsonImage: " + jsonImage.toString());
					JSONObject jsonImageObj = (JSONObject) jsonImage;
					JSONString jsonSrc = (JSONString) jsonImageObj.get("src_");
					String layerURL = jsonSrc.stringValue();
					GWT.log("jsonSrc: " + layerURL);
					mapLayerNameURL.put(layerName, layerURL);
				}

			}
		}
		GWT.log("returning mapLayerNameURL: " + mapLayerNameURL);
		return mapLayerNameURL;
	}

	/**
	 * Adds the point.
	 *
	 * @param coordinate         the coordinate
	 * @param showCoordinateText the show coordinate text
	 * @param asMarker           the as marker
	 */
	public void addPoint(Coordinate coordinate, boolean showCoordinateText, boolean asMarker) {

		if (geometryLayer != null) {
			map.removeLayer(geometryLayer);
		} else {

		}
		Style style = new Style();

		if (asMarker) {
			IconOptions iconOptions = new IconOptions();
			iconOptions.setSrc(markerURL);
			Icon icon = new Icon(iconOptions);
			style.setImage(icon);
		}

		if (showCoordinateText) {
			TextOptions textOptions = new TextOptions();
			textOptions.setOffsetY(-25);
			StrokeOptions strokeOptions = new StrokeOptions();
			strokeOptions.setColor(new Color(255, 255, 255, 1.0));
			strokeOptions.setWidth(4);
			Stroke stroke = new Stroke(strokeOptions);
			stroke.setWidth(5);
			textOptions.setStroke(stroke);
			FillOptions fillOptions = new FillOptions();
			fillOptions.setColor(new Color(0, 0, 255, 1.0));
			textOptions.setFill(new Fill(fillOptions));

			Coordinate transfCoord = MapUtils.transformCoordiante(coordinate, MAP_PROJECTION.EPSG_3857.getName(),
					MAP_PROJECTION.EPSG_4326.getName());
			// DecimalFormat df = new DecimalFormat("#.####");
			NumberFormat fmt = NumberFormat.getFormat("#.####");
			textOptions.setText("Long: " + fmt.format(transfCoord.getX()) + ", Lat: " + fmt.format(transfCoord.getY()));

			Text text = new Text(textOptions);
//			FillOptions fillOptions = new FillOptions();
//			Color color = new Color(217, 217, 223, 0.0);
//			fillOptions.setColor(color);
//			Fill fill = new Fill(fillOptions);
//			style.setFill(fill);
			style.setText(text);

		}
		Point thePoint = new Point(coordinate);
		Feature vf = new Feature(thePoint);
		vf.setStyle(style);
		ol.source.Vector vector = new ol.source.Vector();
		vector.addFeature(vf);
		VectorLayerOptions vectorLayerOptions = new VectorLayerOptions();
		vectorLayerOptions.setSource(vector);
		geometryLayer = new ol.layer.Vector(vectorLayerOptions);
		int zIndex = layerOrder.getOffset(LayerOrder.LAYER_TYPE.VECTOR)+1;
		geometryLayer.setZIndex(zIndex);
		map.addLayer(geometryLayer);
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public Map getMap() {
		return map;
	}

	/**
	 * Gets the projection code.
	 *
	 * @return the projection code
	 */
	public String getProjectionCode() {
		return map.getView().getProjection().getCode();
	}

	/**
	 * Gets the current zoom level.
	 *
	 * @return the current zoom level
	 */
	public double getCurrentZoomLevel() {
		return map.getView().getZoom();
	}

	/**
	 * Gets the bbox.
	 *
	 * @return the bbox
	 */
	public ol.Extent getBBOX() {
		return getExtent();
	}

	/**
	 * Gets the extent.
	 *
	 * @return the extent
	 */
	public ol.Extent getExtent() {
		return this.map.getView().calculateExtent(map.getSize());
	}

}
