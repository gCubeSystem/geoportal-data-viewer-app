package org.gcube.portlets.user.geoportaldataviewer.shared;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.ResultSetPaginatedData;


/**
 * The Class ResultSetPaginatedDataIDs.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 25, 2022
 */
public class ResultSetPaginatedDataIDs extends ResultSetPaginatedData {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4847290903890912325L;

	private List<String> resultSetProjectIDs;

	/**
	 * Instantiates a new result set paginated data I ds.
	 */
	public ResultSetPaginatedDataIDs() {

	}

	public ResultSetPaginatedDataIDs(int offset, int limit, boolean isServerSearchFinished) {
		super(offset,limit,isServerSearchFinished);
	}

	/**
	 * Gets the result set project I ds.
	 *
	 * @return the result set project I ds
	 */
	public List<String> getResultSetProjectIDs() {
		return resultSetProjectIDs;
	}

	/**
	 * Sets the result set project I ds.
	 *
	 * @param resultSetProjectIDs the new result set project I ds
	 */
	public void setResultSetProjectIDs(List<String> resultSetProjectIDs) {
		this.resultSetProjectIDs = resultSetProjectIDs;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResultSetPaginatedDataIDs [resultSetProjectIDs=");
		builder.append(resultSetProjectIDs);
		builder.append("]");
		return builder.toString();
	}

}
