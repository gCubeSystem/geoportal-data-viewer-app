package org.gcube.portlets.user.geoportaldataviewer.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs.FeatureRow;

/**
 * The Class GeoportalSpatialQueryResult.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 30, 2021
 */
public class GeoportalSpatialQueryResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3513120677727206958L;
	private List<FeatureRow> features;
	private LayerObject sourceLayerObject;
	// Map with couple (mongoId project, list of uploaded GeoportalImages for the
	// project)
	private Map<String, List<PayloadDV>> mapImages = null;

	/**
	 * Instantiates a new geo na data object.
	 */
	public GeoportalSpatialQueryResult() {

	}

	/**
	 * Gets the features.
	 *
	 * @return the features
	 */
	public List<FeatureRow> getFeatures() {
		return features;
	}

	/**
	 * Sets the features.
	 *
	 * @param features the new features
	 */
	public void setFeatures(List<FeatureRow> features) {
		this.features = features;
	}

	/**
	 * Gets the source layer object.
	 *
	 * @return the source layer object
	 */
	public LayerObject getSourceLayerObject() {
		return sourceLayerObject;
	}

	/**
	 * Sets the source layer object.
	 *
	 * @param sourceLayerObject the new source layer object
	 */
	public void setSourceLayerObject(LayerObject sourceLayerObject) {
		this.sourceLayerObject = sourceLayerObject;
	}

	/**
	 * Gets the map images.
	 *
	 * @return the map images
	 */
	public Map<String, List<PayloadDV>> getMapImages() {
		return mapImages;
	}

	/**
	 * Sets the map images.
	 *
	 * @param mapImages the map images
	 */
	public void setMapImages(Map<String, List<PayloadDV>> mapImages) {
		this.mapImages = mapImages;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalSpatialQueryResult [features=");
		builder.append(features);
		builder.append(", sourceLayerObject=");
		builder.append(sourceLayerObject);
		builder.append(", mapImages=");
		builder.append(mapImages);
		builder.append("]");
		return builder.toString();
	}

}
