package org.gcube.portlets.user.geoportaldataviewer.client.ui;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ClosedViewDetailsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.ProjectViewer;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class DetailsPanel extends Composite {

	private static DetailsPanelUiBinder uiBinder = GWT.create(DetailsPanelUiBinder.class);
	
	@UiField
	Button closeButton;
	
	@UiField
	HTMLPanel datailsContainerPanel;
	
	@UiField
	HTMLPanel detailsHTMLPanel;
	
	private ProjectView displayedProject = null;

	private HandlerManager applicationBus;

	interface DetailsPanelUiBinder extends UiBinder<Widget, DetailsPanel> {
	}

	
	public DetailsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		closeButton.setType(ButtonType.LINK);
		closeButton.setIcon(IconType.REMOVE);
		closeButton.setIconSize(IconSize.LARGE);
		
		closeButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				hidePanelDetails();
				applicationBus.fireEvent(new ClosedViewDetailsEvent());
			}
		});
	}
	
	public void setApplicationBus(HandlerManager applicationBus) {
		this.applicationBus = applicationBus;
	}

	
	public void showDetailsFor(ProjectView result, GeoportalItemReferences geonaItemRef) {
		this.displayedProject = result;
		datailsContainerPanel.clear();
		
		if(detailsHTMLPanel.getParent()!=null) {
			detailsHTMLPanel.getParent().getElement().setScrollTop(0);
		}
		
		datailsContainerPanel.add(new ProjectViewer(applicationBus, geonaItemRef, result));
		showPanelDetails();
	}
	
	private void showPanelDetails() {
		detailsHTMLPanel.setVisible(true);
	}
	
	public void hidePanelDetails() {
		this.displayedProject = null;
		datailsContainerPanel.clear();
		detailsHTMLPanel.setVisible(false);
	}
	
	public ProjectView getDisplayedProject() {
		return displayedProject;
	}

}
