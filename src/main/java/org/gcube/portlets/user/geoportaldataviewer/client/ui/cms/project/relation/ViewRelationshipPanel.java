package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.relation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.RelationshipDV;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewRelationshipPanel extends Composite {

	private static ViewRelationshipPanelUiBinder uiBinder = GWT.create(ViewRelationshipPanelUiBinder.class);

	interface ViewRelationshipPanelUiBinder extends UiBinder<Widget, ViewRelationshipPanel> {
	}

	private String profileID;

	@UiField
	HTMLPanel firstPanelContainer;

	@UiField
	HTMLPanel panelTitle;

	@UiField
	HTMLPanel secondPanelContainer;

	@UiField
	FlowPanel firstProjectPanelContainer;

	@UiField
	FlowPanel secondProjectPanelContainer;

	@UiField
	Button closeButton;

	@UiField
	Button buttonExpand;

	private HashMap<Integer, ResultDocumentDV> selectedProjects = new HashMap<Integer, ResultDocumentDV>(2);

	private HandlerManager appManagerBus;

	private Map<String, ResultDocumentDV> mapOfTargetProjectForId = new HashMap<String, ResultDocumentDV>();

	public ViewRelationshipPanel(HandlerManager appManagerBus, ProjectDV fromProject, boolean showExpand) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;

		closeButton.setType(ButtonType.LINK);
		closeButton.setIcon(IconType.REMOVE);
		closeButton.setIconSize(IconSize.LARGE);

		closeButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
			}
		});

		buttonExpand.setType(ButtonType.LINK);
		buttonExpand.setIcon(IconType.EXPAND);
		buttonExpand.setTitle("Show this view in new Window");
		buttonExpand.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				Modal mw = new Modal(true, true);
				mw.setTitle("Relationships");

				int width = 0;
				try {
					width = Window.getClientWidth() * 75 / 100;
				} catch (Exception e) {
					// TODO: handle exception
				}

				if (width > 500)
					mw.setWidth(width);

				mw.add(new ViewRelationshipPanel(appManagerBus, fromProject, false));
				mw.show();

			}
		});

		if (!showExpand) {
			buttonExpand.setVisible(false);
			panelTitle.setVisible(false);
			closeButton.setVisible(false);
		}

		showRelationsOf(fromProject);
	}

	public void showRelationsOf(ProjectDV project) {
		firstProjectPanelContainer.clear();
		secondProjectPanelContainer.clear();

		Entry<String, Object> firstEntrySet = project.getTheDocument().getFirstEntryOfMap();
		String htmlMsg = firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue() + "</b> (id: " + project.getId()
				+ ")";

		firstProjectPanelContainer.add(new HTML(htmlMsg));

		HTML labelNoRelations = new HTML("No relationship/s found");
		secondProjectPanelContainer.add(labelNoRelations);

		if (project.getRelationships() != null && project.getRelationships().size() > 0) {

			secondProjectPanelContainer.clear();

			for (RelationshipDV relationDV : project.getRelationships()) {

				final FlexTable flexTable = new FlexTable();
				flexTable.getElement().addClassName("box-table-diplay-project");
				Label label = new Label();
				label.setType(LabelType.INFO);
				label.setText(relationDV.getRelationshipName());

				FlowPanel panelContainer = new FlowPanel();
				Button deleteRelation = new Button("", IconType.SAVE);
				deleteRelation.setTitle("Open this project");
				deleteRelation.setType(ButtonType.LINK);
				deleteRelation.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						ResultDocumentDV toProject = mapOfTargetProjectForId.get(relationDV.getTargetUCD());
//						appManagerBus.fireEvent(
//								new RelationActionHandlerEvent(project, relationDV.getRelationshipName(), toProject));

					}
				});

				panelContainer.add(label);
				panelContainer.add(deleteRelation);
				flexTable.setWidget(0, 0, panelContainer);

//				flexTable.setWidget(1, 0, new LoaderIcon("loading project.."));
//				GeoportalDataEntryServiceAsync.Util.getInstance().getResultDocumentFoProjectByID(
//						relationDV.getTargetUCD(), relationDV.getTargetID(), new AsyncCallback<ResultDocumentDV>() {
//
//							@Override
//							public void onFailure(Throwable caught) {
//								flexTable.setWidget(1, 0, new HTML(caught.getMessage()));
//
//							}
//
//							@Override
//							public void onSuccess(ResultDocumentDV result) {
//								mapOfTargetProjectForId.put(relationDV.getTargetUCD(), result);
//								Entry<String, Object> firstEntrySet = result.getFirstEntryOfMap();
//								String htmlMsg = firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue()
//										+ "</b> (id: " + result.getId() + ")";
//
//								flexTable.setWidget(1, 0, new HTML(htmlMsg));
//
//								ReportTemplateToHTML rtth2 = new ReportTemplateToHTML("", result.getDocumentAsJSON(),
//										false, false);
//								rtth2.showAsJSON(false);
//
//								flexTable.setWidget(2, 0, rtth2);
//							}
//						});

				secondProjectPanelContainer.add(flexTable);
			}

		}

	}

}
