package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.IndexLayerDV;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class AddedLayerToMapEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 18, 2020
 */
public class ApplyCQLToLayerOnMapEvent extends GwtEvent<ApplyCQLToLayerOnMapEventHandler> {
	public static Type<ApplyCQLToLayerOnMapEventHandler> TYPE = new Type<ApplyCQLToLayerOnMapEventHandler>();
	private IndexLayerDV theIndeLayer;
	private String cqlFilterValue;

	/**
	 * Instantiates a new apply CQL to layer map event.
	 *
	 * @param theIndeLayer the the inde layer
	 * @param cqlFilter    the cql filter
	 */
	public ApplyCQLToLayerOnMapEvent(IndexLayerDV theIndeLayer, String cqlFilterValue) {
		this.theIndeLayer = theIndeLayer;
		this.cqlFilterValue = cqlFilterValue;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ApplyCQLToLayerOnMapEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ApplyCQLToLayerOnMapEventHandler handler) {
		handler.onApplyCQL(this);

	}

	public IndexLayerDV getTheIndeLayer() {
		return theIndeLayer;
	}

	public String getCqlFilterValue() {
		return cqlFilterValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApplyCQLToLayerOnMapEvent [theIndeLayer=");
		builder.append(theIndeLayer);
		builder.append(", cqlFilterValue=");
		builder.append(cqlFilterValue);
		builder.append("]");
		return builder.toString();
	}
	
	

}