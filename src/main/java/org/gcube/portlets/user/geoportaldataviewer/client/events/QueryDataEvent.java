package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MapEventType;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.ExtentWrapped;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.GeoQuery;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class QueryDataEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 31, 2021
 */
public class QueryDataEvent extends GwtEvent<QueryDataEventHandler> {
	public static Type<QueryDataEventHandler> TYPE = new Type<QueryDataEventHandler>();
	private GeoQuery select;
	private ExtentWrapped queryClickExtent;
	private String productID;
	private boolean onInit;
	private MapEventType sourceMapEventType;

	/**
	 * Instantiates a new query data event.
	 *
	 * @param select            the select
	 * @param onFailureCenterTo the on failure center to
	 * @param productID         the mongo item id
	 * @param onInit            the on init
	 * @param mapEventType      the map event type
	 */
	public QueryDataEvent(GeoQuery select, ExtentWrapped queryClickExtent, String productID, boolean onInit,
			MapEventType mapEventType) {
		this.select = select;
		this.queryClickExtent = queryClickExtent;
		this.productID = productID;
		this.onInit = onInit;
		this.sourceMapEventType = mapEventType;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<QueryDataEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getProductID() {
		return productID;
	}

	/**
	 * Checks if is on init.
	 *
	 * @return true, if is on init
	 */
	public boolean isOnInit() {
		return onInit;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(QueryDataEventHandler handler) {
		handler.onQueryInteraction(this);

	}

	/**
	 * Gets the geo query.
	 *
	 * @return the geo query
	 */
	public GeoQuery getGeoQuery() {
		return select;
	}

	public ExtentWrapped getQueryClickExtent() {
		return queryClickExtent;
	}

	/**
	 * Gets the source map event type.
	 *
	 * @return the source map event type
	 */
	public MapEventType getSourceMapEventType() {
		return sourceMapEventType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("QueryDataEvent [select=");
		builder.append(select);
		builder.append(", queryClickExtent=");
		builder.append(queryClickExtent);
		builder.append(", productID=");
		builder.append(productID);
		builder.append(", onInit=");
		builder.append(onInit);
		builder.append(", sourceMapEventType=");
		builder.append(sourceMapEventType);
		builder.append("]");
		return builder.toString();
	}

}