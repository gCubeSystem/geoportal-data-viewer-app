package org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.accessidentity;

import javax.servlet.http.HttpServletRequest;

import org.gcube.portlets.user.geoportaldataviewer.server.util.SessionUtil;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserIdentity implements GcubeIdentity {

	private static final Logger LOG = LoggerFactory.getLogger(UserIdentity.class);

	private HttpServletRequest httpRequest;

	private GCubeUser user;

	public UserIdentity() {
	}

	@Override
	public void setIdentity(HttpServletRequest httpRequest) throws Exception {
		LOG.info("setIdentity called");
		this.httpRequest = httpRequest;
		SessionUtil.getCurrentToken(this.httpRequest, true);
		user = SessionUtil.getCurrentUser(this.httpRequest);

	}

	@Override
	public void resetIdentity() {
		LOG.info("resetIdentity called, doing nothing");
		// doing nothing
	}

	@Override
	public String getToken() {
		return SessionUtil.getCurrentToken(this.httpRequest, true);
	}

	@Override
	public String getIdentityDescription() {
		return user != null ? "user: " + user.getUsername() : "null";
	}
	
	@Override
	public String getIdentity() {
		return user.getUsername();
	}

}
