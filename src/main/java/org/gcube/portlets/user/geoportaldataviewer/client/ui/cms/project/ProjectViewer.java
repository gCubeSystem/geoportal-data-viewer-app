package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.RelationshipDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.portal.clientcontext.client.GCubeClientContext;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.events.AddLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.TimelineProjectRelationsEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.TimelineProjectRelationsEvent.EVENT_TYPE;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.MapUtils;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.ModalWindow;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.dialogs.DialogShareableLink;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.PLACE;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.MapView;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.FormElement;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import ol.Coordinate;

/**
 * The Class ProjectViewer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 9, 2022
 */
public class ProjectViewer extends Composite {

	private static ProjectViewerUiBinder uiBinder = GWT.create(ProjectViewerUiBinder.class);

	/**
	 * The Interface ProjectViewerUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 9, 2022
	 */
	interface ProjectViewerUiBinder extends UiBinder<Widget, ProjectViewer> {
	}

	@UiField
	HTMLPanel headerPanel;

	@UiField
	HTMLPanel pageViewDetails;

	@UiField
	HTMLPanel projectViewerMainPanel;

	@UiField
	HTMLPanel centroidPanel;

	@UiField
	HTMLPanel tableOfContentPanel;

	@UiField
	Button shareButton;

	@UiField
	Button expandButton;

	@UiField
	Button exportButton;

	@UiField
	Button relationshipsButton;

	@UiField
	Button addLayersToMap;

	@UiField
	HTMLPanel toc_container;

	@UiField
	HTMLPanel toc_list_container;

	@UiField
	Button reduceToc;

	@UiField
	UListElement toc_list_anchors;

	private ProjectView theProjectView;

	private GeoportalItemReferences geoportalItemReferences;

	private String myLogin;

	private HandlerManager applicationBus;

	private String projectViewerMainPanelID;

	/**
	 * Instantiates a new project viewer.
	 */
	private ProjectViewer() {
		initWidget(uiBinder.createAndBindUi(this));
		pageViewDetails.getElement().addClassName("page-view-details");
		projectViewerMainPanelID = "projectViewer-" + Random.nextInt();
		projectViewerMainPanel.getElement().setId(projectViewerMainPanelID);
	}

	/**
	 * Instantiates a new project viewer.
	 *
	 * @param applicationBus    the application bus
	 * @param geoportalItemRefs the geoportal item refs
	 * @param projectView       the project view
	 */
	public ProjectViewer(HandlerManager applicationBus, GeoportalItemReferences geoportalItemRefs,
			final ProjectView projectView) {
		this();
		GWT.log("Rendering " + projectView.getTheProjectDV().getId());
		this.theProjectView = projectView;
		this.geoportalItemReferences = geoportalItemRefs;
		this.applicationBus = applicationBus;

		GeoportalDataViewerServiceAsync.Util.getInstance().checkExportAsPDFConfig(new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(Boolean result) {
				GWT.log("Export as PDF visible: " + result);
				exportButton.setVisible(result);

			}
		});

		final String theTitle = projectView.getTheProjectDV().getProfileName() != null
				? projectView.getTheProjectDV().getProfileName()
				: "Project ID: " + projectView.getTheProjectDV().getId();

		headerPanel.add(new HTML(theTitle));

		shareButton.setType(ButtonType.LINK);
		shareButton.setIcon(IconType.SHARE);
		shareButton.setTitle("Get a link to share with...");

		shareButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DialogShareableLink dg = new DialogShareableLink(geoportalItemReferences, null);

			}
		});

		expandButton.setType(ButtonType.LINK);
		expandButton.setIcon(IconType.EXPAND);
		expandButton.setTitle("Show this view in new Window");

		expandButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				ProjectViewer cv = new ProjectViewer(applicationBus, geoportalItemReferences, theProjectView);
				cv.setExpandViewButtonVisible(false);
				cv.setRelationshipsButtonVisible(false);
				cv.setAddLayersToMapVisible(false);
				cv.setTocContentVisible(true);
				int width = Window.getClientWidth() * 75 / 100;
				int height = Window.getClientHeight() * 70 / 100;

				String modalTitle = ProjectUtil.toHMLCode(false, theProjectView.getTheProjectDV().getTheDocument(),
						theProjectView.getTheProjectDV().getId());
				ModalWindow mw = new ModalWindow(modalTitle, width, height);
				mw.add(cv);
				mw.setCaller(ProjectViewer.this);
				// mw.setWidth(900);
				mw.show();

			}
		});

		exportButton.setType(ButtonType.LINK);
		exportButton.setIcon(IconType.FILE);
		exportButton.setTitle("Export as PDF");
		exportButton.setVisible(false);

		exportButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String theProjectName = "";
				try {
					theProjectName = (String) projectView.getTheProjectDV().getTheDocument().getFirstEntryOfMap()
							.getValue();
					theProjectName = theProjectName != null ? theProjectName
							: "Project ID: " + projectView.getTheProjectDV().getId();
				} catch (Exception e) {
					// TODO: handle exception
				}

				// Reading contextId and userId from GCubeClientContext
				String currentContextId = GCubeClientContext.getCurrentContextId();
				String currentUserId = GCubeClientContext.getCurrentUserId();

				ProjectDV thePDV = projectView.getTheProjectDV();

				final String targetWindow = "target_window_" + Random.nextInt() + "_" + Random.nextInt();

				final FormPanel form = new FormPanel();
				final FormElement fe = form.getElement().cast();
				//NewBrowserWindow theNewTabWindow = NewBrowserWindow.open("", "", null);
				RootPanel.get().add(form);
				VerticalPanel panel = new VerticalPanel();
				form.setWidget(panel);
				form.setAction(GeoportalDataViewerConstants.GEOPORTAL_EXPORT_ACTION_SERVLET);
				form.setEncoding(FormPanel.ENCODING_URLENCODED);
				form.setMethod(FormPanel.METHOD_POST);
				panel.add(new Hidden(GeoportalDataViewerConstants.UCD_ID_PARAMETER, thePDV.getProfileID()));
				panel.add(new Hidden(GeoportalDataViewerConstants.PROIECT_ID_PARAMETER, thePDV.getId()));
				panel.add(new Hidden(GeoportalDataViewerConstants.CONTEXT_ID_PARAMETER, currentContextId));
				panel.add(new Hidden(GeoportalDataViewerConstants.USER_ID_PARAMETER, currentUserId));
				
				if(myLogin!=null) {
					panel.add(new Hidden(GeoportalDataViewerConstants.USERNAME_PARAMETER, myLogin));
				}
				
//				final Modal contactinServiceModal = new Modal(true, true);
//				contactinServiceModal.setCloseVisible(false);
//				contactinServiceModal.setTitle("Export");
//				FlowPanel panelMsg = new FlowPanel();
//				panelMsg.add(new HTML("Contacting the service..."));
//				contactinServiceModal.add(panelMsg);
//				contactinServiceModal.show();
				
				// Add an event handler to the form.
				form.addSubmitHandler(new SubmitHandler() {
					public void onSubmit(SubmitEvent event) {
						GWT.log("onSubmit done");
						fe.setTarget(targetWindow);
					}
				});
				
				form.addSubmitCompleteHandler(new SubmitCompleteHandler() {
					public void onSubmitComplete(SubmitCompleteEvent event) {
						GWT.log("onSubmitComplete done");
						//contactinServiceModal.hide();
					}
				});

				form.submit();

			}
		});

		relationshipsButton.setType(ButtonType.LINK);
		relationshipsButton.setIcon(IconType.LINK);
		relationshipsButton.setTitle("Show Relationships of this Project");

		List<RelationshipDV> relationships = projectView.getTheProjectDV().getRelationships();
		if (relationships != null && relationships.size() > 0) {
			relationshipsButton.setVisible(true);
		} else {
			relationshipsButton.setVisible(false);
		}

		relationshipsButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				applicationBus.fireEvent(
						new TimelineProjectRelationsEvent(theProjectView.getTheProjectDV(), EVENT_TYPE.SHOW));

			}
		});

		addLayersToMap.setType(ButtonType.LINK);
		addLayersToMap.setIcon(IconType.MAP_MARKER);
		addLayersToMap.setTitle("Add the layer/s of the Project to principal Map");

		addLayersToMap.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				applicationBus.fireEvent(new AddLayerToMapEvent(theProjectView.getTheProjectDV(), true));
			}
		});

		setAddLayersToMapVisible(false);

		GeoportalDataViewerServiceAsync.Util.getInstance().getMyLogin(new AsyncCallback<String>() {

			@Override
			public void onSuccess(String result) {
				myLogin = result;
				addCentroidMap();
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});

		reduceToc.setType(ButtonType.LINK);
		reduceToc.setIcon(IconType.PLUS_SIGN_ALT);
		toc_list_container.setVisible(false);
		reduceToc.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				boolean visible = toc_list_container.isVisible();
				setTocContentVisible(!visible);
			}
		});

		for (SectionView sectionView : projectView.getListSections()) {

			if (!sectionView.isEmpty()) {
				SectionViewer sectionViewer = new SectionViewer(sectionView, projectViewerMainPanelID);
				String sectionId = sectionView.getSectionTitle().replaceAll("[^A-Za-z0-9]", "-") + "_"
						+ Random.nextInt();
				String divTarget = "<div class='anchor-target' id='" + sectionId + "'></div>";
				sectionViewer.getElement().insertFirst(new HTML(divTarget).getElement());
				addAnchorToSection(sectionId, sectionView.getSectionTitle());
				pageViewDetails.add(sectionViewer);
			}

			if (sectionView.hasSpatialLayers()) {
				setAddLayersToMapVisible(true);
			}

		}

		if (toc_list_anchors.getChildCount() > 0) {
			tableOfContentPanel.setVisible(true);
		}
	}

	private void addAnchorToSection(String id, String text) {
		String htmlAnchor = "<a href='#" + id + "'>" + text + "</a>";
		toc_list_anchors.appendChild(new HTML("<li>" + htmlAnchor + "</li>").getElement());
	}

	protected void setTocContentVisible(boolean bool) {

		toc_list_container.setVisible(bool);

		if (bool) {
			reduceToc.setIcon(IconType.MINUS_SIGN_ALT);
		} else {
			reduceToc.setIcon(IconType.PLUS_SIGN_ALT);
		}

	}

	/**
	 * Adds the centroid map.
	 */
	private void addCentroidMap() {
		Location italyLocation = ExtentMapUtil.getLocation(PLACE.ITALY);
		Coordinate transformedCenterCoordinate = italyLocation.getCoordinate(MAP_PROJECTION.EPSG_3857);
		MapView mapView = new MapView(transformedCenterCoordinate,
				GeoportalDataViewerConstants.LIGHT_MAP_ITALY_FIT_ZOOM_ON, "70%", "300px");

		if (theProjectView != null && theProjectView.getTheProjectDV() != null) {

			GeoJSON spatialReference = theProjectView.getTheProjectDV().getSpatialReference();

			Coordinate transfCoord = MapUtils.geoJSONToBBOXCenter(spatialReference, MAP_PROJECTION.EPSG_4326.getName(),
					MAP_PROJECTION.EPSG_3857.getName());

			if (transfCoord != null) {
				boolean authenticatedUser = myLogin != null ? true : false;
				mapView.addMarker(transfCoord, authenticatedUser);
				centroidPanel.add(mapView);
			}

		} else if (theProjectView != null) {
			GeoportalDataViewerConstants
					.printJs("I cannot add centroid as maker one or both coordinates are null. Lat: " + theProjectView
							+ ", Long:" + theProjectView);
		}
	}

	/**
	 * Gets the project view.
	 *
	 * @return the project view
	 */
	public ProjectView getProjectView() {
		return theProjectView;
	}

	/**
	 * Sets the expand view button visible.
	 *
	 * @param bool the new expand view button visible
	 */
	protected void setExpandViewButtonVisible(boolean bool) {
		expandButton.setVisible(bool);
	}

	/**
	 * Sets the adds the layers to map visible.
	 *
	 * @param bool the new adds the layers to map visible
	 */
	protected void setAddLayersToMapVisible(boolean bool) {
		addLayersToMap.setVisible(bool);
	}

	/**
	 * Sets the relationships button visible.
	 *
	 * @param bool the new relationships button visible
	 */
	protected void setRelationshipsButtonVisible(boolean bool) {
		relationshipsButton.setVisible(bool);
	}

}
