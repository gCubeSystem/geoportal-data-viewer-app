package org.gcube.portlets.user.geoportaldataviewer.client.ui.layercollection;

import java.util.HashMap;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.IndexLayerDV;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewer;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEvent.LAYER_TYPE;
import org.gcube.portlets.user.geoportaldataviewer.client.events.UpdateLayerToMapEvent.REQUEST_PARAMETER;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.CloseCollectionEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.OpenCollectionEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.GCubeCollection;
import org.gcube.portlets.user.geoportaldataviewer.shared.ViewerConfiguration;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.GeoInformationForWMSRequest;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class LayerCollectionPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 15, 2023
 */
public class LayerCollectionPanel extends Composite {

	public static String COLORSCALERANGE = "COLORSCALERANGE";

	@UiField
	HTMLPanel basePanel;

	@UiField
	HTMLPanel stylePanel;

	@UiField
	HTMLPanel styleListPanel;
	
	@UiField
	HTMLPanel statisticPanel;

	@UiField
	Button buttonLegend;

	@UiField
	ScrollPanel legendPanel;

	private CheckBox checkbox;

	private GeoInformationForWMSRequest geoInformation;

	private static LayerCollectionPanelUiBinder uiBinder = GWT.create(LayerCollectionPanelUiBinder.class);

	private ListBox listBoxStyles = new ListBox();

	private String layerName;

	private GCubeCollection gcubeCollection;

	private HandlerManager applicationBus;

	/**
	 * The Interface LayerCollectionPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jan 16, 2023
	 */
	interface LayerCollectionPanelUiBinder extends UiBinder<Widget, LayerCollectionPanel> {
	}

	/**
	 * Instantiates a new layer collection panel.
	 *
	 * @param coll           the coll
	 * @param applicationBus the application bus
	 */
	public LayerCollectionPanel(final GCubeCollection coll, HandlerManager applicationBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.applicationBus = applicationBus;

		buttonLegend.setType(ButtonType.LINK);
		legendPanel.setVisible(false);
		statisticPanel.setVisible(false);

		GWT.log("Found available collection " + coll);
		String label = coll.getUcd().getName();
		final String collectionID = coll.getUcd().getId(); // collectionID == UCD_Id

		checkbox = new CheckBox(label);
		checkbox.setId("gcubeCollectionSelector_" + collectionID);
		checkbox.getElement().getStyle().setFontWeight(FontWeight.BOLD);

		checkbox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				GWT.log("Collection selector flag changed to value : " + event.toDebugString());
				GWT.log("Collection ID is : " + collectionID + ", event value: " + event.getValue());
				if (event.getValue()) {
					// OPEN COLLECTION
					applicationBus.fireEvent(new OpenCollectionEvent(collectionID));
					enableLayerStyle(true);
				} else {
					// CLOSE COLLECTION
					hideStyleLegend();
					resetLayerStyle();
					enableLayerStyle(false);
					applicationBus.fireEvent(new CloseCollectionEvent(collectionID));
				}
			}
		});

		basePanel.add(checkbox);
		
		GeoportalDataViewerServiceAsync.Util.getInstance().getCountFor(collectionID, "Published", "OK", new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(Integer result) {
				
				if(result!=null) {
					String resultLabel = result+" project";
					if(result>1) {
						resultLabel+="s";
					}
					resultLabel+= " published";
					statisticPanel.setVisible(true);
					statisticPanel.add(new HTML(resultLabel));
				}
				
			}
		});
		

		ViewerConfiguration theConfig = GeoportalDataViewer.getStatus().getViewerConfig();
		final GCubeCollection toOpen = theConfig.getAvailableCollections().get(collectionID);

		// Check if indexes is empty
		if (toOpen.getIndexes() == null || toOpen.getIndexes().isEmpty()) {
			GWT.log("Unexpected empty indexes in collection " + toOpen);
			Window.alert("Cannot open collection index layer for " + toOpen.getUcd().getName() + ".");
			return;
		}
		this.gcubeCollection = toOpen;
		// TODO Get Default Index Layer
		// For now we just take the first - only 1 is expected
		IndexLayerDV layer = toOpen.getIndexes().get(0);
		// Open Index Layer

		HashMap<String, String> ogcLinks = layer.getLayer().getOgcLinks();
		final String wmsLink = ogcLinks.get("wms");
		this.layerName = URLUtil.getValueOfParameter("layers", wmsLink);

		GeoportalDataViewerServiceAsync.Util.getInstance().parseWmsRequest(wmsLink, layerName,
				new AsyncCallback<GeoInformationForWMSRequest>() {

					@Override
					public void onFailure(Throwable caught) {

						Window.alert(caught.getMessage());
					}

					@Override
					public void onSuccess(GeoInformationForWMSRequest geoInfoWMS) {
						geoInformation = geoInfoWMS;
						GWT.log("Parsed WMS Request returned: " + geoInfoWMS);

						if (geoInfoWMS.getStyles() != null && geoInfoWMS.getStyles().getGeoStyles() != null) {
							if (geoInfoWMS.getStyles().getGeoStyles().size() > 0) {
								stylePanel.setVisible(true);
								listBoxStyles.clear();
								for (String styleName : geoInfoWMS.getStyles().getGeoStyles()) {
									listBoxStyles.addItem(styleName, styleName);
								}

								styleListPanel.add(listBoxStyles);
							}
						}

						buttonLegend.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								GWT.log("is isToggle: " + buttonLegend.isToggle());
								GWT.log("is isToggled: " + buttonLegend.isToggled());
								if (legendPanel.isVisible()) {
									hideStyleLegend();
								} else {
									legendPanel.setVisible(true);
									loadLegend(wmsLink);
								}

							}
						});

						listBoxStyles.addChangeHandler(new ChangeHandler() {

							@Override
							public void onChange(ChangeEvent event) {
								setLayerStyle();

							}
						});
					}
				});

	}

	private void setLayerStyle() {
		UpdateLayerToMapEvent updateLayer = new UpdateLayerToMapEvent(this.gcubeCollection, this.layerName,
				LAYER_TYPE.INDEX);
		updateLayer.setOperation(REQUEST_PARAMETER.STYLE, listBoxStyles.getSelectedValue());
		applicationBus.fireEvent(updateLayer);
		if (legendPanel.isVisible()) {
			legendPanel.clear();
			legendPanel.setVisible(false);

		}
	}

	private void resetLayerStyle() {
		listBoxStyles.setSelectedIndex(0);
		listBoxStyles.setSelectedValue(listBoxStyles.getSelectedValue());
	}

	private void enableLayerStyle(boolean enabled) {
		listBoxStyles.setEnabled(enabled);
	}

	private void hideStyleLegend() {
		legendPanel.clear();
		legendPanel.setVisible(false);
	}

	/**
	 * Gets the checkbox.
	 *
	 * @return the checkbox
	 */
	public CheckBox getCheckbox() {
		return checkbox;
	}

	/**
	 * Load legend.
	 *
	 * @param wmsLink the wms link
	 */
	private void loadLegend(String wmsLink) {

		legendPanel.clear();
		String theLayerName = geoInformation.getLayerName();
		final FlexTable flexTable = new FlexTable();
		legendPanel.add(flexTable);

		// Case no style found
		if (listBoxStyles.getSelectedValue() == null) {
			flexTable.setWidget(0, 0, new HTML("No style found"));
			return;
		}

		FlowPanel flow = new FlowPanel();
		flow.add(new HTMLPanel("Legend for: " + theLayerName));
		final HorizontalPanel hpLegend = new HorizontalPanel();

		String url = geoInformation.getServiceEndPoint() + "?service=WMS&" + "version="
				+ URLUtil.getValueOfParameter("version", wmsLink) + "&" + "request=GetLegendGraphic&" + "layer="
				+ theLayerName;

		String styleName = null;
		try {
			styleName = listBoxStyles.getSelectedValue();
		} catch (Exception e) {
		}

		styleName = styleName != null && !styleName.isEmpty() ? styleName : "";

		if (!geoInformation.isNcWMS()) {

			url += "&format=image/png" + "&STYLE=" + styleName
					+ "&LEGEND_OPTIONS=forceRule:True;dx:0.2;dy:0.2;mx:0.2;my:0.2;fontStyle:bold;"
					+ "borderColor:000000;border:true;fontColor:000000;fontSize:14";
		} else {

			int isNcWmsStyle = styleName.indexOf("/");
			if (isNcWmsStyle != -1) {
				styleName = styleName.substring(isNcWmsStyle + 1, styleName.length());
			}
			url += "&palette=" + styleName;
			if (geoInformation.getMapWMSNoStandard() != null) {
				for (String key : geoInformation.getMapWMSNoStandard().keySet()) { // ADDING COLORSCALERANGE?
					if (key.compareToIgnoreCase(COLORSCALERANGE) == 0) {
						url += "&" + key + "=" + geoInformation.getMapWMSNoStandard().get(key);
						break;
					}
				}
			}
		}

		GWT.log(url);
		flexTable.setStyleName("layer-style-panel-table-legend");
		flexTable.setWidget(0, 0, new Label("Legend"));
		final Image legendImage = new Image(url);
		legendImage.addLoadHandler(new LoadHandler() {
			@Override
			public void onLoad(LoadEvent event) {
			}
		});

		legendImage.addErrorHandler(new ErrorHandler() {
			@Override
			public void onError(ErrorEvent event) {
				GWT.log("ErrorEvent ");
				flexTable.setWidget(0, 1, new HTML("Error on loading the style"));
			}
		});

		hpLegend.add(legendImage);
		flexTable.setWidget(0, 1, hpLegend);
	}

}
