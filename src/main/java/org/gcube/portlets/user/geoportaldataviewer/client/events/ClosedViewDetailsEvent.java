package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class ClosedViewDetailsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 19, 2020
 */
public class ClosedViewDetailsEvent extends GwtEvent<ClosedViewDetailsEventHandler> {
	public static Type<ClosedViewDetailsEventHandler> TYPE = new Type<ClosedViewDetailsEventHandler>();

	public ClosedViewDetailsEvent() {
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ClosedViewDetailsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ClosedViewDetailsEventHandler handler) {
		handler.onClosed(this);

	}
	

}