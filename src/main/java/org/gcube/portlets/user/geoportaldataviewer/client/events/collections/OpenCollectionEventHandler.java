package org.gcube.portlets.user.geoportaldataviewer.client.events.collections;

import com.google.gwt.event.shared.EventHandler;

public interface OpenCollectionEventHandler extends EventHandler {

	
	void onOpenCollection(OpenCollectionEvent event); 

}
