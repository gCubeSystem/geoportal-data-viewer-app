package org.gcube.portlets.user.geoportaldataviewer.client.ui.customoverlays;

import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.events.OverlayCustomLayerToMapEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.GeoInformationForWMSRequest;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ErrorEvent;
import com.google.gwt.event.dom.client.ErrorHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class OverlayCustomLayerPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 15, 2023
 */
public class OverlayCustomLayerPanel extends Composite {

	public static String COLORSCALERANGE = "COLORSCALERANGE";

	@UiField
	HTMLPanel basePanel;

	@UiField
	HTMLPanel stylePanel;

	@UiField
	HTMLPanel styleListPanel;

	@UiField
	Button buttonLegend;

	@UiField
	ScrollPanel legendPanel;

	private CheckBox checkbox;

	private GeoInformationForWMSRequest geoInformation;

	private static OverlayCustomLayerPanelUiBinder uiBinder = GWT.create(OverlayCustomLayerPanelUiBinder.class);

	private ListBox listBoxStyles = new ListBox();

	private HandlerManager applicationBus;

	private LayerItem layerItem;

	/**
	 * The Interface LayerCollectionPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jan 16, 2023
	 */
	interface OverlayCustomLayerPanelUiBinder extends UiBinder<Widget, OverlayCustomLayerPanel> {
	}

	/**
	 * Instantiates a new overlay custom layer panel.
	 *
	 * @param layerItem      the layer item
	 * @param applicationBus the application bus
	 */
	public OverlayCustomLayerPanel(LayerItem layerItem, HandlerManager applicationBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.applicationBus = applicationBus;
		this.layerItem = layerItem;

		buttonLegend.setType(ButtonType.LINK);
		legendPanel.setVisible(false);

		GWT.log("Found available layerItem " + layerItem);
		String label = layerItem.getTitle();
		final String layerName = layerItem.getName();

		checkbox = new CheckBox(label);
		checkbox.setId("gcubeCustomLayerSelector_" + layerName);
		checkbox.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		checkbox.setChecked(true);

		checkbox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				GWT.log("CustomLayer selector flag changed to value : " + event.toDebugString());
				if (event.getValue()) {
					// OPEN COLLECTION
					applicationBus.fireEvent(new OverlayCustomLayerToMapEvent(layerItem, OverlayCustomLayerToMapEvent.ACTION_TYPE.VISIBILITY, true));
				} else {
					// CLOSE COLLECTION
					legendPanel.clear();
					legendPanel.setVisible(false);
					applicationBus.fireEvent(new OverlayCustomLayerToMapEvent(layerItem, OverlayCustomLayerToMapEvent.ACTION_TYPE.VISIBILITY, false));
				}
			}
		});

		basePanel.add(checkbox);

		final String wmsLink = layerItem.getWmsLink();
		GeoportalDataViewerServiceAsync.Util.getInstance().parseWmsRequest(wmsLink, layerName,
				new AsyncCallback<GeoInformationForWMSRequest>() {

					@Override
					public void onFailure(Throwable caught) {

						Window.alert(caught.getMessage());
					}

					@Override
					public void onSuccess(GeoInformationForWMSRequest geoInfoWMS) {
						geoInformation = geoInfoWMS;
						GWT.log("Parsed WMS Request returned: " + geoInfoWMS);

						if (geoInfoWMS.getStyles() != null && geoInfoWMS.getStyles().getGeoStyles() != null) {
							if (geoInfoWMS.getStyles().getGeoStyles().size() > 0) {
								stylePanel.setVisible(true);
								listBoxStyles.clear();
								for (String styleName : geoInfoWMS.getStyles().getGeoStyles()) {
									listBoxStyles.addItem(styleName, styleName);
								}

								styleListPanel.add(listBoxStyles);
							}
						}
						
						listBoxStyles.addChangeHandler(new ChangeHandler() {
							
							@Override
							public void onChange(ChangeEvent event) {
								legendPanel.clear();
								legendPanel.setVisible(false);
							}
						});
						
						
						buttonLegend.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								GWT.log("is isToggle: " + buttonLegend.isToggle());
								GWT.log("is isToggled: " + buttonLegend.isToggled());
								if (legendPanel.isVisible()) {
									legendPanel.clear();
									legendPanel.setVisible(false);

								} else {
									legendPanel.setVisible(true);
									loadLegend(wmsLink);
								}

							}
						});

					}
				});

	}

	/**
	 * Gets the checkbox.
	 *
	 * @return the checkbox
	 */
	public CheckBox getCheckbox() {
		return checkbox;
	}

	/**
	 * Load legend.
	 *
	 * @param wmsLink the wms link
	 */
	private void loadLegend(String wmsLink) {

		legendPanel.clear();
		String theLayerName = geoInformation.getLayerName();
		final FlexTable flexTable = new FlexTable();
		legendPanel.add(flexTable);

		// Case no style found
		if (listBoxStyles.getSelectedValue() == null) {
			flexTable.setWidget(0, 0, new HTML("No style found"));
			return;
		}

		FlowPanel flow = new FlowPanel();
		flow.add(new HTMLPanel("Legend for: " + theLayerName));
		final HorizontalPanel hpLegend = new HorizontalPanel();

		String url = geoInformation.getServiceEndPoint() + "?service=WMS&" + "version="
				+ URLUtil.getValueOfParameter("version", wmsLink) + "&" + "request=GetLegendGraphic&" + "layer="
				+ theLayerName;

		String styleName = null;
		try {
			styleName = listBoxStyles.getSelectedValue();
		} catch (Exception e) {
		}

		styleName = styleName != null && !styleName.isEmpty() ? styleName : "";

		if (!geoInformation.isNcWMS()) {

			url += "&format=image/png" + "&STYLE=" + styleName
					+ "&LEGEND_OPTIONS=forceRule:True;dx:0.2;dy:0.2;mx:0.2;my:0.2;fontStyle:bold;"
					+ "borderColor:000000;border:true;fontColor:000000;fontSize:14";
		} else {

			int isNcWmsStyle = styleName.indexOf("/");
			if (isNcWmsStyle != -1) {
				styleName = styleName.substring(isNcWmsStyle + 1, styleName.length());
			}
			url += "&palette=" + styleName;
			if (geoInformation.getMapWMSNoStandard() != null) {
				for (String key : geoInformation.getMapWMSNoStandard().keySet()) { // ADDING COLORSCALERANGE?
					if (key.compareToIgnoreCase(COLORSCALERANGE) == 0) {
						url += "&" + key + "=" + geoInformation.getMapWMSNoStandard().get(key);
						break;
					}
				}
			}
		}

		GWT.log(url);
		flexTable.setStyleName("layer-style-panel-table-legend");
		flexTable.setWidget(0, 0, new Label("Legend"));
		final Image legendImage = new Image(url);
		legendImage.addLoadHandler(new LoadHandler() {
			@Override
			public void onLoad(LoadEvent event) {
			}
		});

		legendImage.addErrorHandler(new ErrorHandler() {
			@Override
			public void onError(ErrorEvent event) {
				GWT.log("ErrorEvent ");
				flexTable.setWidget(0, 1, new HTML("Error on loading the style"));
			}
		});

		hpLegend.add(legendImage);
		flexTable.setWidget(0, 1, hpLegend);
	}

}
