package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class ZoomOutEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 19, 2020
 */
public class ZoomOutOverMinimumEvent extends GwtEvent<ZoomOutOverMinimumEventHandler> {
	public static Type<ZoomOutOverMinimumEventHandler> TYPE = new Type<ZoomOutOverMinimumEventHandler>();
	private double startZoom;
	private double endZoom;
	private double minimum;

	/**
	 * Instantiates a new zoom out event.
	 *
	 * @param startZoom the start zoom
	 * @param endZoom the end zoom
	 * @param minimum the minimum
	 */
	public ZoomOutOverMinimumEvent(double startZoom, double endZoom, double minimum) {
		this.startZoom = startZoom;
		this.endZoom = endZoom;
		this.minimum = minimum;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ZoomOutOverMinimumEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ZoomOutOverMinimumEventHandler handler) {
		handler.onZoomOut(this);

	}

	public double getStartZoom() {
		return startZoom;
	}

	public double getEndZoom() {
		return endZoom;
	}
	
	public double getMinimum() {
		return minimum;
	}
	
	

}