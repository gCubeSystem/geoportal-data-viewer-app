package org.gcube.portlets.user.geoportaldataviewer.client.ui.dialogs;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.PublicLink;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.util.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.Fieldset;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.ModalFooter;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.Tooltip;
import com.github.gwtbootstrap.client.ui.constants.VisibilityChange;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class DialogShareableLink.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Sep 19, 2019
 */
public class DialogShareableLink extends Composite {

	private static DialogShareableLinkUiBinder uiBinder = GWT.create(DialogShareableLinkUiBinder.class);

	private Modal modalBox = new Modal();

	@UiField
	ControlGroup cgPublicLink;

	@UiField
	ControlGroup cgPrivateLongLink;

	@UiField
	ControlGroup cgPublicLongLink;

	@UiField
	TextBox textPublicLink;

	@UiField
	TextBox textPrivateLink;

	@UiField
	TextBox textPrivateLongLink;

	@UiField
	TextBox textPublicLongLink;

	@UiField
	Alert errorAlert;

	@UiField
	Alert actionAlert;

//	@UiField
//	Well alertFilePublicLink;

	@UiField
	Fieldset fieldSetPrivate;

	@UiField
	Fieldset fieldSetPublic;

	@UiField
	VerticalPanel fieldPrivateSharing;

//	@UiField
//	VerticalPanel filedEnableDisableSharing;

	@UiField
	Label labelLinkSharing;

	@UiField
	Button privateLinkCopyButton;

	@UiField
	Button privateLongLinkCopyButton;

	@UiField
	Button publicLinkCopyButton;

	@UiField
	Button publicLongLinkCopyButton;
//	
//	@UiField
//	Well wellPrivateLinkDescription;

	@UiField
	Button showPrivateLongLinkButton;

	@UiField
	Button showPublicLongLinkButton;

	@UiField
	HTMLPanel info_panel;

//	@UiField
//	HTMLPanel panelFieldsContainer;

	private GeoportalItemReferences geonItemRef;

	private String fileVersion;

	private boolean itemIsPublicStatus;

	private PublicLink openPublicLink;

	private PublicLink restrictedPublicLink;

	private final String privateShareToFileDescription = "By sharing the following Private Link "
			+ "with your coworkers, you will enact the users of the group the folder is shared with, "
			+ "to access the file and the shared folder content. Login required";

	private LoaderIcon loadingIcon = new LoaderIcon("loading...");
	
	/**
	 * The Interface DialogShareableLinkUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Sep 19, 2019
	 */
	interface DialogShareableLinkUiBinder extends UiBinder<Widget, DialogShareableLink> {
	}

	/**
	 * Instantiates a new dialog shareable link.
	 *
	 * @param itemRef the item ref
	 * @param version the version
	 */
	public DialogShareableLink(GeoportalItemReferences itemRef, String version) {
		initWidget(uiBinder.createAndBindUi(this));

		this.geonItemRef = itemRef;
		this.fileVersion = version;
		this.actionAlert.setAnimation(true);

		// cgRemovePublicLink.setVisible(false);
//		fieldSetPrivate.setVisible(false);
//		fieldPrivateSharing.setVisible(false);
		cgPublicLink.setVisible(true);

		fieldPrivateSharing.setVisible(false);
		fieldSetPrivate.setVisible(false);

		GeoportalDataViewerServiceAsync.Util.getInstance().getMyLogin(new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onSuccess(String result) {
				if (result != null) {
					fieldPrivateSharing.setVisible(true);
					fieldSetPrivate.setVisible(true);
				}
			}
		});

		showMessage("", false);
		loadingIcon.getElement().getStyle().setMarginBottom(20,Unit.PX);
		showLoadingIcon(true);

		// alertFilePublicLink.setVisible(true);
		loadAndShowPublicLinksForItem(itemRef);
		// getElement().setClassName("gwt-DialogBoxNew");
		modalBox.setTitle("Share Link...");
		ModalFooter modalFooter = new ModalFooter();
		final Button buttClose = new Button("Close");
		modalFooter.add(buttClose);

		buttClose.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				modalBox.hide();
			}
		});

		addEvents();

		textPrivateLink.setId(Random.nextInt() + Random.nextInt() + "");
		textPrivateLongLink.setId(Random.nextInt() + Random.nextInt() + "");
		textPublicLink.setId(Random.nextInt() + Random.nextInt() + "");
		textPublicLongLink.setId(Random.nextInt() + Random.nextInt() + "");

		modalBox.add(this);
		modalBox.add(modalFooter);
		modalBox.show();

	}

	/**
	 * Adds the events.
	 */
	private void addEvents() {

		showPrivateLongLinkButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				showPrivateLongLinkButton.setVisible(false);
				cgPrivateLongLink.setVisible(true);
				textPrivateLongLink.setText(restrictedPublicLink.getCompleteURL());
			}
		});

		showPublicLongLinkButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (openPublicLink != null && openPublicLink.getCompleteURL() != null) {
					showPublicLongLinkButton.setVisible(false);
					cgPublicLongLink.setVisible(true);
					textPublicLongLink.setText(openPublicLink.getCompleteURL());
				} else {
					textPublicLongLink.setText("Not available");
					showPublicLongLinkButton.setText("Not available");
					showPublicLongLinkButton.addStyleName("href-disabled");
					publicLongLinkCopyButton.setEnabled(false);
//					showPublicLongLinkButton.setVisible(false);
					disableTextBox(textPublicLongLink);
				}
			}
		});

		privateLinkCopyButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final Element elem = event.getRelativeElement();
				elem.setTitle("Copied to clipboard");
				Tooltip.changeVisibility(elem, VisibilityChange.TOGGLE.get());
				Timer timer = new Timer() {

					@Override
					public void run() {
						Tooltip.changeVisibility(elem, VisibilityChange.HIDE.get());
					}
				};

				timer.schedule(1000);
				copyToClipboard(textPrivateLink.getId());
			}
		});

		privateLinkCopyButton.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				final Element elem = event.getRelativeElement();
				elem.setTitle("Copy");
			}
		});

		privateLongLinkCopyButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				final Element elem = event.getRelativeElement();
				Tooltip.changeVisibility(elem, VisibilityChange.TOGGLE.get());
				Timer timer = new Timer() {

					@Override
					public void run() {
						Tooltip.changeVisibility(elem, VisibilityChange.HIDE.get());
					}
				};

				timer.schedule(1000);
				copyToClipboard(textPrivateLongLink.getId());
			}
		});

		privateLongLinkCopyButton.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				final Element elem = event.getRelativeElement();
				elem.setTitle("Copy");
			}
		});

		publicLinkCopyButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				final Element elem = event.getRelativeElement();
				Tooltip.changeVisibility(elem, VisibilityChange.TOGGLE.get());
				Timer timer = new Timer() {

					@Override
					public void run() {
						Tooltip.changeVisibility(elem, VisibilityChange.HIDE.get());
					}
				};

				timer.schedule(1000);
				copyToClipboard(textPublicLink.getId());
			}
		});

		publicLinkCopyButton.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				final Element elem = event.getRelativeElement();
				elem.setTitle("Copy");
			}
		});

		publicLongLinkCopyButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				final Element elem = event.getRelativeElement();
				Tooltip.changeVisibility(elem, VisibilityChange.TOGGLE.get());
				Timer timer = new Timer() {

					@Override
					public void run() {
						Tooltip.changeVisibility(elem, VisibilityChange.HIDE.get());
					}
				};

				timer.schedule(1000);
				copyToClipboard(textPublicLongLink.getId());
			}
		});

		publicLongLinkCopyButton.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				final Element elem = event.getRelativeElement();
				elem.setTitle("Copy");
			}
		});

		textPrivateLink.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				textPrivateLink.selectAll();
			}
		});

		textPrivateLongLink.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				textPrivateLongLink.selectAll();
			}
		});

		textPublicLongLink.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				textPublicLongLink.selectAll();
			}
		});

	}

	/**
	 * Copy to clipboard.
	 *
	 * @param myDivId the my div id
	 */
	private native void copyToClipboard(String myDivId) /*-{

		var copyText = $doc.getElementById(myDivId);
		//console.log("text copied is :" + copyText.value);
		copyText.select();
		//For mobile devices
		copyText.setSelectionRange(0, 99999);
		$doc.execCommand("copy");
		//alert("Copied the text: " + copyText.value);
	}-*/;

	/**
	 * Load and show public links for item.
	 *
	 * @param item the item
	 */
	private void loadAndShowPublicLinksForItem(GeoportalItemReferences item) {

		GeoportalDataViewerServiceAsync.Util.getInstance().getPublicLinksFor(item, new AsyncCallback<GeoportalItemReferences>() {

			@Override
			public void onSuccess(GeoportalItemReferences itemReferences) {
				showLoadingIcon(false);
				openPublicLink = itemReferences.getOpenLink();
				restrictedPublicLink = itemReferences.getRestrictedLink();
				String toPublicURL = openPublicLink.getShortURL() != null && !openPublicLink.getShortURL().isEmpty()
						? openPublicLink.getShortURL()
						: openPublicLink.getCompleteURL();

				textPublicLink.setValue(toPublicURL);
				textPublicLongLink.setValue(openPublicLink.getCompleteURL());

				String toPrivateURL = restrictedPublicLink.getShortURL() != null
						&& !restrictedPublicLink.getShortURL().isEmpty() ? restrictedPublicLink.getShortURL()
								: restrictedPublicLink.getCompleteURL();

				textPrivateLink.setValue(toPrivateURL);
				textPrivateLongLink.setValue(restrictedPublicLink.getCompleteURL());
			}

			@Override
			public void onFailure(Throwable caught) {
				showLoadingIcon(false);
				openPublicLink = null;
				disableTextBox(textPublicLink);
				disableTextBox(textPrivateLink);
				showError(caught.getMessage());
			}
		});
	}
	
	public void showLoadingIcon(boolean show) {
		try {
			if(show)
				info_panel.add(loadingIcon);
			else
				info_panel.remove(loadingIcon);
		}catch (Exception e) {

		}
	}

	/**
	 * Disable text box.
	 *
	 * @param textBox the text box
	 */
	private void disableTextBox(TextBox textBox) {
		textBox.setEnabled(false);
		textBox.getElement().getStyle().setOpacity(0.3);
	}

	/**
	 * Show error.
	 *
	 * @param msg the msg
	 */
	private void showError(String msg) {
		errorAlert.setVisible(true);
		errorAlert.setText(msg);
	}

	/**
	 * Show message.
	 *
	 * @param msg     the msg
	 * @param visible the visible
	 */
	private void showMessage(String msg, boolean visible) {
		actionAlert.setVisible(visible);
		actionAlert.setText(msg == null ? "" : msg);

	}

}
