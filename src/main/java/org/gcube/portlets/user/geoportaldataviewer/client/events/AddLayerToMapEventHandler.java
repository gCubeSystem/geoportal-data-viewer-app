package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface AddLayerToMapEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 9, 2022
 */
public interface AddLayerToMapEventHandler extends EventHandler {

	/**
	 * On adding layer for project.
	 *
	 * @param addLayerToMapEvent the add layer to map event
	 */
	void onAddingLayerForProject(AddLayerToMapEvent addLayerToMapEvent);
}
