package org.gcube.portlets.user.geoportaldataviewer.client.ui;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.ModalFooter;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;


/**
 * The Class ModalWindow.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 20, 2020
 */
public class ModalWindow {
	
	private Modal modal;
	private Object caller;
	
	/**
	 * Instantiates a new modal window.
	 *
	 * @param title the title
	 * @param width the width
	 * @param maxHeight the max height
	 */
	public ModalWindow(String title, int width, int maxHeight) {
		
		modal = new Modal(false);
		modal.hide(false);
		modal.setTitle(title);
		modal.setCloseVisible(true);
		modal.setWidth(width);
		modal.setMaxHeigth(maxHeight+"px");
		
		ModalFooter modalFooter = new ModalFooter();
		final Button buttClose = new Button("Close");
		modalFooter.add(buttClose);
		
		buttClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				modal.hide();
			}
		});
		modal.add(modalFooter);
		
	}
	
	public void add(Widget toAdd) {
		modal.add(toAdd);
	}
	
	public void setCaller(Object caller) {
		this.caller = caller;
	}
	
	public void show() {
		modal.show();
	}
	
	public void setWidth(int width) {
		modal.setWidth(width);
	}

}
