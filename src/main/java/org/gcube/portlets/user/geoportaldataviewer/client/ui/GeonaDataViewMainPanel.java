package org.gcube.portlets.user.geoportaldataviewer.client.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.ORDER;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GroupedLayersDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.ConfiguredLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.LayerIDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewer;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.GisToolbarFacilities;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants.MAP_PROJECTION;
import org.gcube.portlets.user.geoportaldataviewer.client.events.ChangeMapLayerEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.MapExtentToEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.collections.OpenCollectionEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayerMap;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalIcons;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalImages;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.search.SearchFacilityUI;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.crossfiltering.CrossFilteringLayerPanel;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.customoverlays.OverlayCustomLayerPanel;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.layercollection.LayerCollectionPanel;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;
import org.gcube.portlets.user.geoportaldataviewer.client.util.URLUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.GCubeCollection;
import org.gcube.portlets.user.geoportaldataviewer.shared.ItemFieldsResponse;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.CheckBox;
import com.github.gwtbootstrap.client.ui.DropdownButton;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class GeonaDataViewMainPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 19, 2020
 */
public class GeonaDataViewMainPanel extends Composite {



	private static final String PLACEHOLDER_SELECT_SEARCH_IN = "Select Collection...";

	private static GeonaDataViewMainPanelUiBinder uiBinder = GWT.create(GeonaDataViewMainPanelUiBinder.class);

	/**
	 * The Interface GeonaDataViewMainPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Nov 19, 2020
	 */
	interface GeonaDataViewMainPanelUiBinder extends UiBinder<Widget, GeonaDataViewMainPanel> {
	}

	@UiField
	HTMLPanel mainContainerPanel;

	@UiField
	HTMLPanel mainToolBar;

	@UiField
	NavLink dataPointSelection;

	@UiField
	NavLink dataBoxSelection;

	@UiField
	Button removeQuery;

	@UiField
	DetailsPanel detailsPanel;

	@UiField
	Button extentToItaly;

	@UiField
	Button extentToEarth;

	@UiField
	Button linkLayers;

	@UiField
	DropdownButton openCollectionDropDown;

	@UiField
	DropdownButton linkCustomOverlayLayers;

	@UiField
	DropdownButton linkCrossFilteringLayers;

	@UiField
	HTMLPanel panelCustomOverlayLayers;

	@UiField
	HTMLPanel panelCrossFilteringLayers;

	@UiField
	HTMLPanel openCollectionPanel;

	@UiField
	DropdownButton searchFacilityButton;

	@UiField
	ListBox navListSearch;

	//@UiField
	//ScrollPanel overlayLayersPanel;
	
	RootPanel overlayLayersPanel = RootPanel.get(GeoportalDataViewer.DIV_LAYER_VIEW);

	@UiField
	ScrollPanel searchFacilityPanel;

	@UiField
	HTMLPanel panelAttribution;

	@UiField
	HTMLPanel panelBaseLayers;

	@UiField
	DropdownButton linkMap;

	@UiField
	DropdownButton linkPresetLocation;

	private MapPanel mapPanel;

	private OpenLayerMap map;

	private HandlerManager applicationBus;

	private HashMap<String, List<CheckBox>> mapCollectionCheckBoxes;

	private SearchFacilityUI currentSearchFacility;

	private HashMap<String, ItemFieldsResponse> mapItemFieldsForUCD = new HashMap<String, ItemFieldsResponse>();

	public GeonaDataViewMainPanel(HandlerManager applicationBus, int mapHeight,
			List<ItemFieldsResponse> itemFieldsReponse) {
		initWidget(uiBinder.createAndBindUi(this));
		this.applicationBus = applicationBus;
		mapPanel = new MapPanel(mapHeight + "px");
		detailsPanel.setHeight(mapHeight + "px");
		detailsPanel.setApplicationBus(applicationBus);
		overlayLayersPanel.setVisible(false);
		mainContainerPanel.add(mapPanel);
		bindHandlers();
		dataPointSelection.setIcon(IconType.SCREENSHOT);
		dataBoxSelection.setIcon(IconType.BOOKMARK);
		removeQuery.setIcon(IconType.REMOVE);

		Image italyImg = new Image(GeoportalImages.ICONS.italyIcon());
		italyImg.getElement().getStyle().setPaddingLeft(20, Unit.PX);
		extentToItaly.getElement().appendChild(italyImg.getElement());
		extentToItaly.setWidth("140px");

		Image worldImg = new Image(GeoportalImages.ICONS.worldIcon());
		worldImg.getElement().getStyle().setPaddingLeft(20, Unit.PX);
		extentToEarth.getElement().appendChild(worldImg.getElement());
		extentToEarth.setWidth("140px");

		// linkMap.setCustomIconStyle(GeoportalIcons.CustomIconType.MAP.get());
		linkPresetLocation.setCustomIconStyle(GeoportalIcons.CustomIconType.PRESET_LOCATION.get());
		linkLayers.setCustomIconStyle(GeoportalIcons.CustomIconType.LAYERS.get());

		openCollectionDropDown.setIcon(IconType.COMPASS);

		mapItemFieldsForUCD.clear();

		SearchingFilter initialSortFilter = new SearchingFilter();
		initialSortFilter.setOrder(ORDER.ASC);

		navListSearch.addItem(PLACEHOLDER_SELECT_SEARCH_IN, PLACEHOLDER_SELECT_SEARCH_IN);

		navListSearch.getElement().getFirstChildElement().setAttribute("disabled", "disabled");

		for (final ItemFieldsResponse itemFieldResp : itemFieldsReponse) {

			UseCaseDescriptorDV ucd = itemFieldResp.getUseCaseDescriptorDV();
			mapItemFieldsForUCD.put(ucd.getId(), itemFieldResp);
			GWT.log("Added navLink for: " + ucd.getName());
			navListSearch.addItem(ucd.getName(), ucd.getProfileID());
		}

		navListSearch.addChangeHandler(new ChangeHandler() {

			private SearchFacilityUI currentSearchFacility;

			@Override
			public void onChange(ChangeEvent event) {

				if (this.currentSearchFacility != null)
					this.currentSearchFacility.resetCurrentSearch();

				String ucdProfileID = navListSearch.getValue(navListSearch.getSelectedIndex());
				GWT.log("UCD ProfileID selected: " + ucdProfileID);

				if (ucdProfileID.compareTo(PLACEHOLDER_SELECT_SEARCH_IN) != 0) {

					searchFacilityPanel.clear();
					ItemFieldsResponse itemFieldResp = mapItemFieldsForUCD.get(ucdProfileID);

					List<ItemFieldDV> result = itemFieldResp.getListItemFields();
					List<ItemFieldDV> displayFields = new ArrayList<ItemFieldDV>();
					List<ItemFieldDV> searchByFields = new ArrayList<ItemFieldDV>();
					List<ItemFieldDV> sortByFields = new ArrayList<ItemFieldDV>();
					for (ItemFieldDV itemField : result) {
						if (itemField.isDisplayAsResult()) {
							displayFields.add(itemField);
						}

						if (itemField.isSearchable()) {
							searchByFields.add(itemField);
						}

						if (itemField.isSortable()) {
							sortByFields.add(itemField);
						}
					}

					this.currentSearchFacility = new SearchFacilityUI(ucdProfileID, applicationBus, displayFields,
							sortByFields, searchByFields, initialSortFilter);
					searchFacilityPanel.add(currentSearchFacility);
					currentSearchFacility.setSearchButton(searchFacilityButton);
				}

			}
		});

		searchFacilityButton.setIcon(IconType.SEARCH);

		if (itemFieldsReponse.size() == 0) {
			searchFacilityPanel.setVisible(false);
			searchFacilityButton.setVisible(false);
		}

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				searchFacilityPanel.getElement().getParentElement().addClassName("opacity-09");
				panelCustomOverlayLayers.getElement().getParentElement().addClassName("opacity-09");
				panelCrossFilteringLayers.getElement().getParentElement().addClassName("opacity-09");

			}

		});
		
		openCollectionDropDown.setTitle(GisToolbarFacilities.COLLECTION.getTooltip());
		linkCustomOverlayLayers.setTitle(GisToolbarFacilities.OVERLAY.getTooltip());
		linkCrossFilteringLayers.setTitle(GisToolbarFacilities.FILTER.getTooltip());
		searchFacilityButton.setTitle(GisToolbarFacilities.SEARCH.getTooltip());
		linkLayers.setTitle(GisToolbarFacilities.LAYERS.getTooltip());
		linkMap.setTitle(GisToolbarFacilities.MAP.getTooltip());
		linkPresetLocation.setTitle(GisToolbarFacilities.PRESET_LOCATION.getTooltip());

		// layersDDB.setToggle(true);
		bindEvents();
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		linkLayers.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				GWT.log("layersDDB clicked");

				if (overlayLayersPanel.isVisible()) {
					overlayLayersPanel.setVisible(false);
				} else {
					overlayLayersPanel.setVisible(true);
				}
			}
		});

		searchFacilityButton.addDomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// event.preventDefault();
				event.stopPropagation();

			}
		}, ClickEvent.getType());

		openCollectionDropDown.addDomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// event.preventDefault();
				event.stopPropagation();

			}
		}, ClickEvent.getType());

		linkCustomOverlayLayers.addDomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// event.preventDefault();
				event.stopPropagation();

			}
		}, ClickEvent.getType());

		linkCrossFilteringLayers.addDomHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// event.preventDefault();
				event.stopPropagation();

			}
		}, ClickEvent.getType());
	}

	/**
	 * Sets the panels height.
	 *
	 * @param height the new panels height
	 */
	public void setPanelsHeight(int height) {
		String toH = height + "px";
		mapPanel.setMapHeight(height);
		detailsPanel.setHeight(toH);

		setOverlayPanelMaxHeight();

	}
	
	public void setPanelsWidth(int clientWidth) {
		if(clientWidth<GeoportalDataViewerConstants.TABLET_WIDTH) {
			openCollectionDropDown.setText("");
			linkCustomOverlayLayers.setText("");
			linkCrossFilteringLayers.setText("");
			searchFacilityButton.setText("");
			linkLayers.setText("");
			linkPresetLocation.setText("");
			linkMap.setText("");
		}else {
			openCollectionDropDown.setText(GisToolbarFacilities.COLLECTION.getName());
			linkCustomOverlayLayers.setText(GisToolbarFacilities.OVERLAY.getName());
			linkCrossFilteringLayers.setText(GisToolbarFacilities.FILTER.getName());
			searchFacilityButton.setText(GisToolbarFacilities.SEARCH.getName());
			linkLayers.setText(GisToolbarFacilities.LAYERS.getName());
			linkMap.setText(GisToolbarFacilities.MAP.getName());
			linkPresetLocation.setText(GisToolbarFacilities.PRESET_LOCATION.getName());
		}
		
	}

	/**
	 * Sets the overlay panel max height.
	 */
	private void setOverlayPanelMaxHeight() {

		String overlMH = mapPanel.getOffsetHeight() - 130 + "px";
		GWT.log("overlayLayersPanel maxHeight: " + overlMH);
		overlayLayersPanel.getElement().getStyle().setProperty("maxHeight", overlMH);
	}

	/**
	 * Gets the map panel.
	 *
	 * @return the map panel
	 */
	public MapPanel getMapPanel() {
		return mapPanel;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the new map
	 */
	public void setMap(OpenLayerMap map) {
		this.map = map;
	}

	/**
	 * Bind handlers.
	 */
	public void bindHandlers() {

		dataPointSelection.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				map.removeQueryInteractions();
				GWT.log("dataPointSelection.isActive() " + dataPointSelection.isActive());
				if (!dataPointSelection.isActive()) {
					map.addPointVectorSource();
				}
				removeQuery.setVisible(true);
			}
		});

		dataBoxSelection.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				map.removeQueryInteractions();
				GWT.log("dataBoxSelection.isActive() " + dataBoxSelection.isActive());
				if (!dataBoxSelection.isActive()) {
					map.addExtentInteraction();
				}
				removeQuery.setVisible(true);
			}
		});

		removeQuery.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				map.removeQueryInteractions();
				removeQuery.setVisible(false);
			}
		});

		extentToItaly.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Location italyLocation = ExtentMapUtil.getLocation(ExtentMapUtil.PLACE.ITALY);
				applicationBus.fireEvent(new MapExtentToEvent(italyLocation));
				map.setCenter(italyLocation.getCoordinate(MAP_PROJECTION.EPSG_3857));
				map.setZoom(italyLocation.getZoomLevel());

			}
		});

		extentToEarth.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Location earthLocation = ExtentMapUtil.getLocation(ExtentMapUtil.PLACE.WORLD);
				applicationBus.fireEvent(new MapExtentToEvent(earthLocation));
				map.setCenter(earthLocation.getCoordinate(MAP_PROJECTION.EPSG_3857));
				map.setZoom(earthLocation.getZoomLevel());

			}
		});

	}

	/**
	 * Show as details.
	 *
	 * @param result       the result
	 * @param geonaItemRef the geona item ref
	 */
	public void showAsDetails(ProjectView result, GeoportalItemReferences geonaItemRef) {
		detailsPanel.showDetailsFor(result, geonaItemRef);

	}

	/**
	 * Hide panel details.
	 */
	public void hidePanelDetails() {
		detailsPanel.hidePanelDetails();

	}

	/**
	 * Gets the displayed record.
	 *
	 * @return the displayed record
	 */
	public ProjectView getDisplayedProject() {
		return detailsPanel.getDisplayedProject();
	}

	/**
	 * Gets the layers DDB.
	 *
	 * @return the layers DDB
	 */
	public Button getLayersDDB() {
		return linkLayers;
	}

	/**
	 * Show overlay layers.
	 *
	 * @param panel the panel
	 */
	public void showOverlayLayers(FlowPanel panel) {
		GWT.log("showOverlayLayers");
		// layersDDB.setVisible(true);
		overlayLayersPanel.setVisible(true);
		overlayLayersPanel.clear();

		setOverlayPanelMaxHeight();
		overlayLayersPanel.add(panel);
		// layersDDBClickEvent();
	}

	/**
	 * Layers DDB click event.
	 */
	private void layersDDBClickEvent() {
		linkLayers.fireEvent(new GwtEvent<ClickHandler>() {
			@Override
			public com.google.gwt.event.shared.GwtEvent.Type<ClickHandler> getAssociatedType() {
				return ClickEvent.getType();
			}

			@Override
			protected void dispatch(ClickHandler handler) {
				handler.onClick(null);
			}
		});
	}

	/**
	 * Hide overlay layers.
	 */
	public void hideOverlayLayers() {
		GWT.log("hideOverlayLayers");
		overlayLayersPanel.clear();
		// layersDDB.setVisible(false);
		overlayLayersPanel.setVisible(false);
		// layersDDBClickEvent();
	}

	/**
	 * Sets the map attribution.
	 *
	 * @param attribution the new map attribution
	 */
	public void setMapAttribution(String attribution) {
		panelAttribution.clear();
		Label label = new Label("Base Map Credits");
		Paragraph p = new Paragraph(attribution);
		panelAttribution.add(label);
		panelAttribution.add(p);
	}

	/**
	 * Sets the base layers.
	 *
	 * @param listBaseLayers the new base layers
	 */

	public void setAvailableCollections(Collection<GCubeCollection> collection, String openCollectionID) {
		GWT.log("Init Available collections menu");
		if (collection == null || collection.isEmpty())
			GWT.log("!!! No Available collections");

		mapCollectionCheckBoxes = new HashMap<String, List<CheckBox>>();

		for (GCubeCollection coll : collection) {
			GWT.log("Found available collection " + coll);
			final String collectionID = coll.getUcd().getId(); // collectionID == UCD_Id

			LayerCollectionPanel lcp = new LayerCollectionPanel(coll, applicationBus);

			openCollectionPanel.add(lcp);

			List<CheckBox> listCollections = mapCollectionCheckBoxes.get(collectionID);
			if (listCollections == null)
				listCollections = new ArrayList<CheckBox>();

			listCollections.add(lcp.getCheckbox());
			mapCollectionCheckBoxes.put(collectionID, listCollections);

			// Opening All Collections if openCollectionID is null
			if (openCollectionID == null) {
				lcp.getCheckbox().setValue(true, true);
				// String collectionID = checkbox.getId().replace("gcubeCollectionSelector_",
				// "");
				GWT.log("Opening the collection: " + collectionID);
				applicationBus.fireEvent(new OpenCollectionEvent(collectionID));
			}

		}

		// Opening All Collections if openCollectionID is null
		if (openCollectionID != null) {

			List<CheckBox> listCheckBoxes = mapCollectionCheckBoxes.get(openCollectionID);

			if (listCheckBoxes != null) {
				for (CheckBox checkBox : listCheckBoxes) {
					checkBox.setValue(true, true);
					String collectionID = checkBox.getId().replace("gcubeCollectionSelector_", "");
					GWT.log("Opening the input collection: " + collectionID);
					applicationBus.fireEvent(new OpenCollectionEvent(collectionID));
				}
			}
		}
	}

	public void openCollectionMenu() {

		GWT.log("openCollectionMenu");
		GeoportalDataViewerConstants.clickElement(openCollectionDropDown.getElement().getFirstChildElement());
	}

	public void setBaseLayers(List<BaseMapLayer> listBaseLayers) {

		if (listBaseLayers == null)
			return;

		int i = 0;

		for (BaseMapLayer baseMapLayer : listBaseLayers) {

			RadioButton radio = new RadioButton("base-layer");
			radio.setText(baseMapLayer.getName());
			if (i == 0) {
				radio.setValue(true, false);
			}

			radio.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					applicationBus.fireEvent(new ChangeMapLayerEvent(baseMapLayer));

				}
			});

			SimplePanel sp = new SimplePanel(radio);
			sp.getElement().addClassName("form-inline");
			sp.getElement().addClassName("map-layers-radio");
			panelBaseLayers.add(sp);
			i++;
		}

	}

	public void addOverlayLayers(GCubeCollection collection,
			List<GroupedLayersDV<? extends LayerIDV>> listGroupedLayers) {

		if (listGroupedLayers == null)
			return;

		com.github.gwtbootstrap.client.ui.Label collectionGroup = new com.github.gwtbootstrap.client.ui.Label(
				collection.getUcd().getName());
		collectionGroup.setType(LabelType.WARNING);
		LayoutPanel lc = new LayoutPanel();
		lc.add(collectionGroup);
		panelCustomOverlayLayers.add(lc);

		for (GroupedLayersDV gCustomLayerDV : listGroupedLayers) {
			if (gCustomLayerDV.getListCustomLayers() != null) {
				FlowPanel lcInner = new FlowPanel();
				lcInner.getElement().getStyle().setMarginLeft(5.0, Unit.PX);
				com.github.gwtbootstrap.client.ui.Label labelGroup = new com.github.gwtbootstrap.client.ui.Label(
						gCustomLayerDV.getName());
				labelGroup.setType(LabelType.INFO);
				String descr = gCustomLayerDV.getDescription() != null && !gCustomLayerDV.getDescription().isEmpty()
						? gCustomLayerDV.getDescription()
						: gCustomLayerDV.getName();
				labelGroup.setTitle(descr);
				lcInner.add(labelGroup);
				if (descr.compareTo(gCustomLayerDV.getName()) != 0) {
					HTML overlayGroupDescr = new HTML();
					overlayGroupDescr.getElement().setClassName("overlay-panel-style-description");
					overlayGroupDescr.setHTML(descr);
					lcInner.add(overlayGroupDescr);
				}
				List<ConfiguredLayerDV> list = gCustomLayerDV.getListCustomLayers();
				for (ConfiguredLayerDV customLayer : list) {

					if (customLayer.isDisplay()) {
						LayerItem layerItem = new LayerItem();
						if (customLayer.getWMS_URL() != null) {
							final String mapServerHost = URLUtil.getPathURL(customLayer.getWMS_URL());
							GWT.log("mapServerHost: " + mapServerHost);
							layerItem.setMapServerHost(mapServerHost);
							layerItem.setBaseLayer(false);
							layerItem.setName(customLayer.getName());
							layerItem.setTitle(customLayer.getTitle());
							layerItem.setWmsLink(customLayer.getWMS_URL());
							// panelCustomOverlayLayers.add(new CrossFilteringLayerPanel(layerItem,
							// applicationBus));
							lcInner.add(new OverlayCustomLayerPanel(layerItem, applicationBus));
							map.addGroupedCustomWMSLayer(layerItem);
						}
					}
				}
				panelCustomOverlayLayers.add(lcInner);
			}

		}

		linkCustomOverlayLayers.setVisible(listGroupedLayers.size() > 0);

	}

	public void addCrossFilteringLayers(GCubeCollection collection,
			List<GroupedLayersDV<? extends LayerIDV>> listGroupedLayers) {

		if (listGroupedLayers == null)
			return;

		com.github.gwtbootstrap.client.ui.Label collectionGroup = new com.github.gwtbootstrap.client.ui.Label(
				collection.getUcd().getName());
		collectionGroup.setType(LabelType.WARNING);
		LayoutPanel lc = new LayoutPanel();
		lc.add(collectionGroup);
		panelCrossFilteringLayers.add(lc);
		for (GroupedLayersDV<? extends LayerIDV> groupedLayerDV : listGroupedLayers) {
			FlowPanel lcInner = new FlowPanel();
			lcInner.getElement().getStyle().setMarginLeft(5.0, Unit.PX);
			com.github.gwtbootstrap.client.ui.Label labelGroup = new com.github.gwtbootstrap.client.ui.Label(
					groupedLayerDV.getName());
			labelGroup.setType(LabelType.INFO);
			String descr = groupedLayerDV.getDescription() != null && !groupedLayerDV.getDescription().isEmpty()
					? groupedLayerDV.getDescription()
					: groupedLayerDV.getName();
			labelGroup.setTitle(descr);
			lcInner.add(labelGroup);
			// panelCustomOverlayLayers.add(labelGroup);
			if (descr.compareTo(groupedLayerDV.getName()) != 0) {
				HTML overlayGroupDescr = new HTML();
				overlayGroupDescr.getElement().setClassName("filter-panel-style-description");
				overlayGroupDescr.setHTML(descr);
				lcInner.add(overlayGroupDescr);
				// panelCustomOverlayLayers.add(overlayGroupDescr);
			}
			CrossFilteringLayerPanel cfp = new CrossFilteringLayerPanel(collection, groupedLayerDV, applicationBus);
			cfp.setFilterButton(linkCrossFilteringLayers);
			lcInner.add(cfp);
			panelCrossFilteringLayers.add(lcInner);
		}

		linkCrossFilteringLayers.setVisible(listGroupedLayers.size() > 0);

	}



}
