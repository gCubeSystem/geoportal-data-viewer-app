package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface TimelineProjectRelationsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 3, 2022
 */
public interface TimelineProjectRelationsEventHandler extends EventHandler {

	/**
	 * On timeline relation event.
	 *
	 * @param timelineProjectRelationsEvent the timeline project relations event
	 */
	void onTimelineRelationEvent(TimelineProjectRelationsEvent timelineProjectRelationsEvent);
}
