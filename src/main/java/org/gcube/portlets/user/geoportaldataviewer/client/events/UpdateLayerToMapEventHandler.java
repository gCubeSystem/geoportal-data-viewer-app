package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface UpdateLayerToMapEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2023
 */
public interface UpdateLayerToMapEventHandler extends EventHandler {

	/**
	 * On update layer.
	 *
	 * @param updateLayerToMapEvent the update layer to map event
	 */
	void onUpdateLayer(UpdateLayerToMapEvent updateLayerToMapEvent);
}
