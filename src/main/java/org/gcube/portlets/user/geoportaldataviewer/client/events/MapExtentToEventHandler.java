package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface MapExtentToEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Sep 1, 2021
 */
public interface MapExtentToEventHandler extends EventHandler {

	/**
	 * On extent event.
	 *
	 * @param mapExtentToEvent the map extent to event
	 */
	void onExtentEvent(MapExtentToEvent mapExtentToEvent);
}
