package org.gcube.portlets.user.geoportaldataviewer.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;

public class ViewerConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7838513808590351819L;

	public ViewerConfiguration() {

	}

	// TODO BaseLayers
	public List<BaseMapLayer> baseLayers;

	public Map<String, GCubeCollection> availableCollections;

	public List<BaseMapLayer> getBaseLayers() {
		return baseLayers;
	}

	public void setBaseLayers(List<BaseMapLayer> baseLayers) {
		this.baseLayers = baseLayers;
	}

	public Map<String, GCubeCollection> getAvailableCollections() {
		return availableCollections;
	}

	public void setAvailableCollections(Map<String, GCubeCollection> availableCollections) {
		this.availableCollections = availableCollections;
	}
}
