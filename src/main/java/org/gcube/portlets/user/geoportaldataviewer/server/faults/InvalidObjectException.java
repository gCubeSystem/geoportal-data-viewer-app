package org.gcube.portlets.user.geoportaldataviewer.server.faults;

public class InvalidObjectException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7577708930172470038L;

	public InvalidObjectException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidObjectException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidObjectException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidObjectException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidObjectException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
