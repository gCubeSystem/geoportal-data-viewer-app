package org.gcube.portlets.user.geoportaldataviewer.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.IndexLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;

public class GCubeCollection implements Serializable, Comparable<GCubeCollection> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3915546050671671465L;

	private UseCaseDescriptorDV ucd;
	private List<IndexLayerDV> indexes;

	public GCubeCollection() {
	}

	public UseCaseDescriptorDV getUcd() {
		return ucd;
	}

	public List<IndexLayerDV> getIndexes() {
		return indexes;
	}

	public void setUcd(UseCaseDescriptorDV ucd) {
		this.ucd = ucd;
	}

	public void setIndexes(List<IndexLayerDV> indexes) {
		this.indexes = indexes;
	}

	@Override
	public String toString() {
		return "GCubeCollection [ucd=" + ucd + ", indexes=" + indexes + "]";
	}

	@Override
	public int compareTo(GCubeCollection o) {

		if (this == null || this.ucd == null)
			return 1;

		if (o == null || o.getUcd() == null)
			return -1;

		if (this.ucd.getName() == null)
			return 1;

		if (o.getUcd().getName() == null)
			return -1;

		return this.getUcd().getName().compareTo(o.getUcd().getName());

	}

}
