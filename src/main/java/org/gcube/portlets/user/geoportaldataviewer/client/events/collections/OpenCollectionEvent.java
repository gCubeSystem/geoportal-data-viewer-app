package org.gcube.portlets.user.geoportaldataviewer.client.events.collections;

import com.google.gwt.event.shared.GwtEvent;

public class OpenCollectionEvent extends GwtEvent<OpenCollectionEventHandler> {

	private String collectionId = null;

	public static Type<OpenCollectionEventHandler> TYPE = new Type<OpenCollectionEventHandler>();

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<OpenCollectionEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(OpenCollectionEventHandler handler) {
		handler.onOpenCollection(this);

	}

	public OpenCollectionEvent(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getCollectionId() {
		return collectionId;
	}

}
