package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class AddedLayerToMapEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 18, 2020
 */
public class AddedLayerToMapEvent extends GwtEvent<AddedLayerToMapEventHandler> {
	public static Type<AddedLayerToMapEventHandler> TYPE = new Type<AddedLayerToMapEventHandler>();
	private LayerItem layerItem;
	private LAYER_TYPE layerType;

	/**
	 * The Enum LAYER_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Oct 11, 2021
	 */
	public enum LAYER_TYPE {
		BASE, OVERLAY
	}

	/**
	 * Instantiates a new added layer to map event.
	 *
	 * @param layerItem the layer item
	 * @param layerType the layer type
	 */
	public AddedLayerToMapEvent(LayerItem layerItem, LAYER_TYPE layerType) {
		this.layerItem = layerItem;
		this.layerType = layerType;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<AddedLayerToMapEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(AddedLayerToMapEventHandler handler) {
		handler.onLayerRendered(this);

	}

	/**
	 * Gets the layer item.
	 *
	 * @return the layer item
	 */
	public LayerItem getLayerItem() {
		return layerItem;
	}

	/**
	 * Gets the layer type.
	 *
	 * @return the layer type
	 */
	public LAYER_TYPE getLayerType() {
		return layerType;
	}

}