package org.gcube.portlets.user.geoportaldataviewer.server;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.gcube.common.encryption.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer.OL_BASE_MAP;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GNABaseMapsResourceReader.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 18, 2021
 */
public class GNABaseMapsResourceReader {

	private static Logger LOG = LoggerFactory.getLogger(GNABaseMapsResourceReader.class);

	private static final String SE_PROFILE_NAME = "GNABaseMaps";
	private static final String SE_CATEGORY_NAME = "Application";

	/**
	 * Gets the list base maps.
	 *
	 * @return the list base maps
	 * @throws Exception the exception
	 */
	public static List<BaseMapLayer> getListBaseMaps() throws Exception {

		List<BaseMapLayer> lstBaseMaps = new ArrayList<BaseMapLayer>();

		LOG.info("Searching SE in the scope: " + ScopeProvider.instance.get() + " with profile name: " + SE_PROFILE_NAME
				+ " and category name: " + SE_CATEGORY_NAME);

		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Name/text() eq '" + SE_PROFILE_NAME + "'");
		query.addCondition("$resource/Profile/Category/text() eq '" + SE_CATEGORY_NAME + "'");

		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> resources = client.submit(query);

		if (resources.size() > 0)
			LOG.info("The query returned " + resources.size() + " ServiceEndpoint/s");
		else
			throw new RuntimeException("ServiceEndpoint not found. Searching for profile name: " + SE_PROFILE_NAME
					+ " and category name: " + SE_CATEGORY_NAME + ", in the scope: " + ScopeProvider.instance.get());

		ServiceEndpoint se = resources.get(0);
		Collection<AccessPoint> theAccessPoints = se.profile().accessPoints().asCollection();
		for (AccessPoint accessPoint : theAccessPoints) {
			BaseMapLayer bml = new BaseMapLayer();
			bml.setName(accessPoint.name());
			bml.setAttribution(accessPoint.description());
			String url = accessPoint.address();
			String apiTokenName = accessPoint.username();
			String apiTokenPwd = accessPoint.password();
			LOG.debug("Found API_TOKEN name: " + apiTokenName + ", encrypted token: " + apiTokenPwd);
			// decrypting the pwd
			try {
				if (isValorized(apiTokenPwd)) {
					apiTokenPwd = StringEncrypter.getEncrypter().decrypt(apiTokenPwd);
					LOG.debug("Token decrypted is: " + apiTokenPwd.substring(0, apiTokenPwd.length() / 2)
							+ "_MASKED_TOKEN_");
				}
			} catch (Exception e) {
				throw new RuntimeException("Error on decrypting the pwd: ", e);
			}
			if (isValorized(apiTokenName) && isValorized(apiTokenPwd)) {
				url = String.format("%s?%s=%s", url, apiTokenName, apiTokenPwd);
			}
			bml.setUrl(url);

			// SETTING MAP TYPE
			String urlLower = bml.getUrl().toLowerCase();
			if (urlLower.contains("mapbox")) {
				bml.setType(OL_BASE_MAP.MAPBOX);
			} else if (urlLower.contains("openstreetmap")) {
				bml.setType(OL_BASE_MAP.OSM);
			} else
				bml.setType(OL_BASE_MAP.OTHER);

			lstBaseMaps.add(bml);
		}
		
		LOG.debug("Read base maps: "+lstBaseMaps);
		LOG.info("Returning " + lstBaseMaps.size() + " base maps");
		return lstBaseMaps;

	}
	
	private static boolean isValorized(String value) {
		if (value != null && !value.isEmpty()) {
			return true;
		}
		return false;
	}

}
