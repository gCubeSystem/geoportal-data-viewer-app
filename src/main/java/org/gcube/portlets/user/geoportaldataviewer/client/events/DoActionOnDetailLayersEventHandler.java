package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface DoActionOnDetailLayersEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 8, 2021
 */
public interface DoActionOnDetailLayersEventHandler extends EventHandler {

	/**
	 * On do action on detail layers.
	 *
	 * @param doActionOnDetailLayersEvent the do action on detail layers event
	 */
	void onDoActionOnDetailLayers(DoActionOnDetailLayersEvent doActionOnDetailLayersEvent);
}
