/**
 *
 */
package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * The Class FeatureRow.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 29, 2020
 */
public class FeatureRow implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6254861811998867626L;

	private String id;

	private Map<String, List<String>> mapProperties;

	private FeatureGeometry geometry;

	private String crsName;

	public FeatureRow() {
	}

	public FeatureRow(Map<String, List<String>> mapProperties, FeatureGeometry geometry) {
		super();
		this.mapProperties = mapProperties;
		this.setGeometry(geometry);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, List<String>> getMapProperties() {
		return mapProperties;
	}

	public void setMapProperties(Map<String, List<String>> mapProperties) {
		this.mapProperties = mapProperties;
	}

	public String getCrsName() {
		return crsName;
	}

	public void setCrsName(String crsName) {
		this.crsName = crsName;
	}

	public FeatureGeometry getGeometry() {
		return geometry;
	}

	public void setGeometry(FeatureGeometry geometry) {
		this.geometry = geometry;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeatureRow [id=");
		builder.append(id);
		builder.append(", mapProperties=");
		builder.append(mapProperties);
		builder.append(", geometry=");
		builder.append(geometry);
		builder.append(", crsName=");
		builder.append(crsName);
		builder.append("]");
		return builder.toString();
	}

}
