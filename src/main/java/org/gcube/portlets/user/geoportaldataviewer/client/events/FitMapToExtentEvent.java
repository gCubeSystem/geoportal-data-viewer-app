package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.GwtEvent;

import ol.Extent;

/**
 * The Class FitMapToExtentEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 6, 2023
 */
public class FitMapToExtentEvent extends GwtEvent<FitMapToExtentEventHandler> {
	public static Type<FitMapToExtentEventHandler> TYPE = new Type<FitMapToExtentEventHandler>();
	private Extent extent;

	/**
	 * Instantiates a new fit map to extent event.
	 *
	 * @param extent the extent
	 */
	public FitMapToExtentEvent(Extent extent) {
		this.extent = extent;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<FitMapToExtentEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(FitMapToExtentEventHandler handler) {
		handler.onFitToMap(this);

	}
	
	public Extent getExtent() {
		return extent;
	}

}