package org.gcube.portlets.user.geoportaldataviewer.shared.faults;

public class ControlledError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3471094758439575063L;

	public ControlledError() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ControlledError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ControlledError(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ControlledError(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ControlledError(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
