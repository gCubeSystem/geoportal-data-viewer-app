package org.gcube.portlets.user.geoportaldataviewer.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class MapPanel extends Composite {

	private static MapPanelUiBinder uiBinder = GWT.create(MapPanelUiBinder.class);

	interface MapPanelUiBinder extends UiBinder<Widget, MapPanel> {
	}

	@UiField
	HTMLPanel mapPanel;

	public MapPanel(String height) {
		initWidget(uiBinder.createAndBindUi(this));
		mapPanel.getElement().setId("map");
		mapPanel.setHeight(height);
	}
	
	public void setMapHeight(int height) {
		mapPanel.setHeight(height +"px");
	}

}
