package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.client.ui.map.ExtentMapUtil.Location;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class MapExtentToEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Sep 1, 2021
 */
public class MapExtentToEvent extends GwtEvent<MapExtentToEventHandler> {
	public static Type<MapExtentToEventHandler> TYPE = new Type<MapExtentToEventHandler>();
	private Location location;

	/**
	 * Instantiates a new map extent to event.
	 *
	 * @param layerItem the layer item
	 */
	public MapExtentToEvent(Location location) {
		this.location = location;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<MapExtentToEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(MapExtentToEventHandler handler) {
		handler.onExtentEvent(this);

	}

	public Location getLocation() {
		return location;
	}

}