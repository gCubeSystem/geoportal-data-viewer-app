package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface ClosedViewDetailsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 19, 2020
 */
public interface ClosedViewDetailsEventHandler extends EventHandler {
	
	/**
	 * On closed.
	 *
	 * @param closedViewDetailsEvent the closed view details event
	 */
	void onClosed(ClosedViewDetailsEvent closedViewDetailsEvent);
}
