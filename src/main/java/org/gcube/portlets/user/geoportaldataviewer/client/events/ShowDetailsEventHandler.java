package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ShowDetailsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 3, 2020
 */
public interface ShowDetailsEventHandler extends EventHandler {


	/**
	 * On show details.
	 *
	 * @param showDetailsEvent the show details event
	 */
	void onShowDetails(ShowDetailsEvent showDetailsEvent);
}
