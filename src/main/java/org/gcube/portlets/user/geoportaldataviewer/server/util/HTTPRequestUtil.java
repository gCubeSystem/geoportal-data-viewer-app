package org.gcube.portlets.user.geoportaldataviewer.server.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.gcube.portlets.user.geoportaldataviewer.server.GeoportalDataViewerServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class HTTPRequestUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 5, 2023
 */
public class HTTPRequestUtil {

	private static final Logger LOG = LoggerFactory.getLogger(GeoportalDataViewerServiceImpl.class);

	/**
	 * Gets the response.
	 *
	 * @param url the url
	 * @return the response
	 */
	public static String getResponse(String url) {
		if (url == null || url.isEmpty())
			return null;

		StringBuffer response = new StringBuffer();
		String theResponseString = "";
		HttpURLConnection con = null;
		LOG.debug("Calling URL: " + url);
		try {
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			LOG.debug("GET Response Code:  " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			} else {
				LOG.info("GET request did not work.");
			}

			theResponseString = response.toString();
			// LOG.trace(theResponseString);

			if (LOG.isDebugEnabled()) {
				LOG.debug("getResponse is empty? " + theResponseString.isEmpty());
			}
		} catch (Exception e) {
			LOG.error("Error on performing the request to URL: " + url, e);
		} finally {

		}
		LOG.debug("returning response with size: " + theResponseString.length());
		return theResponseString;
	}

}
