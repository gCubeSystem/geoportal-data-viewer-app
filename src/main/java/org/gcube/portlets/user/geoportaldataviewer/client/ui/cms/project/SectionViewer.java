package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.gallery.ImagesSectionGallery;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.layers.LayersSectionViewer;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.util.CustomFlexTable;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.PageHeader;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class SectionViewer extends Composite {

	private static SectionViewerUiBinder uiBinder = GWT.create(SectionViewerUiBinder.class);

	interface SectionViewerUiBinder extends UiBinder<Widget, SectionViewer> {
	}

	private SectionView sectionView;

	@UiField
	HTMLPanel sectionPanelContainer;

	@UiField
	PageHeader sectionTitle;

	public SectionViewer(SectionView sectionView, String topTargetId) {
		initWidget(uiBinder.createAndBindUi(this));
		this.sectionView = sectionView;

		List<SubDocumentView> subDocuments = sectionView.getListSubDocuments();

		sectionTitle.setTitle(sectionView.getSectionTitle());
		sectionTitle.setText(sectionView.getSectionTitle());

		if (subDocuments == null)
			return;

		Button goToTop = new Button("");
		goToTop.setType(ButtonType.LINK);
		goToTop.setIcon(IconType.DOUBLE_ANGLE_UP);
		goToTop.setHref("#" + topTargetId);
		goToTop.setTitle("Go to top");
		goToTop.getElement().setClassName("go-top-right");

		sectionTitle.add(goToTop);

		boolean displayAsGallery = false;

		try {
			for (SubDocumentView subDocumentView : subDocuments) {
				if (subDocumentView.getListImages() != null && subDocumentView.getListImages().size() > 0) {
					GWT.log("Section with images: " + subDocumentView.getListImages());
					displayAsGallery = true;
					break;
				}
			}

			boolean displayAsMapOfLayers = false;
			for (SubDocumentView subDocumentView : subDocuments) {
				if (subDocumentView.getListLayers() != null && subDocumentView.getListLayers().size() > 0) {
					GWT.log("Section with layers: " + subDocumentView.getListImages());
					displayAsMapOfLayers = true;
					break;
				}
			}

			// Displaying the whole section as a Gallery
			if (displayAsGallery) {
				GWT.log("displayAsGallery the: " + sectionView);
				ImagesSectionGallery sectionGallery = new ImagesSectionGallery(sectionView);
				sectionPanelContainer.add(sectionGallery.getGalleryPanel());
				Scheduler.get().scheduleDeferred(new ScheduledCommand() {

					@Override
					public void execute() {
						sectionGallery.fillGallery();

					}

				});

				// Displaying the whole section as a Map of Layers
			} else if (displayAsMapOfLayers) {
				GWT.log("displayAsMapOfLayers the: " + sectionView);
				for (SubDocumentView subDocumentView : subDocuments) {
//				String table = GeoportalDataViewerConstants.jsonToTableHTML(subDocumentView.getMetadataAsJSON());
//				sectionPanelContainer.add(new HTML(table));

					List<GCubeSDIViewerLayerDV> layers = subDocumentView.getListLayers();
					if (layers != null) {
						for (GCubeSDIViewerLayerDV gCubeLayer : layers) {
							LayersSectionViewer layerSectionViewer = new LayersSectionViewer(gCubeLayer,
									subDocumentView);
							sectionPanelContainer.add(layerSectionViewer);
							// showLinkToDownloadWsContent(fileset.getName(), fileset.getListPayload());
						}
					}
				}

			} else {
				GWT.log("displaying default the: " + sectionView);
				for (SubDocumentView subDocumentView : subDocuments) {
					String table = GeoportalDataViewerConstants.jsonToTableHTML(subDocumentView.getMetadataAsJSON());
					sectionPanelContainer.add(new HTML(table));

					List<FilesetDV> files = subDocumentView.getListFiles();
					if (files != null) {
						for (FilesetDV fileset : files) {
							showLinkToDownloadWsContent(fileset.getGcubeProfileFieldName(), fileset.getListPayload());
						}
					}
				}
			}
		} catch (Exception e) {
			GWT.log("Error on rendering the section: "+e.getMessage());
		}

	}

	private void showLinkToDownloadWsContent(String title, List<PayloadDV> listPayloads) {

		if (listPayloads != null) {
			CustomFlexTable customTable = new CustomFlexTable();
			int i = 0;
			String fieldLabel = title;
			for (PayloadDV payload : listPayloads) {

				if (i > 0) {
					fieldLabel = "";
				}

				String downloadLabel = "download";
				if (payload.getName() != null) {
					downloadLabel = payload.getName();
				}

				customTable.addNextKeyWidget(fieldLabel,
						new HTML("<a href=\"" + payload.getLink() + "\">" + downloadLabel + "</a>"));
			}

			sectionPanelContainer.add(customTable);
		}
	}

}
