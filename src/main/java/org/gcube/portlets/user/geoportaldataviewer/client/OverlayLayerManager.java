package org.gcube.portlets.user.geoportaldataviewer.client;

import java.util.ArrayList;
import java.util.HashMap;

import org.gcube.portlets.user.geoportaldataviewer.client.ui.dandd.DragDropLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;

import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.FlowPanel;

public class OverlayLayerManager {

	// private ArrayList<LayerObject> layers = new ArrayList<LayerObject>();
	// private FlowPanel fp = new FlowPanel();
	private FlowPanel clusterCollectionsMainPanel = new FlowPanel();
	private HashMap<String, ArrayList<DragDropLayer>> mapCollectionsPerProfileID = new HashMap<String, ArrayList<DragDropLayer>>();
	private HashMap<String, FlowPanel> mapCollectionsContainersPerProfileID = new HashMap<String, FlowPanel>();
	private HandlerManager applicationBus;

	public OverlayLayerManager(HandlerManager applicationBus) {
		this.applicationBus = applicationBus;
	}

	public void addLayerItem(LayerObject lo) {
		GWT.log("Adding LayerObject having project: "+lo.getProjectDV());
		GWT.log("Adding LayerObject having project id: "+lo.getProjectDV().getId());

		ArrayList<DragDropLayer> listDDLayers = mapCollectionsPerProfileID.get(lo.getProfileID());

		if (listDDLayers == null) {

			String collectionName = lo.getProjectDV() != null && lo.getProjectDV().getProfileName() != null
					? lo.getProjectDV().getProfileName()
					: lo.getProfileID();

			Label labelCollectionName = new Label(collectionName);
			labelCollectionName.setType(LabelType.WARNING);
			clusterCollectionsMainPanel.add(labelCollectionName);
			listDDLayers = new ArrayList<DragDropLayer>();

		}

		FlowPanel fp = new FlowPanel();
		fp.getElement().getStyle().setMarginLeft(10, Unit.PX);

		DragDropLayer dandd = new DragDropLayer(applicationBus, lo, true, true);
		listDDLayers.add(dandd);
		fp.add(dandd);

		mapCollectionsContainersPerProfileID.put(lo.getProfileID(), fp);
		mapCollectionsPerProfileID.put(lo.getProfileID(), listDDLayers);
		clusterCollectionsMainPanel.add(fp);
	}

	public void resetLayers() {
		clusterCollectionsMainPanel.clear();
		mapCollectionsContainersPerProfileID.clear();
		mapCollectionsPerProfileID.clear();

	}

	public FlowPanel getPanel() {
		return clusterCollectionsMainPanel;
	}
}
