package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs;


/**
 * The Interface WFSGeometry.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 6, 2021
 */
public interface WFSGeometry {
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	String getType();

	/**
	 * Gets the coordinates JSON.
	 *
	 * @return the coordinates JSON
	 */
	String getCoordinatesJSON();

}
