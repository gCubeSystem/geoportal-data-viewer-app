package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class TimelineProjectRelationsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 3, 2022
 */
public class TimelineProjectRelationsEvent extends GwtEvent<TimelineProjectRelationsEventHandler> {
	public static Type<TimelineProjectRelationsEventHandler> TYPE = new Type<TimelineProjectRelationsEventHandler>();
	private EVENT_TYPE eventType = EVENT_TYPE.HIDE; //default
	private ProjectDV theProjectDV;

	/**
	 * The Enum EVENT_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 3, 2022
	 */
	public enum EVENT_TYPE {
		SHOW, HIDE
	}

	/**
	 * Instantiates a new timeline project relations event.
	 *
	 * @param theProjectDV the the project DV
	 * @param eventType    the event type
	 */
	public TimelineProjectRelationsEvent(ProjectDV theProjectDV, EVENT_TYPE eventType) {
		this.theProjectDV = theProjectDV;
		this.eventType = eventType;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<TimelineProjectRelationsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(TimelineProjectRelationsEventHandler handler) {
		handler.onTimelineRelationEvent(this);

	}

	/**
	 * Gets the event type.
	 *
	 * @return the event type
	 */
	public EVENT_TYPE getEventType() {
		return eventType;
	}

	/**
	 * Gets the the project DV.
	 *
	 * @return the the project DV
	 */
	public ProjectDV getTheProjectDV() {
		return theProjectDV;
	}

}