package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.gallery;

import java.util.Set;

import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 * The Class ImagesSectionGallery.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jul 21, 2021
 */
public class ImagesSectionGallery {

	private HTMLPanel galleryPanel;
	private String galleryDivId;
	private SectionView sectionView;

	/**
	 * Instantiates a new images section gallery.
	 *
	 * @param sectionView the section view
	 */
	public ImagesSectionGallery(SectionView sectionView) {
		this.galleryDivId = "nanogallery" + Random.nextInt() + Random.nextInt();
		this.galleryPanel = new HTMLPanel("<div id='" + galleryDivId + "'></div>");
		this.sectionView = sectionView;
	}

	/**
	 * Show gallery.
	 *
	 * @param json_array_images the json array images
	 * @param galleryDivId      the gallery div id
	 */
	private native void showGallery(JavaScriptObject json_array_images, String galleryDivId) /*-{

		//console.log("showing: " + json_array_images)
		var waitForJQuery = setInterval(
				function() {
					if (typeof $wnd.$ != 'undefined') {

						$wnd
								.$("#" + galleryDivId)
								.nanogallery2(
										{
											thumbnailHeight : '200 XS150 SM150', // RESPONSIVE THUMBNAIL HEIGHT: default=200px, XS resolution=150px, SM resolution=150px
											thumbnailWidth : '218 XS150 SM150', // RESPONSIVE THUMBNAIL WIDTH:  auto
											thumbnailAlignment : 'left',
											thumbnailBorderHorizontal : 0,
											thumbnailBorderVertical : 0,
											thumbnailGutterWidth : '10 XS10 SM10',
											thumbnailGutterHeight : '10 XS10 SM10',

											// THUMBNAIL TOOLS & LABEL
											thumbnailLabel : {
												display : true,
												position : 'onBottom',
												align : 'left'
											},
											thumbnailToolbarImage : {
												bottomLeft : 'display'
											},
											// replace the default DISPLAY tool icon
											icons : {
												thumbnailDisplay : '<i class="fa fa-long-arrow-right" aria-hidden="true"></i> display'
											},

											// DISPLAY ANIMATION
											galleryDisplayTransition : 'slideUp',
											galleryDisplayTransitionDuration : 1000,
											thumbnailDisplayTransition : 'scaleDown',
											thumbnailDisplayTransitionDuration : 300,
											thumbnailDisplayInterval : 50,

											// THUMBNAIL'S HOVER ANIMATION
											//thumbnailBuildInit2 : 'tools_font-size_1.5em|title_font-size_1.5em',
											thumbnailHoverEffect2 : 'imageScaleIn80|tools_opacity_0_1|tools_translateX_-30px_0px|title_opacity_1_0|title_translateX_0px_-30px',
											touchAnimation : true,
											touchAutoOpenDelay : 800,

											// GALLERY THEME
											galleryTheme : {
												thumbnail : {
													borderRadius : '2px !important',
													background : '#ffffff !important',
													titleShadow : 'none !important',
													titleColor : '#696969 !important',
													labelBackground : '#f3f3f3 !important'
												},
												thumbnailIcon : {
													color : '#000',
													shadow : 'none'
												},
											},
											viewerToolbar : {
												display : true,
												standard : 'minimizeButton, label',
												minimized : 'minimizeButton, label, fullscreenButton, downloadButton'
											},
											viewerTools : {
												topLeft : 'pageCounter',
												topRight : 'playPauseButton, zoomButton, fullscreenButton, downloadButton, closeButton'
											},

											// DEEP LINKING
											locationHash : false,
											items : json_array_images
										})

						clearInterval(waitForJQuery);
					}
				}, 200);

	}-*/;

	/**
	 * Gets the gallery panel.
	 *
	 * @return the gallery panel
	 */
	public HTMLPanel getGalleryPanel() {
		return galleryPanel;
	}

	/**
	 * Fill gallery.
	 */
	public void fillGallery() {

		JSONArray jsonArray = new JSONArray();
		int index = 0;
		for (SubDocumentView subdoc : sectionView.getListSubDocuments()) {
			JSONValue meta = JSONParser.parseStrict(subdoc.getMetadataAsJSON());

			for (FilesetDV image : subdoc.getListImages()) {
				try {
					if (image.getListPayload() != null) {
						for (PayloadDV payloadDV : image.getListPayload()) {
							JSONObject json = new JSONObject();
							json.put("src", new JSONString(payloadDV.getLink()));
							json.put("srct", new JSONString(payloadDV.getLink()));
							json.put("downloadURL", new JSONString(payloadDV.getLink()));
							try {
								json.put("title", new JSONString(getFirstValueOfJSON(meta)));
							} catch (Exception e) {
								GWT.log("Error on reading the title from: " + meta);
								json.put("title", new JSONString(meta.toString()));
							}
							json.put("description", new JSONString(meta.toString()));
							jsonArray.set(jsonArray.size(), json);
							index++;
						}

					}
				} catch (Exception e) {
					GWT.log("Error on adding the image to gallery from: " + image);
				}

			}

		}

		if(jsonArray.size()>0) {
			showGallery(jsonArray.getJavaScriptObject(), galleryDivId);
		}
	}

	/**
	 * Gets the first value of JSON.
	 *
	 * @param metadata the metadata
	 * @return the first value of JSON
	 */
	public String getFirstValueOfJSON(JSONValue metadata) {

		String value = "";

		if (metadata == null)
			return value;

		if (metadata.isArray() != null) {
			JSONArray array = (JSONArray) metadata;
			for (int i = 0; i < array.size(); i++) {
				JSONObject object = (JSONObject) array.get(i);
				Set<String> set = object.keySet();
				value = object.get(set.iterator().next()).toString();
			}
		} else if (metadata.isObject() != null) {
			JSONObject object = (JSONObject) metadata;
			Set<String> set = object.keySet();
			value = object.get(set.iterator().next()).toString();
		} else if (metadata.isString() != null) {
			value = metadata.toString();
		}

		return value;
	}
}
