package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface OverlayCustomLayerToMapEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * May 15, 2023
 */
public interface OverlayCustomLayerToMapEventHandler extends EventHandler {

	/**
	 * On custom overlay layer action.
	 *
	 * @param customOverLayerToMapEvent the custom over layer to map event
	 */
	void onCustomOverlayLayerAction(OverlayCustomLayerToMapEvent customOverLayerToMapEvent);
}
