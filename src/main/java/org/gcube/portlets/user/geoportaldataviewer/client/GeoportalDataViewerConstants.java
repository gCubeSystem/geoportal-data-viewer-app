package org.gcube.portlets.user.geoportaldataviewer.client;

import org.gcube.application.geoportalcommon.ConvertToDataViewModel;
import org.gcube.portlets.user.geoportaldataviewer.client.gis.OpenLayersMapParameters;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * The Class GeoportalDataViewerConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 24, 2020
 */
public class GeoportalDataViewerConstants {

	// Geoportal_Exporter_ACTION_SERVLET
	public static final String UCD_ID_PARAMETER = "ucdID";
	public static final String PROIECT_ID_PARAMETER = "projectID";
	public static final String CONTEXT_ID_PARAMETER = "c_id";
	public static final String USER_ID_PARAMETER = "u_id";
	public static final String USERNAME_PARAMETER = "usn";
	public static final String GEOPORTAL_EXPORT_ACTION_SERVLET = GWT.getModuleBaseURL() + "geoportalexporterservice";

	public static final int TABLET_WIDTH = 1601;

	public static final String MAP_DIV = "map";

	public static final String GET_WMS_PARAMETER = "wmsrequest";
	public static final String GET_GEONA_ITEM_TYPE = "git";
	public static final String GET_GEONA_ITEM_ID = "gid";
	public static final String HEADER_GEOPORTALEXPORT_PRJ = "geoportalexportprj";
	public static final String GET_UUID_PARAMETER = "uuid";
	public static final String GET_LAYER_TITLE = "layertitle";
	public static final String GET_MAX_ZOOM_LEVEL = OpenLayersMapParameters.OL_MAP_PARAM.maxzoomlevel.name();
	public static final String GET_ZOOM = OpenLayersMapParameters.OL_MAP_PARAM.zoom.name();
	public static final String GET_CENTER_MAP_TO_LONG_LAT = OpenLayersMapParameters.OL_MAP_PARAM.centermap.name();

	public static final Double INITIAL_LAYER_OPACITY = 0.8;
	public static final int SEARCH_LIMIT_RESULTS_TO_MAXIMUM = 50;

	public static final DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat("dd MMMM yyyy");

	public static enum MapEventType {
		MOUSE_CLICK, MAP_ZOOM_END, MOVE_END, ADDED_CENTROID_LAYER_TO_MAP
	}

	public static final String PROJECT_ID_KEY_FEATURE = "projectid";

	public static enum GisToolbarFacilities {
		COLLECTION("Collection", "Open the collections"), MAP("Map", "Select the base Map"),
		OVERLAY("Overlay", "Add overlay layers"), SEARCH("Search", "Search in the collections"),
		FILTER("Filter", "Apply spatial filtering"), LAYERS("Layers", "Layers added to Map"),
		PRESET_LOCATION("Preset Location", "Center the Map to Location");

		String name;
		String tooltip;

		GisToolbarFacilities(String name, String tooltip) {
			this.name = name;
			this.tooltip = tooltip;
		}

		public String getName() {
			return name;
		}

		public String getTooltip() {
			return tooltip;
		}
	}

	/**
	 * The Enum LayerType.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Nov 24, 2020
	 */
	public enum LayerType {
		RASTER_BASELAYER, FEATURE_TYPE
	};

	public static DateTimeFormat DT_FORMAT = DateTimeFormat.getFormat(ConvertToDataViewModel.DATE_FORMAT);

	public static final int MAX_WFS_FEATURES = 10; // zero for no limit

	public static final String NEW_LINE_BR = "<br/>";

	public static enum MAP_PROJECTION {

		EPSG_4326("4326", "EPSG:4326"), EPSG_3857("3857", "EPSG:3857");

		String id;
		String name;

		MAP_PROJECTION(String id, String name) {
			this.id = id;
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

	}

	public static final double ITALY_CENTER_LONG = 12.45;

	public static final double ITALY_CENTER_LAT = 42.98;

	public static final int LIGHT_MAP_ITALY_FIT_ZOOM_ON = 5;

	public static final int MAP_ITALY_FIT_ZOOM_ON = 5;

	/**
	 * Prints the.
	 *
	 * @param msg the msg
	 */
	public static native void printJs(String msg)/*-{
		//console.log("js console: " + msg);
	}-*/;

	/**
	 * Prints the.
	 *
	 * @param msg the msg
	 */
	public static native void printJsObj(Object object)/*-{
		//console.log("js obj: " + JSON.stringify(object, null, 4));
	}-*/;

	/**
	 * Prints the.
	 *
	 * @param msg the msg
	 */
	public static native String toJsonObj(Object object)/*-{
		return JSON.stringify(object);
	}-*/;

	/**
	 * Json to HTML.
	 *
	 * @param jsonTxt the json txt
	 * @return the string
	 */
	public static native String jsonToTableHTML(String jsonTxt)/*-{
		try {
			var jsonObj = JSON.parse(jsonTxt);

			if (jsonObj.length == undefined)
				jsonObj = [ jsonObj ]
				//console.log(jsonObj.length)

				// EXTRACT VALUE FOR HTML HEADER.
			var col = [];
			for (var i = 0; i < jsonObj.length; i++) {
				for ( var key in jsonObj[i]) {
					//console.log('key json' +key)
					if (col.indexOf(key) === -1) {
						col.push(key);
					}
				}
			}

			// CREATE DYNAMIC TABLE.
			var table = document.createElement("table");
			try {
				table.classList.add("my-html-table");

			} catch (e) {
				console.log('invalid css add', e);
			}

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < col.length; i++) {
				tr = table.insertRow(-1);
				var firstCell = tr.insertCell(-1);
				//firstCell.style.cssText="font-weight: bold; text-align: center; vertical-align: middle;";
				firstCell.innerHTML = col[i];
				for (var j = 0; j < jsonObj.length; j++) {
					var tabCell = tr.insertCell(-1);
					var theValue = jsonObj[j][col[i]];
					//console.log("the value: "+theValue);
					if (theValue !== null
							&& Object.prototype.toString.call(theValue) === '[object Array]') {
						var formattedValueArray = "";
						for (var k = 0; k < theValue.length; k++) {
							var theValueArray = theValue[k];
							//console.log(theValueArray);
							formattedValueArray += theValueArray + "<br>";
						}
						tabCell.innerHTML = formattedValueArray;
					} else {
						tabCell.innerHTML = theValue;
					}
				}
			}

			return table.outerHTML;
		} catch (e) {
			console.log('invalid json', e);
			return jsonTxt;
		}

	}-*/;

	public static native void clickElement(Element elem) /*-{
		elem.click();
	}-*/;

}
