package org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project;

import java.util.Map.Entry;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerConstants;

public class ProjectUtil {

	public static <T extends DocumentDV> String toHMLCode(T document, String projectID) {
		String htmlCode = "";

		if (document == null)
			return htmlCode;

		Entry<String, Object> firstEntrySet = document.getFirstEntryOfMap();
		if (firstEntrySet != null) {
			htmlCode += firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue() + "</b>";
		}

		if (projectID != null) {
			htmlCode += " (id: " + projectID + ")";
		}

		return htmlCode;
	}

	public static <T extends DocumentDV> String toHMLCode(boolean showkey, T document, String projectID) {
		String htmlCode = "";

		if (document == null)
			return htmlCode;

		Entry<String, Object> firstEntrySet = document.getFirstEntryOfMap();
		if (firstEntrySet != null) {
			htmlCode += showkey ? firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue() + "</b>"
					: firstEntrySet.getValue();
		}

		if (projectID != null) {
			htmlCode += " (id: " + projectID + ")";
		}

		return htmlCode;
	}

	public static <T extends DocumentDV> String toHMLCode(T document) {
		String htmlCode = "";

		if (document == null)
			return htmlCode;

		Entry<String, Object> firstEntrySet = document.getFirstEntryOfMap();
		if (firstEntrySet != null) {
			htmlCode += firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue() + "</b>";
		}

		return htmlCode;
	}

	public static String toHTMLCode(TemporalReferenceDV tempRef) {

		String htmlCode = "<span class='display-date'>";

		if (tempRef != null) {

			String dateToString = "";
			if (tempRef.getStart() != null) {
				dateToString += GeoportalDataViewerConstants.DATE_TIME_FORMAT.format(tempRef.getStart());

			}
			dateToString += " / ";
			if (tempRef.getStart() != null) {
				dateToString += GeoportalDataViewerConstants.DATE_TIME_FORMAT.format(tempRef.getEnd());

			}

			htmlCode += dateToString;
		}
		htmlCode += "</span>";

		return htmlCode;
	}
}
