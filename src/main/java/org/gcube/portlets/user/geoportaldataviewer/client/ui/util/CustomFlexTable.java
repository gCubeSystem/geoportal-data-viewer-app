package org.gcube.portlets.user.geoportaldataviewer.client.ui.util;

import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;


/**
 * The Class CustomFlexTable.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 11, 2020
 */
public class CustomFlexTable extends FlexTable {

	/**
	 * Instantiates a new custom flex table.
	 */
	public CustomFlexTable() {
		
		this.getElement().addClassName("my-html-table");
	}
	

	/**
	 * Adds the next key values.
	 *
	 * @param key the key
	 * @param listValues the list values
	 * @param separator the separator
	 */
	public void addNextKeyValues(String key, List<String> listValues, String separator) {
		GWT.log("adding key "+key +", values: "+listValues);
		
		if(key==null)
			return;
		
		Label keyLabel = new Label(key);
		int row = getRowCount() + 1;
		setWidget(row, 0, keyLabel);
		
		if(listValues==null)
			return;
		
		String valuesAsString = "";
		for (String value : listValues) {
			String theValue = value;
			if(theValue==null)
				theValue = "";
			
			if(theValue.startsWith("http://") || (theValue.startsWith("https://"))){
				theValue = "<a href=\""+value+"\" target=\"_blank\">"+value+"</a>";
			}
			valuesAsString+=theValue+separator;
		}
		valuesAsString = valuesAsString.substring(0, valuesAsString.lastIndexOf(separator));
		HTML valuesLabel = new HTML(valuesAsString);
		setWidget(row, 1, valuesLabel);
	}

	/**
	 * Adds the next key value.
	 *
	 * @param key the key
	 * @param value the value
	 */
	public void addNextKeyValue(String key, String value) {
		
		if(key==null)
			return;
		
		Label keyLabel = new Label(key);

		// keyLabel.getElement().getStyle().setMarginRight(5, Unit.PX);
		int row = getRowCount() + 1;
		setWidget(row, 0, keyLabel);
		if(value==null)
			return;
		
		HTML valueLabel = new HTML(value);
		setWidget(row, 1, valueLabel);
	}

	/**
	 * Adds the next key widget.
	 *
	 * @param key the key
	 * @param value the value
	 */
	public void addNextKeyWidget(String key, Widget value) {
		Label keyLabel = new Label(key);
		int row = getRowCount() + 1;
		setWidget(row, 0, keyLabel);
		setWidget(row, 1, value);
	}

}
