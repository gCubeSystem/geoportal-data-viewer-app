package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs;

import java.io.Serializable;


/**
 * The Class Coordinate.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Nov 16, 2020
 */
public class Coordinate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3352677110683481305L;
	
	private Double x;
	private Double y;
	
	
	/**
	 * Instantiates a new coordinate.
	 */
	public Coordinate() {
		super();
	}
	
	/**
	 * Instantiates a new coordinate.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Coordinate(Double x, Double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Coordinate [x=");
		builder.append(x);
		builder.append(", y=");
		builder.append(y);
		builder.append("]");
		return builder.toString();
	}
	

}
