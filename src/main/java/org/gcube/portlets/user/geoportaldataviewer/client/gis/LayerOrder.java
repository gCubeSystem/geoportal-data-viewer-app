package org.gcube.portlets.user.geoportaldataviewer.client.gis;

import java.util.HashMap;

/**
 * The Class LayerOrder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 12, 2021
 */
public class LayerOrder {

	/**
	 * The Enum LAYER_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 12, 2021
	 */
	public static enum LAYER_TYPE {
		BASE_MAP, CUSTOM_WMS_DETAIL, BASE_WMS, WMS_DETAIL, VECTOR
	}

	public static final HashMap<LAYER_TYPE, Integer> LAYER_OFFSET = new HashMap<LAYER_TYPE, Integer>(5);

	static {

		LAYER_OFFSET.put(LAYER_TYPE.BASE_MAP, 0); //Base OL or MapBox
		LAYER_OFFSET.put(LAYER_TYPE.CUSTOM_WMS_DETAIL, 70); //WMS custom Overlay. Es. Layer "Limiti Amministrativi"
		LAYER_OFFSET.put(LAYER_TYPE.BASE_WMS, 210); // Index layers (i.e. centroids for UCD)
		LAYER_OFFSET.put(LAYER_TYPE.WMS_DETAIL, 140); //Layer/s belonging to a Project (materialized layers. Ex. "Posizionamento Scavo")
		LAYER_OFFSET.put(LAYER_TYPE.VECTOR, 320); //Vector layer to show other info. Ex. Pointer with coordinates (x,y) in the Light OL Map.
	}

	/**
	 * Instantiates a new layer order.
	 */
	public LayerOrder() {
	}

	/**
	 * Gets the offset.
	 *
	 * @param layerType the layer type
	 * @return the offset
	 */
	public Integer getOffset(LAYER_TYPE layerType) {
		return LAYER_OFFSET.get(layerType);
	}

}
