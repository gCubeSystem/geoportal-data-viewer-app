package org.gcube.portlets.user.geoportaldataviewer.shared.gis;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;

/**
 * Represents a layer, holds the layer item and the related Collection info if
 * existing
 * 
 * 
 */
public class LayerObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2282478701630148774L;

	private LayerObjectType type;

	// private IndexLayerDV indexLayer; // expected for INDEX_LAYER type
	private String profileID; // expected for collection layers
	// private GCubeSDILayer projectLayer; // expected for PROJECT_LAYER
	private String projectID; // expected for PROJECT_LAYER

	private LayerItem layerItem;

	private ProjectDV projectDV;

	public LayerObject() {
	}

	public LayerObject(LayerObjectType type, LayerItem item) {
		this.setType(type);
		this.setLayerItem(item);
	}

	public LayerObjectType getType() {
		return type;
	}

	public void setType(LayerObjectType type) {
		this.type = type;
	}

	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public String getProjectID() {
		return projectID;
	}

	public void setProjectID(String projectId) {
		this.projectID = projectId;
	}

	public LayerItem getLayerItem() {
		return layerItem;
	}

	public void setLayerItem(LayerItem layerItem) {
		this.layerItem = layerItem;
	}

	public ProjectDV getProjectDV() {
		return projectDV;
	}

	public void setProjectDV(ProjectDV projectDV) {
		this.projectDV = projectDV;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LayerObject [type=");
		builder.append(type);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", projectID=");
		builder.append(projectID);
		builder.append(", layerItem=");
		builder.append(layerItem);
		builder.append(", projectDV=");
		builder.append(projectDV);
		builder.append("]");
		return builder.toString();
	}

}