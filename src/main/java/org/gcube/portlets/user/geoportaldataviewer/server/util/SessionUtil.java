/**
 *
 */
package org.gcube.portlets.user.geoportaldataviewer.server.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataviewer.server.GNABaseMapsResourceReader;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.urlshortener.UrlShortener;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The Class SessionUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 20, 2020
 */
public class SessionUtil {

	private static final String MAP_UCD_ID_TO_GCUBE_PROFILES = "MAP_UCD_ID_TO_GCUBE_PROFILES";

	/** The log. */
	private static Logger LOG = LoggerFactory.getLogger(SessionUtil.class);

	private static final String URL_SHORTENER_SERVICE = "URL_SHORTENER_SERVICE";
	private static final String LIST_BASE_MAPS_LAYERS = "LIST_BASE_MAPS_LAYERS";
	private static final String TIMELINE_JSON_TEMPLATE = "TIMELINE_JSON_TEMPLATE";
	private static final String USE_CASE_DESCRIPTOR = "USE_CASE_DESCRIPTOR";
	private static final String LIST_OF_CONCESSIONI = "LIST_OF_CONCESSIONI_DATA_VIEWER";
	private static final String GNA_DATAVIEWER_CONFIG_PROFILE = "GNA_DATAVIEWER_CONFIG_PROFILE";

	private static final String COUNT_DOCS_FOR_PROFILE_ID = "GNA_DATAVIEWER_COUNT_DOCS_FOR_PROFILE_ID";

	private static final String UCD_COLLECTIONS_SESSION = "THE_UCD_COLLECTIONS";

	private static final String CHECKED_CONFIG_FOR_PDF_EXPORTER = "CHECKED_CONFIG_FOR_PDF_EXPORTER";

	/**
	 * Checks if is into portal.
	 *
	 * @return true, if is into portal
	 */
	public static boolean isIntoPortal() {
		try {
			UserLocalServiceUtil.getService();
			return true;
		} catch (Exception ex) {
			LOG.warn("Development Mode ON");
			return false;
		}
	}

	/**
	 * Checks if is session expired.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return true, if is session expired
	 * @throws Exception the exception
	 */
	public static boolean isSessionExpired(HttpServletRequest httpServletRequest) throws Exception {
		LOG.trace("workspace session validating...");
		return PortalContext.getConfiguration().getCurrentUser(httpServletRequest) == null;
	}

	/**
	 * Retrieve the current user by using the portal manager.
	 *
	 * @param request the request
	 * @return a GcubeUser object
	 */
	public static GCubeUser getCurrentUser(HttpServletRequest request) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser user = pContext.getCurrentUser(request);
		LOG.debug("Returning user " + user);
		return user;
	}

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentContext(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String context = pContext.getCurrentScope(request);

		if (context != null && setInThread)
			ScopeProvider.instance.set(context);

		LOG.debug("Returning context " + context);
		return context;
	}

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentToken(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String scope = pContext.getCurrentScope(request);
		GCubeUser user = pContext.getCurrentUser(request);
		String token = PortalContext.getConfiguration().getCurrentUserToken(scope, user.getUsername());

		if (token != null) {
			LOG.debug("Returning token " + token.substring(1, 10) + "_MASKED_TOKEN_");
			if (setInThread)
				SecurityTokenProvider.instance.set(token);
		}

		return token;
	}

	/**
	 * Retrieve the group given the scope.
	 *
	 * @param scope the scope
	 * @return the group from scope
	 * @throws UserManagementSystemException the user management system exception
	 * @throws GroupRetrievalFault           the group retrieval fault
	 */
	public static GCubeGroup getGroupFromScope(String scope) throws UserManagementSystemException, GroupRetrievalFault {

		if (scope == null || scope.isEmpty())
			throw new IllegalArgumentException("Scope is missing here!!");

		GroupManager gm = new LiferayGroupManager();
		long groupId = gm.getGroupIdFromInfrastructureScope(scope);
		return gm.getGroup(groupId);

	}

	/**
	 * Gets the url shortener.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the url shortener
	 */
	public static UrlShortener getUrlShortener(HttpServletRequest httpServletRequest) {

		HttpSession session = httpServletRequest.getSession();
		UrlShortener shortener = null;
		try {
			shortener = (UrlShortener) session.getAttribute(URL_SHORTENER_SERVICE);

			if (shortener == null) {
				shortener = new UrlShortener();
				session.setAttribute(URL_SHORTENER_SERVICE, shortener);
			}

		} catch (Exception e) {
			LOG.warn("Error occurred when instancing the " + UrlShortener.class.getSimpleName(), e);
		}

		return shortener;
	}

	/**
	 * Gets the GNA base maps.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the GNA base maps
	 */
	public static List<BaseMapLayer> getGNABaseMaps(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		List<BaseMapLayer> lstBML = null;
		try {
			lstBML = (List<BaseMapLayer>) session.getAttribute(LIST_BASE_MAPS_LAYERS);

			if (lstBML == null) {
				GNABaseMapsResourceReader gnaBMRR = new GNABaseMapsResourceReader();
				lstBML = gnaBMRR.getListBaseMaps();
				session.setAttribute(LIST_BASE_MAPS_LAYERS, lstBML);
			}

		} catch (Exception e) {
			LOG.warn("Error occurred reading the base maps by " + GNABaseMapsResourceReader.class.getSimpleName(), e);
		}
		return lstBML;
	}

	/**
	 * Gets the JSON timeline template.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @return the JSON timeline template
	 */
	public static String getJSONTimelineTemplate(HttpServletRequest httpServletRequest, String profileID) {
		HttpSession session = httpServletRequest.getSession();
		return (String) session.getAttribute(TIMELINE_JSON_TEMPLATE + profileID);
	}

	/**
	 * Sets the JSON timeline template.
	 *
	 * @param httpServletRequest   the http servlet request
	 * @param profileID            the profile ID
	 * @param jsonTimelineTemplate the json timeline template
	 */
	public static void setJSONTimelineTemplate(HttpServletRequest httpServletRequest, String profileID,
			String jsonTimelineTemplate) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(TIMELINE_JSON_TEMPLATE + profileID, jsonTimelineTemplate);
	}

	/**
	 * Gets the UCD for id.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @return the UCD for id
	 */
	public static UseCaseDescriptor getUCDForId(HttpServletRequest httpServletRequest, String profileID) {
		HttpSession session = httpServletRequest.getSession();
		return (UseCaseDescriptor) session.getAttribute(USE_CASE_DESCRIPTOR + profileID);
	}

	/**
	 * Sets the UCD for id.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @param ucd                the ucd
	 */
	public static void setUCDForId(HttpServletRequest httpServletRequest, String profileID, UseCaseDescriptor ucd) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(USE_CASE_DESCRIPTOR + profileID, ucd);
	}

	/**
	 * Gets the list of concessioni.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param reloadFromService  the reload from service
	 * @return the list of concessioni
	 * @throws Exception the exception
	 */
	public static List<Concessione> getListOfConcessioni(HttpServletRequest httpServletRequest,
			boolean reloadFromService) throws Exception {
		HttpSession session = httpServletRequest.getSession();
		List<Concessione> listOfConcessioni = (List<Concessione>) session.getAttribute(LIST_OF_CONCESSIONI);
		throw new Exception("getListConcessioni must be revisited!!!!");
	}

	/**
	 * Gets the GNA data viewer config profile.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the GNA data viewer config profile
	 */
	public static GNADataViewerConfigProfile getGNADataViewerConfigProfile(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		return (GNADataViewerConfigProfile) session.getAttribute(GNA_DATAVIEWER_CONFIG_PROFILE);
	}

	/**
	 * Sets the list item fields config.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param gNADVConfigProfile the g NADV config profile
	 */
	public static void setListItemFieldsConfig(HttpServletRequest httpServletRequest,
			GNADataViewerConfigProfile gNADVConfigProfile) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(GNA_DATAVIEWER_CONFIG_PROFILE, gNADVConfigProfile);
	}

	/**
	 * Gets the total document for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param theProfileID       the the profile ID
	 * @return the total document for profile ID
	 */
	public static Integer getTotalDocumentForProfileID(HttpServletRequest httpServletRequest, String theProfileID) {
		HttpSession session = httpServletRequest.getSession();
		return (Integer) session.getAttribute(COUNT_DOCS_FOR_PROFILE_ID + theProfileID);

	}

	/**
	 * Sets the total document for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param theProfileID       the the profile ID
	 * @param countForProfileId  the count for profile id
	 */
	public static void setTotalDocumentForProfileID(HttpServletRequest httpServletRequest, String theProfileID,
			Integer countForProfileId) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(COUNT_DOCS_FOR_PROFILE_ID + theProfileID, countForProfileId);

	}

	public static void addCollectionToSession(HttpServletRequest httpServletRequest, UseCaseDescriptor u) {
		HttpSession session = httpServletRequest.getSession();
		Map<String, UseCaseDescriptor> ucds = (Map<String, UseCaseDescriptor>) session
				.getAttribute(UCD_COLLECTIONS_SESSION);
		if (ucds == null) {
			ucds = new HashMap<String, UseCaseDescriptor>();
		}
		ucds.put(u.getId(), u);
		session.setAttribute(UCD_COLLECTIONS_SESSION, ucds);
	}

	public static Map<String, UseCaseDescriptor> getAvailableCollections(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		return (Map<String, UseCaseDescriptor>) session.getAttribute(UCD_COLLECTIONS_SESSION);
	}

	public static Boolean getCheckConfigForPFDExporter(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		return (Boolean) session.getAttribute(CHECKED_CONFIG_FOR_PDF_EXPORTER);
	}

	public static void setCheckConfigForPFDExporter(HttpServletRequest httpServletRequest, Boolean configChecked) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(CHECKED_CONFIG_FOR_PDF_EXPORTER, configChecked);
	}

}
