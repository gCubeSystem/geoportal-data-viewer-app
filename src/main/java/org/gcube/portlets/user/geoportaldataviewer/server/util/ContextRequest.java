package org.gcube.portlets.user.geoportaldataviewer.server.util;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.gcube.portlets.user.geoportaldataviewer.server.mongoservice.GeoportalServiceIdentityProxy;
import org.gcube.portlets.user.geoportaldataviewer.shared.faults.ControlledError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ContextRequest<T> {

	private static final Logger LOG = LoggerFactory.getLogger(ContextRequest.class);

	private static List<Runnable> preoperations = new ArrayList<>();
	private GeoportalServiceIdentityProxy geoportalServiceProxy;

	public static void addPreoperation(Runnable preoperation) {
		preoperations.add(preoperation);
	}

	protected HttpServletRequest request;
	private T result = null;

	public ContextRequest(HttpServletRequest httpRequest) {
		this.request = httpRequest;
	}

	public ContextRequest<T> execute() throws Exception, ControlledError {
		try {
			if (!preoperations.isEmpty()) {
				LOG.trace("Running preops (size : {} )", preoperations.size());
				for (Runnable r : preoperations)
					r.run();
			}
			
			geoportalServiceProxy = new GeoportalServiceIdentityProxy(request);
			LOG.trace("Executing actual method..");
			result = run();
			return this;
		} catch (ControlledError e) {
			throw e;
		} catch (Throwable t) {
			LOG.error("Unexpected error ", t);
			throw new Exception("Unexpected error contacting the infrastructure", t);
		}

	}

	public T getResult() {
		return result;
	}
	
	public GeoportalServiceIdentityProxy getGeoportalServiceProxy() {
		return geoportalServiceProxy;
	}

	protected abstract T run() throws Exception, ControlledError;
}
