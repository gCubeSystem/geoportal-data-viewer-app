package org.gcube.portlets.user.geoportaldataviewer.shared.gis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.Property;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.ZAxis;

/**
 * The Class LayerItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 27, 2020
 */
public class LayerItem implements Serializable, Cloneable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1664082688635256899L;

	/** The name. */
	private String name;

	/** The title. */
	private String title = null;

	/** The url. */
	private String url;

	/** The base map server URL. */
	private String mapServerEndpoint;

	/** The style. */
	private String style;

	/** The is trasparent. */
	private boolean isTrasparent;

	/** The is base layer. */
	private boolean isBaseLayer;

	/** The is feature type. */
	private boolean isFeatureType;

	/** The buffer. */
	private int buffer = 0;

	/** The has legend. */
	private boolean hasLegend;

	/** The is visible. */
	private boolean isVisible;

	/** The is on map. */
	private boolean isOnMap;

	/** The click data. */
	private boolean clickData;

	/** The max extent. */
	private BoundsMap maxExtent;

	/** The default style. */
	private String defaultStyle = "";

	/** The opacity. */
	private double opacity = -1;

	/** The styles. */
	private ArrayList<String> styles = new ArrayList<String>();

	/** The properties. */
	private List<Property> properties = new ArrayList<Property>();

	/** The data store. */
	private String dataStore;

	/** The cql filter. */
	private String cqlFilter;

	/** The id. */
	private long id;

	/** The order. */
	private long order;

	/** The wms not standard params. */
	private Map<String, String> wmsNotStandardParams = null;

	/** The is nc wms. */
	private boolean isNcWms = false;

	/** The server wms request. */
	public String wmsLink;

	/** The uuid. */
	private String UUID;

	/** The z axis. */
	private ZAxis zAxis = null;

	/** The z axis selected. */
	private Double zAxisSelected = null;

	/** The Constant FLOAT_TYPE. */
	public static final String FLOAT_TYPE = "xsd:float";

	/** The Constant INT_TYPE. */
	public static final String INT_TYPE = "xsd:int";

	public Double minResolution;

	public Double maxResolution;

	/**
	 * Instantiates a new layer item.
	 */
	public LayerItem() {
	}

	/**
	 * Checks if is checks for legend.
	 *
	 * @return true, if is checks for legend
	 */
	public boolean isHasLegend() {
		return hasLegend;
	}

	/**
	 * Sets the checks for legend.
	 *
	 * @param hasLegend the new checks for legend
	 */
	public void setHasLegend(boolean hasLegend) {
		this.hasLegend = hasLegend;
	}

	/**
	 * Gets the max extent.
	 *
	 * @return the max extent
	 */
	public BoundsMap getMaxExtent() {
		return maxExtent;
	}

	/**
	 * Sets the max extent.
	 *
	 * @param maxExtent the new max extent
	 */
	public void setMaxExtent(BoundsMap maxExtent) {
		this.maxExtent = maxExtent;
	}

	/**
	 * Sets the max extent.
	 *
	 * @param lowerLeftX  the lower left x
	 * @param lowerLeftY  the lower left y
	 * @param upperRightX the upper right x
	 * @param upperRightY the upper right y
	 * @param crs         the crs
	 */
	public void setMaxExtent(double lowerLeftX, double lowerLeftY, double upperRightX, double upperRightY, String crs) {
		this.maxExtent = new BoundsMap(lowerLeftX, lowerLeftY, upperRightX, upperRightY, crs);
	}

	/**
	 * Checks if is feature type.
	 *
	 * @return true, if is feature type
	 */
	public boolean isFeatureType() {
		return isFeatureType;
	}

	/**
	 * Sets the feature type.
	 *
	 * @param isFeatureType the new feature type
	 */
	public void setFeatureType(boolean isFeatureType) {
		this.isFeatureType = isFeatureType;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the style.
	 *
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * Sets the style.
	 *
	 * @param style the new style
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	/**
	 * Checks if is trasparent.
	 *
	 * @return true, if is trasparent
	 */
	public boolean isTrasparent() {
		return isTrasparent;
	}

	/**
	 * Sets the trasparent.
	 *
	 * @param isTrasparent the new trasparent
	 */
	public void setTrasparent(boolean isTrasparent) {
		this.isTrasparent = isTrasparent;
	}

	/**
	 * Checks if is base layer.
	 *
	 * @return true, if is base layer
	 */
	public boolean isBaseLayer() {
		return isBaseLayer;
	}

	/**
	 * Sets the base layer.
	 *
	 * @param isBaseLayer the new base layer
	 */
	public void setBaseLayer(boolean isBaseLayer) {
		this.isBaseLayer = isBaseLayer;
	}

	/**
	 * Gets the buffer.
	 *
	 * @return the buffer
	 */
	public int getBuffer() {
		return buffer;
	}

	/**
	 * Sets the buffer.
	 *
	 * @param buffer the new buffer
	 */
	public void setBuffer(int buffer) {
		this.buffer = buffer;
	}

	/**
	 * Checks if is visible.
	 *
	 * @return true, if is visible
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * Sets the visible.
	 *
	 * @param isVisible the new visible
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	/**
	 * Checks if is on map.
	 *
	 * @return true, if is on map
	 */
	public boolean isOnMap() {
		return isOnMap;
	}

	/**
	 * Sets the on map.
	 *
	 * @param isOnMap the new on map
	 */
	public void setOnMap(boolean isOnMap) {
		this.isOnMap = isOnMap;
	}

	/**
	 * Checks if is click data.
	 *
	 * @return true, if is click data
	 */
	public boolean isClickData() {
		return clickData;
	}

	/**
	 * Sets the click data.
	 *
	 * @param clickData the new click data
	 */
	public void setClickData(boolean clickData) {
		this.clickData = clickData;
	}

	/**
	 * Gets the default style.
	 *
	 * @return the default style
	 */
	public String getDefaultStyle() {
		return defaultStyle;
	}

	/**
	 * Sets the default style.
	 *
	 * @param defaultStyle the new default style
	 */
	public void setDefaultStyle(String defaultStyle) {
		this.defaultStyle = defaultStyle;
	}

	/**
	 * Gets the styles.
	 *
	 * @return the styles
	 */
	public ArrayList<String> getStyles() {
		return styles;
	}

	/**
	 * Sets the styles.
	 *
	 * @param styles the new styles
	 */
	public void setStyles(ArrayList<String> styles) {
		this.styles = styles;
	}

	/**
	 * Gets the opacity.
	 *
	 * @return the opacity
	 */
	public double getOpacity() {
		return opacity;
	}

	/**
	 * Sets the opacity.
	 *
	 * @param opacity the new opacity
	 */
	public void setOpacity(double opacity) {
		this.opacity = opacity;
	}

//	@Override
	/**
	 * Clone.
	 *
	 * @return the layer item
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public LayerItem clone() {
		LayerItem ele = new LayerItem();
		ele.setBaseLayer(this.isBaseLayer);
		ele.setClickData(this.clickData);
		ele.setBuffer(this.buffer);
		ele.setDefaultStyle(this.defaultStyle);
		ele.setHasLegend(this.hasLegend);
		ele.setMaxExtent(maxExtent);
		ele.setName(this.name);
		ele.setOnMap(this.isOnMap);
		ele.setOpacity(this.opacity);
		ele.setStyle(this.style);
		ele.setStyles(this.styles);
		ele.setTrasparent(this.isTrasparent);
		ele.setUrl(this.url);
		ele.setVisible(this.isVisible);
		ele.setProperties(this.getProperties());
		ele.setWmsNotStandardParams(this.wmsNotStandardParams);
//		ele.setUUID(this.UUID);
		return ele;
	}

	/**
	 * Checks if is nc wms.
	 *
	 * @return the isNcWms
	 */
	public boolean isNcWms() {
		return isNcWms;
	}

	/**
	 * Sets the nc wms.
	 *
	 * @param isNcWms the isNcWms to set
	 */
	public void setNcWms(boolean isNcWms) {
		this.isNcWms = isNcWms;
	}

	/**
	 * Gets the properties.
	 *
	 * @return the properties
	 */
	public List<Property> getProperties() {
		return properties;
	}

	/**
	 * Sets the properties.
	 *
	 * @param properties the new properties
	 */
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	// search a float property in the layer's feature type
	/**
	 * Contains property.
	 *
	 * @param propertyName the property name
	 * @return true, if successful
	 */
	public boolean containsProperty(String propertyName) {
		boolean contains = false;
		for (Property p : properties) {
			if (p.getName().equals(propertyName) && (p.getType().equals(FLOAT_TYPE) || p.getType().equals(INT_TYPE))) {
				contains = true;
			}
		}
		return contains;
	}

	// search a property in the layer's feature type, giving property name and
	// property type
	/**
	 * Contains property.
	 *
	 * @param propertyName the property name
	 * @param propertyType the property type
	 * @return true, if successful
	 */
	public boolean containsProperty(String propertyName, String propertyType) {
		if (properties == null) {
			return false;
		}

		boolean contains = false;
		for (Property p : properties) {
			if (p.getName().equals(propertyName) && p.getType().equals(propertyType)) {
				contains = true;
			}
		}
		return contains;
	}

	// search a property in the layer's feature type, giving a property object
	/**
	 * Contains property.
	 *
	 * @param property the property
	 * @return true, if successful
	 */
	public boolean containsProperty(Property property) {
		if (properties == null) {
			return false;
		}

		boolean contains = false;
		for (Property p : properties) {
			if (p.getName().equals(property.getName()) && p.getType().equals(property.getType())) {
				contains = true;
			}
		}
		return contains;
	}

	// search for float Property objects belongs to the layer
	/**
	 * Gets the all float properties.
	 *
	 * @return the all float properties
	 */
	public List<Property> getAllFloatProperties() {
		if (properties == null) {
			return null;
		}

		List<Property> floatProperties = new ArrayList<Property>();
		for (Property p : properties) {
			if (p.getType().equals(FLOAT_TYPE)) {
				floatProperties.add(p);
			}
		}
		return floatProperties;
	}

	// search for float Property names belongs to the layer
	/**
	 * Gets the all float property names.
	 *
	 * @return the all float property names
	 */
	public List<String> getAllFloatPropertyNames() {
		if (properties == null) {
			return null;
		}

		List<String> floatPropertyNames = new ArrayList<String>();
		for (Property p : properties) {
			if (p.getType().equals(FLOAT_TYPE)) {
				floatPropertyNames.add(p.getName());
			}
		}

		return floatPropertyNames;
	}

	// get the first float Property names belongs to the layer
	/**
	 * Gets the first float property name.
	 *
	 * @return the first float property name
	 */
	public String getFirstFloatPropertyName() {
		if (properties == null) {
			return null;
		}

		for (Property p : properties) {
			if (p.getType().equals(FLOAT_TYPE)) {
				return p.getName();
			}
		}

		return null;
	}

	/**
	 * Sets the z axis selected.
	 *
	 * @param value the new z axis selected
	 */
	public void setZAxisSelected(Double value) {
		this.zAxisSelected = value;
	}

	/**
	 * Gets the z axis selected.
	 *
	 * @return the zAxisSelected
	 */
	public Double getZAxisSelected() {

		return zAxisSelected;
	}

	/**
	 * Sets the list z axis.
	 *
	 * @param zAxis the new list z axis
	 */
	public void setZAxis(ZAxis zAxis) {
		this.zAxis = zAxis;
	}

	/**
	 * Gets the list z axis.
	 *
	 * @return the zAxis
	 */
	public ZAxis getZAxis() {

		return zAxis;
	}

	/**
	 * Sets the data store.
	 *
	 * @param dataStore the new data store
	 */
	public void setDataStore(String dataStore) {
		this.dataStore = dataStore;
	}

	/**
	 * Gets the data store.
	 *
	 * @return the data store
	 */
	public String getDataStore() {
		return dataStore;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Sets the order.
	 *
	 * @param order the new order
	 */
	public void setOrder(long order) {
		this.order = order;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public long getOrder() {
		return order;
	}

	/**
	 * Gets the cql filter.
	 *
	 * @return the cql filter
	 */
	public String getCqlFilter() {
		return cqlFilter;
	}

	/**
	 * Sets the cql filter.
	 *
	 * @param cqlFilter the new cql filter
	 */
	public void setCqlFilter(String cqlFilter) {
		this.cqlFilter = cqlFilter;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uUID the new uuid
	 */
	public void setUUID(String uUID) {
		this.UUID = uUID;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uUID
	 */
	public String getUUID() {
		return UUID;
	}

	/**
	 * Checks if is cql filter available.
	 *
	 * @return true, if is cql filter available
	 */
	public boolean isCqlFilterAvailable() {
		return (cqlFilter != null) && (!cqlFilter.isEmpty());
	}

	/**
	 * Gets the wms link.
	 *
	 * @return the wms link
	 */
	public String getWmsLink() {
		return wmsLink;
	}

	/**
	 * Sets the wms link.
	 *
	 * @param wmsLink the new wms link
	 */
	public void setWmsLink(String wmsLink) {
		this.wmsLink = wmsLink;
	}

	/**
	 * Gets the map server endpoint.
	 *
	 * @return the map server endpoint
	 */
	public String getMapServerEndpoint() {
		return mapServerEndpoint;
	}

	/**
	 * Sets the map server host.
	 *
	 * @param mapServerEndpoint the new map server host
	 */
	public void setMapServerHost(String mapServerEndpoint) {
		this.mapServerEndpoint = mapServerEndpoint;
	}

	/**
	 * Gets the wms not standard params.
	 *
	 * @return the wmsNotStandardParams
	 */
	public Map<String, String> getWmsNotStandardParams() {
		return wmsNotStandardParams;
	}

	/**
	 * Sets the wms not standard params.
	 *
	 * @param wmsNotStandardParams the wmsNotStandardParams to set
	 */
	public void setWmsNotStandardParams(Map<String, String> wmsNotStandardParams) {
		this.wmsNotStandardParams = wmsNotStandardParams;
	}

	/**
	 * Gets the z axis.
	 *
	 * @return the z axis
	 */
	public ZAxis getzAxis() {
		return zAxis;
	}

	/**
	 * Sets the z axis.
	 *
	 * @param zAxis the new z axis
	 */
	public void setzAxis(ZAxis zAxis) {
		this.zAxis = zAxis;
	}

	/**
	 * Gets the z axis selected.
	 *
	 * @return the z axis selected
	 */
	public Double getzAxisSelected() {
		return zAxisSelected;
	}

	/**
	 * Sets the z axis selected.
	 *
	 * @param zAxisSelected the new z axis selected
	 */
	public void setzAxisSelected(Double zAxisSelected) {
		this.zAxisSelected = zAxisSelected;
	}

	/**
	 * Sets the max resolution.
	 *
	 * @param maxResolution the new max resolution
	 */
	public void setMaxResolution(Double maxResolution) {
		this.maxResolution = maxResolution;
	}

	/**
	 * Sets the min resolution.
	 *
	 * @param minResolution the new min resolution
	 */
	public void setMinResolution(Double minResolution) {
		this.minResolution = minResolution;
	}

	/**
	 * Gets the min resolution.
	 *
	 * @return the min resolution
	 */
	public Double getMinResolution() {
		return minResolution;
	}

	/**
	 * Gets the max resolution.
	 *
	 * @return the max resolution
	 */
	public Double getMaxResolution() {
		return maxResolution;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LayerItem [name=");
		builder.append(name);
		builder.append(", title=");
		builder.append(title);
		builder.append(", url=");
		builder.append(url);
		builder.append(", mapServerEndpoint=");
		builder.append(mapServerEndpoint);
		builder.append(", style=");
		builder.append(style);
		builder.append(", isTrasparent=");
		builder.append(isTrasparent);
		builder.append(", isBaseLayer=");
		builder.append(isBaseLayer);
		builder.append(", isFeatureType=");
		builder.append(isFeatureType);
		builder.append(", buffer=");
		builder.append(buffer);
		builder.append(", hasLegend=");
		builder.append(hasLegend);
		builder.append(", isVisible=");
		builder.append(isVisible);
		builder.append(", isOnMap=");
		builder.append(isOnMap);
		builder.append(", clickData=");
		builder.append(clickData);
		builder.append(", maxExtent=");
		builder.append(maxExtent);
		builder.append(", defaultStyle=");
		builder.append(defaultStyle);
		builder.append(", opacity=");
		builder.append(opacity);
		builder.append(", styles=");
		builder.append(styles);
		builder.append(", properties=");
		builder.append(properties);
		builder.append(", dataStore=");
		builder.append(dataStore);
		builder.append(", cqlFilter=");
		builder.append(cqlFilter);
		builder.append(", id=");
		builder.append(id);
		builder.append(", order=");
		builder.append(order);
		builder.append(", wmsNotStandardParams=");
		builder.append(wmsNotStandardParams);
		builder.append(", isNcWms=");
		builder.append(isNcWms);
		builder.append(", wmsLink=");
		builder.append(wmsLink);
		builder.append(", UUID=");
		builder.append(UUID);
		builder.append(", zAxis=");
		builder.append(zAxis);
		builder.append(", zAxisSelected=");
		builder.append(zAxisSelected);
		builder.append(", minResolution=");
		builder.append(minResolution);
		builder.append(", maxResolution=");
		builder.append(maxResolution);
		builder.append("]");
		return builder.toString();
	}

}
