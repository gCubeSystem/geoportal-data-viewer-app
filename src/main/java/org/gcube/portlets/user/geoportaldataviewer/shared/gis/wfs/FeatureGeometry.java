package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wfs;

import java.io.Serializable;

/**
 * The Class FeatureGeometry.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 6, 2021
 */
public class FeatureGeometry implements WFSGeometry, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8134176011135233810L;
	private String type;
	private String coordinatesJSON;
	private String toJSON;

	/**
	 * Instantiates a new feature geometry.
	 */
	public FeatureGeometry() {
	}

	/**
	 * Instantiates a new feature geometry.
	 *
	 * @param type            the type
	 * @param coordinatesJSON the coordinates JSON
	 */
	public FeatureGeometry(String type, String coordinatesJSON) {
		super();
		this.type = type;
		this.coordinatesJSON = coordinatesJSON;
	}

	public String getType() {
		return type;
	}

	public String getCoordinatesJSON() {
		return coordinatesJSON;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCoordinatesJSON(String coordinatesJSON) {
		this.coordinatesJSON = coordinatesJSON;
	}

	public String getToJSONObject() {
		if(toJSON==null)
			toJSON = "{\"type\":\""+type+"\",\"coordinates\":"+coordinatesJSON+"}";
		return toJSON;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeatureGeometry [type=");
		builder.append(type);
		builder.append(", coordinatesJSON=");
		builder.append(coordinatesJSON);
		builder.append("]");
		return builder.toString();
	}

}