package org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms;

import java.io.Serializable;
import java.util.HashMap;

/**
 * The Class GeoInformationForWMSRequest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 27, 2020
 */
public class GeoInformationForWMSRequest implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6846636281073641003L;

	private String serviceEndPoint;

	/** The wms request. */
	private String wmsRequest;

	/** The layer name. */
	private String layerName;

	/** The version WMS. */
	private String versionWMS;

	/** The crs. */
	private String crs;

	/** The map WMS no standard params. */
	private HashMap<String, String> mapWMSNoStandardParams;

	/** The styles. */
	private Styles styles;

	/** The is nc WMS. */
	// TODO TO BE REMOVED
	private boolean isNcWMS;

	/** The z axis. */
	private ZAxis zAxis;

	/**
	 * Instantiates a new geo information for wms request.
	 */
	public GeoInformationForWMSRequest() {

	}

	/**
	 * Instantiates a new geo information for wms request.
	 *
	 * @param serviceEndPoint  the base wms service host
	 * @param wmsRequest       the wms request
	 * @param layerName        the layer name
	 * @param versionWms       the version wms
	 * @param crs              the crs
	 * @param mapWmsNoStandard the map wms not standard
	 * @param styles           the layer styles
	 * @param isNcWMS          the is nc WMS
	 * @param zAxis            the z axis
	 */
	public GeoInformationForWMSRequest(String serviceEndPoint, String wmsRequest, String layerName, String versionWms,
			String crs, HashMap<String, String> mapWmsNoStandard, Styles styles, boolean isNcWMS, ZAxis zAxis) {
		this.serviceEndPoint = serviceEndPoint;
		this.wmsRequest = wmsRequest;
		this.layerName = layerName;
		this.versionWMS = versionWms;
		this.crs = crs;
		this.mapWMSNoStandardParams = mapWmsNoStandard;
		this.styles = styles;
		this.zAxis = zAxis;
		this.isNcWMS = isNcWMS;
	}

	/**
	 * Gets the z axis.
	 *
	 * @return the zAxis
	 */
	public ZAxis getZAxis() {

		return zAxis;
	}

	/**
	 * Sets the z axis.
	 *
	 * @param zAxis the zAxis to set
	 */
	public void setZAxis(ZAxis zAxis) {

		this.zAxis = zAxis;
	}

	/**
	 * Gets the service end point.
	 *
	 * @return the service end point
	 */
	public String getServiceEndPoint() {
		return serviceEndPoint;
	}

	/**
	 * Sets the service end point.
	 *
	 * @param serviceEndPoint the new service end point
	 */
	public void setServiceEndPoint(String serviceEndPoint) {
		this.serviceEndPoint = serviceEndPoint;
	}

	/**
	 * Gets the wms request.
	 *
	 * @return the wmsRequest
	 */
	public String getWmsRequest() {

		return wmsRequest;
	}

	/**
	 * Gets the layer name.
	 *
	 * @return the layerName
	 */
	public String getLayerName() {

		return layerName;
	}

	/**
	 * Gets the version wms.
	 *
	 * @return the versionWMS
	 */
	public String getVersionWMS() {

		return versionWMS;
	}

	/**
	 * Gets the crs.
	 *
	 * @return the crs
	 */
	public String getCrs() {

		return crs;
	}

	/**
	 * Gets the map wms no standard.
	 *
	 * @return the mapWMSNoStandard
	 */
	public HashMap<String, String> getMapWMSNoStandard() {

		return mapWMSNoStandardParams;
	}

	/**
	 * Gets the styles.
	 *
	 * @return the styles
	 */
	public Styles getStyles() {

		return styles;
	}

	/**
	 * Checks if is nc wms.
	 *
	 * @return the isNcWMS
	 */
	public boolean isNcWMS() {

		return isNcWMS;
	}

	/**
	 * Sets the base wms service host.
	 *
	 * @param baseWmsServiceHost the new base wms service host
	 */
	public void setBaseWmsServiceHost(String baseWmsServiceHost) {

		this.serviceEndPoint = baseWmsServiceHost;
	}

	/**
	 * Sets the wms request.
	 *
	 * @param wmsRequest the wmsRequest to set
	 */
	public void setWmsRequest(String wmsRequest) {

		this.wmsRequest = wmsRequest;
	}

	/**
	 * Sets the layer name.
	 *
	 * @param layerName the layerName to set
	 */
	public void setLayerName(String layerName) {

		this.layerName = layerName;
	}

	/**
	 * Sets the version wms.
	 *
	 * @param versionWMS the versionWMS to set
	 */
	public void setVersionWMS(String versionWMS) {

		this.versionWMS = versionWMS;
	}

	/**
	 * Sets the crs.
	 *
	 * @param crs the crs to set
	 */
	public void setCrs(String crs) {

		this.crs = crs;
	}

	/**
	 * Sets the map wms no standard.
	 *
	 * @param mapWMSNoStandard the mapWMSNoStandard to set
	 */
	public void setMapWMSNoStandard(HashMap<String, String> mapWMSNoStandard) {

		this.mapWMSNoStandardParams = mapWMSNoStandard;
	}

	/**
	 * Sets the styles.
	 *
	 * @param styles the styles to set
	 */
	public void setStyles(Styles styles) {

		this.styles = styles;
	}

	/**
	 * Sets the nc wms.
	 *
	 * @param isNcWMS the isNcWMS to set
	 */
	public void setNcWMS(boolean isNcWMS) {

		this.isNcWMS = isNcWMS;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("GeoInformationForWMSRequest [serviceEndPoint=");
		builder.append(serviceEndPoint);
		builder.append(", wmsRequest=");
		builder.append(wmsRequest);
		builder.append(", layerName=");
		builder.append(layerName);
		builder.append(", versionWMS=");
		builder.append(versionWMS);
		builder.append(", crs=");
		builder.append(crs);
		builder.append(", mapWMSNoStandardParams=");
		builder.append(mapWMSNoStandardParams);
		builder.append(", styles=");
		builder.append(styles);
		builder.append(", isNcWMS=");
		builder.append(isNcWMS);
		builder.append(", zAxis=");
		builder.append(zAxis);
		builder.append("]");
		return builder.toString();
	}

}
