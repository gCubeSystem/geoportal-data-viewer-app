package org.gcube.portlets.user.geoportaldataviewer.client.ui.dandd;

import java.util.LinkedHashMap;

import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.portlets.user.geoportaldataviewer.client.GeoportalDataViewerServiceAsync;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.DO_LAYER_ACTION;
import org.gcube.portlets.user.geoportaldataviewer.client.events.DoActionOnDetailLayersEvent.SwapLayer;
import org.gcube.portlets.user.geoportaldataviewer.client.resources.GeoportalImages;
import org.gcube.portlets.user.geoportaldataviewer.client.ui.cms.project.ProjectUtil;
import org.gcube.portlets.user.geoportaldataviewer.client.util.LoaderIcon;
import org.gcube.portlets.user.geoportaldataviewer.client.util.StringUtil;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DragLeaveEvent;
import com.google.gwt.event.dom.client.DragLeaveHandler;
import com.google.gwt.event.dom.client.DragOverEvent;
import com.google.gwt.event.dom.client.DragOverHandler;
import com.google.gwt.event.dom.client.DragStartEvent;
import com.google.gwt.event.dom.client.DragStartHandler;
import com.google.gwt.event.dom.client.DropEvent;
import com.google.gwt.event.dom.client.DropHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DragDropLayer extends FlowPanel {

	private static final String _22PX = "22px";
	private static DragDropLayer dragging = null;
	private static DragDropLayer draggingTarget = null;
	final boolean droppable;
	private Label labelLayerName;
	private Image imgLayerVisible = new Image(GeoportalImages.ICONS.layerVisible());
	private Image imgLayerInvisible = new Image(GeoportalImages.ICONS.layerInvisible());

	private Image imgLocate = new Image(GeoportalImages.ICONS.locate());
	private Image imgLocateNone = new Image(GeoportalImages.ICONS.locateNone());
	private boolean locateEnabled = false;
	private HTML buttonLocate = new HTML();
	
	private DragDropLayer INSTANCE = this;
	
	private LoaderIcon loaderIcon = new LoaderIcon("Loading features", null);

	private Button draggableButton = new Button();
	private HandlerManager applicationBus;
	private LayerObject layerObject;
	private LayerItem layerItem;
	private com.google.gwt.user.client.ui.Label labelProject;
	private boolean layerVisibility = true;
	private HTML buttonLayerVisibility = new HTML();

	public DragDropLayer(HandlerManager applicationBus, LayerObject layerObject, boolean draggable, boolean droppable) {
		this.applicationBus = applicationBus;
		this.layerObject = layerObject;
		this.layerItem = layerObject.getLayerItem();
		GWT.log("DragDropLayer for projectDV: " + layerObject.getProjectDV());

		// Checking the spatial reference
		if (layerObject.getProjectDV() != null && layerObject.getProjectDV().getSpatialReference() == null) {
			GeoportalDataViewerServiceAsync.Util.getInstance().getSpatialReference(layerObject.getProfileID(),
					layerObject.getProjectID(), new AsyncCallback<GeoJSON>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub

						}

						@Override
						public void onSuccess(GeoJSON result) {
							if (result != null)
								layerObject.getProjectDV().setSpatialReference(result);

						}
					});
		}

		String referProjectID = "Project ID: " + layerObject.getProjectDV().getId();
		labelProject = new com.google.gwt.user.client.ui.Label("");
		labelProject.setTitle(referProjectID);
		GeoportalDataViewerServiceAsync.Util.getInstance().getEntrySetsDocumentForProjectID(
				layerObject.getProjectDV().getProfileID(), layerObject.getProjectDV().getId(), 1,
				new AsyncCallback<LinkedHashMap<String, Object>>() {

					@Override
					public void onSuccess(LinkedHashMap<String, Object> result) {
						String realProjectName = "";
						if (result != null) {
							for (String key : result.keySet()) {
								result.get(key);
								//referProject = key + ": " + result.get(key);
								realProjectName = result.get(key)+"";
								break;
							}
						}

						if (realProjectName != null && !realProjectName.isEmpty()) {
							labelProject.setText(StringUtil.ellipsize(realProjectName, 40));
							labelProject.setTitle(realProjectName);
						}
					}

					@Override
					public void onFailure(Throwable caught) {
						labelProject.setTitle("Error on reading the project name");

					}
				});

		String layerName = StringUtil.fullNameToLayerName(layerItem.getName(), ":");

		this.labelLayerName = new Label(layerName);
		this.labelLayerName.setTitle(layerItem.getName());
		this.labelLayerName.setType(LabelType.INFO);

		getElement().getStyle().setMarginTop(5, Unit.PX);
		getElement().getStyle().setMarginBottom(10, Unit.PX);
		this.getElement().addClassName("dand-layer");

		if (draggable) {
			initDrag();
		}
		if (droppable) {
			initDrop();
		}
		this.droppable = droppable;

		if (droppable) {
			draggableButton.addStyleName("droppable");
		} else if (draggable) {
			draggableButton.addStyleName("draggable");
		}

		RangeSlider rs = new RangeSlider(applicationBus, layerItem);
		rs.getElement().addClassName("range-slider");

		// FlexTable ft = new FlexTable();
		// ft.setWidget(0, 0, draggableButton);
		draggableButton.setTitle("Drag and Drop me up or down to change my position on the Map");

		imgLayerVisible.getElement().addClassName("layer-visibility-st");
		imgLayerInvisible.getElement().addClassName("layer-visibility-st");

		imgLocate.setWidth(_22PX);
		imgLocate.setHeight(_22PX);
		imgLocateNone.setWidth(_22PX);
		imgLocateNone.setHeight(_22PX);
		imgLocate.getElement().addClassName("layer-visibility-st");
		imgLocateNone.getElement().addClassName("layer-visibility-st");
		setLocateEnabledButtonImage();

		setLayerVisibilityButtonImage();

		VerticalPanel vp = new VerticalPanel();
		VerticalPanel vpInner = new VerticalPanel();
		HorizontalPanel hp0 = new HorizontalPanel();
		HorizontalPanel hpFunct = new HorizontalPanel();
		vpInner.getElement().getStyle().setMarginLeft(47, Unit.PX);

		hp0.add(draggableButton);
		labelProject.getElement().getStyle().setMarginLeft(10, Unit.PX);
		labelProject.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		hp0.add(labelProject);
		hpFunct.add(buttonLayerVisibility);
		hpFunct.add(buttonLocate);
		hpFunct.add(loaderIcon);
		setVisibleLoaderFeatures(false, null);
		
		if(layerObject.getProjectDV().getTemporalReference()!=null) {
			TemporalReferenceDV tempRef = layerObject.getProjectDV().getTemporalReference();
			if(tempRef.getStart()!=null || tempRef.getEnd()!=null)
				vpInner.add(new HTML(ProjectUtil.toHTMLCode(tempRef)));
		}
		
		vpInner.add(labelLayerName);
		// vpInner.add(new SimplePanel(rs));

		vp.add(hp0);
		hpFunct.getElement().getStyle().setMarginTop(5, Unit.PX);
		vpInner.add(hpFunct);
		vpInner.add(new SimplePanel(rs));
		vp.add(vpInner);

		add(vp);

		// add(ft);
		draggableButton.setIcon(IconType.MOVE);

		buttonLayerVisibility.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				layerVisibility = !layerVisibility;
				setLayerVisibilityButtonImage();
				DoActionOnDetailLayersEvent dae = new DoActionOnDetailLayersEvent(DO_LAYER_ACTION.VISIBILITY, layerItem,
						layerObject);
				dae.setVisibility(layerVisibility);
				applicationBus.fireEvent(dae);
			}
		});

		buttonLocate.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				locateEnabled = !locateEnabled;
				setLocateEnabledButtonImage();
				DoActionOnDetailLayersEvent dae = new DoActionOnDetailLayersEvent(DO_LAYER_ACTION.LOCATE_LAYER,
						layerItem, layerObject);
				dae.setUIEventSource(INSTANCE);
				setVisibleLoaderFeatures(true, null);
				applicationBus.fireEvent(dae);
			}
		});
	}

	private void setLayerVisibilityButtonImage() {
		buttonLayerVisibility.getElement().removeAllChildren();
		if (layerVisibility) {
			buttonLayerVisibility.getElement().appendChild(imgLayerVisible.getElement());
			buttonLayerVisibility.setTitle("Hide the layer");
		} else {
			buttonLayerVisibility.getElement().appendChild(imgLayerInvisible.getElement());
			buttonLayerVisibility.setTitle("Show the layer");
		}

	}

	private void setLocateEnabledButtonImage() {
		buttonLocate.getElement().removeAllChildren();
		if (locateEnabled) {
			buttonLocate.getElement().appendChild(imgLocate.getElement());
			buttonLocate.setTitle("Unselect the layer features");
		} else {
			buttonLocate.getElement().appendChild(imgLocateNone.getElement());
			buttonLocate.setTitle("Highlight the layer features");
		}

	}

	private void initDrag() {
		draggableButton.getElement().setDraggable(Element.DRAGGABLE_TRUE);

		addDomHandler(new DragStartHandler() {

			@Override
			public void onDragStart(DragStartEvent event) {
				dragging = DragDropLayer.this;
				event.setData("ID", "UniqueIdentifier");
				event.getDataTransfer().setDragImage(getElement(), 10, 10);

			}
		}, DragStartEvent.getType());
	}

	private void initDrop() {

		addDomHandler(new DragOverHandler() {
			@Override
			public void onDragOver(DragOverEvent event) {
				draggableButton.addStyleName("dropping-over");
				GWT.log("drag over");
				draggingTarget = (DragDropLayer) event.getSource();
			}
		}, DragOverEvent.getType());

		addDomHandler(new DragLeaveHandler() {
			@Override
			public void onDragLeave(DragLeaveEvent event) {
				draggableButton.removeStyleName("dropping-over");
				GWT.log("drag leave");
				// draggingTarget = (DragDropLayer) event.getSource();
			}
		}, DragLeaveEvent.getType());

		addDomHandler(new DropHandler() {
			@Override
			public void onDrop(DropEvent event) {
				event.preventDefault();
				Object dropTarget = event.getSource();
				GWT.log("on drop");
				draggableButton.removeStyleName("dropping-over");
				if (dragging != null) {
					Widget source = null;
					Widget target = null;
					FlowPanel tree = (FlowPanel) DragDropLayer.this.getParent();

					source = dragging;
					int sourceIndex = tree.getWidgetIndex(source);
					int targetIndex = tree.getWidgetIndex(draggingTarget);

					GWT.log("sourceIndex: " + sourceIndex + ", sourceItem: " + dragging.getLayerItem().getName());
					GWT.log("targetIndex: " + targetIndex + ", targetItem: " + draggingTarget.getLayerItem().getName());
					if (sourceIndex != targetIndex && targetIndex >= 0) {
						tree.remove(source);
						target = tree;

						if (source != null && target != null) {
							tree.insert(source, targetIndex);
							// target.setState(true);
							DoActionOnDetailLayersEvent actionSwapLayers = new DoActionOnDetailLayersEvent(
									DO_LAYER_ACTION.SWAP, layerItem, layerObject);
							SwapLayer swapLS = new DoActionOnDetailLayersEvent.SwapLayer(dragging.getLayerItem(),
									sourceIndex);
							SwapLayer swapLT = new DoActionOnDetailLayersEvent.SwapLayer(draggingTarget.getLayerItem(),
									targetIndex);
							actionSwapLayers.setSwapLayers(swapLS, swapLT);
							applicationBus.fireEvent(actionSwapLayers);
						}
						dragging = null;
					}
				}
			}
		}, DropEvent.getType());
	}

	public LayerItem getLayerItem() {
		return layerItem;
	}

	
	/**
	 * Sets the visible loader features.
	 *
	 * @param bool the bool
	 * @param msg the msg
	 */
	public void setVisibleLoaderFeatures(boolean bool, String msg) {
		loaderIcon.setVisible(bool);
		if(msg!=null)
			loaderIcon.setText(msg);
	}

}
