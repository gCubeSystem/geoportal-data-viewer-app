package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface SearchPerformedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 14, 2021
 */
public interface SearchPerformedEventHandler extends EventHandler {

	/**
	 * On search done.
	 *
	 * @param searchPerformedEvent the search performed event
	 */
	void onSearchDone(SearchPerformedEvent searchPerformedEvent);
}
