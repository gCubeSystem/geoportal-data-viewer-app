package org.gcube.portlets.user.geoportaldataviewer.client.events;

import org.gcube.portlets.user.geoportaldataviewer.shared.GCubeCollection;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class UpdateLayerToMapEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2023
 */
public class UpdateLayerToMapEvent extends GwtEvent<UpdateLayerToMapEventHandler> {
	public static Type<UpdateLayerToMapEventHandler> TYPE = new Type<UpdateLayerToMapEventHandler>();
	private GCubeCollection collection;
	private String layerName;
	private REQUEST_PARAMETER operation;
	private String value;
	private LAYER_TYPE layerType;

	/**
	 * The Enum REQUEST_PARAMETER.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 15, 2023
	 */
	public static enum REQUEST_PARAMETER {
		STYLE
	}

	/**
	 * The Enum LAYER_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Jun 15, 2023
	 */
	public static enum LAYER_TYPE {
		INDEX, DETAILS
	}

	/**
	 * Instantiates a new adds the layer to map event.
	 *
	 * @param collection the collection
	 * @param layerName  the layer name
	 * @param type       the type
	 */
	public UpdateLayerToMapEvent(GCubeCollection collection, String layerName, LAYER_TYPE type) {
		this.collection = collection;
		this.layerName = layerName;
		this.layerType = type;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<UpdateLayerToMapEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Sets the operation.
	 *
	 * @param operation the operation
	 * @param value     the value
	 */
	public void setOperation(REQUEST_PARAMETER operation, String value) {
		this.operation = operation;
		this.value = value;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Gets the layer type.
	 *
	 * @return the layer type
	 */
	public LAYER_TYPE getLayerType() {
		return layerType;
	}

	/**
	 * Gets the operation.
	 *
	 * @return the operation
	 */
	public REQUEST_PARAMETER getOperation() {
		return operation;
	}

	/**
	 * Gets the layer name.
	 *
	 * @return the layer name
	 */
	public String getLayerName() {
		return layerName;
	}

	/**
	 * Gets the collection.
	 *
	 * @return the collection
	 */
	public GCubeCollection getCollection() {
		return collection;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(UpdateLayerToMapEventHandler handler) {
		handler.onUpdateLayer(this);

	}

}