package org.gcube.portlets.user.geoportaldataviewer.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ApplyCQLToLayerOnMapEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         May 31, 2023
 */
public interface ApplyCQLToLayerOnMapEventHandler extends EventHandler {

	/**
	 * On apply CQL.
	 *
	 * @param applyCQLToLayerMapEvent the apply CQL to layer map event
	 */
	void onApplyCQL(ApplyCQLToLayerOnMapEvent applyCQLToLayerMapEvent);
}
