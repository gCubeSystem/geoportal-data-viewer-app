package org.gcube.portlets.user.geoportaldataviewer.client.events;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class SearchPerformedEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Dec 14, 2021
 */
public class SearchPerformedEvent extends GwtEvent<SearchPerformedEventHandler> {
	public static Type<SearchPerformedEventHandler> TYPE = new Type<SearchPerformedEventHandler>();
	private List<String> listProjectIDs;
	private boolean searchReset;
	private String profileID;

	/**
	 * Instantiates a new search performed event.
	 *
	 * @param profileID the profile ID
	 * @param listProjectIds the list project ids
	 * @param searchReset    the search reset
	 */
	public SearchPerformedEvent(String profileID, List<String> listProjectIds, boolean searchReset) {
		this.profileID = profileID;
		this.listProjectIDs = listProjectIds;
		this.searchReset = searchReset;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<SearchPerformedEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(SearchPerformedEventHandler handler) {
		handler.onSearchDone(this);

	}

	/**
	 * Gets the profile ID. It is equal to collectionID
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the list project Ids.
	 *
	 * @return the list project Ids
	 */
	public List<String> getListProjectIDs() {
		return listProjectIDs;
	}

	/**
	 * Checks if is search reset.
	 *
	 * @return true, if is search reset
	 */
	public boolean isSearchReset() {
		return searchReset;
	}

}