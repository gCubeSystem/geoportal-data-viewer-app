package org.gcube.portlets.user.geoportaldataviewer.client;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.GNADataViewerConfigProfile;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GroupedLayersDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.layers.LayerIDV;
import org.gcube.application.geoportalcommon.shared.geoportal.geojson.GeoJSON;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.gis.BoundsMap;
import org.gcube.portlets.user.geoportaldataviewer.shared.GeoportalSpatialQueryResult;
import org.gcube.portlets.user.geoportaldataviewer.shared.ItemFieldsResponse;
import org.gcube.portlets.user.geoportaldataviewer.shared.ResultSetPaginatedDataIDs;
import org.gcube.portlets.user.geoportaldataviewer.shared.ViewerConfiguration;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.BaseMapLayer;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerItem;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.LayerObject;
import org.gcube.portlets.user.geoportaldataviewer.shared.gis.wms.GeoInformationForWMSRequest;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Nov 17, 2020
 */
@RemoteServiceRelativePath("geoportaldataviewerservice")
public interface GeoportalDataViewerService extends RemoteService {

	/**
	 * Parses the wms request.
	 *
	 * @param wmsRequest the wms request
	 * @param layerName  the layer name
	 * @return the geo information for WMS request
	 * @throws Exception the exception
	 */
	GeoInformationForWMSRequest parseWmsRequest(String wmsRequest, String layerName) throws Exception;

	/**
	 * Gets the data result.
	 *
	 * @param layerObjects  the layer objects
	 * @param mapSrsName    the map srs name
	 * @param mapBBOX       the map BBOX
	 * @param maxWFSFeature the max WFS feature
	 * @param zoomLevel     the zoom level
	 * @return the data result
	 * @throws Exception the exception
	 */
	List<GeoportalSpatialQueryResult> getDataResult(List<LayerObject> layerObjects, String mapSrsName,
			BoundsMap mapBBOX, int maxWFSFeature, double zoomLevel) throws Exception;

	/**
	 * Gets the my login.
	 *
	 * @return the my login
	 */
	String getMyLogin();

	/**
	 * Gets the geo na data view profile.
	 *
	 * @return the geo na data view profile
	 * @throws Exception the exception
	 */
	GNADataViewerConfigProfile getGeoNaDataViewProfile() throws Exception;

	/**
	 * Gets the public links for.
	 *
	 * @param item the item
	 * @return the public links for
	 * @throws Exception the exception
	 */
	GeoportalItemReferences getPublicLinksFor(GeoportalItemReferences item) throws Exception;

	/**
	 * Gets the layers for id.
	 *
	 * @param theProfileID the the profile ID
	 * @param theProductID the the product ID
	 * @return the layers for id
	 * @throws Exception the exception
	 */
	List<GCubeSDIViewerLayerDV> getLayersForId(String theProfileID, String theProductID) throws Exception;

	/**
	 * Gets the WFS features.
	 *
	 * @param layerObjects  the layer objects
	 * @param mapSrsName    the map srs name
	 * @param selectBBOX    the select BBOX
	 * @param maxWFSFeature the max WFS feature
	 * @param zoomLevel     the zoom level
	 * @return the WFS features
	 */
	List<GeoportalSpatialQueryResult> getWFSFeatures(List<LayerObject> layerObjects, String mapSrsName,
			BoundsMap selectBBOX, int maxWFSFeature, double zoomLevel);

	/**
	 * Gets the list base layers.
	 *
	 * @return the list base layers
	 */
	List<BaseMapLayer> getListBaseLayers();

	/**
	 * List of fields for searching.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<ItemFieldsResponse> getConfigListOfFieldsForSearching() throws Exception;

	/**
	 * Gets the project view for id.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project view for id
	 * @throws Exception the exception
	 */
	ProjectView getProjectViewForId(String profileID, String projectID) throws Exception;

	/**
	 * Gets the list projects.
	 *
	 * @param theProfileID      the the profile ID
	 * @param start             the start
	 * @param limit             the limit
	 * @param filter            the filter
	 * @param reloadFromService the reload from service
	 * @return the list projects
	 * @throws Exception the exception
	 */
	ResultSetPaginatedDataIDs getListProjects(String theProfileID, Integer start, Integer limit, SearchingFilter filter,
			boolean reloadFromService) throws Exception;

	/**
	 * NEW CODE HERE.
	 *
	 * @return the initial configuration
	 * @throws Exception the exception
	 */

	ViewerConfiguration getInitialConfiguration() throws Exception;

	/**
	 * Gets the entry sets document for project ID.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @param limit     the limit
	 * @return the entry sets document for project ID
	 */
	LinkedHashMap<String, Object> getEntrySetsDocumentForProjectID(String profileID, String projectID, int limit);

	/**
	 * Gets the relationships for timeline.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the relationships for timeline
	 * @throws Exception the exception
	 */
	List<String> getRelationshipsForTimeline(String profileID, String projectID) throws Exception;

	/**
	 * Gets the spatial reference.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the spatial reference
	 */
	GeoJSON getSpatialReference(String profileID, String projectID);

	/**
	 * Gets the WFS request.
	 *
	 * @param layerItem    the layer item
	 * @param mapSrsName   the map srs name
	 * @param mapBBOX      the map BBOX
	 * @param maxFeatures  the max features. If 0 means all feature
	 * @param outputFormat the output format
	 * @return the WFS request
	 */
	String getWFSRequest(LayerItem layerItem, String mapSrsName, BoundsMap mapBBOX, int maxFeatures,
			String outputFormat);

	/**
	 * Gets the WFS response.
	 *
	 * @param layerItem    the layer item
	 * @param mapSrsName   the map srs name
	 * @param mapBBOX      the map BBOX
	 * @param maxFeatures  the max features
	 * @param outputFormat the output format
	 * @return the WFS response
	 */
	String getWFSResponse(LayerItem layerItem, String mapSrsName, BoundsMap mapBBOX, int maxFeatures,
			String outputFormat);

	/**
	 * Gets the avaiable custom grouped layers for UCD.
	 *
	 * @param theHandler the the handler
	 * @return the avaiable custom grouped layers for UCD
	 */
	Map<String, List<GroupedLayersDV<? extends LayerIDV>>> getAvaiableCustomGroupedLayersForUCD(
			GEOPORTAL_DATA_HANDLER theHandler);

	/**
	 * Gets the HTTP response for URL.
	 *
	 * @param url the url
	 * @return the HTTP response for URL
	 */
	String getHTTPResponseForURL(String url);

	Integer getCountFor(String profileID, String phase, String status);

	Boolean checkExportAsPDFConfig();

}
