# GeoPortal Data Viewer App

The "Geoportal data-viewer" technology allows users to access and search for spatio-temporal documents through a web GIS which allows them to: (i) navigate and select the projects published in the system by "geographical map" (spatial dimension) , (ii) navigate and select the relationships between projects (evolution of over time) via "timeline" (temporal dimension), (iii) view metadata and resources (images, layers, etc. .) of documents via the "project view detail", (iv) export a view of the map with the layer of interest or add the layers of a project to the main map, (v) manage the order and overlapping of the layers, (vi) execute spatial queries on the layers and view the results, (vii) search through metadata or temporal dimension queries for the projects of interest, (vii) apply spatial filters for cross-layer.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

**Uses**

* GWT v.2.10.0. [GWT](http://www.gwtproject.org) is licensed under [Apache License 2.0](http://www.gwtproject.org/terms.html)
* GWT-Bootstrap v.2.3.2.0. [GWT-Bootstrap](https://github.com/gwtbootstrap) is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* GWT-OpenLayers 3+ v.8.5.0-gwt2_9. [GWT-OL3](https://github.com/TDesjardins/gwt-ol)
* OpenLayers v.6.x [OpenLayers](https://openlayers.org/) is licensed under [BSD 2-Clause "Simplified"](https://github.com/openlayers/openlayers/blob/main/LICENSE.md)
* NanoGallery v2.X [NanoGallery2](https://github.com/nanostudio-org/nanogallery2) is licensed under [GNU General Public License "gplv3"](https://www.gnu.org/licenses/gpl-3.0.html)
* Vis-Timeline v7.x [VisTimeline](https://github.com/visjs/vis-timeline) is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) and MIT
* Handlebarsjs v4.x [Handlebarsjs](https://handlebarsjs.com/) is licensed under MIT

## Architecture

<img src="https://gcube.wiki.gcube-system.org/images_gcube/8/82/GeoPortalDataViewer_Architecture.png" style="max-width:800px;" alt="GeoPortal Data-Viewer - Architecture" />

## Showcase

##### D4GNA instance of Geoportal D4Science

see at [Dataset per il Geoportale Nazionale per l’Archeologia (D4GNA)](https://gna.d4science.org/)

**D4GNA Viewer - OSM**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/0/06/D4GNA_Viewer_OSM.png" style="max-width:800px;" alt="Data_Viewer_OSM" />

**D4GNA Viewer - MapBox**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/9/9c/D4GNA_Viewer_MapBox.png" style="max-width:800px;" alt="Data_Viewer_MapBox" />

**D4GNA Viewer - Discovering...**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/8/85/D4GNA_ERCIM_3.png" style="max-width:800px;" alt="D4GNA_ERCIM_3" />
<br />
<br />
<img src="https://gcube.wiki.gcube-system.org/images_gcube/9/9c/D4GNA_ERCIM_2.png" style="max-width:800px;" alt="D4GNA_ERCIM_2" />

## Documentation

Geoportal Service Documentation is available at [gCube CMS Suite](https://geoportal.d4science.org/geoportal-service/docs/index.html)

User Guide (DRAFT ITA) is available at [Guida al DataViewer D4GNA (DRAFT-ITA)](https://gcube.wiki.gcube-system.org/images_gcube/b/b3/D4science_Guida_al_DataViewer_D4GNA_bozza.pdf)

Developer's Guide is available at [Developer's Guide](DEVELOPER.md)

## Change log

See the [Releases](https://code-repo.d4science.org/gCubeSystem/geoportal-data-viewer-app/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://infrascience.isti.cnr.it)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


