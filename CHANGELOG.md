# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.7.1]

- Bug fixing the "Export as PDF" facility [#28580]

## [v3.7.0] - 2024-12-02

- Provided the "Export as PDF" facility [#26026]
- Integrated the GeoportalExporter as service [#27321]
- Passed to the new KC-client [#27398]

## [v3.6.0] - 2023-09-21

- GUI optimization [#25461]
- Using the latest libraries version: gwt-ol.v8.4.1 (with openlayers.v6.6.1) and gwt.v2.10
- Showing basic statistics (i.e. "X project published") via 'countByPhase' method [#25598]

## [v3.5.0] - 2023-06-26

- Supported the cross-filtering [#25074]
- Supported the grouped custom layers [#25110]
- Managed the (WMS) Layer Style facility [#25066]

## [v3.4.0] - 2023-05-11

- Integrated the geoportal-data-mapper component [#24978]

## [v3.3.1] - 2023-03-30

- Just including new dependencies
- Improved css

## [v3.3.0] - 2023-02-20

#### Fixes

- [#24632] Returning the public layer index if the private is missing
- [#24632] No temporal info shown if the temporal extent is missing

## [v3.2.1] - 2023-02-03

#### Fixes

- [#24432] Reverting serialization LinkedHashMap<String, String> to LinkedHashMap<String, Object>.
- Improved Alert UX in case of broken shared link
- Moved to gwt 2.9.0

## [v3.2.0] - 2023-01-31

#### Enhancements

- [#24506] Added facility: Locate/Highlight the Layer on the Map

## [v3.1.0] - 2023-01-12

#### Enhancements
- [#24300] Improved the GUI of the search functionality when multiple collections are available
- [#24404] Improved the handlers of events: Search, Locate
- Improved the user experience providing an animated map and zoom to layers facility

#### Fixes
- Reduced the BBOX when resolving a project by share link
- GNA Project binding with automatically adding of the layers to Map
- [#24390] Read the access policy from the fileset json section
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>

## [v3.0.0] - 2022-11-28

- [#23940] Passed to CMS-UCD
- [#23941] Implemented and integrated the DTO Project/Document per UCD config and gCube Metadata Profiles
- [#23953] Passed the 'Search facility' to CMS-UCD model
- [#23954] Passed the 'Details Panel facility' to CMS-UCD model
- [#23955] Implemented the Temporal facility to navigate temporal relations among (JSON) Projects
- [#24028] Passed the spatial dimension to CMS-Project model
- [#24136] Integrated the temporal dimension on the front-end side
- [#24195] Integrated the temporal query

## [v2.4.1-SNAPSHOT] - 2022-09-28

- [#23906] Tested building on jenkins with JDK_11

## [v2.4.0] - 2022-09-07

#### Enhancement

- [#23819] Adding layers to principal map belonging to more centroids with the same spatial position

## [v2.3.1] - 2022-08-29

#### Bug fixed

- [#23770] OpenLayers CDN URLs updated (github relative URL changed from "master/en/vX.Y.Z/" to "main/dist/en/vX.Y.Z/")

## [v2.3.0] - 2022-07-26

#### Bug fixed

- [#23689] Fixed the search facility is buggy when used from public access
- [#23704] Used new maven range for geoportal-client
- Moved to 3.6.4

## [v2.2.0] - 2021-12-07

#### Enhancements

- [#22518] Added the search for fields facility

## [v2.1.0-SNAPSHOT] - 2021-11-10

#### Enhancements

- [#22518] Added the search for name facility
- [#22027] Integrated with MapBox Satellite
- Added link "Open Project" in the pop-up of WFS details
- Moved to gwt-ol3 v8.1.0-gwt2_9

## [v2.0.1] - 2021-11-10

#### Fixes

- [#22370] Use a max-height for preview image shown in the pop-up

## [v2.0.0] - 2021-07-30

#### Enhancements

- [#21890] Porting to ConcessioniManagerI and pass to mongoID
- [#20595] Porting and using the model view provided by geoportal-common
- [#21946] Show layers of a concessione automatically on map according to zoom level
- [#21976] Access policies checked on server-side
- [#22042] Implemented the public access
- [#22040] Revisited the "Abstract and Relazione di Scavo"
- [#22041] Files have been ported to FileSet model
- [#21991] Implemented the Layer Switcher

## [v1.2.0] - 2021-07-19

#### Enhancements

- [#21847] Integrated with an Image and LighBox gallery for images
- Moved to maven-portal-bom 3.6.3


## [v1.1.0] - 2020-12-21

#### Enhancements

- [#20357] Improvements feedback-driven 

## [v1.0.0] - 2020-12-09

- [#20004] First release
