# GeoPortal Data Viewer App - DEVELOPER

This section documents the functionalities of the 'GeoPortal Data Viewer App' and its interactions with other software layers and components. It is intended for developers who want to understand how it works, as well as those looking to write or improve its code and features.

### General Architecture

This diagram illustrates the overall architecture of the GeoPortal Data Viewer App, showing the interaction between its components.
The front-end layer includes various applications and widgets that communicate with the common data layer. The back-end layer handles core geoportal services and client interactions. DScience Services, such as IC-Client/IS and URI-Resolver, are integrated and used by both layers

```mermaid
graph LR
    A[Geoportal Data-Viewer App] --> D[Geoportal Data Common]
    B[Geoportal Data Mapper] --> D
    A --> B
    B --> C[Metadata Profile Form Builder Widget]
    D --> E[Geoportal Common]
    D --> F[Geoportal Client/Service]
    F --> E
    D -.-> G[IC-Client/IS]
    C -.-> G
    A -.-> G
    D -.-> H[URI-Resolver Client/Service]


    subgraph "Geoportal Front-End layer"
        B
        C
        D
    end
    subgraph "Geoportal Back-End layer"
        E
        F
    end
 


    G[IC-Client/IS]:::external_service
    H[URI-Resolver Client/Service]:::external_service
    

    %% Note as parallelograms (to simulate a cloud-like effect)
    X[/"Caches and DTOs"/]:::note -.-> B
    Y[/"UCD/Project Callers and UI data model"/]:::note -.-> D

    classDef frontend fill:#f8cecc,stroke:#b85450;
    classDef backend fill:#d5e8d4,stroke:#82b366;
    classDef app fill:#fff2cc,stroke:#d6b656;
    classDef external_service fill:#dae8fc,stroke:#6c8ebf,stroke-dasharray: 5 5;
    classDef note fill:#ffffff,stroke:#000000,stroke-dasharray: 3 3,font-style:italic;

    class A app;
    class B,C,D frontend;
    class E,F backend;
    class G,H external_service;
    class X,Y note;

```

### Export to PDF functionality

The diagram illustrates the process of exporting data to a PDF from the GeoPortal Data Viewer App. The client sends a POST request to GeoportalDataViewer, which triggers a GET request to GeoportalExporter with authentication details. The exporter responds with a jobID, and the client repeatedly polls the job status. Once the export is complete, GeoportalExporter sends the PDF file to the client, which then displays it.

```mermaid
sequenceDiagram
    actor Client as (Client/Browser) Export
    participant GeoportalDataViewer as GeoportalDataViewer Servlet
    participant GeoportalExporter as Geoportal Exporter<br>(URI-Resolver)

    Client->>GeoportalDataViewer: POST/ Export to PDF
    GeoportalDataViewer->>GeoportalExporter: GET with D4Science authentication <br>(header: geoportalexportprj:{UCD-ID}-{PRJ-ID}-{Timestamp})
    GeoportalExporter-->>Client: waiting page - jobID <br>(header: geoportalexportprj:{UCD-ID}-{PRJ-ID}-{Timestamp})
    loop Polling jobID status
        Client->>GeoportalExporter: GET - polling jobID status <br>(header: geoportalexportprj:{UCD-ID}-{PRJ-ID}-{Timestamp})
    end
    GeoportalExporter-->>Client: PDF file
    Note over Client: Show PDF file

```
## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


